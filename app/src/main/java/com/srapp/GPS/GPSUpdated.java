package com.srapp.GPS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.location.Location;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.MainActivity;
import com.srapp.R;
import com.srapp.location.MyLocation;
import com.srapp.print.ParentActivity;

public class GPSUpdated extends ParentActivity implements  OnClickListener {
	
	private Button btnBack, btnHome;
	TextView HeaderTitleTv, CodeNoTv;
	
	TextView SaveLocationTxt, CurrentLocationTxt;
	Button UpdateGPSButton;
	String latitude = "";
	String longitude = "";
	private Location CurrentLocation;
	Data_Source db;
	private ProgressDialog dialog;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.gps_updated);
		db= new Data_Source(this);
		dialog=new ProgressDialog(this);
		dialog.setMessage("Update Location, Please wait..");
		
		TextView txtUser=(TextView)findViewById(R.id.txtUser);
		//txtUser.setText(getPreference("UserName"));

		//HeaderTitleTv = (TextView)findViewById(R.id.txtTitleName);
		//CodeNoTv = (TextView)findViewById(R.id.txtSOCode);
		//HeaderTitleTv.setText("GPS Update");
		//CodeNoTv.setText(getPreference("FiscalYearID"));
		//initiateButtons(btnBack, R.id.btn_back);
		//initiateButtons(btnHome, R.id.btn_home);
		
		SaveLocationTxt = (TextView)findViewById(R.id.SaveLocationTxt);
		CurrentLocationTxt = (TextView)findViewById(R.id.CurrentLocationTxt);
		
		UpdateGPSButton = (Button)findViewById(R.id.UpdateGPSButton);

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels/15;

		LinearLayout layout = (LinearLayout)findViewById(R.id.layoutBg);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);            
		params.setMargins(width, 0, width, 0); 
		layout.setLayoutParams(params);

		CurrentLocationTxt.setText("Click the Update GPS Button and Please, Wait GPS is loading...");
		/*new MyLocation().getLocation(this, new LocationResult() {
			
			@Override
			public void gotLocation(Location location) {
				CurrentLocation=location;
				CurrentLocationTxt.setText("Current Location: "+location.getLatitude()+","+location.getLongitude());
				
			}
		});*/
	
		
		
		Cursor c = db.rawQuery("SELECT * FROM outlets where outlet_id="+"'"+getPreference("FiscalYearID")+"'");
		Log.e("Current Locations", "SELECT * FROM outlets where outlet_id="+"'"+getPreference("FiscalYearID")+"'");
		if (c != null) {
			if (c.moveToFirst()) {
				do {

					String latitu = ""+c.getDouble(c.getColumnIndex("latitude"));
					String longitu = ""+c.getDouble(c.getColumnIndex("langitude"));
					
					
					Log.e("LATITUDE:", latitu);
					Log.e("LONGITUDE:", longitu);
					SaveLocationTxt.setText("Saved Location: "+latitu+","+longitu);

				} while (c.moveToNext());
			}
		
		}

		UpdateGPSButton.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(dialog!=null)
					dialog.show();
				
				GPSTracker gps = new GPSTracker(GPSUpdated.this);
				if (gps.canGetLocation()) {

					try
					{
						new MyLocation().getLocation(GPSUpdated.this, new MyLocation.LocationResult() {

							@Override
							public void gotLocation(Location location) {
								try
								{
									if (location==null){
										Log.e("GPS"," NO location");
										return;
									}

									CurrentLocation=location;
									CurrentLocationTxt.setText("Current Location: "+location.getLatitude()+","+location.getLongitude());

									latitude = String.valueOf(CurrentLocation.getLatitude());
									longitude = String.valueOf(CurrentLocation.getLongitude());
									Log.e("latitude----------", "latitude---"+latitude);
									Log.e("longitude----------","longitude---"+longitude);
								}
								catch (Exception e) {
									Log.e("GPSUP",e.getMessage());
									//Toast.makeText(GPSUpdated.this, "GPS do not get the lat and long values",Toast.LENGTH_LONG).show();
								}

								Log.e("latitude----------", "latitude2---"+latitude);
								Log.e("longitude----------", "longitude2---"+longitude);
								if(latitude.equals("0.0") && longitude.equals("0.0"))
								{
									Toast.makeText(GPSUpdated.this, "GPS do not get the lat and long values", Toast.LENGTH_LONG).show();
								}
								else
								{

									if (latitude.isEmpty()){
										latitude="0" ; longitude="0";

										return;
									}


									String updateQuery="UPDATE outlets SET latitude="+latitude+", longitude="+longitude+", updated_at='"+getCurrentDateTime24()+"', isUpdated=1"+", isPushed=0 WHERE outlet_id='"+getPreference("FiscalYearID")+"'";
									db.excQuery(updateQuery);
									//db.excQuery("UPDATE outlets SET latitude = "+"'"+latitude+"'"+", langitude="+"'"+longitude+"'"+" , isUpdated = "+"'"+1+"'"+", isPushed=0 WHERE outlet_id = "+"'"+getPreference("FiscalYearID")+"'");
									Log.e("UpdateQuery:",updateQuery);

									GPSUpdated.this.runOnUiThread(new Runnable() {
										public void run() {
											Toast.makeText(GPSUpdated.this, "GPS Location Save Successfully", Toast.LENGTH_LONG).show();
										}
									});


									if(dialog.isShowing())
										dialog.dismiss();
								}

							}
						});
					}catch (Exception e) {
						Log.e("GPSUP",e.getMessage());
					}

					if(dialog.isShowing())
						dialog.dismiss();
				}
				else
				{
					gps.showSettingsAlert();
					if(dialog.isShowing())
						dialog.dismiss();
				}
				
			}
		});
	
	}
	
	public void initiateButtons(Button btn, int id)
	{ 
		btn = (Button) findViewById(id);
		btn.setOnClickListener((OnClickListener) this);  
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			/*Intent idd = new Intent(GPSUpdated.this, OutletAccountActivity.class);
			startActivity(idd);
			finish();*/
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		/*if(v.getId()== R.id.btn_back)
		{

			Intent idd = new Intent(GPSUpdated.this, OutletAccountActivity.class);
			startActivity(idd);
			finish();
		}

		else if(v.getId()==R.id.btn_home){ 
			Intent intent = new Intent(GPSUpdated.this, MainActivity.class);
			startActivity(intent);
			finish();  
		}*/
	}

}
