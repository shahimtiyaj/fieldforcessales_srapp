package com.srapp.GPS;

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import com.srapp.Db_Actions.Data_Source;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class GPSUpdateService extends Service {

//	public static long NOTIFY_INTERVAL = 1000*5; // 30minute
//	public static long NOTIFY_INTERVAL = 1000*30; // 1minute
	public static long NOTIFY_INTERVAL = 0; // 30minute
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    Data_Source db;
 
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
 
    @Override
    public void onCreate() {
        // cancel if already existed
    	  db = new Data_Source(this);
        if(mTimer != null) {
            mTimer.cancel();
        } else {
            // recreate new
            mTimer = new Timer();
        }
        // schedule task
        NOTIFY_INTERVAL = 1000*60*Integer.parseInt(getPreference("interval"));
        
        Log.e("----NOTIFY_INTERVAL---", ""+NOTIFY_INTERVAL);
        
        mTimer.scheduleAtFixedRate(new TimeDisplayTimerTask12(), 0, NOTIFY_INTERVAL);
    }
 
    class TimeDisplayTimerTask12 extends TimerTask {
 
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
 
                @Override
                public void run() {
                    // display toast
                	
                	Log.e("uuuuuuuuuuuuu", "Hello start GPS service");
                	
                	
                	GPSTracker gps = new GPSTracker(GPSUpdateService.this);
                	 
                    // check if GPS enabled     
                    if(gps.canGetLocation()){
                         
                        double latitude = gps.getLatitude();
                        double longitude = gps.getLongitude();
                        
                        gps.stopUsingGPS();
                        
                        
                        new Thread(new Runnable() {  
                            public void run() {
                                  // TODO Auto-generated method stub


                                  try
                                  {
                                        Thread.sleep(20000);


                                  }
                                  catch(Exception e){}
                                                           }
                      }).start();
                        
                         
                        Log.e("------latitude--------", ""+latitude);
                        Log.e("-----longitude--------", ""+longitude);
                        
                        
                        
                        String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                        
                        HashMap<String,String> map= new HashMap<String,String>();
						//map.put("sr_id", getPreference(SR_ID));
						map.put("_id", "21695");
						map.put("latitude", ""+latitude);
						map.put("longitude", ""+longitude);
						map.put("gps_bts", getPreference("gps_bts"));
						map.put("isPushed", "0");
                        map.put("created_at", "0");
                        map.put("updated_at", date);
						db.InsertTable(map, "gps_tracker");

                    }
                	
                }
 
            });
        }

    }
    
    @Override
	public void onDestroy() 
	{
		super.onDestroy();
		
		if (mTimer != null) 
		{
			mTimer.cancel();
			
		}
		
		Log.d("onDestroy()", "Service Destroyed");
	}

    public static class Helper {

        public static boolean isAppRunning(final Context context, final String packageName) {
            final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
            final List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
            if (procInfos != null)
            {
                for (final ActivityManager.RunningAppProcessInfo processInfo : procInfos) {
                    if (processInfo.processName.equals(packageName)) {
                        return true;
                    }
                }
            }
            return false;
        }
    }
    
    
    public String getPreference(String key)
	{
		String value="";
		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		value = prefs.getString(key, "NO PREFERENCE");
		
		return value;
		
	} 
}
