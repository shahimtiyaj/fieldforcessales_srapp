package com.srapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.ParentActivity;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class CreditHistoryPayActivity extends ParentActivity implements View.OnClickListener, BasicFunctionListener {

    Button SubmitCreditHistoryPayBtn;
    EditText MemoNoEd,MemoValueEd,PaymentAmountEd,InsNoEd,BankBranchEd,CollectDateEd, TaxAmountEd, TaxNoEd;
    Spinner InsTypeSp, BranchSp,InsSp;
    BasicFunction bf;
    String date_, memo_no_, memo_value_,collect_date_,outlet_id_,due_amount,paid_maount;

    private ImageView btnBack, btnHome;
    TextView HeaderTitleTv, CodeNoTv,DueEd,txtPaid;

    ArrayList<HashMap<String,String>> arrBankBanch=new ArrayList<HashMap<String,String>>();

    Button btnDate;
    private int year1;
    private int month1;
    private int day1;
    static final int DATE_PICKER_ID1 = 1111;

    String date="";

    Data_Source db;
   // HashMap<String,String> map=new HashMap<String,String>();
    ArrayList<String> InstID=new ArrayList<String>();
    ArrayList<String> InstName=new ArrayList<String>();

    String _InstID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_credit_history_pay);
        TextView txtUser=(TextView)findViewById(R.id.txtUser);
        txtUser.setText(getPreference("UserName"));

        db=new Data_Source(this);
        bf = new BasicFunction(this,this);
        Bundle b =getIntent().getExtras();
        outlet_id_ = b.getString("outlet_id");
        date_ = b.getString("date");
        memo_no_ = b.getString("memo_no");
        memo_value_ = b.getString("memo_value");
        collect_date_ = b.getString("collect_date");
        due_amount = b.getString("due_amount");
        paid_maount = b.getString("paid_amount");



        /*HeaderTitleTv.setText("Credit Collection");
        CodeNoTv.setText(getPreference("OutletID"));*/
        initiateButtons(btnBack, R.id.back);
        initiateButtons(btnHome, R.id.home);



        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width=dm.widthPixels/15;


        ScrollView layout = (ScrollView)findViewById(R.id.layoutBg12);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(width, 0, width, 0);
        layout.setLayoutParams(params);


        MemoNoEd = (EditText)findViewById(R.id.MemoNoEd);
        MemoValueEd = (EditText)findViewById(R.id.MemoValueEd);
        DueEd = (TextView)findViewById(R.id.DueEd);
        PaymentAmountEd = (EditText)findViewById(R.id.PaymentAmountEd);
        TaxAmountEd = (EditText)findViewById(R.id.TaxAmountEd);
        TaxNoEd = (EditText)findViewById(R.id.TaxNoEd);
        InsNoEd = (EditText)findViewById(R.id.InsNoEd);
//		BankBranchEd = (EditText)findViewById(R.id.BankBranchEd);
        btnDate = (Button)findViewById(R.id.btnDate);
        txtPaid=(TextView)findViewById(R.id.txtPaid);
        InsTypeSp = (Spinner)findViewById(R.id.InsTypeSp);
        InsSp = (Spinner)findViewById(R.id.InsSp);
        BranchSp = (Spinner)findViewById(R.id.BranchSp);
        btnDate=(Button)findViewById(R.id.btnDate);
        MemoNoEd.setText(memo_no_);
        MemoValueEd.setText(memo_value_);


        PaymentAmountEd.setText(""+roundTwoDecimals(Double.parseDouble(due_amount)));
        txtPaid.setText(""+roundTwoDecimals(Double.parseDouble(paid_maount)));
        DueEd.setText(""+0.00);

        TextView PaymentAmountTV = (TextView)findViewById(R.id.PaymentAmountTV);
        addAsMandetory(PaymentAmountTV, "Payment Amount:");

        TextView InsTypeTV = (TextView)findViewById(R.id.InsTypeTV);
        addAsMandetory(InsTypeTV, "Type:");

        instrument_TableParse();


        final Calendar c = Calendar.getInstance();
        year1  = c.get(Calendar.YEAR);
        month1 = c.get(Calendar.MONTH);
        day1   = c.get(Calendar.DAY_OF_MONTH);



        String day="",month="";
        if(day1<10)
            day="0"+String.valueOf(day1);
        else
            day=String.valueOf(day1);
        if(month1+1<10)
            month="0"+String.valueOf(month1+1);
        else
            month=String.valueOf(month1+1);
        Log.e("day","Day"+ String.valueOf(day));
        Log.e("month", String.valueOf(month));

        date=new StringBuilder().append(year1 )
                .append("-").append(month).append("-").append(day)
                .append("").toString();

        btnDate.setText(DateFormatedConverter(date));



        InsSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
                                       long arg3) {
                // TODO Auto-generated method stub

                _InstID = InstID.get(arg2);
                Log.e("-----","_InstID: "+_InstID);
//				_InstID = InstID.get(InsSp.getSelectedItemPosition()).get(arg2);


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });



        btnDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_PICKER_ID1);
            }
        });



        //		MemoNoEd.setText(memo_no_);



        ArrayList<String> instrumentType=new ArrayList<String>();
        instrumentType.add("Select Instrument");
        instrumentType.add("Cash");
        instrumentType.add("Instrument");


        ArrayList<String> BankBanch=new ArrayList<String>();
        BankBanch.add("Select Banch");
        String query="SELECT * FROM bank_branch";
        Cursor c1=db.rawQuery(query);
        if(c1!=null)
        {
            if(c1.moveToFirst())
            {
                do{
                    HashMap<String,String> map=new HashMap<String,String>();
                    map.put("ID", ""+c1.getString(2));
                    map.put("Name", c1.getString(3));
                    arrBankBanch.add(map);
                    BankBanch.add(c1.getString(3));
                }while(c1.moveToNext());
            }
        }




        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CreditHistoryPayActivity.this,R.layout.spinner_text, BankBanch);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        BranchSp.setAdapter(dataAdapter);

        ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(CreditHistoryPayActivity.this,R.layout.spinner_text, instrumentType);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        InsTypeSp.setAdapter(dataAdapter1);



        InsTypeSp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                // TODO Auto-generated method stub

                TextView BranchTV = (TextView)findViewById(R.id.BranchTV);
                LinearLayout InsLayout = (LinearLayout)findViewById(R.id.InsLayout);

                if(String.valueOf(InsTypeSp.getSelectedItemPosition()).equals("2"))
                {

                    addAsMandetory(BranchTV, "Bank Branch:");
                    InsLayout.setVisibility(View.VISIBLE);
                }
                else
                {
                    BranchTV.setText("Bank Branch:");
                    InsLayout.setVisibility(View.GONE);
                }


				/*LinearLayout InsLayout = (LinearLayout)findViewById(R.id.InsLayout);
				if(String.valueOf(InsTypeSp.getSelectedItemId()).equalsIgnoreCase("2"))
				{
					InsLayout.setVisibility(View.VISIBLE);
					instrument_TableParse();
				}
				else
				{
					InsLayout.setVisibility(View.GONE);
				}*/


            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });


        PaymentAmountEd.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub
                Log.e("count",count+".....");
                if (s.length()==0){

                    double vale=Double.parseDouble(MemoValueEd.getText().toString())-Double.parseDouble(paid_maount);

                    DueEd.setText(String.valueOf(vale));
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub

                if (s.length()==0){

                    double vale=Double.parseDouble(MemoValueEd.getText().toString())-Double.parseDouble(paid_maount);

                    DueEd.setText(String.valueOf(vale));
                }
                Log.e("count",count+".....");

            }

            @Override
            public void afterTextChanged(Editable s) {

                if(s.length()>0)
                {
                    if(Double.parseDouble(PaymentAmountEd.getText().toString())>Double.parseDouble(due_amount)){
                        PaymentAmountEd.setText(""+Double.parseDouble(due_amount));
                    }
                    if (!PaymentAmountEd.getText().toString().equalsIgnoreCase(".")) {
                        if (!PaymentAmountEd.getText().toString().equalsIgnoreCase("")) {
                            if (!PaymentAmountEd.getText().toString().equalsIgnoreCase("0")) {
                                Double total = Double.parseDouble(MemoValueEd.getText().toString());
                                Double payment = Double.parseDouble(PaymentAmountEd.getText().toString());
                                Double paid = Double.parseDouble(paid_maount);
                                if (!TaxAmountEd.getText().toString().equalsIgnoreCase("")) {
                                    Double tax = Double.parseDouble(TaxAmountEd.getText().toString());
                                    DueEd.setText(String.valueOf(total - (payment + paid + tax)));
                                } else
                                    DueEd.setText(String.valueOf(total - (payment + paid)));

                            }

                        }

                    }
                }
            }

        });

        TaxAmountEd.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // TODO Auto-generated method stub


            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub





            }

            @Override
            public void afterTextChanged(Editable s) {

				/*if (s.length()==0){

					DueEd.setText(MemoValueEd.getText().toString());
				}*/

                if(s.length()>0) {
                    if (!TaxAmountEd.getText().toString().equalsIgnoreCase("."))
                    {
                        if(!TaxAmountEd.getText().toString().equalsIgnoreCase(""))
                        {
                            if (!TaxAmountEd.getText().toString().equalsIgnoreCase("0"))
                            {
                                Double total = Double.parseDouble(MemoValueEd.getText().toString());
                                Double payment = Double.parseDouble(PaymentAmountEd.getText().toString());
                                Double paid = Double.parseDouble(paid_maount);
                                if (!TaxAmountEd.getText().toString().equalsIgnoreCase("")) {
                                    Double tax = Double.parseDouble(TaxAmountEd.getText().toString());
                                    DueEd.setText(String.valueOf(total - (payment + paid + tax)));
                                } else
                                    DueEd.setText(String.valueOf(total - (payment + paid)));
                            }
                        }

                    }
                }
            }
        });


        SubmitCreditHistoryPayBtn = (Button)findViewById(R.id.SubmitCreditHistoryPayBtn);

        SubmitCreditHistoryPayBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                SubmitCreditHistoryPayBtn.setEnabled(false);
                if (PaymentAmountEd.getText().toString().length()<=0){
                    SubmitCreditHistoryPayBtn.setEnabled(true);
                    Toast.makeText(CreditHistoryPayActivity.this,"Input Pay amount",Toast.LENGTH_LONG).show();
                    return;
                }

                // TODO Auto-generated method stub
                String TaxAmount = TaxAmountEd.getText().toString();
                String TaxNo = TaxNoEd.getText().toString();

                java.text.DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
                Calendar cal = Calendar.getInstance();
                String paymentTempId  ="P"+getPreference("SO")+dateFormat.format(cal.getTime());

                if(checkValidation())
                {
                    String PaymentId = PaymentID();
                    if(TaxAmount.length()>0)
                    {
                        if(TaxNo.equalsIgnoreCase(""))
                        {
                            SubmitCreditHistoryPayBtn.setEnabled(true);
                            Toast.makeText(CreditHistoryPayActivity.this,"Enter Tax No.",Toast.LENGTH_LONG).show();
                        }
                        else
                        {
                            if(Double.parseDouble(memo_value_)<(Double.parseDouble(PaymentAmountEd.getText().toString())+(Double.parseDouble(TaxAmountEd.getText().toString()))))
                            {
                                SubmitCreditHistoryPayBtn.setEnabled(true);
                                Toast.makeText(CreditHistoryPayActivity.this,"Enter actual Payment & Tax",Toast.LENGTH_LONG).show();
                            }
                            else
                            {
                                try {

                                    JSONObject object = new JSONObject();
                                   JSONObject map = new JSONObject();
                                Log.e("--_InstID---","--_InstID---"+_InstID);

                                map.put("payment_temp_id", paymentTempId);
                                map.put("outlet_id", outlet_id_);
                                map.put("memo_number", memo_no_);
                                map.put("memo_value", memo_value_);
                                map.put("due_amount", DueEd.getText().toString());
                                map.put("paid_amount", (Double.parseDouble(PaymentAmountEd.getText().toString())+Double.parseDouble(txtPaid.getText().toString())));
                                map.put("instrument_type", String.valueOf(InsTypeSp.getSelectedItemId()));
                              if (InsTypeSp.getSelectedItemId()==1){
                                  _InstID = "0";
                              }
                                map.put("inst", _InstID);
                                    map.put("date", getCurrentDate());
//							map.put("bank_branch_id", ""+BranchSp.getSelectedItemPosition());
                                map.put("bank_branch_id", "");
                                map.put("collection_date",date );
                                map.put("memo_date",date );
                                map.put("is_pushed", "0");
                                map.put("is_credit_collection", "1");
                                map.put("tax_amount", TaxAmountEd.getText().toString());
                                map.put("tax_no", TaxNoEd.getText().toString());
                                map.put("instrument_no", InsNoEd.getText().toString());
                                map.put("payment_id", PaymentId);
                                    map.put("mac", bf.getPreference("mac"));
                                    map.put(SR_ID, bf.getPreference(SR_ID));
                               // map.put("mac", bf.getPreference("mac"));

                               // db.InsertTable(map, "payments");
                                JSONArray jsonArray = new JSONArray();
                                    jsonArray.put(map);
                                    //jsonArray.put(map);
                                    object.put("credit_collection",jsonArray);
                                    object.put("mac", bf.getPreference("mac"));
                                    object.put(SR_ID, bf.getPreference(SR_ID));
                                    bf.getResponceData(URL.SUBMIT_CREDIT_COLLECTION,object.toString(),11);

                                } catch (JSONException e) {
                                    Log.e("test_json",e.getMessage());
                                    e.printStackTrace();
                                }



							/*String due=   String.valueOf( Double.parseDouble(due_amount)-Double.parseDouble(PaymentAmountEd.getText().toString()));
							String paid=   String.valueOf( Double.parseDouble(paid_maount)+Double.parseDouble(PaymentAmountEd.getText().toString()));*/

                                Double due_amount5 = Double.parseDouble(due_amount);
                                Double PaymentAmount5 = Double.parseDouble(PaymentAmountEd.getText().toString());
                                Double TaxAmount5 = Double.parseDouble(TaxAmountEd.getText().toString());
                                Double ToalPaymentAmount = PaymentAmount5+TaxAmount5;
                                Double ToalDue = due_amount5-ToalPaymentAmount;

                                Log.e("due_amount5","due_amount5: "+due_amount5);
                                Log.e("PaymentAmount5","PaymentAmount5: "+PaymentAmount5);
                                Log.e("TaxAmount5","TaxAmount5: "+TaxAmount5);
                                Log.e("ToalPaymentAmount","ToalPaymentAmount: "+ToalPaymentAmount);
                                Log.e("ToalDue","ToalDue: "+ToalDue);


                                String paid=   String.valueOf( Double.parseDouble(paid_maount)+Double.parseDouble(PaymentAmountEd.getText().toString())+Double.parseDouble(TaxAmountEd.getText().toString()));


                                db.excQuery("UPDATE credit_collections SET due_amount='"+ToalDue +"' ,paid_amount="+paid+",isPushed=0 WHERE memo_no='"+memo_no_+"'");

                                Log.e("SELECTED iTEM", String.valueOf(InsTypeSp.getSelectedItemPosition()));
                                Log.e("SELECTED iTEM position:", String.valueOf(InsTypeSp.getSelectedItemPosition()));

                                if(InsTypeSp.getSelectedItemPosition()==2)
                                {

                                    HashMap<String,String> map1=new HashMap<String,String>();
                                    map1.put("instrument_no_id", InsNoEd.getText().toString());
                                    map1.put("instrument_no_name", String.valueOf(InsTypeSp.getSelectedItem()));
                                    map1.put("memo_no",memo_no_ );
                                    map1.put("memo_value", memo_value_);
                                    map1.put("isUsed", "0");
                                    map1.put("payment", PaymentAmountEd.getText().toString());
                                    map1.put("payment_id", PaymentId);
                                    map1.put("isPushed", "0");
                                    db.InsertTable(map1, "instrument_no");

                                    Log.e("instrument_no_table", "inserted!!!!!!");
                                }


                                if(String.valueOf(InsTypeSp.getSelectedItemId()).equalsIgnoreCase("1"))
                                {
                                    double depositBalance=0.0;
                                    String	DepositQuery = "SELECT deposit_balance_amount FROM deposit_balance";
                                    Cursor c1 =db.rawQuery(DepositQuery);
                                    if(c1!=null)
                                    {
                                        if(c1.moveToFirst())
                                        {
                                            do{
                                                depositBalance =c1.getDouble(0);

                                            }while(c1.moveToNext());
                                        }
                                    }

                                    String Total_depositBalance = roundTwoDecimals(depositBalance+Double.parseDouble(PaymentAmountEd.getText().toString()));

                                    String UpdateDepositBalanceQuery="UPDATE deposit_balance SET deposit_balance_amount="+Total_depositBalance+" WHERE _id=1";
                                    Log.e("----", "-UpdateDepositBalanceQuery-"+UpdateDepositBalanceQuery);
                                    db.excQuery(UpdateDepositBalanceQuery);
                                }


                            }

                        }
                    }
                    else
                    {

                        try {
                        JSONObject object = new JSONObject();
                        JSONObject map = new JSONObject();
                        map.put("payment_temp_id", paymentTempId);
                        map.put("outlet_id", outlet_id_);
                        map.put("memo_number", memo_no_);
                        map.put("memo_value", memo_value_);
                        map.put("due_amount", DueEd.getText().toString());
                        map.put("paid_amount", (Double.parseDouble(PaymentAmountEd.getText().toString())+Double.parseDouble(txtPaid.getText().toString())));
                        map.put("instrument_type", String.valueOf(InsTypeSp.getSelectedItemId()));
                            if (InsTypeSp.getSelectedItemId()==1){
                                _InstID = "0";
                            }
                        map.put("inst", _InstID);
                        map.put("date", getCurrentDate());
//					map.put("bank_branch_id", ""+BranchSp.getSelectedItemPosition());
                        map.put("bank_branch_id", "");
                        map.put("collection_date",date );
                        map.put("memo_date",date );
                        map.put("is_pushed", "0");
                        map.put("is_credit_collection", "1");
                        map.put("tax_amount", "");
                        map.put("tax_no", "");
                        map.put("instrument_no", InsNoEd.getText().toString());
                        map.put("payment_id", PaymentId);
                            map.put("mac", bf.getPreference("mac"));
                            map.put(SR_ID, bf.getPreference(SR_ID));

                        // db.InsertTable(map, "payments");

                            JSONArray jsonArray = new JSONArray();
                            jsonArray.put(map);
                          //  jsonArray.put(map);
                            object.put("credit_collection",jsonArray);
                            object.put("mac", bf.getPreference("mac"));
                            object.put(SR_ID, bf.getPreference(SR_ID));
                            bf.getResponceData(URL.SUBMIT_CREDIT_COLLECTION,object.toString(),11);

                        } catch (JSONException e) {
                            Log.e("test_json",e.getMessage());
                            e.printStackTrace();
                        }


                        String due=   String.valueOf( Double.parseDouble(due_amount)-Double.parseDouble(PaymentAmountEd.getText().toString()));
                        String paid=   String.valueOf( Double.parseDouble(paid_maount)+Double.parseDouble(PaymentAmountEd.getText().toString()));

//                        db.excQuery("UPDATE credit_collections SET due_amount="+due +" ,paid_amount="+paid+",isPushed=0 WHERE memo_no='"+memo_no_+"'");

                        Log.e("SELECTED iTEM", String.valueOf(InsTypeSp.getSelectedItemPosition()));
                        Log.e("SELECTED iTEM position:", String.valueOf(InsTypeSp.getSelectedItemPosition()));

                        if(InsTypeSp.getSelectedItemPosition()==2)
                        {

                            HashMap<String,String> map1=new HashMap<String,String>();
                            map1.put("instrument_no_id", InsNoEd.getText().toString());
                            map1.put("instrument_no_name", String.valueOf(InsTypeSp.getSelectedItem()));
                            map1.put("memo_no",memo_no_ );
                            map1.put("memo_value", memo_value_);
                            map1.put("isUsed", "0");
                            map1.put("payment", PaymentAmountEd.getText().toString());
                            map1.put("payment_id", PaymentId);
                            map1.put("isPushed", "0");
                            db.InsertTable(map1, "instrument_no");

                            Log.e("instrument_no_table", "inserted!!!!!!");
                        }



                        if(String.valueOf(InsTypeSp.getSelectedItemId()).equalsIgnoreCase("1"))
                        {
                            double depositBalance=0.0;
                            String	DepositQuery = "SELECT deposit_balance_amount FROM deposit_balance";
                            Cursor c1 =db.rawQuery(DepositQuery);
                            if(c1!=null)
                            {
                                if(c1.moveToFirst())
                                {
                                    do{
                                        depositBalance =c1.getDouble(0);

                                    }while(c1.moveToNext());
                                }
                            }

                            String Total_depositBalance = roundTwoDecimals(depositBalance+Double.parseDouble(PaymentAmountEd.getText().toString()));

                            String UpdateDepositBalanceQuery="UPDATE deposit_balance SET deposit_balance_amount="+Total_depositBalance+" WHERE _id=1";
                            Log.e("----", "-UpdateDepositBalanceQuery-"+UpdateDepositBalanceQuery);
                            db.excQuery(UpdateDepositBalanceQuery);
                        }

                       /* startActivity(new Intent(CreditHistoryPayActivity.this,CreditHistoryActivity.class));
                        finish();*/
                    }
                }else {
                    SubmitCreditHistoryPayBtn.setEnabled(true);
                }

            }
        });

    }

    public boolean checkValidation()
    {





        if(InsTypeSp.getSelectedItemPosition()==0)
        {
            Toast.makeText(getApplicationContext(), "Select Instument Type!", Toast.LENGTH_LONG).show();
            return false;
        }

        else
        {
            if(InsTypeSp.getSelectedItemPosition()==2)
            {

			/*if(BranchSp.getSelectedItemPosition()==0)
			{
				Toast.makeText(getApplicationContext(), "Select Banch Name!", 1000).show();

				return false;
			}*/

                if(InsNoEd.getText().toString().length()==0)
                {
                    Toast.makeText(getApplicationContext(), "Enter instrument no!", Toast.LENGTH_LONG).show();
                    return false;

                }

            }

        }

        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if(id==DATE_PICKER_ID1)
            return new DatePickerDialog(this, myDateListener1, year1, month1,day1);
        return null;


    }

    private DatePickerDialog.OnDateSetListener myDateListener1 = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {

            int year1  = arg1;
            int  month1 = arg2+1;
            int  day1  = arg3;



            String day="",month="";
            if(day1<10)
                day="0"+String.valueOf(day1);
            else
                day=String.valueOf(day1);
            if(month1<10)
                month="0"+String.valueOf(month1);
            else
                month=String.valueOf(month1);
            Log.e("day","Day"+ String.valueOf(day));
            Log.e("month", String.valueOf(month));

            date=new StringBuilder().append(year1 )
                    .append("-").append(month).append("-").append(day)
                    .append("").toString();

            btnDate.setText(DateFormatedConverter(date));


        }
    };



           /* @Override
            public boolean onCreateOptionsMenu(Menu menu) {
                // Inflate the menu; this adds items to the action bar if it is present.
                getMenuInflater().inflate(R.menu.main, menu);
                return true;
            }*/

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(CreditHistoryPayActivity.this, CreditHistoryActivity.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


   void savePaymentInfo(){

        try {
            java.text.DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
            Calendar cal = Calendar.getInstance();
            String paymentTempId = "P" + getPreference(SR_ID) + dateFormat.format(cal.getTime());
            JSONObject object = new JSONObject();
            JSONObject map = new JSONObject();
            Log.e("--_InstID---","--_InstID---"+_InstID);

            map.put("payment_temp_id", paymentTempId);
            map.put("outlet_id", outlet_id_);
            map.put("memo_no", memo_no_);
            map.put("memo_value", memo_value_);
            map.put("due_amount", DueEd.getText().toString());
            map.put("paid_amount", (Double.parseDouble(PaymentAmountEd.getText().toString())));
            map.put("inst_type", String.valueOf(InsTypeSp.getSelectedItemId()));
            map.put("inst", _InstID);
            map.put("date", getCurrentDate());
//							map.put("bank_branch_id", ""+BranchSp.getSelectedItemPosition());
            map.put("bank_branch_id", "");
            map.put("collect_date",date );
            map.put("memo_date",date );
            map.put("is_pushed", "0");
            map.put("is_credit_collection", "1");
            map.put("tax_amount", TaxAmountEd.getText().toString());
            map.put("tax_no", TaxNoEd.getText().toString());
            map.put("instrument_no", InsNoEd.getText().toString());
            map.put("payment_id", "NO_PAYMENT_ID");
            map.put("mac", bf.getPreference("mac"));
            map.put("so_id", bf.getPreference(SR_ID));
            // map.put("mac", bf.getPreference("mac"));

            // db.InsertTable(map, "payments");
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(map);
            //jsonArray.put(map);
            object.put("payment_list",jsonArray);
            object.put("mac", bf.getPreference("mac"));
            object.put(SR_ID, bf.getPreference(SR_ID));
            bf.getResponceData(URL.SUBMIT_PAYMENT_COLLECTION,object.toString(),101);

        } catch (JSONException e) {
            Log.e("test_json",e.getMessage());
            e.printStackTrace();
        }
    }


    public void initiateButtons(ImageView btn, int id)
    {
        btn = (ImageView) findViewById(id);
        btn.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if(v.getId()==R.id.back)
        {

            Intent idd = new Intent(CreditHistoryPayActivity.this, CreditHistoryActivity.class);
            startActivity(idd);
            finish();
        }

        else if(v.getId()==R.id.home){
            Intent intent = new Intent(CreditHistoryPayActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }


    public String PaymentID() {
        final Calendar c = Calendar.getInstance();
        year1 = c.get(Calendar.YEAR);
        month1 = c.get(Calendar.MONTH);
        day1 = c.get(Calendar.DAY_OF_MONTH);

        String day = "", month = "";
        if (day1 < 10)
            day = "0" + String.valueOf(day1);
        else
            day = String.valueOf(day1);
        if (month1 + 1 < 10)
            month = "0" + String.valueOf(month1 + 1);
        else
            month = String.valueOf(month1 + 1);
        date = year1 + "-" + month + "-" + day;

        Log.e("Day", day);
        Log.e("Month", month);
        Log.e("Hour", "" + c.get(c.HOUR_OF_DAY));
        Log.e("Minute", "" + c.get(c.MINUTE));
        Log.e("SECOND", "" + c.get(c.SECOND));

        String Payment_ID = day + month + c.get(c.HOUR_OF_DAY) + c.get(c.MINUTE) + c.get(c.SECOND);
        return Payment_ID;
    }
    public void   addAsMandetory(TextView text,String lavel)
    {
        SpannableStringBuilder builder = new SpannableStringBuilder();
        builder.append(lavel);
        int start = builder.length();
        builder.append("*");
        int end = builder.length();

        builder.setSpan(new ForegroundColorSpan(Color.RED), start, end,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        text.setText(builder);
    }


    private void instrument_TableParse()
    {
        InstID.clear();
        InstName.clear();

		/*InstID.add("2");
		InstID.add("C");*/

        Cursor c = db.rawQuery("SELECT * FROM instrument_type where instrument_type_id!=1 and instrument_type_id!=2");
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String instrument_type_id = c.getString(c.getColumnIndex("instrument_type_id"));
                    String instrument_type_name = c.getString(c.getColumnIndex("instrument_type_name"));

                    InstID.add(instrument_type_id);
                    InstName.add(instrument_type_name);

                } while (c.moveToNext());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CreditHistoryPayActivity.this,R.layout.spinner_text, InstName);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            InsSp.setAdapter(dataAdapter);


			/*if(!getPreference("InstID").equalsIgnoreCase("NO PREFERENCE")&&!getPreference("SalesWeekID").equalsIgnoreCase(""))
			{
				int SelectedPos=InstID.indexOf(getPreference("SalesWeekID"));
				Log.e("SalesWeek POS:", ".........."+SelectedPos);
				spSalesWeek.setSelection(SelectedPos);
			}*/

        }

    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (i==101) {
            startActivity(new Intent(CreditHistoryPayActivity.this, CreditHistoryActivity.class));
            finish();
        }else {
            savePaymentInfo();
        }
    }

    @Override
    public void OnConnetivityError() {

    }
}
