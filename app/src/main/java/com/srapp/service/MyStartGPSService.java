package com.srapp.service;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;

import androidx.core.app.ActivityCompat;

import com.srapp.Db_Actions.Data_Source;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by User on 1/24/2018.
 */

public class MyStartGPSService extends Service {

    Context mContext;
    LocationManager locationManager;
    Handler handler = new Handler();
    //    Location location;
    private String provider;
    double latitude;
    double longitude;
    public static long NOTIFY_INTERVAL = 0; // 30minute
    /*  TextView lat;
      TextView lon;*/
    Data_Source db;

    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void onCreate() {
//        Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();

        db = new Data_Source(this);
        NOTIFY_INTERVAL = 1000 * 60 * Integer.parseInt(getPreference("interval"));
//        NOTIFY_INTERVAL = 1000*60;
        Log.e("--------", "NOTIFY_INTERVAL " + NOTIFY_INTERVAL);
    }

    @Override
    public void onStart(Intent intent, int startId) {
//        Toast.makeText(this, " MyService Started", Toast.LENGTH_LONG).show();

        Log.e("--------", "TEST ");

        LocationListener mlocListener = new MyLocationListener();

        locationManager = (LocationManager) this.getSystemService(getApplicationContext().LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0,  mlocListener);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        Location location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(location==null){
            location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);


            if(location!=null)
            {
                Log.e("------","--NETWORK_PROVIDER--"+location);

                savePreference("gps_bts", "net");
                latitude = location.getLatitude();
                longitude = location.getLongitude();
            }

        }
        else {

            Log.e("------","--GPS_PROVIDER--"+location);
            savePreference("gps_bts", "gps");
            latitude = location.getLatitude();
            longitude = location.getLongitude();
        }

        locationManager = (LocationManager) getSystemService(getApplicationContext().LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        //criteria.setAccuracy(Criteria.ACCURACY_COARSE);
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        provider = locationManager.getBestProvider(criteria, true);
//            locationManager.requestLocationUpdates(provider, 61000, 250,mlocListener);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mlocListener);


       /* mContext=this;
        LocationListener mlocListener = new MyLocationListener();
        locationManager=(LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates( LocationManager.GPS_PROVIDER,0,0, mlocListener);*/
        handler.postDelayed(runLocation, 8000);

    }

    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        handler.removeCallbacks(runLocation);
//        Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }



    /*@Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }*/


    public Runnable runLocation = new Runnable(){
        /*public Runnable runLocation;
        public Handler handler;*/

        @Override
        public void run() {
           /* lat.setText(String.valueOf(latitude));
            lon.setText(String.valueOf(longitude));*/
//            Toast.makeText(getApplicationContext(), "location check", Toast.LENGTH_SHORT).show();

            Log.e("--------","latitude: "+String.valueOf(latitude));
            Log.e("--------","longitude: "+String.valueOf(longitude));

            String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            if(latitude!=0.0 && longitude!=0.0)
            {
                HashMap<String,String> map= new HashMap<String,String>();
                //map.put("so_id", getPreference("SO"));
                //map.put("_id", "21695");
                map.put("latitude", String.valueOf(latitude));
                map.put("longitude", String.valueOf(longitude));
                map.put("gps_bts", getPreference("gps_bts"));
                map.put("is_pushed", "0");
                map.put("updated_at", date);
                db.InsertTable(map, "gps_tracker");
            }


           MyStartGPSService.this.handler.postDelayed(MyStartGPSService.this.runLocation, NOTIFY_INTERVAL);
        }
    };



    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        value = prefs.getString(key, "NO PREFERENCE");

        return value;

    }

    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public class MyLocationListener implements LocationListener {
        public void onLocationChanged(Location loc) {

            latitude = loc.getLatitude();
            longitude =  loc.getLongitude();

            String Text = "My current location is: " + "Latitude = "
                    + latitude + "Longitude = " + longitude;
//            Toast.makeText(getApplicationContext(), Text, Toast.LENGTH_SHORT).show();
            Log.d("TAG", "Starting..");
        }

        public void onProviderDisabled(String provider) {
//            Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }

        public void onProviderEnabled(String provider) {
//            Toast.makeText(getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {
        }
    }/* End of Class MyLocationListener */
}