package com.srapp.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.widget.Toast;

public class MyServiceStart extends Service {
 
    @Override
    public IBinder onBind(Intent intent) {
        // TODO Auto-generated method stub
        return null;
    }
 
    @Override
    public void onCreate() {
//        Toast.makeText(this, " MyService Created ", Toast.LENGTH_LONG).show();
    }
 
    @Override
    public void onStart(Intent intent, int startId) {
//        Toast.makeText(this, " MyService Started", Toast.LENGTH_LONG).show();
    	
    	/*Intent serviceIntent = new Intent(MyServiceStart.this,
				GPSUpdateService.class);*/
        Intent serviceIntent = new Intent(MyServiceStart.this,
                MyStartGPSService.class);
		startService(serviceIntent);
    }
 
    @Override
    public void onDestroy() {
        // TODO Auto-generated method stub
        Toast.makeText(this, "Servics Stopped", Toast.LENGTH_SHORT).show();
        super.onDestroy();
    }
 
}