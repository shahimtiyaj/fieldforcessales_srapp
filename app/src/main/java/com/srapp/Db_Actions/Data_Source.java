package com.srapp.Db_Actions;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.srapp.TempData;
import com.srapp.Util.Parent;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static com.srapp.Db_Actions.Tables.ADVANCE_COLLECTION;
import static com.srapp.Db_Actions.Tables.ADVANCE_COLLECTION_paid_amount;
import static com.srapp.Db_Actions.Tables.Allfild;
import static com.srapp.Db_Actions.Tables.CREDIT_COLLECTION;
import static com.srapp.Db_Actions.Tables.GIFT_ISSUE;
import static com.srapp.Db_Actions.Tables.GIFT_ISSUE_DETAILS;
import static com.srapp.Db_Actions.Tables.MARKETS;
import static com.srapp.Db_Actions.Tables.MARKET_ID;
import static com.srapp.Db_Actions.Tables.MEMOS;
import static com.srapp.Db_Actions.Tables.MEMOS_gross_value;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_date;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_number;
import static com.srapp.Db_Actions.Tables.MEMOS_outlet_id;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS;
import static com.srapp.Db_Actions.Tables.ORDER;
import static com.srapp.Db_Actions.Tables.ORDER_DETAILS;
import static com.srapp.Db_Actions.Tables.ORDER_STATUS;
import static com.srapp.Db_Actions.Tables.ORDER_gross_value;
import static com.srapp.Db_Actions.Tables.ORDER_is_pushed;
import static com.srapp.Db_Actions.Tables.ORDER_order_date;
import static com.srapp.Db_Actions.Tables.ORDER_order_number;
import static com.srapp.Db_Actions.Tables.ORDER_outlet_id;
import static com.srapp.Db_Actions.Tables.OUTLETS_ID;
import static com.srapp.Db_Actions.Tables.OUTLETS_MARKET_ID;
import static com.srapp.Db_Actions.Tables.OUTLETS_OUTLET_NAME;
import static com.srapp.Db_Actions.Tables.OUTLET_CATAGORY;
import static com.srapp.Db_Actions.Tables.OUTLET_CATEGORY_CATEGORY_ID;
import static com.srapp.Db_Actions.Tables.PAYMENTS;
import static com.srapp.Db_Actions.Tables.PROCESSING_COMPELETE;
import static com.srapp.Db_Actions.Tables.PROCESSING_PENDING;
import static com.srapp.Db_Actions.Tables.PRODUCT_BOOLEAN_OUTLET_ID;
import static com.srapp.Db_Actions.Tables.PRODUCT_BOOLEAN_PRODUCT_ID;
import static com.srapp.Db_Actions.Tables.PRODUCT_BOOLEAN_QUANTITY;
import static com.srapp.Db_Actions.Tables.PRODUCT_ID;
import static com.srapp.Db_Actions.Tables.PRODUCT_PRICE_EFFECTIVE_DATE;
import static com.srapp.Db_Actions.Tables.PRODUCT_PRODUCT_ID;
import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ADVANCE_COLLECTION;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MARKETS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MEMOS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_OUTLETS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_OUTLET_CATEGORY;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT_BOOLEAN;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT_COMBINATION;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT_PRICE;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ROOT;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_THANA;

public class Data_Source extends Parent {
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    private Context context;
    public DBListener dbListener;
    BasicFunction basicFunction;
    ProgressDialog progressDialog;

    public Data_Source(Context context, DBListener dbListener, BasicFunctionListener basicFunctionListener) {
        this.context = context;
        this.dbListener = dbListener;
        dBhelper = new DB_Helper(context);
        basicFunction = new BasicFunction(basicFunctionListener, context);
    }

    public Data_Source(Context context, DBListener dbListener) {
        this.context = context;
        this.dbListener = dbListener;
        dBhelper = new DB_Helper(context);

    }

    public Data_Source(Context context) {
        this.dBhelper = new DB_Helper(context);
        this.context = context;
    }

    public void open() {
        sqLiteDatabase = dBhelper.getWritableDatabase();
    }

    public void close() {

        sqLiteDatabase.close();
    }

    public void excQuery(String Query) {
        Log.e("execSQL", Query);
        open();
        sqLiteDatabase.execSQL(Query);
        close();
    }


    public ArrayList<HashMap<String, String>> AllDataFromTableForInstrumentType(String table_name)
    {
       this.open();

        Cursor  cursor = sqLiteDatabase.rawQuery("select * from "+table_name+" where instrument_type_id in (1,2)",null);
        ArrayList<HashMap<String, String>> Data= new  ArrayList<HashMap<String, String>>();

        try {

            if (cursor.moveToFirst()) {
                do {
                    HashMap<String, String> map= new HashMap<String, String>();

                    if(table_name.equalsIgnoreCase("instrument_type"))
                    {
                        map.put("id", cursor.getString(1));
                        map.put("name", cursor.getString(2));
                    }

                    Data.add(map);
                } while (cursor.moveToNext());
            }

        } finally {
            try { cursor.close(); } catch (Exception ignore) {}
        }



        return Data;

    }
    //.................Price List BY SLAP...................//

    public ArrayList<HashMap<String, String>> AllDataFromTable(String table_name)
    {
        this.open();

        Cursor  cursor = sqLiteDatabase.rawQuery("select * from "+table_name,null);
        ArrayList<HashMap<String, String>> Data= new  ArrayList<HashMap<String, String>>();

        try {
            if (cursor.moveToFirst()) {
                do {
                    HashMap<String, String> map= new HashMap<String, String>();
                    if(table_name.equalsIgnoreCase("doctor_table"))
                    {
                        map.put("id", cursor.getString(1));
                        map.put("name", cursor.getString(2));
                        map.put("type", cursor.getString(3));
                    }

                    if(table_name.equalsIgnoreCase("bank_banch"))
                    {
                        map.put("id", cursor.getString(2));
                        map.put("name", cursor.getString(3));
                    }

                	/* if(table_name.equalsIgnoreCase("instrument_type"))
                	{
                    map.put("id", cursor.getString(1));
                    map.put("name", cursor.getString(2));
                	}*/

                    if(table_name.equalsIgnoreCase("instrument_no"))
                    {
                        map.put("id", cursor.getString(1));
                        map.put("name", cursor.getString(2));
                    }
                    if(table_name.equalsIgnoreCase("sales_weeks"))
                    {
                        map.put("id", cursor.getString(1));
                        map.put("name", cursor.getString(2));
                    }
                    Data.add(map);
                } while (cursor.moveToNext());
            }

        } finally {
            try { cursor.close(); } catch (Exception ignore) {}
        }



        return Data;

    }
    public HashMap<String, ArrayList<ArrayList<String>>> getSlapWisePriceList(String date){
        Log.e("slap------->", "getSlapWisePriceList: ---->CALLED.....>" );
        this.open();

        HashMap<String, ArrayList<ArrayList<String>>> priceListHashMap = new HashMap<>();
        ArrayList<ArrayList<String>>productNameList = new ArrayList<>();
        ArrayList<ArrayList<String>>productIdList = new ArrayList<>();
        ArrayList<ArrayList<String>>mainPriceList = new ArrayList<>();
        ArrayList<ArrayList<String>>slapPriceList = new ArrayList<>();
        ArrayList<ArrayList<String>>slapQtyList = new ArrayList<>();

        //String query = "SELECT DISTINCT(product_id) FROM "+ TABLE_NAME_PRODUCT_PRICE+" where price > 0 and effective_date<='"+getCurrentDate()+"'";
        String query = "SELECT DISTINCT(product_id) FROM "+ TABLE_NAME_PRODUCT_PRICE+" where price > 0 and effective_date<='"+getCurrentDate()+"'";
        Cursor csr = sqLiteDatabase.rawQuery(query,null);
        csr.moveToFirst();
        if (csr != null && csr.getCount()>0){
            for (int i =0; i<csr.getCount();i++){
                ArrayList<String>preNameList = new ArrayList<>();
                ArrayList<String>preIdList = new ArrayList<>();
                //preNameList.add(csr.getString(csr.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME)));
                //Log.e("--->", "getSlapWisePriceList:----------> "+cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME)) );
                // productNameList.add(preNameList);

                String productId = csr.getString(csr.getColumnIndex(Tables.PRODUCT_PRODUCT_ID));
                preIdList.add(productId);
                productIdList.add(preIdList);
                if (getQtyAndPriceByProductId(productId,date).get(Tables.ProductName).get(0) != null) {

                    productNameList.add(getQtyAndPriceByProductId(productId, date).get(Tables.ProductName).get(0));
                    Log.e("---", "getSlapWisePriceList: --------->"+productIdList );

                    mainPriceList.add(getQtyAndPriceByProductId(productId,date).get("Main_Price").get(0));
                    Log.e("--------->", "getSlapWisePriceList:-----> main price "+mainPriceList );
                    slapPriceList.add(getQtyAndPriceByProductId(productId,date).get(Tables.MinPrice).get(0));
                    slapQtyList.add(getQtyAndPriceByProductId(productId,date).get(Tables.MinQty).get(0));
                }


                csr.moveToNext();
            }

            priceListHashMap.put(Tables.ProductName,productNameList);
            priceListHashMap.put("Main_PRICE",mainPriceList);
            priceListHashMap.put(Tables.MinQty,slapQtyList);
            priceListHashMap.put(Tables.MinPrice,slapPriceList);

        }


        return priceListHashMap;
    }

    public HashMap<String, ArrayList<ArrayList<String>>> getQtyAndPriceByProductId(String productId, String date){
        this.open();

        HashMap<String, ArrayList<ArrayList<String>>>qtyAndPriceHashMap = new HashMap<>();
        ArrayList<ArrayList<String>>qtyList = new ArrayList<>();
        ArrayList<ArrayList<String>>priceList = new ArrayList<>();
        ArrayList<ArrayList<String>>mainPriceList = new ArrayList<>();
        ArrayList<ArrayList<String>>mainNameList = new ArrayList<>();
        ArrayList<String>preMinQty = new ArrayList<>();
        ArrayList<String>preMinPriceList = new ArrayList<>();
        ArrayList<String> prePriceList = new ArrayList<>();
        ArrayList<String> preNameList = new ArrayList<>();

        //String query1 = "SELECT DISTINCT "+Tables.PRODUCT_COMBINATION_MIN_QUANTITY+" FROM "+ TABLE_NAME_PRODUCT_COMBINATION+" where  price>0 and  "+ PRODUCT_PRODUCT_ID+" = "+productId+" and effective_date <= (SELECT max(effective_date) from product_combinations where product_id='"+productId+"' AND effective_date<='"+date+"') ORDER BY "+Tables.PRODUCT_COMBINATION_MIN_QUANTITY+" ASC ";
        Cursor dateQuery = sqLiteDatabase.rawQuery("SELECT max(effective_date) from product_combinations where product_id='"+productId+"' AND effective_date<='"+getCurrentDate()+"' and combination_id='0'",null);
        dateQuery.moveToFirst();
        Log.e("querytt","SELECT max(effective_date) from product_combinations where product_id='"+productId+"' AND effective_date<='"+getCurrentDate()+"' and combination_id='0'"+dateQuery.getCount());
        if (dateQuery!=null && dateQuery.getCount()>0){

            date = dateQuery.getString(0);


        }
        dateQuery.close();
        String query1 = "SELECT DISTINCT "+Tables.PRODUCT_COMBINATION_MIN_QUANTITY+" FROM "+ TABLE_NAME_PRODUCT_COMBINATION+" where  price>0 and  "+ PRODUCT_PRODUCT_ID+ " = '"+productId+"' and effective_date = '" +date+"' ORDER BY "+Tables.PRODUCT_COMBINATION_MIN_QUANTITY+" ASC ";
        Log.e("minnQquery1", " "+query1 );
        Cursor cursor = sqLiteDatabase.rawQuery(query1,null);
        cursor.moveToFirst();

        if (cursor != null && cursor.getCount()>0){
            for (int i = 0; i<cursor.getCount();i++){
                String min_qty = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_COMBINATION_MIN_QUANTITY));

                preMinQty.add(min_qty);

                String query2 = "SELECT "+Tables.PRODUCT_COMBINATION_PRICE+" from "+Tables.TABLE_NAME_PRODUCT_COMBINATION+" where "+ PRODUCT_PRODUCT_ID+" = "+productId+" AND " +Tables.PRODUCT_COMBINATION_MIN_QUANTITY+" = "+min_qty+" AND "+
                        ""+Tables.PRODUCT_COMBINATION_EFFECTIVE+"<=  (SELECT max(effective_date) from product_combinations where product_id='"+productId+"' AND effective_date<='"+date+"')"+" ORDER BY "+Tables.PRODUCT_COMBINATION_EFFECTIVE+" DESC LIMIT 1 ";
                Log.e("minriceslap", "getQtyAndPriceByProductId:---------->price slap--------> "+query2 );
                Cursor cursor1 = sqLiteDatabase.rawQuery(query2,null);
                cursor1.moveToFirst();
                if (cursor1 != null && cursor1.getCount() >0){
                    String price = cursor1.getString(cursor1.getColumnIndex(Tables.PRODUCT_COMBINATION_PRICE));
                    preMinPriceList.add(price);
                }
                cursor.moveToNext();
                cursor1.close();
            }
            qtyList.add(preMinQty);
            priceList.add(preMinPriceList);

            qtyAndPriceHashMap.put(Tables.MinQty,qtyList);
            qtyAndPriceHashMap.put(Tables.MinPrice,priceList);
            cursor.close();
        }

        String query3 = "SELECT " + Tables.PRODUCT_PRICE_PRICE+ " FROM " + TABLE_NAME_PRODUCT_PRICE + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + "";
        Cursor priceCursor = sqLiteDatabase.rawQuery(query3,null);
        priceCursor.moveToFirst();
        if (priceCursor != null && priceCursor.getCount()>0){
            String price = priceCursor.getString(priceCursor.getColumnIndex(Tables.PRODUCT_PRICE_PRICE));
            prePriceList.add(price);
        }

        String query4 = "SELECT " + Tables.PRODUCT_PRODUCT_NAME+ " FROM " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + "";
        Cursor cursor4 = sqLiteDatabase.rawQuery(query4,null);
        Log.e("PRODUCT_NAMEttt",query4+" size="+cursor4.getCount());
        cursor4.moveToFirst();
        if (cursor4 != null && cursor4.getCount()>0){
            String name = cursor4.getString(cursor4.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME));
            Log.e("PRODUCT_NAMEttt",name+" 000");
            preNameList.add(name);
        }

        cursor4.close();
        mainPriceList.add(prePriceList);
        mainNameList.add(preNameList);
        qtyAndPriceHashMap.put("Main_Price",mainPriceList);
        qtyAndPriceHashMap.put(Tables.ProductName,mainNameList);

        Log.e("test---->", "getQtyAndPriceByProductId:--------> "+qtyAndPriceHashMap );



        return qtyAndPriceHashMap;

    }


    public HashMap<String, String> getMinQtyAndPrice(String productId, String date) {
        this.open();
        String qty = null;
        String minPrice = null;
        String price = null;
        HashMap<String, String> hashMap = new HashMap<>();
        String query = "SELECT " + Tables.PRODUCT_COMBINATION_MIN_QUANTITY + ", " + Tables.PRODUCT_COMBINATION_PRICE + " FROM " + TABLE_NAME_PRODUCT_COMBINATION + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + " ORDER BY  "+ PRODUCT_PRICE_EFFECTIVE_DATE+" <= "+date+" DESC LIMIT 1 ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            qty = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_COMBINATION_MIN_QUANTITY));
            minPrice = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_COMBINATION_PRICE));
        }
        String query2 = "SELECT " + Tables.PRODUCT_PRICE_PRICE+ " FROM " + TABLE_NAME_PRODUCT_PRICE + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + " ORDER BY  "+ PRODUCT_PRICE_EFFECTIVE_DATE+" DESC LIMIT 1";
        Cursor priceCursor = sqLiteDatabase.rawQuery(query2,null);
        priceCursor.moveToFirst();
        if (priceCursor != null && priceCursor.getCount()>0){
            price = priceCursor.getString(priceCursor.getColumnIndex(Tables.PRODUCT_PRICE_PRICE));
        }
        hashMap.put(Tables.MinQty, qty);
        hashMap.put(Tables.MinPrice, minPrice);
        hashMap.put(Tables.PRODUCT_PRICE_PRICE, price);
        return hashMap;
    }

    //...............................Price List retrive...........................//
    public HashMap<String, ArrayList<String>>getPriceList(String date){
        this.open();
        HashMap<String, ArrayList<String>>priceListHashmap = new HashMap<>();
        ArrayList<String>productNameList = new ArrayList<>();
        ArrayList<String>productIdList = new ArrayList<>();
        ArrayList<String>priceList1 = new ArrayList<>();
        ArrayList<String>priceList2 = new ArrayList<>();
        ArrayList<String>quantityList = new ArrayList<>();
        String query = "SELECT * FROM "+ TABLE_NAME_PRODUCT+"";
        Cursor cursor = sqLiteDatabase.rawQuery(query,null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount()>0){
            for (int i =0; i<cursor.getCount();i++){
                productNameList.add(cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME)));
                productIdList.add(cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_PRODUCT_ID)));
                // now retrive price 1, price2, quantity according to product id
                String productId = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_PRODUCT_ID));
                priceList1.add(getMinQtyAndPrice(productId,date).get(Tables.PRODUCT_PRICE_PRICE));
                priceList2.add(getMinQtyAndPrice(productId,date).get(Tables.MinPrice));
                quantityList.add(getMinQtyAndPrice(productId,date).get(Tables.MinQty));
                cursor.moveToNext();
            }
        }
        priceListHashmap.put(PRODUCT_ID,productIdList);
        priceListHashmap.put(Tables.PRODUCT_PRODUCT_NAME,productNameList);
        priceListHashmap.put(Tables.PRODUCT_PRICE_PRICE,priceList1);
        priceListHashmap.put(Tables.MinPrice,priceList2);
        priceListHashmap.put(Tables.MinQty,quantityList);
        return  priceListHashmap;
    }
    public ArrayList<HashMap<String, String>> Getproductlist(String CategoryID) {


        ArrayList<HashMap<String, String>> ItemListFromDB = new ArrayList<>();
        open();
        String query = "";
        if (!CategoryID.equalsIgnoreCase("0"))
            query = "SELECT DISTINCT * FROM " + TABLE_NAME_PRODUCT_BOOLEAN + " WHERE product_category_id=" + "'" + CategoryID + "'" + " AND outlet_id=" + "'" + getPreference(OUTLETS_ID) + "'";
        else
            query = "SELECT DISTINCT * FROM " + TABLE_NAME_PRODUCT_BOOLEAN + " WHERE   outlet_id=" + "'" + getPreference(OUTLETS_ID) + "'";


        //subrata da..................................................
        ItemListFromDB.clear();
        Cursor c = sqLiteDatabase.rawQuery(query, null);
        Log.e("INquery", query + " size=" + c.getCount());
        if (c != null) {
            if (c.moveToFirst()) {
                do {


                    String outlet_id = c.getString(c.getColumnIndex(OUTLETS_ID));
                    String product_id = c.getString(c.getColumnIndex(PRODUCT_PRODUCT_ID));
                    String quantity = c.getString(c.getColumnIndex("product_id"));


                    String product_name = "";
                    String query1 = "SELECT product_name FROM " + TABLE_NAME_PRODUCT + " WHERE product_id='" + product_id + "'";
                    Cursor c2 = sqLiteDatabase.rawQuery(query1, null);
                    if (c2 != null) {
                        if (c2.moveToFirst()) {
                            do {
                                product_name = c2.getString(0);
                            } while (c2.moveToNext());
                        }
                    }

                    c2.close();

                    String boolean12 = c.getString(c.getColumnIndex("boolean"));

                    Log.e("GGGGGGGGGGGGGGGGGGGGGGG", "HHHHHHHHHHHHHHHHHHHHHHHHHHHHHH outlet_id " + outlet_id);
                    Log.e("GGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG product_id " + product_id);
                    Log.e("GGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG product_name " + product_name);
                    Log.e("GGGGGGGGGGGGGGGGGGGGGGG", "GGGGGGGGGGGGGGGGGGGGGGGGGGGGGG product_boolean " + boolean12);

                    HashMap<String, String> product_list_map = new HashMap<String, String>();

                    product_list_map.put("product_category_id", CategoryID);
                    product_list_map.put("product_id", product_id);
                    product_list_map.put("product_name", product_name);
                    product_list_map.put("boolean", boolean12);


                /*    if (TempData.OutletCatagoryID.equalsIgnoreCase("17")) {
                        Cursor ispoduct = db.rawQuery("Select * from distibutor_product_prices where product_id='" + product_id + "'");
                        if (ispoduct != null && ispoduct.getCount() > 0)
                            ItemListFromDB.add(product_list_map);

                    } else {


                    }*/


                    ItemListFromDB.add(product_list_map);

                } while (c.moveToNext());
            }


        }
        close();
        return ItemListFromDB;
    }


    public void insertData(String json) {

        Log.e("jsndata",json);
        new InsertData(json).execute();

    }

    public void generatePushJson() {

        new generatePushJson().execute();

    }

    public void generateRequirJson() {

        new Requireddata().execute();

    }

    public void getlastupdateddate() {

        new lastUpdated().execute();

    }

    public void insertBoolenatable(ContentValues contentValues, String table_name) {
        open();

        long id = sqLiteDatabase.insert(table_name, null, contentValues);
        Log.e("id", id + "");

        close();


    }

    public Cursor rawQuery(String query) {
        open();
        Cursor c = sqLiteDatabase.rawQuery(query, null);
       /* close();
        c.close();*/
        return c;
    }

    public long createNewOutlet(HashMap<String, String> newOutlet) {
        this.open();
        Date currentTime = Calendar.getInstance().getTime();
        ContentValues contentValues = new ContentValues();
        for (String i : newOutlet.keySet()) {
            contentValues.put(i, newOutlet.get(i));
        }
        contentValues.put(Tables.OUTLETS_CREATED_AT, String.valueOf(currentTime));
        long id = sqLiteDatabase.insert(Tables.TABLE_NAME_OUTLETS, null, contentValues);
        return id;
    }

    public long updateOutlet(HashMap<String, String> updatedOutlet, String outletId) {
        this.open();
        String id[] = {outletId};
        Date currentTime = Calendar.getInstance().getTime();
        ContentValues contentValues = new ContentValues();
        for (String i : updatedOutlet.keySet()) {
            contentValues.put(i, updatedOutlet.get(i));
        }
        contentValues.put(Tables.OUTLETS_CREATED_AT, String.valueOf(currentTime));
        long x = sqLiteDatabase.update(Tables.TABLE_NAME_OUTLETS, contentValues, Tables.OUTLETS_ID + " =?", id);
        return x;
    }

    public long insertIntoMarket(HashMap<String, String> dataForMarketCreate) {
        this.open();
        Date currentTime = Calendar.getInstance().getTime();
        ContentValues contentValues = new ContentValues();
        for (String i : dataForMarketCreate.keySet()) {
            contentValues.put(i, dataForMarketCreate.get(i));
        }
        contentValues.put(Tables.MARKETS_created_at, String.valueOf(currentTime));
        long id = sqLiteDatabase.insert(Tables.TABLE_NAME_MARKETS, null, contentValues);
        this.close();
        //  Log.e("Insert to Market Table", "onClick: " + id + " n" + dataForMarketCreate);
        return id;
    }

    public String getUnitName(String unitId) {
        this.open();
        String unitName = null;
        String query = "SELECT " + Tables.UNIT_UNAME + " FROM " + Tables.TABLE_NAME_UNIT + " WHERE " + Tables.UNIT_U_ID + " = " + unitId + " ";
        Log.e("unit_Query",query);
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            unitName = cursor.getString(cursor.getColumnIndex(Tables.UNIT_UNAME));
        }
        return unitName;
    }

    public String getProductName(String productId) {
        this.open();
        String productName = null;
        String query = "SELECT " + Tables.PRODUCT_PRODUCT_NAME + " FROM " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + " ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            productName = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME));
        }
        return productName;
    }

    public  void updateDBId(String db_id, int id){

        this.open();
        ContentValues data=new ContentValues();
        data.put("db_id", db_id);
        sqLiteDatabase.update(TABLE_NAME_ORDER, data, "_id=" + id, null);

        this.close();
    }


    public String getProductId(String p_id) {
        this.open();
        String productId = null;
        String query = "SELECT * FROM product_history WHERE is_bonus='0' and product_id="+p_id;
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            productId = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_HISTORY_IS_BONUS));
        }

        else {
            productId="1";
        }
        return productId;
    }

    public HashMap<String, String> getMinQtyAndPrice(String productId) {
        this.open();
        String qty = null;
        String price = null;
        HashMap<String, String> hashMap = new HashMap<>();
        String query = "SELECT " + Tables.PRODUCT_COMBINATION_MIN_QUANTITY + ", " + Tables.PRODUCT_COMBINATION_PRICE + " FROM " + TABLE_NAME_PRODUCT_COMBINATION + " WHERE " + PRODUCT_PRODUCT_ID + " = " + productId + " ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            qty = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_COMBINATION_MIN_QUANTITY));
            price = cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_COMBINATION_PRICE));
        }
        hashMap.put("min_qty", qty);
        hashMap.put("min_price", price);
        return hashMap;
    }

    public HashMap<String, ArrayList<String>> getProductTypeList() {
        this.open();
        HashMap<String, ArrayList<String>> productTypeHashmap = new HashMap<>();
        ArrayList<String> productTypeNameList;
        ArrayList<String> productTypeIdList;
        String query;
        query = "SELECT " + Tables.PRODUCT_TYPE_ID + ", " + Tables.PPRODCUT_TYPE_PRODUCT_NAME + " FROM " + Tables.TABLE_NAME_PRODCUT_TYPE + "";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        productTypeNameList = new ArrayList<>();
        productTypeIdList = new ArrayList<>();
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
           /*thanaIdList.add("0");
           thanaNameList.add("");*/
            for (int i = 0; i < cursor.getCount(); i++) {
                productTypeIdList.add(cursor.getString(cursor.getColumnIndex(Tables.PRODUCT_TYPE_ID)));
                productTypeNameList.add(cursor.getString(cursor.getColumnIndex(Tables.PPRODCUT_TYPE_PRODUCT_NAME)));
                cursor.moveToNext();
            }
            productTypeHashmap.put(Tables.PPRODCUT_TYPE_PRODUCT_NAME, productTypeNameList);
            productTypeHashmap.put(Tables.PRODUCT_TYPE_ID, productTypeIdList);
        }
        this.close();
        return productTypeHashmap;
    }


    public HashMap<String, ArrayList<String>> getProductCategoryeList() {
        this.open();
        HashMap<String, ArrayList<String>> productCategoryHashmap = new HashMap<>();
        ArrayList<String> productCatgNameList;
        ArrayList<String> productCatgIdList;
        String query;
        query = "SELECT " + Tables.PPRODUCT_CATEGORY_C_id + ", " + Tables.PPRODUCT_CATEGORY_C_NAME + " FROM " + Tables.TABLE_NAME_PRODUCT_CATEGORY + "";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        productCatgNameList = new ArrayList<>();
        productCatgIdList = new ArrayList<>();
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            for (int i = 0; i < cursor.getCount(); i++) {
                productCatgIdList.add(cursor.getString(cursor.getColumnIndex(Tables.PPRODUCT_CATEGORY_C_id)));
                productCatgNameList.add(cursor.getString(cursor.getColumnIndex(Tables.PPRODUCT_CATEGORY_C_NAME)));
                cursor.moveToNext();
            }
            productCategoryHashmap.put(Tables.PPRODUCT_CATEGORY_C_NAME, productCatgNameList);
            productCategoryHashmap.put(Tables.PPRODUCT_CATEGORY_C_id, productCatgIdList);
        }
        this.close();
        return productCategoryHashmap;
    }

    public long InsertTable(HashMap<String, String> data, String TableName) {


        ContentValues values = new ContentValues();

        Iterator it = data.entrySet().iterator();
        long insertedID = 0;

        while (it.hasNext()) {
            Map.Entry<String, String> pair = (Map.Entry) it.next();
            values.put(pair.getKey(), pair.getValue()); // Contact Name

            it.remove();
        }
        if (TableName.equalsIgnoreCase("product_history")) {
            Log.e("Content producthistory:", values.toString());
        }

        if (TableName.equalsIgnoreCase("memos")){
            Log.e("memosT", "   :" + "INSERTED");
        }
        Log.e(" Content values  :", values.toString());
        Log.e(TableName, "   :" + "INSERTED");
        open();
        insertedID = sqLiteDatabase.insert(TableName, null, values);

        close();
        return insertedID;

    }

    //................Retrieve Territory List for Market Create .........................//

    public HashMap<String, ArrayList<String>> getTerritoryList(String salesPersonId) {

        this.open();

        HashMap<String, ArrayList<String>> territoryHashMap = new HashMap<>();
        ArrayList<String> territoryNameList;
        ArrayList<String> territoryIdList;

        String id = salesPersonId;
        String query = "SELECT " + Tables.TERRITORY_NAME + ", " + Tables.TERRITORY_T_id + " FROM " + Tables.TABLE_NAME_TERRITORY + " WHERE " + Tables.SR_ID + " = " + id + "";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        territoryNameList = new ArrayList<>();
        territoryIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*territoryIdList.add("0");
            territoryNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {


                territoryIdList.add(cursor.getString(cursor.getColumnIndex(Tables.TERRITORY_T_id)));
                territoryNameList.add(cursor.getString(cursor.getColumnIndex(Tables.TERRITORY_NAME)));
                cursor.moveToNext();
            }

            territoryHashMap.put(Tables.TERRITORY_NAME, territoryNameList);
            territoryHashMap.put(Tables.TERRITORY_T_id, territoryIdList);


        }

        this.close();

        return territoryHashMap;

    }

    //........................RETRIVE INDIVIDUAL OUTLET DETAILS ................//

    public HashMap<String, String> getIndivOutletDetails(String outletId) {

        this.open();
        HashMap<String, String> indivOutlet = new HashMap<>();
        String query = "SELECT * FROM " + Tables.TABLE_NAME_OUTLETS + " WHERE " + Tables.OUTLETS_ID + " = '" + outletId + "'";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        Log.e("query", query + "  " + cursor.getCount() + "");
        if (cursor != null && cursor.getCount() > 0) {
            for (int i = 1; i < Tables.OUTLETS.length; i++) {
                // Log.e(Tables.OUTLETS[i],cursor.getString(cursor.getColumnIndex(Tables.OUTLETS[i])));
                indivOutlet.put(Tables.OUTLETS[i], cursor.getString(cursor.getColumnIndex(Tables.OUTLETS[i])));


            }

        }
        this.close();

        return indivOutlet;

    }

    public HashMap<String, ArrayList<String>> getIndividualOutlet(String outletId) {

        this.open();

        HashMap<String, ArrayList<String>> outletHashMap = new HashMap<>();
        ArrayList<String> outletAttributeList;
        ArrayList<String> outletValueList;
        String qry1, qry2, qry3;
        String categoryId = null, thanaId = null, marketId = null;
        String category = null, thana = null, market = null;


        String query = "SELECT * FROM " + Tables.TABLE_NAME_OUTLETS + " WHERE " + Tables.OUTLETS_ID + " = " + outletId + "";


        Cursor cursor = sqLiteDatabase.rawQuery(query, null);


        outletAttributeList = new ArrayList<>();
        outletValueList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            categoryId = cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_CATAGORY_ID));
            thanaId = cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_THANA_ID));
            marketId = cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_MARKET_ID));

            qry1 = "SELECT " + Tables.OUTLET_CATEGORY_NAME + " FROM " + Tables.TABLE_NAME_OUTLET_CATEGORY + " WHERE " + Tables.OUTLET_CATEGORY_CATEGORY_ID + " = " + categoryId + "";
            qry2 = "SELECT " + Tables.THANA_NAME + " FROM " + Tables.TABLE_NAME_THANA + " WHERE " + Tables.THANA_TH_id + " = " + thanaId + "";
            qry3 = "SELECT " + Tables.MARKETS_market_name + " FROM " + Tables.TABLE_NAME_MARKETS + " WHERE " + Tables.MARKETS_market_id + " = " + marketId + "";

            Cursor c1 = sqLiteDatabase.rawQuery(qry1, null);
            c1.moveToFirst();
            if (c1 != null && c1.getCount() > 0) {
                category = c1.getString(c1.getColumnIndex(Tables.OUTLET_CATEGORY_NAME));

            }
            c1.close();

            Cursor c2 = sqLiteDatabase.rawQuery(qry2, null);
            c2.moveToFirst();
            if (c2 != null && c2.getCount() > 0) {
                thana = c2.getString(c2.getColumnIndex(Tables.THANA_NAME));
            }
            c2.close();

            Cursor c3 = sqLiteDatabase.rawQuery(qry3, null);
            c3.moveToFirst();
            if (c3 != null && c3.getCount() > 0) {
                market = c3.getString(c3.getColumnIndex(Tables.MARKETS_market_name));
            }
            c3.close();


            outletAttributeList.add("Outlet Name");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_OUTLET_NAME)));

            outletAttributeList.add("Outlet Type");
            outletValueList.add(category);

            outletAttributeList.add("NGO/Ins");
            if (cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_ISNGO)).equals("0")) {
                outletValueList.add("No");
            } else {
                outletValueList.add("Yes");
            }

            outletAttributeList.add("NGO Name");
            outletValueList.add("");

            outletAttributeList.add("Pharma Type");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_PHARMA_TYPE)));

            outletAttributeList.add("Incharge Name");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_INCHARGE_NAME)));

            outletAttributeList.add("Owner Name");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_OWNER_NAME)));

            outletAttributeList.add("Market Name");
            outletValueList.add(market);

            outletAttributeList.add("Thana Name");
            outletValueList.add(thana);

            outletAttributeList.add("Mobile");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_MOBILE)));

            outletAttributeList.add("Address");
            outletValueList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_ADDRESS)));


        }


        outletHashMap.put("Attributes", outletAttributeList);
        outletHashMap.put("Values", outletValueList);

        cursor.close();

        this.close();

        return outletHashMap;
    }


    //................Retrieve mARKET List for oUTLET Create .........................//
    public HashMap<String, ArrayList<String>> getMarketData(String byRouteId, String byThanaId) {

        this.open();

        String query = null;
        HashMap<String, ArrayList<String>> marketHashMap = new HashMap<>();
        ArrayList<String> marketNameList;
        ArrayList<String> marketIdList;

        if (byRouteId.equals("no") && byThanaId.equals("no")) {
            query = "SELECT " + Tables.MARKETS_market_name + ", " + Tables.MARKETS_market_id + " FROM " + Tables.TABLE_NAME_MARKETS + "";
        } else if (!byRouteId.equals("no") && byThanaId.equals("no")) {

            query = "SELECT " + Tables.MARKETS_market_name + ", " + Tables.MARKETS_market_id + " FROM " + Tables.TABLE_NAME_MARKETS + " WHERE " + Tables.ROUTE_ID + " = " + byRouteId + "";
        } else if (!byThanaId.equals("no") && byRouteId.equals("no")) {

            query = "SELECT " + Tables.MARKETS_market_name + ", " + Tables.MARKETS_market_id + " FROM " + Tables.TABLE_NAME_MARKETS + " WHERE " + Tables.MARKETS_thana_id + " = " + byThanaId + "";
        }

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        marketNameList = new ArrayList<>();
        marketIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*marketIdList.add("0");
            marketNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                marketIdList.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_market_id)));
                marketNameList.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_market_name)));
                cursor.moveToNext();
            }

            marketHashMap.put(Tables.MARKETS_market_name, marketNameList);
            marketHashMap.put(Tables.MARKETS_market_id, marketIdList);

        }

        cursor.close();

        return marketHashMap;

    }

    //................Retrieve mARKET List for oUTLET Create .........................//
    public HashMap<String, ArrayList<String>> getMarketDataOutlet(String bydistID) {

        this.open();
       Log.e("database_db_id",bydistID);
        HashMap<String, ArrayList<String>> marketHashMap = null;
        try {
            String query = null;
            marketHashMap = new HashMap<>();
            ArrayList<String> marketNameList;
            ArrayList<String> marketIdList;
            query = "SELECT " + Tables.MARKETS_market_name + ", " + Tables.MARKETS_market_id + " FROM " + Tables.TABLE_NAME_MARKETS + " WHERE " + Tables.MARKETS_db_id + " = " + bydistID + "";

            Cursor cursor = sqLiteDatabase.rawQuery(query, null);

            marketNameList = new ArrayList<>();
            marketIdList = new ArrayList<>();

            cursor.moveToFirst();
            if (cursor != null && cursor.getCount() > 0) {

                /*marketIdList.add("0");
                marketNameList.add("");*/

                for (int i = 0; i < cursor.getCount(); i++) {

                    marketIdList.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_market_id)));
                    marketNameList.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_market_name)));
                    cursor.moveToNext();
                }

                marketHashMap.put(Tables.MARKETS_market_name, marketNameList);
                marketHashMap.put(Tables.MARKETS_market_id, marketIdList);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        this.close();

        return marketHashMap;

    }

    //..............Retrive Outlet data against Market id.........................//
    public HashMap<String, ArrayList<String>> getOutletData(String byThanaId, String byMarketId) {

        this.open();

        String query = null;
        HashMap<String, ArrayList<String>> outletHashMap = new HashMap<>();
        ArrayList<String> outletNameList;
        ArrayList<String> outletIdList;

        if (byMarketId.equals("no") && byThanaId.equals("no")) {
            query = "SELECT " + Tables.OUTLETS_OUTLET_NAME + ", " + Tables.OUTLETS_ID + " FROM " + Tables.TABLE_NAME_OUTLETS + "";
        } else if (byThanaId.equals("no") && !byMarketId.equals("no")) {

            query = "SELECT " + Tables.OUTLETS_OUTLET_NAME + ", " + Tables.OUTLETS_ID + " FROM " + Tables.TABLE_NAME_OUTLETS + " WHERE " + Tables.OUTLETS_MARKET_ID + " = " + byMarketId + "";
        } else if (!byThanaId.equals("no") && byMarketId.equals("no")) {

            query = "SELECT " + Tables.OUTLETS_OUTLET_NAME + ", " + Tables.OUTLETS_ID + " FROM " + Tables.TABLE_NAME_OUTLETS + " WHERE " + Tables.OUTLETS_THANA_ID + " = " + byThanaId + "";
        } else if (!byThanaId.equals("no") && !byMarketId.equals("no")) {

            query = "SELECT " + Tables.OUTLETS_OUTLET_NAME + ", " + Tables.OUTLETS_ID + " FROM " + Tables.TABLE_NAME_OUTLETS + " WHERE " + Tables.OUTLETS_THANA_ID + " = " + byThanaId + " AND " + Tables.OUTLETS_MARKET_ID + " = " + byMarketId + "";
        }

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        outletNameList = new ArrayList<>();
        outletIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*outletIdList.add("0");
            outletNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                outletIdList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_ID)));
                outletNameList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLETS_OUTLET_NAME)));
                cursor.moveToNext();
            }

            outletHashMap.put(Tables.OUTLETS_OUTLET_NAME, outletNameList);
            outletHashMap.put(Tables.OUTLETS_ID, outletIdList);

        }

        this.close();

        return outletHashMap;
    }



    //................Retrieve Thana List for Market Create .........................//
    public HashMap<String, ArrayList<String>> getThanaList(String byDbId) {

        this.open();

        HashMap<String, ArrayList<String>> thanaHashMap = new HashMap<>();
        ArrayList<String> thanaNameList;
        ArrayList<String> thanaIdList;
        String query;

       String id = byDbId; // previous terrtoryId
      //  query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + " WHERE " + Tables.THANA_DB_Id + " = " + byDbId + ""; // new added

        if (id.equals("no")) {

            query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + "";
            Log.e("thana_db_no", id);

        } else  {
           // query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + " WHERE " + Tables.THANA_TERRITORY_T_ID + " = " + id + ""; //
            query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + " WHERE " + Tables.THANA_DB_Id + " = " + id + ""; // new added

            Log.e("thana_db", id);
        }

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        thanaNameList = new ArrayList<>();
        thanaIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*thanaIdList.add("0");
            thanaNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                thanaIdList.add(cursor.getString(cursor.getColumnIndex(Tables.THANA_TERRITORY_T_ID)));
                thanaNameList.add(cursor.getString(cursor.getColumnIndex(Tables.THANA_NAME)));
                cursor.moveToNext();
            }

            thanaHashMap.put(Tables.THANA_NAME, thanaNameList);
            thanaHashMap.put(Tables.THANA_TH_id, thanaIdList);

        }

        cursor.close();

        return thanaHashMap;

    }

    //................Retrieve Thana List for Market Create .........................//
    public HashMap<String, ArrayList<String>> getThanaForMarketList(String byDbId) {

        this.open();

        HashMap<String, ArrayList<String>> thanaHashMap = new HashMap<>();
        ArrayList<String> thanaNameList;
        ArrayList<String> thanaIdList;
        String query;

        String id = byDbId; // previous terrtoryId

        if (id.equals("no")) {

            query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + "";
            Log.e("thana_db_no", id);

        } else  {
            query = "SELECT " + Tables.THANA_NAME + ", " + Tables.THANA_TERRITORY_T_ID + " FROM " + Tables.TABLE_NAME_THANA + " WHERE " + Tables.THANA_TERRITORY_T_ID + " = " + id + ""; //

            Log.e("thana_db", id);
        }

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        thanaNameList = new ArrayList<>();
        thanaIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*thanaIdList.add("0");
            thanaNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                thanaIdList.add(cursor.getString(cursor.getColumnIndex(Tables.THANA_TERRITORY_T_ID)));
                thanaNameList.add(cursor.getString(cursor.getColumnIndex(Tables.THANA_NAME)));
                cursor.moveToNext();
            }

            thanaHashMap.put(Tables.THANA_NAME, thanaNameList);
            thanaHashMap.put(Tables.THANA_TH_id, thanaIdList);

        }

        this.close();

        return thanaHashMap;

    }


    //................Retrieve Route List for Market Create .........................//
    public HashMap<String, ArrayList<String>> getRouteList() {
        this.open();
        HashMap<String, ArrayList<String>> routeHashMap = new HashMap<>();
        ArrayList<String> routeNameList;
        ArrayList<String> routeIdList;
        String query = "SELECT " + Tables.ROUTE_NAME + ", " + Tables.ROUTE_ID + " FROM " + Tables.TABLE_NAME_ROOT + "" ;
        Log.i("rout_log", query);

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        routeNameList = new ArrayList<>();
        routeIdList = new ArrayList<>();
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
           /*routeIdList.add("0");
           routeNameList.add("");*/
            for (int i = 0; i < cursor.getCount(); i++) {
                routeIdList.add(cursor.getString(cursor.getColumnIndex(Tables.ROUTE_ID)));
                routeNameList.add(cursor.getString(cursor.getColumnIndex(Tables.ROUTE_NAME)));
                cursor.moveToNext();
            }
            routeHashMap.put(Tables.ROUTE_ID, routeIdList);
            routeHashMap.put(Tables.ROUTE_NAME, routeNameList);
        }
        this.close();
        return routeHashMap;
    }

    //................Retrieve Location List for Market Create .........................//
    public HashMap<String, ArrayList<String>> getLocationList() {
        this.open();

        HashMap<String, ArrayList<String>> locationHashMap = new HashMap<>();
        ArrayList<String> locationNameList;
        ArrayList<String> locationIdList;

        String query = "SELECT " + Tables.LOCATION_location_name + ", " + Tables.LOCATION_location_id + " FROM " + Tables.TABLE_NAME_LOCATION + "";

         Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        locationNameList = new ArrayList<>();
        locationIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*locationIdList.add("0");
            locationNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                locationIdList.add(cursor.getString(cursor.getColumnIndex(Tables.LOCATION_location_id)));
                locationNameList.add(cursor.getString(cursor.getColumnIndex(Tables.LOCATION_location_name)));
                cursor.moveToNext();
            }

            locationHashMap.put(Tables.LOCATION_location_name, locationNameList);
            locationHashMap.put(Tables.LOCATION_location_id, locationIdList);

        }

        //this.close();

        return locationHashMap;

    }

    //................Retrieve Outlet Category List for Outlet Create .........................//
    public HashMap<String, ArrayList<String>> getOutletCategories() {

        this.open();

        HashMap<String, ArrayList<String>> outletCategoryHashMap = new HashMap<>();
        ArrayList<String> outletCategoryNameList;
        ArrayList<String> outletCategoryIdList;

        String query = "SELECT " + Tables.OUTLET_CATEGORY_NAME + ", " + Tables.OUTLET_CATEGORY_CATEGORY_ID + " FROM " + Tables.TABLE_NAME_OUTLET_CATEGORY + "";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        outletCategoryNameList = new ArrayList<>();
        outletCategoryIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*outletCategoryIdList.add("0");
            outletCategoryNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                outletCategoryIdList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLET_CATEGORY_CATEGORY_ID)));
                outletCategoryNameList.add(cursor.getString(cursor.getColumnIndex(Tables.OUTLET_CATEGORY_NAME)));
                cursor.moveToNext();
            }

            outletCategoryHashMap.put(Tables.OUTLET_CATEGORY_NAME, outletCategoryNameList);
            outletCategoryHashMap.put(Tables.OUTLET_CATEGORY_CATEGORY_ID, outletCategoryIdList);

        }

       this.close();

      //  cursor.close();

        return outletCategoryHashMap;

    }

    //................Retrieve Productt Category List ........................................//
    public HashMap<String, ArrayList<String>> getProductCategories() {

        this.open();

        HashMap<String, ArrayList<String>> productCategoryHashMap = new HashMap<>();
        ArrayList<String> producttCategoryNameList;
        ArrayList<String> productCategoryIdList;

        String query = "SELECT " + Tables.PPRODUCT_CATEGORY_C_id + ", " + Tables.PPRODUCT_CATEGORY_C_NAME + " FROM " + Tables.TABLE_NAME_PRODUCT_CATEGORY + "";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        producttCategoryNameList = new ArrayList<>();
        productCategoryIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*productCategoryIdList.add("0");
            producttCategoryNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                productCategoryIdList.add(cursor.getString(cursor.getColumnIndex(Tables.PPRODUCT_CATEGORY_C_id)));
                producttCategoryNameList.add(cursor.getString(cursor.getColumnIndex(Tables.PPRODUCT_CATEGORY_C_NAME)));
                cursor.moveToNext();
            }

            productCategoryHashMap.put(Tables.PPRODUCT_CATEGORY_C_NAME, producttCategoryNameList);
            productCategoryHashMap.put(Tables.PPRODUCT_CATEGORY_C_id, productCategoryIdList);

        }

        this.close();

        return productCategoryHashMap;

    }

    //................Retrieve  thana id , territory id List for Market Create .........................//
    public ArrayList<String> getIdsFromMarketToOutlet(String marketId) {

        this.open();

        ArrayList<String> thanaAndTerritoryId = new ArrayList<>();
        Log.e("for outlet create", "getIdsFromMarketToOutlet: " + marketId);

        String query = "SELECT " + Tables.MARKETS_thana_id + ", " + Tables.MARKETS_Territory_id + ", " + Tables.MARKETS_root_id + " FROM " + Tables.TABLE_NAME_MARKETS + " WHERE " + Tables.MARKETS_market_id + " = " + marketId + "";

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            thanaAndTerritoryId.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_thana_id)));
            thanaAndTerritoryId.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_Territory_id)));
            thanaAndTerritoryId.add(cursor.getString(cursor.getColumnIndex(Tables.MARKETS_root_id)));

        }

        this.close();

        return thanaAndTerritoryId;

    }

    public void deleteproductboolean(int prductId) {
        open();

        sqLiteDatabase.execSQL("delete from " + TABLE_NAME_PRODUCT_BOOLEAN + " where " + PRODUCT_BOOLEAN_PRODUCT_ID + " = '" + prductId + "'");

        Cursor c = sqLiteDatabase.rawQuery("Select * from " + TABLE_NAME_PRODUCT_BOOLEAN, null);
        c.moveToFirst();

        for (int i = 0; i < c.getCount(); i++) {


            Log.e("productname", c.getString(c.getColumnIndex(PRODUCT_BOOLEAN_PRODUCT_ID)));

            c.moveToNext();
        }


        close();

    }

    public void update(String tableNameProductBoolean, ContentValues contentValues, int prductId, String outlet) {
        open();

        String ar[] = {prductId + "", outlet};
        long id = sqLiteDatabase.update(tableNameProductBoolean, contentValues, PRODUCT_BOOLEAN_PRODUCT_ID + " =? and " + PRODUCT_BOOLEAN_OUTLET_ID + " =?", ar);
        Log.e("id", ar.toString() + " quantity " + contentValues.toString() + " id " + id);
        close();
    }

    public Double getQuantity(String outlet, int prductId) {
        Double Quantity = 0.00;
        open();
        Cursor c = sqLiteDatabase.rawQuery("Select * from " + TABLE_NAME_PRODUCT_BOOLEAN + " where " + PRODUCT_BOOLEAN_PRODUCT_ID + " = '" + prductId + "' and " + PRODUCT_BOOLEAN_OUTLET_ID + " = '" + outlet + "'", null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            Quantity = c.getDouble(c.getColumnIndex(PRODUCT_BOOLEAN_QUANTITY));
            Log.e("quantityget", Quantity + " pid " + prductId);

        }
        Log.e("quantitynot", Quantity + "");
        return Quantity;
    }

    public HashMap<String, ArrayList<String>> getAccessories(boolean flag, String value, String[] tableName, String filter ) {

        String query;
        HashMap<String, ArrayList<String>> map = new HashMap<>();
        ArrayList<String> idlist = new ArrayList<String>();
        ArrayList<String> namelist = new ArrayList<String>();
        if (tableName[0].equalsIgnoreCase(TABLE_NAME_THANA) || tableName[0].equalsIgnoreCase(TABLE_NAME_OUTLET_CATEGORY)) {
            idlist.add("0");
            namelist.add("All");
        }
        if (value.equalsIgnoreCase("00")) {
            query = "select * from " + tableName[0];
        } else {

            query = "select * from " + tableName[0] + " where " + filter + " = '" + value + "'";
        }

       /* if (tableName[0].equalsIgnoreCase(TABLE_NAME_OUTLETS) && !getPreference(OUTLET_CATAGORY[2]).equalsIgnoreCase("00") && !getPreference(MARKETS[2]).equalsIgnoreCase("00")) {

            query = "select * from " + tableName[0] + " where " + OUTLET_CATEGORY_CATEGORY_ID + " = '" + getPreference(OUTLET_CATAGORY[2]) + "' and " + OUTLETS_MARKET_ID + " = '" + getPreference(MARKETS[2]) + "'";

        }*/

        open();

        Cursor c = sqLiteDatabase.rawQuery(query, null);
        Log.e("getAccessories", query+" "+c.getCount());
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            do {
               // Log.e("log", c.getString(c.getColumnIndex(tableName[2])) + " yesss " + c.getString(c.getColumnIndex(tableName[3])));
                idlist.add(c.getString(c.getColumnIndex(tableName[2])));
                namelist.add(c.getString(c.getColumnIndex(tableName[3])));

            } while (c.moveToNext());
        }
        map.put(tableName[2], idlist);
        map.put(tableName[3], namelist);
       close();


        return map;


    }


    //................Retrieve Thana List for Market Create .........................//
    public HashMap<String, ArrayList<String>> getAccessories1(String byDbId, String out_cat_id) {

        this.open();

        HashMap<String, ArrayList<String>> thanaHashMap = new HashMap<>();
        ArrayList<String> thanaNameList;
        ArrayList<String> thanaIdList;
        String query;

        String id = byDbId; // previous terrtoryId
        if (!byDbId.equals("0") && !out_cat_id.equals("0")){
            query = "SELECT outlet_name, outlet_id FROM outlets WHERE db_id =" + byDbId + " and outlet_category_id =" + out_cat_id; // new added
        }
        else {
            query = "SELECT outlet_name, outlet_id FROM outlets WHERE db_id =" + byDbId; // new added
        }

        //query = "SELECT " + OUTLETS_OUTLET_NAME + ", " + OUTLETS_ID+ " FROM " + TABLE_NAME_OUTLETS + " WHERE " + Tables.THANA_DB_Id + " = " + byDbId + ""; // new added

        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        Log.e("select_out_let_data", query);

        thanaNameList = new ArrayList<>();
        thanaIdList = new ArrayList<>();

        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            /*thanaIdList.add("0");
            thanaNameList.add("");*/

            for (int i = 0; i < cursor.getCount(); i++) {

                thanaIdList.add(cursor.getString(cursor.getColumnIndex(OUTLETS_ID)));
                thanaNameList.add(cursor.getString(cursor.getColumnIndex(OUTLETS_OUTLET_NAME)));
                cursor.moveToNext();
            }

            thanaHashMap.put(Tables.OUTLETS_OUTLET_NAME, thanaNameList);
            thanaHashMap.put(Tables.OUTLETS_ID, thanaIdList);

        }

        this.close();

        return thanaHashMap;

    }

    public ArrayList<HashMap<String, String>> getProducts(String outletID) {

        ArrayList<HashMap<String, String>> order_reportList = new ArrayList<>();


        String query = "Select * from " + Tables.TABLE_NAME_PRODUCT;
        open();
        Cursor c = sqLiteDatabase.rawQuery(query, null);

        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {


                HashMap<String, String> map = new HashMap<>();

                map.put("product_name", c.getString(c.getColumnIndex(Tables.PRODUCT_PRODUCT_NAME)));
                map.put("boolean", "false");
                map.put("so_id", getPreference(Tables.DELETED_MEMOS_sales_person_id));
                map.put("outlet_id", outletID);
                map.put("product_id", c.getString(c.getColumnIndex(PRODUCT_PRODUCT_ID)));

                ContentValues values = new ContentValues();
                values.put("boolean", "false");
                values.put("so_id", getPreference(Tables.DELETED_MEMOS_sales_person_id));
                values.put("outlet_id", outletID);
                values.put("product_id", c.getString(c.getColumnIndex(PRODUCT_PRODUCT_ID)));

                insertBoolenatable(values, Tables.TABLE_NAME_PRODUCT_BOOLEAN);

                order_reportList.add(map);
            } while (c.moveToNext());
        }
        close();

        return order_reportList;
    }

    public void insertIntobooolean(String _OutletID) {

        open();
        String query1 = "SELECT * FROM " + TABLE_NAME_PRODUCT + "  group by product_id,product_name,product_category_id,product_type_id ORDER BY product_order ASC";
        Log.e("query", query1);
        Cursor c2 = sqLiteDatabase.rawQuery(query1, null);
        c2.moveToFirst();
        if (c2 != null) {
            if (c2.moveToFirst()) {
                do {

                    Log.e("EEEEEEEEEEEEEEEEEE", "-----------***********----------");


                    String product_id = c2.getString(c2.getColumnIndex("product_id"));
                    String product_name = c2.getString(c2.getColumnIndex("product_name"));
                    String product_category_id = c2.getString(c2.getColumnIndex("product_category_id"));
                    String product_type_id = c2.getString(c2.getColumnIndex("product_type_id"));


                    //   savePreference(product_id, "0.0");


                    ContentValues map = new ContentValues();

                    map.put("outlet_id", _OutletID);
                    map.put("product_id", product_id);
                    map.put("quantity", "0");
                    map.put("boolean", "false");
                    map.put("product_category_id", product_category_id);
                    map.put("product_type_id", product_type_id);
                    open();
                    Cursor cursor = sqLiteDatabase.rawQuery("Select * from " + TABLE_NAME_PRODUCT_BOOLEAN + " Where " + PRODUCT_BOOLEAN_OUTLET_ID + " = '" + _OutletID + "' and " + PRODUCT_BOOLEAN_PRODUCT_ID + " = '" + product_id + "'", null);
                    if (cursor.getCount() <= 0) {
                        insertBoolenatable(map, TABLE_NAME_PRODUCT_BOOLEAN);
                    }


                } while (c2.moveToNext());
            }
        }

        close();
    }

    public ArrayList<HashMap<String, String>> getSelectedProducts() {
        ArrayList<HashMap<String, String>> list = new ArrayList<>();

        open();
        String queryy = "SELECT p.product_name ,pb.product_category_id, pb.quantity ,p.product_id from product as p INNER JOIN product_boolean as pb on p.product_id=pb.product_id  WHERE pb.boolean='true' AND pb.outlet_id='" + getPreference(OUTLETS_ID) + "'";

        Cursor c = sqLiteDatabase.rawQuery(queryy, null);
        Log.e("query", queryy + c.getCount());
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {
                HashMap<String, String> product_list_map = new HashMap<String, String>();
                product_list_map.put("product_id", c.getString(3));
                product_list_map.put("product_name", c.getString(0));
                product_list_map.put("quantity", c.getString(2));
                product_list_map.put("product_category_id", c.getString(1));

                list.add(product_list_map);

            } while (c.moveToNext());


        }


        close();
        return list;
    }

    public String getOutletCatagoryId() {
        open();
        String catagotyid = "100";
        Cursor c = sqLiteDatabase.rawQuery("select " + OUTLET_CATEGORY_CATEGORY_ID + " from " + TABLE_NAME_OUTLETS + " where " + OUTLETS_ID + " = '" + getPreference(OUTLETS_ID) + "'", null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            catagotyid = c.getString(0);
        }
        close();
        return catagotyid;
    }

    public void deleteRowOfMemo(String payments, String memo_no6) {
    }

    public String getLastmemodate(String outletID) {
        String memodate = "No Memo On this Outlet";


        String Query = "SELECT order_date from ORDER_table WHERE outlet_id = '" + outletID + "' ORDER by order_date_time DESC LIMIT 1";

        Cursor c = sqLiteDatabase.rawQuery(Query, null);

        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            return c.getString(0);


        }


        return memodate;
    }

    public void prepareDataForOrder(String _OutletID) {

        new prepareDataForOrder(_OutletID).execute();


    }

    public ArrayList<HashMap<String, String>> updateWithServer(JSONObject jsonObject) {

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("orders");

            for (int j = 0; j < jsonArray.length(); j++) {

                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                Log.e("jsonobj", jsonObject1.getString(ORDER[2]));

                ContentValues contentValues = new ContentValues();
                for (int i = 2; i < ORDER.length; i++) {

                    if (jsonObject1.has(ORDER[i])) {
                        try {
                            Log.e(ORDER[i], jsonObject1.getString(ORDER[i]));
                            contentValues.put(ORDER[i], jsonObject1.getString(ORDER[i]));
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Log.e("updateOrderWithServer1", ex.getMessage());
                        }
                    }

                }

                String ar[] = {jsonObject1.getString(ORDER[2])};
                open();
                Log.e("cv", contentValues.toString());
                if (updateOrInsert(ORDER[0], ORDER[2], jsonObject1.getString(ORDER[2])))
                    sqLiteDatabase.update(ORDER[0], contentValues, ORDER_order_number + " =? ", ar);
                    //sqLiteDatabase.insert(ORDER[0], null, contentValues);

                else {
                    contentValues.put(ORDER_is_pushed, 1);
                    contentValues.put(ORDER_STATUS, PROCESSING_PENDING);
                    sqLiteDatabase.insert(ORDER[0], null, contentValues);
                }

                close();


                excQuery("DELETE from ORDER_DETAILS where order_number ='" + jsonObject1.getString(ORDER[2]) + "'");
                JSONArray jsonArray1 = jsonObject1.getJSONArray("order_details");

                for (int k = 0; k < jsonArray1.length(); k++) {
                    JSONObject jsonObject2 = jsonArray1.getJSONObject(k);
                    ContentValues contentValues2 = new ContentValues();

                    for (int i = 2; i < ORDER_DETAILS.length; i++) {
                        if (jsonObject2.has(ORDER_DETAILS[i]))
                            contentValues2.put(ORDER_DETAILS[i], jsonObject2.getString(ORDER_DETAILS[i]));
                    }

                    open();

                    Log.e("cv2", contentValues2.toString());

                    sqLiteDatabase.insert(ORDER_DETAILS[0], null, contentValues2);

                    close();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("updateOrderWithServer", e.getMessage());
        }

        return null;
    }

    public ArrayList<HashMap<String, String>> updateMemoWithServer(JSONObject jsonObject) {

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("memos");

            for (int j = 0; j < jsonArray.length(); j++) {

                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                Log.e("jsonobj", jsonObject1.getString(MEMOS[2]));


                ContentValues contentValues = new ContentValues();
                for (int i = 2; i < MEMOS.length; i++) {

                    if (jsonObject1.has(MEMOS[i])) {
                        try {
                            Log.e(MEMOS[i], jsonObject1.getString(MEMOS[i]));
                            contentValues.put(MEMOS[i], jsonObject1.getString(MEMOS[i]));
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Log.e("updateOrderWithServer1", ex.getMessage());
                        }
                    }


                }

                String ar[] = {jsonObject1.getString(MEMOS[2])};
                open();
                Log.e("cv", contentValues.toString());
                if (updateOrInsert(MEMOS[0], MEMOS[2], jsonObject1.getString(MEMOS[2]))) {
                    sqLiteDatabase.update(MEMOS[0], contentValues, MEMOS_memo_number + " =? ", ar);
                    Log.e("memo", "update");
                } else {
                    contentValues.put(ORDER_is_pushed, 1);
                    // contentValues.put(ORDER_STATUS, PROCESSING_PENDING);
                    Log.e("memo", "insert");
                    sqLiteDatabase.insert(MEMOS[0], null, contentValues);
                }

                close();


                excQuery("DELETE from memo_details where memo_number ='" + jsonObject1.getString(MEMOS[2]) + "'");
                JSONArray jsonArray1 = jsonObject1.getJSONArray("memo_details");

                for (int k = 0; k < jsonArray1.length(); k++) {
                    JSONObject jsonObject2 = jsonArray1.getJSONObject(k);
                    ContentValues contentValues2 = new ContentValues();
                    for (int i = 2; i < MEMO_DETAILS.length; i++) {

                        if (jsonObject2.has(MEMO_DETAILS[i]))
                            contentValues2.put(MEMO_DETAILS[i], jsonObject2.getString(MEMO_DETAILS[i]));


                    }

                    open();
                    Log.e("cv2", contentValues2.toString());

                    sqLiteDatabase.insert(MEMO_DETAILS[0], null, contentValues2);

                    close();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("updateOrderWithServer", e.getMessage());
        }


        return null;
    }

    public ArrayList<HashMap<String, String>> updateWithServerProcessed(JSONObject jsonObject) {

        try {
            Log.e("updateWithServer", jsonObject + "");
            JSONArray jsonArray = jsonObject.getJSONArray("orders");

            for (int j = 0; j < jsonArray.length(); j++) {

                JSONObject jsonObject1 = jsonArray.getJSONObject(j);
                Log.e("jsonobj", jsonObject1.getString(ORDER[2]));


                ContentValues contentValues = new ContentValues();
                for (int i = 2; i < ORDER.length; i++) {

                    if (jsonObject1.has(ORDER[i])) {
                        try {
                            Log.e(ORDER[i], jsonObject1.getString(ORDER[i]));
                            contentValues.put(ORDER[i], jsonObject1.getString(ORDER[i]));
                        } catch (JSONException ex) {
                            ex.printStackTrace();
                            Log.e("updateOrderWithServer1", ex.getMessage());
                        }
                    }


                }

                String ar[] = {jsonObject1.getString(ORDER[2])};
                open();
                Log.e("cv", contentValues.toString());
                if (updateOrInsert(ORDER[0], ORDER[2], jsonObject1.getString(ORDER[2])))
                    sqLiteDatabase.update(ORDER[0], contentValues, ORDER_order_number + " =? ", ar);
                else {
                    contentValues.put(ORDER_is_pushed, 1);
                    contentValues.put(ORDER_STATUS, PROCESSING_COMPELETE);
                    sqLiteDatabase.insert(ORDER[0], null, contentValues);
                }

                close();


                excQuery("DELETE from ORDER_DETAILS where order_number ='" + jsonObject1.getString(ORDER[2]) + "'");
                JSONArray jsonArray1 = jsonObject1.getJSONArray("order_details");

                for (int k = 0; k < jsonArray1.length(); k++) {
                    JSONObject jsonObject2 = jsonArray1.getJSONObject(k);
                    ContentValues contentValues2 = new ContentValues();
                    for (int i = 2; i < ORDER_DETAILS.length; i++) {

                        if (jsonObject2.has(ORDER_DETAILS[i]))
                            contentValues2.put(ORDER_DETAILS[i], jsonObject2.getString(ORDER_DETAILS[i]));


                    }

                    open();
                    Log.e("cv2", contentValues2.toString());

                    sqLiteDatabase.insert(ORDER_DETAILS[0], null, contentValues2);

                    close();
                }
            }


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("updateOrderWithServer", e.getMessage());
        }


        return null;
    }

    private boolean updateOrInsert(String table_name, String check_colum, String check_value, String check_colum2, String check_value2) {
        Boolean update = false;
        open();
        Cursor c = sqLiteDatabase.rawQuery("Select * from " + table_name + " where " + check_colum + " ='" + check_value + "' and " + check_colum2 + " ='" + check_value2 + "'", null);
        Log.e("updateOrInsert2", "Select * from " + table_name + " where " + check_colum + " ='" + check_value + "' and " + check_colum2 + " ='" + check_value2 + "'" + c.getCount());
        if (c != null && c.getCount() > 0) {

            update = true;
        }
        Log.e("updateOrInsertdetails", update + "   " + check_value2);
        return update;
    }

    private boolean updateOrInsert(String table_name, String check_colum, String check_value) {
        Boolean update = false;
        open();
        Cursor c = sqLiteDatabase.rawQuery("Select * from " + table_name + " where " + check_colum + " ='" + check_value + "'", null);

        if (c != null && c.getCount() > 0) {

            update = true;
        }

        Log.e("updateOrInsert", update + "");
        return update;

    }

    public ArrayList<HashMap<String, String>> getNotPushedOrder(String start_date, String end_date, String outlate_id,String outlate_category_id, String distributorStoreId) {
        ArrayList<HashMap<String, String>> list = null;
        try {
            list = new ArrayList<>();

            Log.e("Item",outlate_id+"  "+outlate_category_id);
            open();
            String Query = "";

            if (outlate_id.equals("0") && outlate_category_id.equalsIgnoreCase("0"))
                Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where db_id=" + distributorStoreId + " and " +Tables.ORDER_order_date + ">=" + "'" + start_date + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "'  ORDER BY _id DESC";

            else if (!outlate_id.equals("0") &&  outlate_category_id.equalsIgnoreCase("0")) {
                Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where db_id=" + distributorStoreId + " and " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' AND " + ORDER_outlet_id + "='" + outlate_id + "' ORDER BY _id DESC";
            }

            else if (!outlate_id.equals("0") && !outlate_category_id.equalsIgnoreCase("0")){
                Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id='"+outlate_category_id+"' and  " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' and ot.db_id=" +distributorStoreId+ " ORDER BY _id DESC";
                // Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id='"+outlate_category_id+"' and  " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' AND ot." + ORDER_outlet_id + "='" + outlate_id + "' ORDER BY _id DESC";
            }
            else if (outlate_id.equals("0") && !outlate_category_id.equalsIgnoreCase("0")){
                Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id='"+outlate_category_id+"' and  " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' and ot.db_id=" +distributorStoreId+ " ORDER BY _id DESC";
                // Query = "SELECT * FROM order_table as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id="+outlate_category_id+ " and  order_date>="+start_date+" and order_date <="+end_date+" and ot.db_id=" + distributorStoreId + " ORDER BY _id DESC";
            }

            Log.e("DataView1", Query);

            Cursor c = sqLiteDatabase.rawQuery(Query, null);
            c.moveToFirst();
            if (c != null && c.getCount() > 0) {

                do {

                    HashMap<String, String> map = new HashMap<>();

                    for (int i = 2; i < ORDER.length; i++) {

                        if (ORDER[i].equalsIgnoreCase(ORDER_outlet_id)) {

                            map.put(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])));
                            map.put(OUTLETS_OUTLET_NAME, getSingleFilter(TABLE_NAME_OUTLETS, c.getString(c.getColumnIndex(ORDER[i])), OUTLETS_ID, OUTLETS_OUTLET_NAME));
                        } else {
                            map.put(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])));
                            Log.e(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])) + "");
                        }
                    }

                    list.add(map);

                } while (c.moveToNext());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public ArrayList<HashMap<String, String>> getProcessedOrder(String start_date, String end_date, String outlate_id, String distributorStoreId) {
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        open();
        String Query = "";

        //Change query for showing distributor wise data in delivery report --
        if (outlate_id.equals("0") && !start_date.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where db_id=" + distributorStoreId + " and " + Tables.ORDER_order_date + ">=" + "'" + start_date + "' and " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "'  ORDER BY _id DESC";
        else if (!outlate_id.equals("0") && !start_date.equalsIgnoreCase("0"))

        Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " as ot join markets as m on ot.market_id=m.market_id and m.route_id='" + outlate_id + "' where " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'and " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' and ot.db_id=" +distributorStoreId+ "  ORDER BY _id DESC";

        else if (outlate_id.equals("0") && start_date.equalsIgnoreCase("0") && end_date.equalsIgnoreCase("0")) {

            Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where  " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'   ORDER BY _id DESC";
        }

      /*  if (outlate_id.equals("0") && !start_date.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where " + Tables.ORDER_order_date + ">=" + "'" + start_date + "' and " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "'  ORDER BY _id DESC";
        else if (!outlate_id.equals("0") && !start_date.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " as ot join markets as m on ot.market_id=m.market_id and m.route_id='" + outlate_id + "' where " + Tables.ORDER_order_date + ">=" + "'" + start_date + "'and " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'" + " and " + Tables.ORDER_order_date + " <=" + "'" + end_date + "' ORDER BY _id DESC";
        else if (outlate_id.equals("0") && start_date.equalsIgnoreCase("0") && end_date.equalsIgnoreCase("0")) {

            Query = "SELECT * FROM " + Tables.TABLE_NAME_ORDER + " where  " + ORDER_STATUS + " = '" + PROCESSING_COMPELETE + "'   ORDER BY _id DESC";
        }*/

        Log.e("DataView", Query);

        Cursor c = sqLiteDatabase.rawQuery(Query, null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {

                HashMap<String, String> map = new HashMap<>();

                for (int i = 2; i < ORDER.length; i++) {

                    if (ORDER[i].equalsIgnoreCase(ORDER_outlet_id)) {

                        map.put(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])));
                        map.put(OUTLETS_OUTLET_NAME, getSingleFilter(TABLE_NAME_OUTLETS, c.getString(c.getColumnIndex(ORDER[i])), OUTLETS_ID, OUTLETS_OUTLET_NAME));
                    } else {
                        map.put(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])));
                        Log.e(ORDER[i], c.getString(c.getColumnIndex(ORDER[i])) + "");
                    }
                }
                map.put(ORDER_STATUS, c.getString(c.getColumnIndex(ORDER_STATUS)));
                list.add(map);

            } while (c.moveToNext());
        }

        return list;
    }

    public ArrayList<HashMap<String, String>> getMemos(String start_date, String end_date, String outlate_id,String outlate_category_id) {
        ArrayList<HashMap<String, String>> list = new ArrayList<>();
        open();
        String Query = "";
        if (outlate_id.equals("0")&& outlate_category_id.equalsIgnoreCase("0"))
            //my query
            //Query = "SELECT * FROM " + TABLE_NAME_MEMOS + " where " + MEMOS_memo_date + ">=" + "'" + start_date + "'" + " and " + Tables.MEMOS_memo_date + " <=" + "'" + end_date + "'  GROUP BY memo_number ORDER By _id DESC";
            Query = "SELECT * FROM " + TABLE_NAME_MEMOS + " where " + MEMOS_memo_date + ">=" + "'" + start_date + "'" + " and " + Tables.MEMOS_memo_date + " <=" + "'" + end_date + "'  ORDER BY _id DESC";
        else if (!outlate_id.equals("0") &&  outlate_category_id.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_MEMOS + " where " + Tables.MEMOS_memo_date + ">=" + "'" + start_date + "'" + " and " + Tables.MEMOS_memo_date + " <=" + "'" + end_date + "' AND " + ORDER_outlet_id + "='" + outlate_id + "' ORDER BY _id DESC";
        else if (!outlate_id.equals("0") && !outlate_category_id.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_MEMOS + " as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id='"+outlate_category_id+"' and  " + Tables.MEMOS_memo_date + ">=" + "'" + start_date + "'" + " and " + Tables.MEMOS_memo_date + " <=" + "'" + end_date + "' AND ot." + ORDER_outlet_id + "='" + outlate_id + "' ORDER BY _id DESC";
        else if (outlate_id.equals("0") && !outlate_category_id.equalsIgnoreCase("0"))
            Query = "SELECT * FROM " + Tables.TABLE_NAME_MEMOS + " as ot INNER join outlets as o on ot.outlet_id=o.outlet_id  where o.outlet_category_id='"+outlate_category_id+"' and  " + Tables.MEMOS_memo_date + ">=" + "'" + start_date + "'" + " and " + Tables.MEMOS_memo_date + " <=" + "'" + end_date + "'  ORDER BY _id DESC";
        Log.e("DataView", Query);

        Cursor c = sqLiteDatabase.rawQuery(Query, null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {

                HashMap<String, String> map = new HashMap<>();

                for (int i = 2; i < MEMOS.length; i++) {

                    if (MEMOS[i].equalsIgnoreCase(ORDER_outlet_id)) {

                        map.put(MEMOS[i], c.getString(c.getColumnIndex(MEMOS[i])));
                        map.put(OUTLETS_OUTLET_NAME, getSingleFilter(TABLE_NAME_OUTLETS, c.getString(c.getColumnIndex(MEMOS[i])), OUTLETS_ID, OUTLETS_OUTLET_NAME));
                    } else {
                        map.put(MEMOS[i], c.getString(c.getColumnIndex(MEMOS[i])));
                        Log.e(MEMOS[i], c.getString(c.getColumnIndex(MEMOS[i])) + "");
                    }
                }

                list.add(map);

            } while (c.moveToNext());
        }


        return list;
    }

    private String getSingleFilter(String tableName, String value, String outletsId, String retunr_field) {
        open();
        Cursor cursor = sqLiteDatabase.rawQuery("select " + retunr_field + " from " + tableName + " where " + outletsId + "='" + value + "'", null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            return cursor.getString(0);

        }


        return "not exixt";
    }

    public void getOrderDetails() {
        open();


    }

    public void generateSingleOrder(String orderNo) {
        Log.e("order", orderNo);
        new generatePushJsonForSingle(orderNo).execute();
    }

    public void generateSingleTempMemo(String memoNo) {
        Log.e("order", memoNo);
        new generatePushJsonForSingleTempMemo(memoNo).execute();
    }

    public void generateSingleMemo(String memoNo) {
        Log.e("order", memoNo);
        new generatePushJsonForSingleMemo(memoNo).execute();
    }

    public void updateOrderStatus(String dist_order_no, String process_status) {
        open();
        sqLiteDatabase.execSQL("update " + TABLE_NAME_ORDER + " set " + Tables.ORDER_STATUS + " = '" + process_status + "' where " + ORDER_order_number + " ='" + dist_order_no + "'");
        close();
    }

    public long updateMarket(HashMap<String, String> updatedMarket, String marketId) {
        this.open();
        String id[] = {marketId};
        Date currentTime = Calendar.getInstance().getTime();
        ContentValues contentValues = new ContentValues();
        for (String i : updatedMarket.keySet()) {
            contentValues.put(i, updatedMarket.get(i));
        }
        //contentValues.put(Tables.OUT,String.valueOf(currentTime));
        long x = sqLiteDatabase.update(Tables.TABLE_NAME_MARKETS, contentValues, Tables.MARKETS_market_id + " =?", id);
        return x;
    }

    public String getPendingOrderCount() {

        open();
        Cursor c = sqLiteDatabase.rawQuery("SELECT count(_id) from ORDER_table WHERE is_Pushed='0'", null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            return c.getString(0);
        }
        return "0";
    }

    public void updatePushStatus() {
        Log.e("log", "update ORDER_table set is_pushed ='1'");
        excQuery("update ORDER_table set is_pushed ='1'");
    }

    public String getTotalCashOfCurrentDay() {

        open();
        Cursor c = sqLiteDatabase.rawQuery("SELECT sum(gross_value) FROM ORDER_table  where order_date>='" + getCurrentDate() + "' and order_date <='" + getCurrentDate() + "' ORDER BY _id DESC", null);

        Log.e("Query", "SELECT sum(gross_value) FROM ORDER_table  where order_date>='" + getCurrentDate() + "'  and order_date <='" + getCurrentDate() + "' ORDER BY _id DESC");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            return c.getString(0);

        }

        return "0";
    }

    public String getTotalOCofCurrentDay() {

        open();
        Cursor c = sqLiteDatabase.rawQuery("SELECT count(DISTINCT(outlet_id)) FROM ORDER_table  where order_date>='" + getCurrentDate() + "'  and order_date <='" + getCurrentDate() + "' ORDER BY _id DESC", null);
        Log.e("Query", "SELECT count(_id) FROM ORDER_table  where order_date>='" + getCurrentDate() + "'  and order_date <='" + getCurrentDate() + "' ORDER BY _id DESC");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            return c.getString(0);

        }

        return "0";
    }

    public ArrayList<HashMap<String, String>> getlastorders(String outlate_id,String memoNumber) {

        ArrayList<HashMap<String,String>> list = new ArrayList<>();
        open();

        Cursor c = sqLiteDatabase.rawQuery("Select "+ORDER_order_number+" , "+ORDER_order_date+" , "+ORDER_gross_value +" from "+TABLE_NAME_ORDER+" where status !='2' and outlet_id='"+outlate_id+"'",null);

        c.moveToFirst();

        if (c!=null && c.getCount()>0) {
            do {
                HashMap<String, String> map = new HashMap<>();


                map.put(ORDER_order_number, c.getString(c.getColumnIndex(ORDER_order_number)));
                map.put(ORDER_order_date, c.getString(c.getColumnIndex(ORDER_order_date)));
                map.put(ORDER_gross_value, c.getString(c.getColumnIndex(ORDER_gross_value)));
                map.put("is_memo", "0");


                list.add(map);

            } while (c.moveToNext());
        }

        Cursor c1 = sqLiteDatabase.rawQuery("Select "+MEMOS_memo_number+" , "+MEMOS_memo_date+" , "+MEMOS_gross_value +" from "+TABLE_NAME_MEMOS+" where memo_number ='"+memoNumber+"' and outlet_id='"+outlate_id+"'",null);
        c1.moveToFirst();
        Log.e("Query ","Select "+MEMOS_memo_number+" , "+MEMOS_memo_date+" , "+MEMOS_gross_value +" from "+TABLE_NAME_MEMOS+" where memo_number !='"+memoNumber+"' and outlet_id='"+outlate_id+"'"+ " "+c1.getCount());
        if (c1!=null && c1.getCount()>0){
            do {
                HashMap<String, String> map = new HashMap<>();


                map.put(ORDER_order_number, c1.getString(c1.getColumnIndex(MEMOS_memo_number)));
                map.put(ORDER_order_date, c1.getString(c1.getColumnIndex(MEMOS_memo_date)));
                map.put(ORDER_gross_value, c1.getString(c1.getColumnIndex(MEMOS_gross_value)));
                map.put("is_memo", "1");


                list.add(0,map);

            } while (c1.moveToNext());

        }






        return list;
    }

    public String getNextVisitday(String routeID) {
        String date=" ";
       Calendar calendar = Calendar.getInstance();
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
        int weekNumber;

        if (dayOfMonth <=7){
            weekNumber=1;
        }else if (dayOfMonth>7 && dayOfMonth<=14){
            weekNumber = 2;
        }else if (dayOfMonth>14 && dayOfMonth<=21){
            weekNumber = 3;
        }else {
            weekNumber = 4;
        }

        for (int i = weekNumber+1 ; i <=4 ; i++) {

            Cursor c = sqLiteDatabase.rawQuery("Select * from visit_list where route id ='" + routeID + "' and week_id!='" + i + "'", null);

            if (c != null && c.getCount() > 0) {

                for (int i1 =2 ; i1<=8 ; i1++ ){

                    if (c.getInt(i1)==1){

                        break;
                    }

                }

            }


        }





        return date;
    }

    public boolean getDuplicateOutlet(String outletName, String marketId) {
        this.open();

        Cursor c = sqLiteDatabase.rawQuery("select * from outlets where outlet_name='"+outletName+"' and market_id='"+marketId+"'",null);

        if (c!=null && c.getCount()>0){
            return true;

        }
        return false;
    }

    public HashMap<String, ArrayList<String>> getBonusCarttype() {
        HashMap<String, ArrayList<String>> bonus = new HashMap<>();
        ArrayList<String> bonus_id = new ArrayList<>();
        ArrayList<String> bonus_name = new ArrayList<>();

       Cursor c =  rawQuery("Select id,name from "+Tables.TABLE_NAME_Bonus_Card_type);
       c.moveToFirst();

       if (c!=null && c.getCount()>0){
           do{

               bonus_id.add(c.getString(0));
               bonus_name.add(c.getString(1));

           }while (c.moveToNext());


       }
        bonus.put("bonus_id",bonus_id);
        bonus.put("bonus_name",bonus_name);

        return bonus;

    }


    private class lastUpdated extends AsyncTask<String, String, String> {
        JSONObject jsonObject;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            jsonObject = new JSONObject();
        }

        @Override
        protected String doInBackground(String... strings) {

            try {
                for (int i = 0; i < Tables.All_TABLE_NAME.length; i++) {
                    JSONObject jsonObject1 = new JSONObject();
                    open();

                    Cursor c = sqLiteDatabase.rawQuery("select " + Tables.MESSAGE_UPDATED_AT + " from " + Tables.All_TABLE_NAME[i] + " order by _id desc limit 1", null);
                    c.moveToFirst();
                    if (c != null && c.getCount() > 0) {

                        Log.e(Tables.MESSAGE_UPDATED_AT, c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)) + " ");
                        if (c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)) == null) {

                            try {
                                jsonObject1.put("last_update", "1995-01-18 10:12:00");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        } else {

                            try {
                               // jsonObject1.put("last_update", c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)));
                                jsonObject1.put("last_update", "2020-12-14 15:28:19"); // only  for getting data
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                       // c.close();

                    } else {
                        try {
                            jsonObject1.put("last_update", "1995-01-18 10:12:00");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }


                   /* for (int j = 0; j < TABLE_NAME_ALL_DATA.length; j++) {

                        if (TABLE_NAME_ALL_DATA[j].equalsIgnoreCase(Tables.All_TABLE_NAME[i])) {
                            try {
                                Log.e("Table_name", Tables.All_TABLE_NAME[i]);
                                jsonObject1.put("last_update", "1995/01/18");
                                break;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                    }*/

                    try {
                        jsonObject.put(Tables.All_TABLE_NAME[i], jsonObject1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                try {
                    jsonObject.put("sales_person_id", basicFunction.getPreference(SR_ID));
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                dbListener.OnLocalDBdataRetrive(jsonObject.toString());
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                close();
            }

            return null;
        }
    }

    private class prepareDataForOrder extends AsyncTask<String, String, String> {

        String _OutletID;

        public prepareDataForOrder(String _OutletID) {
            this._OutletID = _OutletID;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Please Wait...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {


          //  Cursor c2 = rawQuery("SELECT P.product_id,P.product_name,P.product_category_id,P.product_type_id FROM product as P  group by P.product_id,P.product_name,P.product_category_id,P.product_type_id ORDER BY P.product_order ASC");
            Cursor c2 = rawQuery("SELECT P.product_id,P.product_name,P.product_category_id,P.product_type_id FROM product as P inner join product_price as pp on p.product_id==pp.product_id  where pp.product_id>0  group by P.product_id,P.product_name,P.product_category_id,P.product_type_id ORDER BY P.product_order ASC");
            if (c2 != null) {
                if (c2.moveToFirst()) {
                    do {

                        Log.e("EEEEEEEEEEEEEEEEEE", "-----------***********----------");


                        String product_id = c2.getString(c2.getColumnIndex("product_id"));
                        String product_name = c2.getString(c2.getColumnIndex("product_name"));

                        String product_category_id = c2.getString(c2.getColumnIndex("product_category_id"));
                        String product_type_id = c2.getString(c2.getColumnIndex("product_type_id"));


                        savePreference(product_id, "0.0");


                        String query = "SELECT * FROM " + Tables.TABLE_NAME_PRODUCT_BOOLEAN + " WHERE product_id='" + product_id + "' AND outlet_id='" + _OutletID + "'";
                        Log.e("ProductInsertCheckQuery", query);
                        Cursor c1 = rawQuery(query);
                        Log.e("lkog", c1.getCount() + "");
                        if (c1.getCount() == 0) {
                            HashMap<String, String> map = new HashMap<String, String>();

                            map.put("outlet_id", _OutletID);
                            map.put("product_id", product_id);
                            map.put("quantity", "0.00");
                            map.put("boolean", "false");
                            map.put("product_category_id", product_category_id);
                            map.put("product_type_id", product_type_id);

                            InsertTable(map, Tables.TABLE_NAME_PRODUCT_BOOLEAN);
                        }
                    } while (c2.moveToNext());
                }
            }

            progressDialog.dismiss();
            dbListener.OnLocalDBdataRetrive("done");


            return null;
        }
    }

    private class generatePushJson extends AsyncTask<String, String, String> {
        JSONObject finaljsonObject = new JSONObject();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Getting Ready For Push...");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            open();
            String[] operation = ORDER;

            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + operation[0] + " where " + Tables.ORDER_is_pushed + " ='0' and " + ORDER_STATUS + " !='" + PROCESSING_COMPELETE + "'", null);
            cursor.moveToFirst();
            Log.e("Query", "select * from " + operation[0] + " where " + Tables.ORDER_is_pushed + " ='0' and " + ORDER_STATUS + " !='" + PROCESSING_COMPELETE + "'");
            JSONArray jsonArray = new JSONArray();
            if (cursor != null && cursor.getCount() > 0) {
                do {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = gethelper(jsonObject, cursor.getString(cursor.getColumnIndex(MARKET_ID)));
                    for (int i = 1; i < operation.length; i++) {

                        try {
                            Log.e(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                            if (operation[i].equalsIgnoreCase("territory_id")) {

                                jsonObject.put(operation[i], "123456");
                            } else
                                jsonObject.put(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.getMessage());
                        }

                    }

                    try {
                        jsonObject.put(ADVANCE_COLLECTION_paid_amount,getAdvanceCollection(Tables.ORDER_order_number));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String[] operationin = ORDER_DETAILS;
                    Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + operationin[0] + " where " + Tables.ORDER_order_number + " ='" + cursor.getString(cursor.getColumnIndex(operation[2])) + "'", null);
                    cursorin.moveToFirst();

                    JSONArray jsonArrayin = new JSONArray();
                    if (cursorin != null && cursorin.getCount() > 0) {
                        do {
                            JSONObject jsonObjectin = new JSONObject();
                            for (int i = 2; i < operationin.length - 2; i++) {

                                try {
                                    Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    if (operationin[i].equalsIgnoreCase("territory_id")) {

                                        jsonObjectin.put(operationin[i], "123456");
                                    } else {
                                        if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                            jsonObjectin.put(operationin[i], 0);
                                        else
                                            jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e("json", e.getMessage());
                                }

                            }


                            jsonArrayin.put(jsonObjectin);


                        } while (cursorin.moveToNext());


                    }

                    try {
                        jsonObject.put("order_details", jsonArrayin);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    jsonArray.put(jsonObject);
                } while (cursor.moveToNext());


            }

            try {
                finaljsonObject.put("order_list", jsonArray);
                finaljsonObject.put("mac", basicFunction.getPreference("mac"));
                finaljsonObject.put(Tables.SR_ID, basicFunction.getPreference(Tables.SR_ID));
                //finaljsonObject = getGiftIssue(finaljsonObject);

                progressDialog.dismiss();
                dbListener.OnLocalDBdataRetrive(finaljsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("log", e.getMessage());
            }

            return null;
        }
    }



    private String getAdvanceCollection(String order_order_number) {

        String advance="0";
        this.open();
        Cursor x = sqLiteDatabase.rawQuery("select paid_amount from "+TABLE_NAME_ADVANCE_COLLECTION+" where order_number='"+order_order_number+"'",null);
        x.moveToFirst();
        if (x!=null & x.getCount()>0){
            advance = x.getString(0);
        }
        return advance;
    }

    private class generatePushJsonForSingle extends AsyncTask<String, String, String> {
        JSONObject finaljsonObject = new JSONObject();
        String OrderNO;

        public generatePushJsonForSingle(String orderNO) {
            OrderNO = orderNO;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Getting Ready For Push...");
                 //   progressDialog.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            open();
            String[] operation = ORDER;

            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + operation[0] + " where  " + ORDER_order_number + " = '" + OrderNO + "'", null);

            Log.e("generatePushJsonForSe", "select * from " + operation[0] + " where " + Tables.ORDER_is_pushed + " ='1' and " + ORDER_order_number + " = '" + OrderNO + "'");
            cursor.moveToFirst();

            JSONArray jsonArray = new JSONArray();
            if (cursor != null && cursor.getCount() > 0) {
                do {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = gethelper(jsonObject, cursor.getString(cursor.getColumnIndex(MARKET_ID)));
                    for (int i = 1; i < operation.length; i++) {

                        try {
                            Log.e(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                            if (operation[i].equalsIgnoreCase("territory_id")) {

                                jsonObject.put(operation[i], "123456");
                            } else
                                jsonObject.put(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.getMessage());
                        }

                    }
                    close();

                    String[] operationin = ORDER_DETAILS;
                    open();

                    Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + operationin[0] + " where " + Tables.ORDER_order_number + " ='" + cursor.getString(cursor.getColumnIndex(operation[2])) + "'", null);
                    cursorin.moveToFirst();

                    JSONArray jsonArrayin = new JSONArray();
                    if (cursorin != null && cursorin.getCount() > 0) {
                        do {
                            JSONObject jsonObjectin = new JSONObject();
                            for (int i = 2; i < operationin.length - 2; i++) {

                                try {
                                    Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    if (operationin[i].equalsIgnoreCase("territory_id")) {

                                        jsonObjectin.put(operationin[i], "123456");
                                    } else {
                                        if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                            jsonObjectin.put(operationin[i], 0);
                                        else
                                            jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e("json", e.getMessage());
                                }

                            }


                            jsonArrayin.put(jsonObjectin);


                        } while (cursorin.moveToNext());


                    }

                    try {
                        jsonObject.put("order_details", jsonArrayin);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    jsonArray.put(jsonObject);
                } while (cursor.moveToNext());


            }

            try {
                finaljsonObject.put("order_list", jsonArray);
                finaljsonObject.put("mac", basicFunction.getPreference("mac"));
                finaljsonObject.put(Tables.SR_ID, basicFunction.getPreference(Tables.SR_ID));
                //finaljsonObject = getGiftIssue(finaljsonObject);

                progressDialog.dismiss();
                dbListener.OnLocalDBdataRetrive(finaljsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("log", e.getMessage());
            }

            close();

            return null;
        }
    }

    private class generatePushJsonForSingleTempMemo extends AsyncTask<String, String, String> {
        JSONObject finaljsonObject = new JSONObject();
        String OrderNO;


        public generatePushJsonForSingleTempMemo(String orderNO) {
            OrderNO = orderNO;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Getting Ready For Push...");
                    progressDialog.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            open();
            String[] operation = MEMOS;

            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + "temp_memos" + " where  " + MEMOS_memo_number + " = '" + OrderNO + "'", null);

            Log.e("PushJsonForSingleMemo", "select * from " + "temp_memos" + " where " + Tables.ORDER_is_pushed + " ='1' and " + MEMOS_memo_number + " = '" + OrderNO + "'");
            cursor.moveToFirst();

            JSONArray jsonArray = new JSONArray();
            if (cursor != null && cursor.getCount() > 0) {
                do {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = gethelper(jsonObject, cursor.getString(cursor.getColumnIndex(MARKET_ID)));
                    for (int i = 1; i < operation.length; i++) {

                        try {
                            Log.e(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                            if (operation[i].equalsIgnoreCase("territory_id")) {

                                jsonObject.put(operation[i], "123456");
                            } else
                                jsonObject.put(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.getMessage());
                        }

                    }

                    String[] operationin = MEMO_DETAILS;
                    open();

                    Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + "temp_memo_details" + " where " + Tables.MEMOS_memo_number + " ='" + cursor.getString(cursor.getColumnIndex(operation[2])) + "'", null);
                    cursorin.moveToFirst();

                    JSONArray jsonArrayin = new JSONArray();
                    if (cursorin != null && cursorin.getCount() > 0) {
                        do {
                            JSONObject jsonObjectin = new JSONObject();
                            for (int i = 2; i < operationin.length - 2; i++) {

                                try {
                                    Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    if (operationin[i].equalsIgnoreCase("territory_id")) {

                                        jsonObjectin.put(operationin[i], "123456");
                                    } else {
                                        if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                            jsonObjectin.put(operationin[i], 0);
                                        else
                                            jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e("json", e.getMessage());
                                }

                            }


                            jsonArrayin.put(jsonObjectin);


                        } while (cursorin.moveToNext());


                    }



                    try {
                        jsonObject.put("memo_details", jsonArrayin);
                        jsonObject.put("payment_list",getPayment(OrderNO));
                       // jsonObject.put("advance_collection",getAdvanceCollectionList(cursor.getString(cursor.getColumnIndex(ORDER_order_number)),OrderNO));
                        jsonObject.put("credit_collection",getCreditCollection(OrderNO));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    jsonArray.put(jsonObject);
                } while (cursor.moveToNext());


            }



            try {
                finaljsonObject.put("memo_list", jsonArray);
                finaljsonObject.put("mac", basicFunction.getPreference("mac"));
                finaljsonObject.put(Tables.SR_ID, basicFunction.getPreference(Tables.SR_ID));
               // finaljsonObject = getGiftIssue(finaljsonObject);

                progressDialog.dismiss();
                dbListener.OnLocalDBdataRetrive(finaljsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("log", e.getMessage());
            }

            close();

            return null;


        }

    }

    private JSONArray getPayment(String orderNO) {
        String json = "";
        String[] operationin = PAYMENTS;
        open();
        Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + "temp_payments" + " where " + " memo_no ='" + orderNO + "'", null);
        cursorin.moveToFirst();

        JSONArray jsonArrayin = new JSONArray();
        if (cursorin != null && cursorin.getCount() > 0) {
            do {
                JSONObject jsonObjectin = new JSONObject();
                for (int i = 2; i < operationin.length - 2; i++) {

                    try {
                        Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        if (operationin[i].equalsIgnoreCase("territory_id")) {

                            jsonObjectin.put(operationin[i], "123456");
                        } else {
                            if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                jsonObjectin.put(operationin[i], 0);
                            else
                                jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }

                try {
                    jsonObjectin.put("so_id",getPreference(SR_ID));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                jsonArrayin.put(jsonObjectin);


            } while (cursorin.moveToNext());


        }

        close();

       return  jsonArrayin;

    }



    private JSONArray getCreditCollection(String orderNO) {
        String json = "";
        String[] operationin = CREDIT_COLLECTION;
        open();
        Cursor cursorin = sqLiteDatabase.rawQuery("select * from " +"temp_credit_collection" + " where " + Tables.MEMOS_memo_number + " ='" + orderNO + "'", null);
        cursorin.moveToFirst();

        JSONArray jsonArrayin = new JSONArray();
        if (cursorin != null && cursorin.getCount() > 0) {
            do {
                JSONObject jsonObjectin = new JSONObject();
                for (int i = 2; i < operationin.length - 2; i++) {

                    try {
                        Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        if (operationin[i].equalsIgnoreCase("territory_id")) {

                            jsonObjectin.put(operationin[i], "123456");
                        } else {
                            if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                jsonObjectin.put(operationin[i], 0);
                            else
                                jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }


                jsonArrayin.put(jsonObjectin);


            } while (cursorin.moveToNext());


        }

        close();
        return  jsonArrayin;
    }
    private JSONArray getAdvanceCollectionList(String orderNO,String Memo_no) throws JSONException {
        String json = "";
        String[] operationin = ADVANCE_COLLECTION;
        open();
        Cursor cursorin = sqLiteDatabase.rawQuery("select * from " +"advance_collection" + " where " + ORDER_order_number + " ='" + orderNO + "'", null);
        cursorin.moveToFirst();

        JSONArray jsonArrayin = new JSONArray();
        if (cursorin != null && cursorin.getCount() > 0) {
            do {
                JSONObject jsonObjectin = new JSONObject();
                for (int i = 2; i < operationin.length - 2; i++) {

                    try {
                        Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        if (operationin[i].equalsIgnoreCase("territory_id")) {

                            jsonObjectin.put(operationin[i], "123456");
                        } else {
                            if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                jsonObjectin.put(operationin[i], 0);
                            else
                                jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }
                jsonObjectin.put("Memo_no",Memo_no);




                jsonArrayin.put(jsonObjectin);


            } while (cursorin.moveToNext());


        }

        close();
        return  jsonArrayin;
    }


    private class generatePushJsonForSingleMemo extends AsyncTask<String, String, String> {
        JSONObject finaljsonObject = new JSONObject();
        String OrderNO;


        public generatePushJsonForSingleMemo(String orderNO) {
            OrderNO = orderNO;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            runOnUiThread(new Runnable() {
                public void run() {
                    progressDialog = new ProgressDialog(context);
                    progressDialog.setTitle("Getting Ready For Push...");
                    progressDialog.show();
                }
            });

        }

        @Override
        protected String doInBackground(String... strings) {
            open();
            String[] operation = MEMOS;

            Cursor cursor = sqLiteDatabase.rawQuery("select * from " + "memos" + " where  " + MEMOS_memo_number + " = '" + OrderNO + "'", null);

            Log.e("generatePushJsonForSingleMemo", "select * from " + "memos" + " where " + Tables.ORDER_is_pushed + " ='1' and " + MEMOS_memo_number + " = '" + OrderNO + "'");
            cursor.moveToFirst();

            JSONArray jsonArray = new JSONArray();
            if (cursor != null && cursor.getCount() > 0) {
                do {

                    JSONObject jsonObject = new JSONObject();
                    jsonObject = gethelper(jsonObject, cursor.getString(cursor.getColumnIndex(MARKET_ID)));
                    for (int i = 1; i < operation.length; i++) {

                        try {
                            Log.e(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                            if (operation[i].equalsIgnoreCase("territory_id")) {

                                jsonObject.put(operation[i], "123456");
                            } else
                                jsonObject.put(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("json", e.getMessage());
                        }

                    }
                    String[] operationin = MEMO_DETAILS;
                    open();
                    Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + "memo_details" + " where " + Tables.MEMOS_memo_number + " ='" + cursor.getString(cursor.getColumnIndex(operation[2])) + "'", null);
                    cursorin.moveToFirst();

                    JSONArray jsonArrayin = new JSONArray();
                    if (cursorin != null && cursorin.getCount() > 0) {
                        do {
                            JSONObject jsonObjectin = new JSONObject();
                            for (int i = 2; i < operationin.length - 2; i++) {

                                try {
                                    Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    if (operationin[i].equalsIgnoreCase("territory_id")) {

                                        jsonObjectin.put(operationin[i], "123456");
                                    } else {
                                        if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                            jsonObjectin.put(operationin[i], 0);
                                        else
                                            jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    Log.e("json", e.getMessage());
                                }

                            }


                            jsonArrayin.put(jsonObjectin);


                        } while (cursorin.moveToNext());


                    }

                    close();

                    try {
                        jsonObject.put("memo_details", jsonArrayin);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    try {
                        jsonObject.put("memo_details", jsonArrayin);
                        jsonObject.put("payment_list",getPaymentForDalivarNow(OrderNO));
                        // jsonObject.put("advance_collection",getAdvanceCollectionList(cursor.getString(cursor.getColumnIndex(ORDER_order_number)),OrderNO));
                        jsonObject.put("credit_collection",getCreditCollectionForDalivarNow(OrderNO));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    jsonArray.put(jsonObject);
                } while (cursor.moveToNext());


            }

            try {
                finaljsonObject.put("memo_list", jsonArray);
                finaljsonObject.put("mac", basicFunction.getPreference("mac"));
                finaljsonObject.put(Tables.SR_ID, basicFunction.getPreference(Tables.SR_ID));
               // finaljsonObject = getGiftIssue(finaljsonObject);

                progressDialog.dismiss();
                dbListener.OnLocalDBdataRetrive(finaljsonObject.toString());

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("log", e.getMessage());
            }

            close();

            return null;
        }
    }



    private JSONArray getPaymentForDalivarNow(String orderNO) {
        open();
        String json = "";
        String[] operationin = PAYMENTS;
        Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + "payments" + " where " + " memo_no ='" + orderNO + "'", null);
        cursorin.moveToFirst();

        JSONArray jsonArrayin = new JSONArray();
        if (cursorin != null && cursorin.getCount() > 0) {
            do {
                JSONObject jsonObjectin = new JSONObject();
                for (int i = 2; i < operationin.length - 2; i++) {

                    try {
                        Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        if (operationin[i].equalsIgnoreCase("territory_id")) {

                            jsonObjectin.put(operationin[i], "123456");
                        } else {
                            if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                jsonObjectin.put(operationin[i], 0);
                            else
                                jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }

                try {
                    jsonObjectin.put("so_id",getPreference(SR_ID));
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                jsonArrayin.put(jsonObjectin);


            } while (cursorin.moveToNext());


        }
        close();

        return  jsonArrayin;

    }



    private JSONArray getCreditCollectionForDalivarNow(String orderNO) {
        open();
        String json = "";
        String[] operationin = CREDIT_COLLECTION;
        Cursor cursorin = sqLiteDatabase.rawQuery("select * from " +"credit_collection" + " where " + Tables.MEMOS_memo_number + " ='" + orderNO + "'", null);
        cursorin.moveToFirst();

        JSONArray jsonArrayin = new JSONArray();
        if (cursorin != null && cursorin.getCount() > 0) {
            do {
                JSONObject jsonObjectin = new JSONObject();
                for (int i = 2; i < operationin.length - 2; i++) {

                    try {
                        Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        if (operationin[i].equalsIgnoreCase("territory_id")) {

                            jsonObjectin.put(operationin[i], "123456");
                        } else {
                            if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                jsonObjectin.put(operationin[i], 0);
                            else
                                jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }


                jsonArrayin.put(jsonObjectin);


            } while (cursorin.moveToNext());


        }

        close();

        return  jsonArrayin;
    }

    private JSONObject getGiftIssue(JSONObject finaljsonObject) {


        String[] operation = GIFT_ISSUE;

        Cursor cursor = sqLiteDatabase.rawQuery("select * from " + operation[0] + " where " + Tables.ORDER_is_pushed + " ='0'", null);
        cursor.moveToFirst();

        JSONArray jsonArray = new JSONArray();
        if (cursor != null && cursor.getCount() > 0) {
            do {

                JSONObject jsonObject = new JSONObject();
                //  jsonObject = gethelper(jsonObject,cursor.getString(cursor.getColumnIndex(MARKET_ID)));
                for (int i = 1; i < operation.length; i++) {

                    try {
                        Log.e(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                        if (operation[i].equalsIgnoreCase("territory_id")) {

                            jsonObject.put(operation[i], "123456");
                        } else
                            jsonObject.put(operation[i], cursor.getString(cursor.getColumnIndex(operation[i])) + "");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Log.e("json", e.getMessage());
                    }

                }
                String[] operationin = GIFT_ISSUE_DETAILS;
                Cursor cursorin = sqLiteDatabase.rawQuery("select * from " + operationin[0] + " where " + Tables.GIFT_ISSUE_DETAILS_gift_issue_id + " ='" + cursor.getString(cursor.getColumnIndex(operation[6])) + "'", null);
                cursorin.moveToFirst();

                JSONArray jsonArrayin = new JSONArray();
                if (cursorin != null && cursorin.getCount() > 0) {
                    do {
                        JSONObject jsonObjectin = new JSONObject();
                        for (int i = 2; i < operationin.length - 2; i++) {

                            try {
                                Log.e(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                if (operationin[i].equalsIgnoreCase("territory_id")) {

                                    jsonObjectin.put(operationin[i], "123456");
                                } else {
                                    if (cursorin.getString(cursorin.getColumnIndex(operationin[i])) == null)
                                        jsonObjectin.put(operationin[i], 0);
                                    else
                                        jsonObjectin.put(operationin[i], cursorin.getString(cursorin.getColumnIndex(operationin[i])) + "");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                Log.e("json", e.getMessage());
                            }

                        }


                        jsonArrayin.put(jsonObjectin);


                    } while (cursorin.moveToNext());


                }

                try {
                    jsonObject.put(operationin[0], jsonArrayin);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                jsonArray.put(jsonObject);
            } while (cursor.moveToNext());


        }

        try {
            finaljsonObject.put(operation[0], jsonArray);


            // progressDialog.dismiss();


        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("log", e.getMessage());
        }


        return finaljsonObject;
    }

    private JSONObject gethelper(JSONObject jsonObject, String market_id) {
        open();
        Cursor c = sqLiteDatabase.rawQuery("Select thana_id , route_id from markets where market_id='" + market_id + "'", null);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {


            try {
                jsonObject.put("thana_id", c.getString(0));
                jsonObject.put("route_id", c.getString(1));
            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        close();

        return jsonObject;

    }

    private class Requireddata extends AsyncTask<String, String, String> {
        JSONObject finaljsonObject = new JSONObject();

        @Override
        protected String doInBackground(String... strings) {
            open();
            try {


                for (int i = 0; i < Tables.Allfild.length; i++) {

                    JSONArray jsonArray = new JSONArray();
                    String query = "select * from " + Tables.Allfild[i][0];

                    Cursor cursor = sqLiteDatabase.rawQuery(query, null);
                    Log.e("data", query + " size" + cursor.getCount());
                    if (cursor != null && cursor.getCount() > 0) {

                        do {

                            JSONObject jsonObject = new JSONObject();
                            for (int j = 2; j < Tables.Allfild[i].length - 2; j++) {

                                if (Tables.Allfild[i][j].equalsIgnoreCase(Tables.MEMOS_is_pushed) || Tables.Allfild[i][j].equalsIgnoreCase("isPushed"))
                                    continue;

                                //    Log.e("error", Tables.Allfild[i][j] + " table name" + Tables.Allfild[i][0] + " j= " + j + " length" + (Tables.Allfild[i].length - 2));
                                jsonObject.put(Tables.Allfild[i][j], Tables.Allfild[i][j]);

                            }

                            jsonArray.put(jsonObject);


                        } while (cursor.moveToNext());


                    } else {
                        JSONObject jsonObject = new JSONObject();
                        for (int j = 2; j < (Tables.Allfild[i].length) - 2; j++) {
                            if (Tables.Allfild[i][j].equalsIgnoreCase(Tables.MEMOS_is_pushed) || Tables.Allfild[i][j].equalsIgnoreCase("isPushed"))
                                continue;
                            Log.e("error1", Tables.Allfild[i][j] + " table name" + Tables.Allfild[i][0] + " j= " + j + " length" + (Tables.Allfild[i].length - 2));
                            jsonObject.put(Tables.Allfild[i][j], "Demo_velue");
                        }

                        jsonArray.put(jsonObject);
                    }
                    finaljsonObject.put(Tables.Allfild[i][0], jsonArray);
                }

                finaljsonObject.put("seals_person_id", TempData.sales_person_id);

            } catch (JSONException e) {

            }

            dbListener.OnLocalDBdataRetrive(finaljsonObject.toString());
            // insertData(finaljsonObject.toString());

            return null;
        }
    }

    private class InsertData extends AsyncTask<String, String, String> {

        String json;
        int code;

        public InsertData(String json) {
            this.json = json;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle("Please Wait...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                JSONObject jsonObject = new JSONObject(json);

                updateWithServer(jsonObject);
               // updateMemoWithServer(jsonObject);

                for (int i = 0; i < Allfild.length; i++) {

                        Log.e("Logfile",Allfild[i][0]+" sda"+jsonObject.has(Allfild[i][0]));


                    if (jsonObject.has(Allfild[i][0])) {

                        JSONArray jsonArray = jsonObject.getJSONArray(Allfild[i][0]);

                        for (int t = 0; t < jsonArray.length(); t++) {

                            JSONObject jsonObjecttemp = jsonArray.getJSONObject(t);

                            ContentValues cv = new ContentValues();
                            for (int j = 2; j < Allfild[i].length; j++) {

                                Log.e("cvprint","cvprint");
                                //Log.e("table",Allfild[i][0]);

                                if (Allfild[i][j].equalsIgnoreCase(Tables.MESSAGE_ISPUSHED) || Allfild[i][j].equalsIgnoreCase(Tables.NCP_RETURN_ISPUSHED)) {
                                    cv.put(Allfild[i][j], "1");
                                    //Log.e("Check",Allfild[i][j]);

                                } else if (Allfild[i][j].equalsIgnoreCase(Tables.NCP_RETURN_CREATE_AT)) {
                                    cv.put(Allfild[i][j], basicFunction.getCurrentDateTime());

                                } else if (Allfild[i][j].equalsIgnoreCase(Tables.UNIT_UPDATED_AT)) {
                                    cv.put(Allfild[i][j], basicFunction.getCurrentDateTime());

                                } else if (Allfild[i][j].equalsIgnoreCase("is_used")) {
                                    cv.put(Allfild[i][j],"0");

                                }else {
                                    if (Allfild[i][j].equalsIgnoreCase("plan_id"))
                                        Log.e("plan_id",Allfild[i][j]+"  "+jsonObjecttemp.toString());

                                    if (!jsonObjecttemp.has(Allfild[i][j])) {
                                        continue;
                                    }
                                    cv.put(Allfild[i][j], jsonObjecttemp.getString(Allfild[i][j]));

                                }


                            }

                            if (Allfild[i][0].equalsIgnoreCase(TABLE_NAME_THANA)||Allfild[i][0].equalsIgnoreCase(TABLE_NAME_OUTLETS)||Allfild[i][0].equalsIgnoreCase(TABLE_NAME_ROOT)||Allfild[i][0].equalsIgnoreCase(TABLE_NAME_MARKETS)){

                               //excQuery("delete  from "+Allfild[i][0]+" where "+Allfild[i][2]+"='"+jsonObjecttemp.getString(Allfild[i][2])+"'"); // Why it is use I dont know
                            }

                            if (Allfild[i][0].equalsIgnoreCase(TABLE_NAME_MEMOS)){
                                Log.e("Memo_number",jsonObjecttemp.getString(Allfild[i][2]));
                            }

                            open();
                            sqLiteDatabase.insert(Allfild[i][0], null, cv);
                            Log.e("tabledata", Allfild[i][0] + "  " + cv.toString());
                            close();
                        }

                    }
                }
                progressDialog.dismiss();
                dbListener.OnLocalDBdataRetrive("done");
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("JsonException", e.getMessage());
            }


            return null;
        }
    }


    public void savePreference(String key, String value) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key) {
        String value = "";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "0");

        return value;

    }

    /* Bondhon Code Begaing*/


    public String getRouteName(String routeId) {
        this.open();
        String routeName = null;
        String query = "SELECT " + Tables.ROUTE_NAME + " FROM " + Tables.TABLE_NAME_ROOT + " WHERE " + Tables.ROUTE_ID + " = " + routeId + " ";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {
            routeName = cursor.getString(cursor.getColumnIndex(Tables.ROUTE_NAME));
        }
        return routeName;
    }



    //dist_data_pull.json creating------------------------------------------------------------------
    public JsonObject getGPSJson(String sales_id){
        JsonObject primaryData = new JsonObject();
        try {
            primaryData.addProperty("mac","null");
            primaryData.addProperty("sales_person_id", sales_id);
            primaryData.addProperty("last_update","all");
        } catch (JsonIOException e) {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
        return primaryData;
    }


    //dist_data_pull.json creating------------------------------------------------------------------
    public JsonObject getDistributorFullData(String sales_id){
        JsonObject jsonObject= null;
        try {
            jsonObject = new JsonObject();


            sqLiteDatabase = dBhelper.getWritableDatabase();

            for (int i = 0; i < Tables.All_TABLE_NAME.length; i++) {

                JsonObject jsonObject1 = new JsonObject();

                Cursor c = sqLiteDatabase.rawQuery("select " + Tables.MESSAGE_UPDATED_AT + " from " + Tables.All_TABLE_NAME[i] + " order by _id desc limit 1", null);
                c.moveToFirst();
                if (c != null && c.getCount() > 0) {

                    Log.e(Tables.MESSAGE_UPDATED_AT, c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)) + " ");
                    if (c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)) == null) {

                        try {
                            jsonObject1.addProperty("last_update", "1995-01-18 10:12:00");
                        } catch (JsonIOException e) {
                            e.printStackTrace();
                        }


                    } else {


                        try {
                            jsonObject1.addProperty("last_update", c.getString(c.getColumnIndex(Tables.MESSAGE_UPDATED_AT)));
                        } catch (JsonIOException e) {
                            e.printStackTrace();
                        }
                    }

                } else {
                    try {
                        jsonObject1.addProperty("last_update", "1995-01-18 10:12:00");
                    } catch (JsonIOException e) {
                        e.printStackTrace();
                    }
                }

                try {
                    jsonObject.addProperty(Tables.All_TABLE_NAME[i], String.valueOf(jsonObject1));
                } catch (JsonIOException e) {
                    e.printStackTrace();
                }

            }

            try {
                jsonObject.addProperty(Tables.SR_ID, sales_id);
                Log.e("sales_person_from_db", sales_id);
            } catch (JsonIOException e) {
                e.printStackTrace();
            }

            Log.e("getdatajson", jsonObject.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        sqLiteDatabase.close();

        return jsonObject;
    }

}
