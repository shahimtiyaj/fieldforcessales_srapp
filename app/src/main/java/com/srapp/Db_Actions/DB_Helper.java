package com.srapp.Db_Actions;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import androidx.annotation.Nullable;

import static com.srapp.Db_Actions.Tables.CREATE_BONUS_PARTY_TABLE;

public class DB_Helper extends SQLiteOpenHelper {

    int oldv, newv;

    public DB_Helper(Context context) {
        super(context, Tables.DATABASE_NAME, null, Tables.DATABASE_VIRSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {



        sqLiteDatabase.execSQL(Tables.CREATE_BANK_BRANCH_TABLE);
        sqLiteDatabase.execSQL(CREATE_BONUS_PARTY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_BONUSES_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_CREDIT_COLLECTION_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_CURRENT_PROMOTION_PRODUCTS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DELETE_MEMO_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DELETED_MEMOS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DEPOSIT_BALANCE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DEPOSIT_DELETE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DEPOSITS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DESIGNATION_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DISTRIBUTOR_PRODUCT_PRICES_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DISTRIBUTOR_PRODUCT_COMBINATIONS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_GENERAL_NOTICE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_GIFT_ISSUE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_GIFT_ISSUE_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_GPS_TRACKER_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_GPS_TRACKING_TIME_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_INSTRUMENT_NUMBER_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_INSTRUMENT_TYPE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_LOCATION_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MARKETS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MATERIALS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MEMO_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MEMOS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MEMOS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MONTHLY_EFFECTIVE_CALL_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_NCP_CHALLAN_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_NCP_CHALLAN_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_NCP_RETURNS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_STOCKS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_NGO_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_MESSAGES_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_OUTLET_CATEGORY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_OUTLET_VISIT_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_OUTLET_DALETE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_OUTLETS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PAYMENTS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_BOOLEAN_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODCUCT_CATEGORY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_COMBINATIONS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_HISTORY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_PRICE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_TYPE_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_SALES_WEEKS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_SO_TARGETS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_TERRITORY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_THANA_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_THANA_TERRITORY_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_UNIT_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_ROOT_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_VISIT_PLAN_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_ORDER_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_ORDER_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_VISIT_PLAN_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_DISCOUNT_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_TEMP_MEMOS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_TEMP_MEMO_DETAILS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_ADVANCE_COLLECTION_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_TEMP_PAYMENTS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_TEMP_CREDIT_COLLECTION_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_PRODUCT_SERIALS_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_Bonus_Card_type_updated_at_TABLE);
        sqLiteDatabase.execSQL(Tables.CREATE_FISCAL_YEAR);

        Log.e("DB Create test", "onCreate:  created db" );

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        oldv = i;
        newv = i1;
        Log.e("versionDB","N="+newv+" old="+oldv);


        if (i == 1 && i1 == 2) {
//            sqLiteDatabase.execSQL(Tables.DROP_TABLE + TABLE_NAME_CLASS);
//            sqLiteDatabase.execSQL(Tables.DROP_TABLE + Tables.TABLE_NAME_TEACHER);
            onCreate(sqLiteDatabase);
            return;
        }


        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_BANK_BRANCH);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_BONUS_PARTY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_BONUSES);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_CREDIT_COLLECTION);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_CURRENT_PROMOTION_PRODUCTS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DELETE_MEMO_DETAILS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DELETED_MEMOS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DEPOSIT_BALANCE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DEPOSIT_DELETE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DEPOSITS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DESIGNATION);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DISTRIBUTOR_PRODUCT_PRICES);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DISTRIBUTOR_PRODUCT_COMBINATIONS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_GENERAL_NOTICE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_GIFT_ISSUE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_GIFT_ISSUE_DETAILS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_GPS_TRACKER);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_GPS_TRACKING_TIME);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_INSTRUMENT_NUMBER);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_INSTRUMENT_TYPE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_LOCATION);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MARKETS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MATERIALS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MEMO_DETAILS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MEMOS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MONTHLY_EFFECTIVE_CALL);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_NCP_CHALLAN);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_NCP_CHALLAN_DETAILS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_NCP_RETURN);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_STOCKS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_NGO);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_MESSAGE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_OUTLET_CATEGORY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_OUTLET_VISIT);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_OUTLET_DELETE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_OUTLETS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PAYMENTS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_BOOLEAN);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_CATEGORY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_COMBINATION);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_HISTORY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_PRICE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODCUT_TYPE);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_SALES_WEEK);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_SO_TARGETS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_TERRITORY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_THANA);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_THANA_TERRITORY);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_UNIT);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_ROOT);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_VISIT_PLAN);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_ORDER_DETAILS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_ORDER);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_DISCOUNT);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_TEMP_PAYMENTS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_TEMP_PAYMENTS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_PRODUCT_SERIALS);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_Bonus_Card_type);
        sqLiteDatabase.execSQL(Tables.DROP_TABLE+ Tables.TABLE_NAME_FISCAL_YEAR);

        onCreate(sqLiteDatabase);

    }
}
