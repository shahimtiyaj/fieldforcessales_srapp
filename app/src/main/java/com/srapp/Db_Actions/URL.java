/*
package com.srapp.Db_Actions;

public class URL {


    // public static final String Domain = "http://192.168.10.116/order_basis_eslaes/api_data_pssr_retrives/";
    public static final String Domain = "http://202.126.123.157/field-force/api_data_pssr_retrives/";

    //public static final String Domain = "http://192.168.10.116/smc_sales/api_data_pssr_retrives/";
    // public static final String Domain = "http://192.168.137.6/smc_sales/api_data_pssr_retrives/";
    //  public static final String Domain = "http://182.160.103.236:8079/dms_arena/api_data_pssr_retrives/"; //live //not used
//  public static final String Domain = "http://182.160.103.236:8079/dms/api_data_pssr_retrives/"; //live
    //  public static final String Domain = "http://192.168.10.116/order_basis_eslaes/api_data_pssr_retrives/"; //live
    //public static final String Domain = "http://192.168.10.116/field-force/api_data_pssr_retrives/"; //live


    public static final String Login = Domain + "pssr_user_login.json";
    public static final String Push = Domain + "create_outlet.json";
    public static final String PULL = Domain + "pssr_data_pull.json";
    public static final String Log = "http://192.168.10.74/pushdata.php";
    public static final String GET_DEPOSITS = Domain+"pssr_get_collection_for_deposit_list.json";
    public static final String SUBMIT_DEPOSITS = Domain+"pssr_create_deposit.json";
    public static final String GET_DEPOSIT_LIST = Domain+"pssr_get_deposit_list.json";
    public static final String EDIT_DEPOSIT = Domain+"pssr_edit_deposit.json";
    public static final String SUBMIT_CREDIT_COLLECTION = Domain + "pssr_update_credit_collection.json";
    public static final String SUBMIT_PAYMENT_COLLECTION = Domain + "pssr_create_payment.json";
    public static final String CREATE_MEMO = Domain +"pssr_create_memo.json";
    public static final String ORDERPUSH = Domain + "pssr_create_order.json";
    public static final String ORDERS_FOR_PROCESS = Domain + "pssr_order_ready_for_process_list.json";
    public static final String ORDERS_FOR_UNPROCESS = Domain + "pssr_process_order_list.json";
    public static final String VERSION = "1.1.3";
    public static final String VERSION_txt = "Version("+VERSION+")";
    public static final String GET_LAST_RECORD = Domain + "pssr_last_orders.json";
    public static final String GET_LAST_MEMO = Domain + "pssr_last_memos.json";
    public static final String GET_ORDER_DETAILS = Domain + "pssr_order_details.json";
    public static final String PROCESS_ORDER_LIST = Domain + "pssr_order_delivery_schedule.json";
    public static final String UNPROCESS_ORDER_LIST = Domain + "pssr_order_delivery_unprocess.json";
    public static final String ORDER_DETAILS = Domain + "pssr_schedule_order_details.json";
    public static final String OutletDetails = Domain + "pssr_outlet_details.json";
    public static final String CreateOutlet = Domain + "pssr_create_outlet.json";
    public static final String OutletList = Domain + "pssr_outlet_list.json";
    public static final String MarketDetails = Domain + "pssr_market_details.json";
    public static final String CreateMarket = Domain + "pssr_create_market.json";
    public static final String MarketList= Domain +"pssr_market_list.json";
    public static final String VisitPlan= Domain +"pssr_visit_plan_list.json";
    public static final String CANCEL_ORDER= Domain +"pssr_order_cancel.json";
    public static final String CANCEL_ORDER_LIST= Domain +"pssr_order_cancel_list.json";
    public static final String PROCESS_ORDER_LIST_FOR_MEMO= Domain +"pssr_process_order_list_for_delivery.json";
    public static final String CHANGE_PASSWORD =Domain+"pssr_change_password.json" ;
    public static String PriceList=Domain+"pssr_price_list.json";
    public static String BonusParty=Domain+"pssr_bonus_party_affiliation.json";
    public static String PRODUCT_WISE_BONUS=Domain+"pssr_product_order_bonus_report.json";
    public static String PRODUCT_WISE_BONUS_SUMMERY=Domain+"pssr_product_bonus_report.json";
    public static String Stock=Domain+"pssr_order_stock_info.json";
    public static String PRODUCT_WISE_SALES=Domain+"pssr_product_order_report.json";
    public static String PRODUCT_WISE_SALES_SUMMERY=Domain+"pssr_product_sales_report.json";
    public static final String ProductTarget  = Domain+"pssr_sales_targets.json";
    public static final String LAST_MEMO  = Domain+"pssr_last_memo_details.json";
    public static final String OUT_OF_PLAN_VISIT  = Domain+"pssr_create_out_of_plan_visit.json";
    public static final String GET_CREDIT_COLLECTION  = Domain+"pssr_get_collection_list.json";
}
*/

package com.srapp.Db_Actions;

public class URL {

    // public static final String Domain = "http://192.168.10.116/order_basis_eslaes/api_data_dist_retrives/";
    // public static final String Domain = "http://202.126.123.157/order_basis_eslaes/api_data_dist_retrives/";

     //public static final String Domain = "http://192.168.10.116/smc_sales/api_data_dist_retrives/";
    // public static final String Domain = "http://192.168.137.6/smc_sales/api_data_dist_retrives/";
    // public static final String Domain = "http://182.160.103.236:8079/dms_arena/api_data_dist_retrives/"; //live //not used
    // public static final String Domain = "http://182.160.103.236:8079/dms/api_data_dist_retrives/"; //live
    // public static final String Domain = "http://192.168.10.116/order_basis_eslaes/api_data_dist_retrives/"; //live
   //  public static final String Domain = "http://202.126.123.157/field-force/api_data_dist_retrives/"; //live
     //  public static final String Domain = "http://202.126.123.157/field-force/api_data_dist_retrives110/"; //live change from backend on : 11-01-2020
        public static final String Domain = "http://103.134.89.62/api_data_dist_retrives110/"; //live change from backend on : 10th Feb,-2020

    public static final String Login = Domain + "dist_user_login.json";
    public static final String Push = Domain + "create_outlet.json";
    public static final String PULL = Domain + "dist_data_pull.json";
    public static final String Log = "http://192.168.10.74/pushdata.php";
    public static final String GET_DEPOSITS = Domain+"dist_get_collection_for_deposit_list.json";
    public static final String SUBMIT_DEPOSITS = Domain+"dist_create_deposit.json";
    public static final String GET_DEPOSIT_LIST = Domain+"dist_get_deposit_list.json";
    public static final String EDIT_DEPOSIT = Domain+"dist_edit_deposit.json";
    public static final String SUBMIT_CREDIT_COLLECTION = Domain + "dist_update_credit_collection.json";
    public static final String SUBMIT_PAYMENT_COLLECTION = Domain + "dist_create_payment.json";
    public static final String CREATE_MEMO = Domain +"dist_create_memo.json";
    public static final String ORDERPUSH = Domain + "dist_create_order.json";
    public static final String ORDERS_FOR_PROCESS = Domain + "dist_order_ready_for_process_list.json";
    public static final String ORDERS_FOR_UNPROCESS = Domain + "dist_process_order_list.json";
    public static final String VERSION = "1.1.3";
    public static final String VERSION_txt = "Version("+VERSION+")";
    public static final String GET_LAST_RECORD = Domain + "dist_last_orders.json";
    public static final String GET_LAST_MEMO = Domain + "dist_last_memos.json";
    public static final String GET_ORDER_DETAILS = Domain + "dist_order_details.json";
    public static final String PROCESS_ORDER_LIST = Domain + "dist_order_delivery_schedule.json";
    public static final String UNPROCESS_ORDER_LIST = Domain + "dist_order_delivery_unprocess.json";
    public static final String ORDER_DETAILS = Domain + "dist_schedule_order_details.json";
    public static final String OutletDetails = Domain + "dist_outlet_details.json";
    public static final String CreateOutlet = Domain + "dist_create_outlet.json";
    public static final String OutletList = Domain + "dist_outlet_list.json";
    public static final String MarketDetails = Domain + "dist_market_details.json";
    public static final String CreateMarket = Domain + "dist_create_market.json";
    public static final String MarketList= Domain +"dist_market_list.json";
    public static final String VisitPlan= Domain +"dist_visit_plan_list.json";
    public static final String CANCEL_ORDER= Domain +"dist_order_cancel.json";
    public static final String CANCEL_ORDER_LIST= Domain +"dist_order_cancel_list.json";
    public static final String PROCESS_ORDER_LIST_FOR_MEMO= Domain +"dist_process_order_list_for_delivery.json";
    public static final String CHANGE_PASSWORD =Domain+"dist_change_password.json" ;
    public static String PriceList=Domain+"dist_price_list.json";
    public static String BonusParty=Domain+"dist_bonus_party_affiliation.json";
    public static String PRODUCT_WISE_BONUS=Domain+"dist_product_order_bonus_report.json";
    public static String PRODUCT_WISE_BONUS_SUMMERY=Domain+"dist_product_bonus_report.json";
    public static String Stock=Domain+"dist_order_stock_info.json";
    public static String PRODUCT_WISE_SALES=Domain+"dist_product_order_report.json";
    public static String PRODUCT_WISE_SALES_SUMMERY=Domain+"dist_product_sales_report.json";
    public static final String ProductTarget  = Domain+"dist_sales_targets.json";
    public static final String LAST_MEMO  = Domain+"dist_last_memo_details.json";
    public static final String OUT_OF_PLAN_VISIT  = Domain+"dist_create_out_of_plan_visit.json";
    public static final String GET_CREDIT_COLLECTION  = Domain+"dist_get_collection_list.json";
    public static String ATTENDANCE_HISTORY = Domain + "sr_login_report.json";
    public static final String GET_ATTENDANCE_STATUS = Domain + "get_sr_check_in_out.json";
    public static final String SET_ATTENDANCE_STATUS = Domain + "set_sr_check_in_out.json";
    public  static final String  Get_SO_TrackingTime = Domain+"live_sales_tracking.json"; // for SR visiting location tracking
    public static final String PUSH_SO_Tracking = Domain +"map_sales_tracking.json";

}

