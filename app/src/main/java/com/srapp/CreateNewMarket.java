package com.srapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.Temp;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.StaticFlags;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class CreateNewMarket extends AppCompatActivity implements BasicFunctionListener, DBListener {
    ImageView homeBtn,backBtn;
    Toolbar toolbar;
    Spinner territorySpinner, thanaSpinner, routeSpinner,locationTypeSpinner;
    EditText marketNameTv, addressTv;
    Button saveBtn;
    Data_Source ds;
    TextView lable;
    TextView territoryLable, thanaLable,routeLable,locationLable;
    //ArrayList<String> dataForMarketCreate;
    int page_from;
    HashMap<String,ArrayList<String>> territoryData;
    HashMap<String,ArrayList<String>> thanaData;
    HashMap<String,ArrayList<String>> locationData;
    HashMap<String,ArrayList<String>> routeData;

    String territoryId,thanaId,locationId,routeId, marketName, address, salesPersonId;
    HashMap<String,String> dataForMarketCreate;

    int routeSelection, thanaSelection, locationaSelection;
    BasicFunction basic;
    //.........intent rcv.......//
    HashMap<String,String>markets;
    private String market_id_byIntent;
    TextView userIdTV;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorCreateSpinner;
    BasicFunction basicFunction;

    //------------------------------
    ArrayList<String> Route_id = new ArrayList<String>();
    ArrayList<String> Route_name = new ArrayList<String>();
    String _routeid;
    int disid;
    Data_Source db;



    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_new_market);
        db = new Data_Source(this, this);

        basicFunction = new BasicFunction(this,this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();
        Intent intent = getIntent();
        disid = intent.getIntExtra("disid", 0);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorCreateSpinner.setAdapter(distributorData);
                distributorCreateSpinner.setSelection(disid);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        ds = new Data_Source(this);
        dataForMarketCreate = new HashMap<>();
        basic = new BasicFunction(this,this);
        //................intent part...............//
        market_id_byIntent = intent.getStringExtra("market_id");


        //................spinner lables................//
        //territoryLable = findViewById(R.id.territory_name_tv);
        thanaLable = findViewById(R.id.thana_name_tv);
        locationLable = findViewById(R.id.location_name_tv);
        routeLable = findViewById(R.id.route_name_tv);
        page_from = intent.getIntExtra("page_from",0);
        Log.e("page_from", String.valueOf(page_from));
        lable = findViewById(R.id.market_create_lable);
        saveBtn = findViewById(R.id.save_btn);
        userIdTV = findViewById(R.id.user_txt_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);



        String mark = getColoredSpanned("*","#ff6c58");

//        territoryLable.setText(Html.fromHtml("Territory Name"+mark));
        thanaLable.setText(Html.fromHtml("Thana Name"+mark));
        locationLable.setText(Html.fromHtml("Location Type"+mark));
        routeLable.setText(Html.fromHtml("Route"+mark));

        territoryData = new HashMap<>();
        thanaData = new HashMap<>();
        locationData = new HashMap<>();
        routeData = new HashMap<>();

        territoryId = basic.getPreference("territory_id");
        salesPersonId = basic.getPreference("sales_person_id");

        //territoryData = ds.getTerritoryList("1008");
        locationData = ds.getLocationList();
        //routeData = ds.getRouteList();
        //thanaData = ds.getThanaList("no");
        Log.e("null check", "setAdapterToThanaSpinner: "+thanaData );


        Log.e("Location Spinner Log", "onCreate: "+locationData );

        //territorySpinner = findViewById(R.id.terrritory_spinner);
        thanaSpinner = findViewById(R.id.thana_spinner);
        routeSpinner = findViewById(R.id.route_spinner);
        locationTypeSpinner = findViewById(R.id.location_type_spinner);
        distributorCreateSpinner = findViewById(R.id.distributor_spinner);

        routeSpinner.setDropDownWidth(300);



        Log.e("territories for spinner", "onCreate: "+territoryData );

       // SpinnerAdapter territorySpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item, territoryData.get(Tables.TERRITORY_NAME));

       /* //thana spinner adapter setup
        if (thanaData != null && thanaData.size()>0) {
            SpinnerAdapter thanaSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item, thanaData.get(Tables.THANA_NAME));
            thanaSpinner.setAdapter(thanaSpinnerAdapter);
        }else {
            thanaSpinner.setAdapter(null);
        }*/

      /*  //route spinner adapter setup
        if (routeData != null && routeData.size()>0) {
            SpinnerAdapter routeSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item, routeData.get(Tables.ROUTE_NAME));
            routeSpinner.setAdapter(routeSpinnerAdapter);

        }else {
            routeSpinner.setAdapter(null);

        }*/

        // location type spinner adapter setup
        if (locationData != null && locationData.size()>0) {
            SpinnerAdapter locationTypeSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item, locationData.get(Tables.LOCATION_location_name));
            locationTypeSpinner.setAdapter(locationTypeSpinnerAdapter);
        }else {
            locationTypeSpinner.setAdapter(null);
        }

//      territorySpinner.requestFocus();
        thanaSpinner.requestFocus();
        routeSpinner.requestFocus();
        locationTypeSpinner.requestFocus();

        /*territorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                territoryId = territoryData.get(Tables.TERRITORY_territory_id).get(position);
                Log.e("Terr Spinn Select", "onItemSelected: "+territoryId );

                setAdapterToThanaSpinner();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });*/

         distributorCreateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                Log.e("distributorId_from_spin", distributorStoreId);

                     if (distributorStoreId!=null){
                         ThanaParse();
                             thanaData = ds.getThanaList(distributorStoreId);
                             Log.e("distributorId_select", distributorStoreId);
                             //thana spinner adapter setup
                             if (thanaData != null && thanaData.size()>0) {
                                 SpinnerAdapter thanaSpinnerAdapter = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, thanaData.get(Tables.THANA_NAME));
                                 thanaSpinner.setAdapter(thanaSpinnerAdapter);
                                 Log.e("thana_all_data", thanaData.toString());

                             }else {
                                 thanaSpinner.setAdapter(null);
                                 Log.e("thana_all_data_not", thanaData.toString());

                             }
                         }

                else {
                    Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        thanaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                thanaId = thanaData.get(Tables.THANA_TH_id).get(position);
                Log.e("Thana Spinn Select", "onItemSelected: "+thanaId );

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        routeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                dataViewModel.getDistributorId(parent.getItemAtPosition(position).toString());
                distributorId = dataViewModel.distributorId;

                //routeId = routeData.get(Tables.ROUTE_ID).get(position);

                routeId = Route_id.get(position);
                Log.e("rout_id_selection", routeId);
                //String name = Route_name.get(position);
               // savePreference("Thana", name);
                //savePreference("thanaID", _routeid);

                if (distributorId!=null){
                    thanaData = ds.getThanaList(distributorId);
                    Log.e("distributorId_from_spin", distributorId);
                    //thana spinner adapter setup
                    if (thanaData != null && thanaData.size()>0) {
                        SpinnerAdapter thanaSpinnerAdapter = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, thanaData.get(Tables.THANA_NAME));
                        thanaSpinner.setAdapter(thanaSpinnerAdapter);
                        Log.e("thana_all_data", thanaData.toString());

                    }else {
                        thanaSpinner.setAdapter(null);
                        Log.e("thana_all_data_not", thanaData.toString());

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Log.e("loc id befor select", "onCreate: "+locationId );
        locationTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                locationId = locationData.get(Tables.LOCATION_location_id).get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        marketNameTv = findViewById(R.id.market_name_tv);
        //marketNameTv.setHint(Html.fromHtml("Market Name"+mark));
        addressTv = findViewById(R.id.address_tv);

        //........EDIT...............//
        if (market_id_byIntent != null){

            StaticFlags.MARKET_ID_FOR_POSITION = market_id_byIntent;

           // Toast.makeText(CreateNewMarket.this,""+market_id_byIntent,Toast.LENGTH_SHORT).show();

            lable.setText("Market Update");
            saveBtn.setText("Update");


            JSONObject primaryData = new JSONObject();
            try {
                primaryData.put("mac",basic.getPreference("mac"));
                primaryData.put("sales_person_id",basic.getPreference("sales_person_id"));
                primaryData.put("market_id",market_id_byIntent);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            basic.getResponceData(URL.MarketDetails, String.valueOf(primaryData),801);


        }


        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( CreateNewMarket.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (page_from==-1 || page_from==25){ // 25 value passes from Create_New_Memo activity
                    Intent intent = new Intent(CreateNewMarket.this,Create_New_Memo.class);
                    startActivity(intent);
                    finish();

                }else {

                    Intent intent = new Intent(CreateNewMarket.this, MarketList.class);
                    intent.putExtra("resume", 1);
                    startActivity(intent);
                    finish();
                }
            }
        });


        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    address = addressTv.getText().toString().trim();
                    marketName = marketNameTv.getText().toString().trim();

                    dataForMarketCreate.put(Tables.MARKETS_Territory_id,territoryId);
                    dataForMarketCreate.put(Tables.SR_ID, salesPersonId);
                    dataForMarketCreate.put(Tables.MARKETS_thana_id, thanaId);
                    dataForMarketCreate.put(Tables.ROUTE_ID, routeId);
                    Log.e("Tables.ROUTE_ID", routeId);
                    dataForMarketCreate.put(Tables.MARKETS_db_id, distributorStoreId);
                    Log.e("distributorStoreId_test", distributorStoreId);
                    dataForMarketCreate.put(Tables.MARKETS_location_type_id, locationId);
                    dataForMarketCreate.put(Tables.MARKETS_market_name, marketName);
                    dataForMarketCreate.put(Tables.MARKETS_is_active, "1");
                    dataForMarketCreate.put(Tables.MARKETS_address, address);
                    dataForMarketCreate.put(Tables.MARKETS_market_id, salesPersonId + System.currentTimeMillis());

                    if (thanaId != null && locationId != null &&  !TextUtils.isEmpty(marketName)) {

                        if (market_id_byIntent != null) {
                            //update here

                            //................Push to Server..............//
                            //use code 202
                            JSONObject marketObj = new JSONObject();
                            JSONArray marketList = new JSONArray();
                            JSONObject market = new JSONObject();
                            try {

                                //Date currentTime = Calendar.getInstance().getTime();
                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date dateobj = new Date();
                                String currentTime = df.format(dateobj);


                                for (String i: dataForMarketCreate.keySet()){

                                    Log.e("tttt", dataForMarketCreate.get(i));
                                    if (i.equals(Tables.MARKETS_market_id)){
                                        market.put("temp_id",market_id_byIntent);
                                    }else if (i.equals(Tables.SR_ID)){
                                        continue;

                                    }
                                    else {
                                        market.put(i, dataForMarketCreate.get(i));
                                    }
                                }
                                market.put("territory_id",basic.getPreference("territory_id"));
                                market.put("code","0");
                                market.put("updated_at",currentTime);

                                marketList.put(market);
                                marketObj.put("mac","354214080719502");
                                marketObj.put("sales_person_id","20423");
                                marketObj.put("market_list",marketList);

                                Log.e("hashmap", "onClick: "+dataForMarketCreate );
                                Log.e("json test", "onClick: "+marketObj );

                                //api call to push
                                basic.getResponceData(URL.CreateMarket, marketObj.toString(), 202);


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }





                        } else {

                            //................ push to server..........//
                            JSONObject marketObj = new JSONObject();
                            JSONArray marketList = new JSONArray();
                            JSONObject market = new JSONObject();
                            try {
                                //Date currentTime = Calendar.getInstance().getTime();
                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date dateobj = new Date();
                                String currentTime = df.format(dateobj);

                                for (String i: dataForMarketCreate.keySet()){
                                    Log.e("tttt", dataForMarketCreate.get(i)+"   djn");
                                    if (i.equals(Tables.MARKETS_market_id)){
                                        market.put("temp_id","mk"+dataForMarketCreate.get(i));
                                    }else if (i.equals(Tables.SR_ID)){
                                        continue;

                                    }
                                    else {
                                        market.put(i, dataForMarketCreate.get(i));
                                    }
                                }
                                market.put("territory_id",basic.getPreference("territory_id"));
                                market.put("code","0");
                                market.put("updated_at",currentTime);

                                marketList.put(market);
                                marketObj.put("mac","354214080719502");
                                marketObj.put("sales_person_id","20423");
                                marketObj.put("market_list",marketList);

                                Log.e("hashmap", "onClick: "+dataForMarketCreate );
                                Log.e("json test", "onClick: "+marketObj );

                                //api call to push
                                basic.getResponceData(URL.CreateMarket, marketObj.toString(), 201);


                            } catch (JSONException e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }

                        }

                    } else {
                        Toast.makeText(CreateNewMarket.this, "Please fill up mandatory fields ", Toast.LENGTH_SHORT).show();
                    }


            }
        });


    }

    private void setAdapterToThanaSpinner() {

        if (territoryId != null && !territoryId.equals("0")) {

            thanaData = ds.getThanaList(territoryId);
            Log.e("null check", "setAdapterToThanaSpinner: "+thanaData );

            if (thanaData != null && thanaData.size()>0) {
                SpinnerAdapter thanaSpinnerAdapter = new SpinnerAdapter(this, R.layout.spinner_item, thanaData.get(Tables.THANA_NAME));
                thanaSpinner.setAdapter(thanaSpinnerAdapter);
            }else {
                thanaSpinner.setAdapter(null);
            }
        }
    }

    private String getColoredSpanned(String text, String color) {
        String input = "<font color=" + color + ">" + text + "</font>";
        return input;
    }

    public int getPosition(ArrayList<String>data,String id){

        int position = data.indexOf(id);

        return  position;
    }


    @Override
    public void OnServerResponce(JSONObject jsonObject, int RequestCode) {

        Log.e("server response", "OnServerResponce: "+jsonObject );
        if (RequestCode == 201){
            int response = 0;
            try {
                response = jsonObject.getJSONObject("market").getInt("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            if (response == 1  ){
                // add add market id to dataForMarketCreate from response
                try {
                    String marketIdFinal = jsonObject.getJSONObject("market").getJSONArray("replaced_relation").getJSONObject(0).getString("new_id");
                    dataForMarketCreate.put(Tables.MARKETS_market_id,marketIdFinal);
                    dataForMarketCreate.remove(Tables.SR_ID);
                    StaticFlags.MARKET_ID_FOR_POSITION= marketIdFinal;
                    if (page_from==-1){
                        basic.savePreference("market_id",marketIdFinal);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                StaticFlags.MARKET_THANA_ID = thanaId;

                //create market and push to server

                long id = ds.insertIntoMarket(dataForMarketCreate);
                Log.e("after response", "OnServerResponce: "+dataForMarketCreate );



                /*addressTv.setText("");
                marketNameTv.setText("");
                routeSpinner.setSelection(0);
                thanaSpinner.setSelection(0);
                locationTypeSpinner.setSelection(0);*/
                // territorySpinner.setSelection(0);
                if (id > 0) {

                    Toast.makeText(CreateNewMarket.this, "Market Created ", Toast.LENGTH_LONG).show();
                    saveBtn.setEnabled(false);
                } else {
                    Toast.makeText(CreateNewMarket.this, "Whoops !", Toast.LENGTH_SHORT).show();
                }

                Log.e("print_memo", String.valueOf(TempData.ismemo));
//                if(TempData.ismemo = true){
//                    Intent intent1 = new Intent(CreateNewMarket.this,Create_New_Memo.class);
//                    intent1.putExtra("resume",2);
//                    startActivity(intent1);
//                    finish();
//                }else{
//                    Intent intent2 = new Intent(CreateNewMarket.this,MarketList.class);
//                    intent2.putExtra("resume",2);
//                    startActivity(intent2);
//                    finish();
//                }

                if (page_from==25){
                    savePreference("MarketID", dataForMarketCreate.get("temp_id"));
                    Intent intent = new Intent(CreateNewMarket.this,Create_New_Memo.class);
                    startActivity(intent);
                    finish();
                    return;
                }
                Intent intent = new Intent(CreateNewMarket.this,MarketList.class);
                intent.putExtra("resume",2);
                startActivity(intent);
                finish();

            }else {
                Toast.makeText(CreateNewMarket.this, "Server Problem", Toast.LENGTH_LONG).show();
            }
        }

        else if (RequestCode == 202){
            // update local database here

            int response = 0;
            try {
                response = jsonObject.getJSONObject("market").getInt("status");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (response == 1) {



            dataForMarketCreate.remove(Tables.MARKETS_market_id);
            dataForMarketCreate.remove(Tables.SR_ID);
            long id = ds.updateMarket(dataForMarketCreate,market_id_byIntent);
            if (id >0) {
                saveBtn.setEnabled(false);
                Toast.makeText(CreateNewMarket.this, "Updated", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(CreateNewMarket.this, "Whoops !", Toast.LENGTH_SHORT).show();
            }

            StaticFlags.MARKET_ID_FOR_POSITION = market_id_byIntent;
            StaticFlags.MARKET_THANA_ID = thanaId;

                Intent intent = new Intent(CreateNewMarket.this,MarketList.class);
                intent.putExtra("resume",2);
                startActivity(intent);
                finish();


            }
        }

        else if (RequestCode == 801){
            Log.e("market edit...>", "OnServerResponce: "+jsonObject );
            try {

                markets = new HashMap<>();

                markets.put(Tables.MARKETS_market_id,jsonObject.getJSONArray("markets").getJSONObject(0).getString("market_id"));
                markets.put(Tables.MARKETS_market_name,jsonObject.getJSONArray("markets").getJSONObject(0).getString("market_name"));
                markets.put(Tables.MARKETS_thana_id,jsonObject.getJSONArray("markets").getJSONObject(0).getString("thana_id"));
                markets.put(Tables.MARKETS_location_type_id,jsonObject.getJSONArray("markets").getJSONObject(0).getString("location_type_id"));
                markets.put(Tables.MARKETS_address,jsonObject.getJSONArray("markets").getJSONObject(0).getString("address"));
              //  markets.put(Tables.MARKETS_root_id,jsonObject.getJSONArray("markets").getJSONObject(0).getString("route_id"));

               // markets = ds.getIndivMarketDetails(market_id_byIntent);


                try {
//                routeSelection = getPosition(routeData.get(Tables.ROUTE_ID),markets.get(Tables.MARKETS_root_id));
                    thanaSelection = getPosition(thanaData.get(Tables.THANA_TH_id),markets.get(Tables.MARKETS_thana_id));
                    locationaSelection = getPosition(locationData.get(Tables.LOCATION_location_id),markets.get(Tables.MARKETS_location_type_id));
                } catch (Exception e) {
                    e.printStackTrace();
                }

                routeSpinner.setSelection(routeSelection);
                thanaSpinner.setSelection(thanaSelection);
                locationTypeSpinner.setSelection(locationaSelection);

                marketNameTv.setText(markets.get(Tables.MARKETS_market_name));
                addressTv.setText(markets.get(Tables.MARKETS_address));

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("markete",e.getMessage());
            }
        }

    }

    public void savePreference(String key, String value)
    {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }
    @Override
    public void OnConnetivityError() {
        Toast.makeText(CreateNewMarket.this,"No Internet Connection",Toast.LENGTH_SHORT).show();

    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {

            if (page_from==-1 || page_from==25){
                Intent intent = new Intent(CreateNewMarket.this,Create_New_Memo.class);
                startActivity(intent);
                finish();

            }else {

                Intent intent = new Intent(CreateNewMarket.this, MarketList.class);
                intent.putExtra("resume", 1);
                startActivity(intent);
                finish();
            }

            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


    private void ThanaParse() {
        Route_id.clear();
        Route_name.clear();
        Cursor c = db.rawQuery("SELECT * FROM route where db_id=" + "'" + distributorStoreId + "' ORDER BY route_name ASC");
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String Thana_Id = c.getString(c.getColumnIndex("route_id"));
                    String Thana_Name = c.getString(c.getColumnIndex("route_name"));

                    Route_id.add(Thana_Id);
                    Route_name.add(Thana_Name);

                } while (c.moveToNext());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(CreateNewMarket.this, R.layout.spinner_text, Route_name);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            routeSpinner.setAdapter(dataAdapter);

           /* if (!getPreference("thanaID").equalsIgnoreCase("NO PREFERENCE") && !getPreference("thanaID").equalsIgnoreCase("")) {
                int SelectedPos = Route_id.indexOf(getPreference("thanaID"));
                Log.e("MARKET POS:", ".........." + SelectedPos);
                routeSp.setSelection(SelectedPos);
            }*/


        }

    }

    @Override
    public void OnLocalDBdataRetrive(String json) {

    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }
}
