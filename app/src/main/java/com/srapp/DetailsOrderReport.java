package com.srapp;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;

import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.srapp.Adapter.AdapterForMemoDetails;
import com.srapp.Adapter.AdapterForOrderDetails;
import com.srapp.Adapter.AdapterForSelial;
import com.srapp.Adapter.AdapterForSerialShow;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.Temp;
import com.srapp.Db_Actions.URL;
import com.srapp.Model.OrderDetailsModel;
import com.srapp.Util.Parent;
import com.srapp.print.PrintActivity;
import com.srapp.print.PrintActivity_For_Memo;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static com.srapp.Db_Actions.Tables.MEMOS;
import static com.srapp.Db_Actions.Tables.MEMOS_ORDER_NUMBER;
import static com.srapp.Db_Actions.Tables.MEMOS_TOTAL_Vat;
import static com.srapp.Db_Actions.Tables.MEMOS_editable;
import static com.srapp.Db_Actions.Tables.MEMOS_for_memo_delete;
import static com.srapp.Db_Actions.Tables.MEMOS_from_app;
import static com.srapp.Db_Actions.Tables.MEMOS_gross_value;
import static com.srapp.Db_Actions.Tables.MEMOS_is_pushed;
import static com.srapp.Db_Actions.Tables.MEMOS_market_id;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_date;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_date_time;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_number;
import static com.srapp.Db_Actions.Tables.MEMOS_outlet_id;
import static com.srapp.Db_Actions.Tables.MEMOS_table_id;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS_Serial_NUMBER;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS_memo_number;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS_order_number;
import static com.srapp.Db_Actions.Tables.MESSAGE_UPDATED_AT;
import static com.srapp.Db_Actions.Tables.ORDER_ADVANCE_COLLECTION;
import static com.srapp.Db_Actions.Tables.ORDER_STATUS;
import static com.srapp.Db_Actions.Tables.ORDER_TOTAL_VAT;
import static com.srapp.Db_Actions.Tables.ORDER_editable;
import static com.srapp.Db_Actions.Tables.ORDER_for_order_delete;
import static com.srapp.Db_Actions.Tables.ORDER_from_app;
import static com.srapp.Db_Actions.Tables.ORDER_gross_value;
import static com.srapp.Db_Actions.Tables.ORDER_is_out_of_plan;
import static com.srapp.Db_Actions.Tables.ORDER_is_pushed;
import static com.srapp.Db_Actions.Tables.ORDER_market_id;
import static com.srapp.Db_Actions.Tables.ORDER_order_date;
import static com.srapp.Db_Actions.Tables.ORDER_order_date_time;
import static com.srapp.Db_Actions.Tables.ORDER_order_number;
import static com.srapp.Db_Actions.Tables.ORDER_outlet_id;
import static com.srapp.Db_Actions.Tables.ORDER_plan_id;
import static com.srapp.Db_Actions.Tables.ORDER_table_id;
import static com.srapp.Db_Actions.Tables.PROCESSING_COMPELETE;
import static com.srapp.Db_Actions.Tables.PROCESSING_PENDING;
import static com.srapp.Db_Actions.Tables.PRODUCT_BOOLEAN_QUANTITY;
import static com.srapp.Db_Actions.Tables.PRODUCT_ID;
import static com.srapp.Db_Actions.Tables.PRODUCT_PRICE_PRICE;
import static com.srapp.Db_Actions.Tables.PRODUCT_PRODUCT_NAME;
import static com.srapp.Db_Actions.Tables.PRODUCT_SERIALS;
import static com.srapp.Db_Actions.Tables.PRODUCT_SERIALS_serial_no;
import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MEMOS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MEMO_DETAILS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER_DETAILS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_PRODUCT_BOOLEAN;
import static com.srapp.TempData.BonusArrayList;
import static com.srapp.TempData.Converting_TO_MEMO;
import static com.srapp.TempData.MEMO_EDIT;
import static com.srapp.TempData.MEMO_EDITING;
import static com.srapp.TempData.ORDER_TO_MEMO;
import static com.srapp.TempData.SALE_STATE;
import static com.srapp.TempData.TotalBonusProductList;
import static com.srapp.TempData.editMemo;


public class DetailsOrderReport extends Parent implements BasicFunctionListener, DBListener {
    String memoNo;
    double subtotal = 0.00;
    ListView list;
    Button btnEdit, cancel, print;
    // AdapterForMemoDetails adapter;
    OrderDetailsModel orderDetailsModel;
    ArrayList<HashMap<String, String>> Data;
    TextView txtGift, txtBonus, total_price, discountt, sub_total, title, vat, userIdTV;
    Data_Source db;
    ImageView homeBtn, backBtn;
    int memo = 0;
    int Back = 0;
    HashMap<String, String> map;
    BasicFunction bf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_order_report);
        userIdTV = findViewById(R.id.user_txt_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        db = new Data_Source(this, this, this);
        bf = new BasicFunction(this, this);
        txtGift = findViewById(R.id.txtGift);
        btnEdit = findViewById(R.id.btnEdit);
        discountt = findViewById(R.id.discount);
        sub_total = findViewById(R.id.sub_total);
        txtBonus = findViewById(R.id.txtBonus);
        vat = findViewById(R.id.vat);
        cancel = findViewById(R.id.cancel);
        print = findViewById(R.id.print);
        total_price = findViewById(R.id.total_price);
        list = findViewById(R.id.list);
        title = findViewById(R.id.title);
        if (SALE_STATE == MEMO_EDITING) {
            title.setText("Invoice Details");
            btnEdit.setText("Edit Invoice");
            print.setText("Print Invoice");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = simpleDateFormat.parse(TempData.MemoDate);
                Date date2 = simpleDateFormat.parse(getCurrentDate());
                if (printDifference(date1, date2) > 3) {
                    btnEdit.setVisibility(View.GONE);
                }
                if (TempData.DayCloseMemoEditable != null)
                    if (TempData.DayCloseMemoEditable.equalsIgnoreCase("1")) {
                        btnEdit.setVisibility(View.VISIBLE);
                    }
            } catch (ParseException e) {
                e.printStackTrace();
            }


        } else {

            title.setText("Order Details");
            btnEdit.setText("Edit Order");
            print.setText("Print Order");
            cancel.setText("Cancel Order");

            if (SALE_STATE == Converting_TO_MEMO) {
                cancel.setText("Deliver Now");
                cancel.setVisibility(View.VISIBLE);
            }

            if (TempData.ORDER_STATUE == 2) {
                cancel.setVisibility(View.GONE);
            }
        }
        String Query = "";
        if (SALE_STATE != MEMO_EDITING)
            Query = "SELECT " + ORDER_TOTAL_VAT + " FROM " + TABLE_NAME_ORDER + " where " + ORDER_order_number + "='" + TempData.orderNumber + "'";
        else
            Query = "SELECT " + MEMOS_TOTAL_Vat + " FROM " + TABLE_NAME_MEMOS + " where " + MEMOS_memo_number + "='" + TempData.memoNumber + "'";
        Cursor c = db.rawQuery(Query);
        Log.e("Quearymem", Query + " sixe" + c.getCount());
        c.moveToFirst();

        if (c != null && c.getCount() > 0) {

            vat.setText(c.getString(0));
        }


        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {


                startActivity(new Intent(DetailsOrderReport.this, Dashboard.class));
                finishAffinity();
            }
        });

        txtBonus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SALE_STATE == MEMO_EDITING)
                    showSirialNumber(1);

            }
        });

        txtGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (SALE_STATE == MEMO_EDITING)
                    showSirialNumber(2);

            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                if (Back == 1) {
                    startActivity(new Intent(DetailsOrderReport.this, DeliveryReport.class));
                    finish();
                }

                if (SALE_STATE == MEMO_EDITING) {
                    startActivity(new Intent(DetailsOrderReport.this, MemoReport.class));
                    finish();

                } else if (SALE_STATE == Converting_TO_MEMO) {
                    startActivity(new Intent(DetailsOrderReport.this, DeliveryReport.class));
                    finish();

                } else if (SALE_STATE == 0) {

                    startActivity(new Intent(DetailsOrderReport.this, Order_Report_Activity.class));
                    finish();
                }

            }
        });


        if (getIntent() != null) {
            memo = getIntent().getIntExtra("memo", 0);

        }
        if (SALE_STATE == MEMO_EDITING) {
            cancel.setVisibility(View.GONE);
        } else {
            if (TempData.ORDER_STATUE == 1 && SALE_STATE == 0)
                cancel.setVisibility(View.GONE);
        }

        Log.e("ordernum", TempData.orderNumber + "no");
        db.getOrderDetails();

        SaleAbleList();
        SaleAbleListForAll();
        Bonus();
        Gift();

        print.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                String Query = "Log";

                if (SALE_STATE == MEMO_EDITING) {
                    Query = "SELECT outlet_id,market_id,discount_value,total_vat FROM " + TABLE_NAME_MEMOS + " where " + MEMOS_memo_number + "='" + TempData.memoNumber + "'";

                } else {

                    Query = "SELECT outlet_id,market_id,discount_value,total_vat FROM " + TABLE_NAME_ORDER + " where " + ORDER_order_number + "='" + TempData.orderNumber + "'";
                }

                Log.e("Query", Query);
                Cursor c7 = db.rawQuery(Query);
                if (c7.getCount() > 0) {
                    if (c7.moveToFirst()) {
                        do {

                            String outletId = c7.getString(0);
                            String market_id = c7.getString(1);
                            TempData.DISCOUNT = c7.getDouble(2);
                            TempData.VAT = c7.getDouble(3);
                            vat.setText(TempData.VAT + "");
                            Cursor c8 = db.rawQuery("SELECT m.market_name,t.thana_name FROM outlets O LEFT JOIN markets m ON (O.market_id = m.market_id) INNER JOIN thana as t on m.thana_id=t.thana_id where o.outlet_id='" + outletId + "' limit 1");
                            if (c8.getCount() > 0) {
                                if (c8.moveToFirst()) {
                                    do {
                                        TempData.tempMarket = c8.getString(0);
                                        TempData.tempThana = c8.getString(1);

                                    } while (c8.moveToNext());
                                }
                            }

                            Cursor c6 = db.rawQuery("SELECT OC.outlet_category_name FROM outlets O LEFT JOIN outlet_categories OC ON (O.outlet_category_id = OC.outlet_category_id) where outlet_id='" + outletId + "'");
                            if (c6.getCount() > 0) {
                                if (c6.moveToFirst()) {
                                    do {

                                        String outlet_category_name = c6.getString(0);
                                        savePreference("OutletCategoryName", outlet_category_name);


                                    } while (c6.moveToNext());
                                }
                            }


                        } while (c7.moveToNext());
                    }
                }

                //TempData.TempBonus="";
                if (SALE_STATE == MEMO_EDITING) {
                    Intent idn = new Intent(DetailsOrderReport.this, PrintActivity_For_Memo.class);
                    idn.putExtra("From", "MemoDetails");
                    startActivity(idn);
                    finish();
                } else {

                    Intent idn = new Intent(DetailsOrderReport.this, PrintActivity.class);
                    idn.putExtra("From", "MemoDetails");
                    startActivity(idn);
                    finish();
                }
            }
        });


        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (SALE_STATE != Converting_TO_MEMO) {

                    if (TempData.ORDER_STATUE != PROCESSING_COMPELETE) {
                        if (TempData.isPushed.equalsIgnoreCase("1")) {
                            if (bf.isInternetOn()) {

                                JSONObject jsonObject = new JSONObject();
                                try {
                                    jsonObject.put("mac", bf.getPreference("mac"));
                                    jsonObject.put(SR_ID, bf.getPreference(SR_ID));
                                    jsonObject.put(ORDER_order_number, TempData.orderNumber);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                bf.getResponceData(URL.CANCEL_ORDER, jsonObject.toString(), 111);
                            } else {
                                Toast.makeText(DetailsOrderReport.this, "NO Internet Connection", Toast.LENGTH_LONG).show();
                                btnEdit.setEnabled(true);
                                return;
                            }
                        } else {

                            db.excQuery("delete from " + TABLE_NAME_ORDER + " where " + ORDER_order_number + " ='" + TempData.orderNumber + "'");
                            db.excQuery("delete from " + TABLE_NAME_ORDER_DETAILS + " where " + ORDER_order_number + " ='" + TempData.orderNumber + "'");
                            Intent idn = new Intent(DetailsOrderReport.this, Order_Report_Activity.class);
                            startActivity(idn);
                            finish();
                        }
                    } else {
                        Toast.makeText(DetailsOrderReport.this, "This Order is Already Processed Please cancel invoice to Edit", Toast.LENGTH_LONG).show();
                    }
                } else if (SALE_STATE == Converting_TO_MEMO) {
                    cancel.setEnabled(false);
                    if (bf.isInternetOn())
                        MakeMemo();
                    else {
                        cancel.setEnabled(true);
                        Toast.makeText(DetailsOrderReport.this, "No Internet Connection", Toast.LENGTH_LONG).show();
                    }

                }


            }
        });


        btnEdit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                btnEdit.setEnabled(false);

                db.excQuery("delete from " + TABLE_NAME_PRODUCT_BOOLEAN);
                Log.e("disable", "disable");
                TempData.editMemo = "true";
                //  Log.e("editMemo No:", TempData.orderNumber = TempData.orderNumber);
                String Query = "";
                if (SALE_STATE != MEMO_EDITING)
                    Query = "SELECT " + ORDER_table_id + "," + ORDER_order_number + ", " + ORDER_is_pushed + "," + ORDER_outlet_id + "," + ORDER_order_date + "," + ORDER_market_id + ", " + ORDER_from_app + ", " + ORDER_order_date_time + ", " + ORDER_editable + ", " + ORDER_gross_value + "," + ORDER_for_order_delete + "," + ORDER_STATUS + "," + ORDER_is_out_of_plan + "," + ORDER_plan_id + "," + ORDER_ADVANCE_COLLECTION + " FROM " + TABLE_NAME_ORDER + " where " + ORDER_order_number + "='" + TempData.orderNumber + "'";
                else
                    Query = "SELECT " + MEMOS_table_id + "," + MEMOS_ORDER_NUMBER + ", " + MEMOS_is_pushed + "," + MEMOS_outlet_id + "," + MEMOS_memo_date + "," + MEMOS_market_id + ", " + MEMOS_from_app + ", " + MEMOS_memo_date_time + ", " + MEMOS_editable + ", " + MEMOS_gross_value + "," + MEMOS_for_memo_delete + "," + MEMOS_memo_number + " FROM " + TABLE_NAME_MEMOS + " where " + MEMOS_memo_number + "='" + TempData.orderNumber + "'";


                Cursor c = db.rawQuery(Query);
                Log.e("Quearymem", Query + " sixe" + c.getCount());
                if (c != null) {

                    Log.e("NOt", "null" + c.getCount());

                    if (c.moveToFirst()) {
                        do {
                            TempData.orderNumber = c.getString(1);
                            TempData.MemoTotalView = c.getString(0);
                            TempData.isPushed = c.getString(2);
                            TempData.OutletID = c.getString(3);
                            TempData.MemoDate = c.getString(4);
                            TempData.MarketID = c.getString(5);
                            TempData.From_App = c.getString(6);
                            TempData.MemoDateTime = c.getString(7);
                            TempData.DayCloseMemoEditable = c.getString(8);
                            TempData.gross_value = c.getString(9);
                            TempData.for_memo_delete = c.getString(10);
                            if (SALE_STATE != MEMO_EDITING) {
                                TempData.ORDER_STATUE = c.getInt(11);
                                TempData.ORDER_PlanVisit = c.getInt(12);
                                TempData.ORDER_plan_id = c.getInt(13);
                                TempData.Advance_collection = c.getInt(14);
                            } else {
                                TempData.memoNumber = c.getString(11);
                            }

                            Log.e("OutletID test------", "~~~~~~" + TempData.OutletID);
                            Log.e("MemoTotalView test----", "~~~~~~" + TempData.MemoTotalView);
                            Log.e("----PrimaryID----", "~~~~~~" + TempData.orderNumber);
                            Log.e("----MarketID----", "~~~~~~" + TempData.MarketID);
                            Log.e("----From_App----", "~~~~~~" + TempData.From_App);
                            Log.e("----for_memo_delete----", "~~~~~~" + TempData.for_memo_delete);
                            Log.e("----ORDER_STATUE----", "~~~~~~" + TempData.ORDER_STATUE);
                            Log.e("----ORDER_id----", "~~~~~~" + TempData.SelectedOrderID);

                        } while (c.moveToNext());
                    }
                }

                db.excQuery("delete from product_boolean where outlet_id='" + TempData.OutletID + "'");

                if (TempData.ORDER_STATUE == PROCESSING_PENDING || TempData.ORDER_STATUE == 3 || SALE_STATE == Converting_TO_MEMO || SALE_STATE == MEMO_EDITING) {
                    if (TempData.isPushed.equalsIgnoreCase("1")) {
                        if (bf.isInternetOn()) {
                            SwitchToSaleOrder();
                        } else {
                            Toast.makeText(DetailsOrderReport.this, "NO Internet Connection", Toast.LENGTH_LONG).show();
                            btnEdit.setEnabled(true);
                            return;
                        }
                    } else {
                        SwitchToSaleOrder();
                    }
                } else {
                    Toast.makeText(DetailsOrderReport.this, "This Order is Already Processed Please cancel invoice to Edit", Toast.LENGTH_LONG).show();
                }

            }
        });



     /*   productList = new ArrayList<>();
        oAdapter = new OderDetailsAdapter();
        oAdapter.setOrders(productList);*/
    }

    private void showSirialNumber(int i) {


        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(R.layout.show_serial, null);
        ListView sirials = view.findViewById(R.id.sirials);
        ImageView Cancelimage = view.findViewById(R.id.cancel_popup);
        builder.setView(view);
        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(DialogFragment.STYLE_NO_TITLE);
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        // dialog.getWindow().setLayout(100,100);
        dialog.show();
        dialog.setCanceledOnTouchOutside(false);
        Cancelimage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();

            }
        });


        if (i == 1) {
            AdapterForSerialShow adapter = new AdapterForSerialShow(this, TempData.BonusShowList);
            sirials.setAdapter(adapter);
        } else {
            Log.e("TempData.GiftArrayList", TempData.GiftArrayList.toString());
            AdapterForSerialShow adapter = new AdapterForSerialShow(this, TempData.GiftArrayList);
            sirials.setAdapter(adapter);
        }

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        // The absolute width of the available display size in pixels.
        int displayWidth = displayMetrics.widthPixels;
        // The absolute height of the available display size in pixels.
        int displayHeight = displayMetrics.heightPixels;
        // Initialize a new window manager layout parameters
        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        // Copy the alert dialog window attributes to new layout parameter instance
        layoutParams.copyFrom(dialog.getWindow().getAttributes());
        // Set alert dialog width equal to screen width 70%
        int dialogWindowWidth = (int) (displayWidth * 0.95f);
        // Set alert dialog height equal to screen height 70%
        int dialogWindowHeight = (int) (displayHeight * 0.6f);
        // Set the width and height for the layout parameters
        // This will bet the width and height of alert dialog
        layoutParams.width = dialogWindowWidth;
        layoutParams.height = dialogWindowHeight;
        // Apply the newly created layout parameters to the alert dialog window
        dialog.getWindow().setAttributes(layoutParams);
    }


    private void MakeMemo() {

        Cursor c = db.rawQuery("select * from order_table where order_number='" + TempData.orderNumber + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            HashMap<String, String> map = new HashMap<>();
            for (int i = 7; i < MEMOS.length - 2; i++) {

                map.put(MEMOS[i], c.getString(c.getColumnIndex(MEMOS[i])));
            }
            map.put(MEMOS_gross_value, c.getString(c.getColumnIndex(ORDER_gross_value)));
            map.put(MEMOS_ORDER_NUMBER, c.getString(c.getColumnIndex(ORDER_order_number)));
            map.put(MEMOS_memo_date, getCurrentDate());
            map.put(MEMOS_memo_date_time, getCurrentDateTime());
            map.put(MEMOS_memo_number, SalesMemoNO());
            map.put(MEMOS_for_memo_delete, c.getString(c.getColumnIndex(ORDER_for_order_delete)));
            map.put(Tables.MEMOS_created_at, getCurrentDateTime());
            map.put(Tables.MEMOS_updated_at, getCurrentDateTime());
            db.InsertTable(map, "memos");

        }

        Cursor c1 = db.rawQuery("select * from order_details where order_number='" + TempData.orderNumber + "'");
        c1.moveToFirst();
        if (c1 != null && c.getCount() > 0) {
            do {
                HashMap<String, String> map1 = new HashMap<>();
                for (int i = 4; i < MEMO_DETAILS.length - 2; i++) {
                    Log.e("column", MEMO_DETAILS[i]);
                    map1.put(MEMO_DETAILS[i], c1.getString(c1.getColumnIndex(MEMO_DETAILS[i])));
                }
                map1.put(MEMO_DETAILS_memo_number, memoNo);
                map1.put(MEMO_DETAILS_order_number, TempData.orderNumber);
                map1.put(Tables.MEMOS_created_at, getCurrentDateTime());
                map1.put(Tables.MEMO_DETAILS_memo_date, getCurrentDate());
                map1.put(Tables.MEMOS_updated_at, getCurrentDateTime());
                db.InsertTable(map1, "memo_details");
            } while (c1.moveToNext());

        }
        EntryForCreditCollection();
        db.generateSingleMemo(memoNo);

    }


    public void EntryForCreditCollection() {

        Log.e("creditcollection", memoNo);
        if (TempData.editMemo.equalsIgnoreCase("true") && SALE_STATE == MEMO_EDITING) {
            ///memoNo=TempData.memoNumber;
            db.excQuery("DELETE FROM credit_collections WHERE memo_no='" + memoNo + "'");
        }
        Log.e("memoNo", memoNo);
        double dueAmount = 0.0;

        double memo_value = Double.parseDouble(TempData.gross_value);
        double paid_amount = Double.parseDouble(TempData.advance_Collection);

        memo_value = memo_value - paid_amount;

        paid_amount = memo_value;

        Log.e("payment amount:", TempData.advance_Collection);


        if (paid_amount == 0.0) {
            dueAmount = memo_value;
        } else {
            dueAmount = memo_value - paid_amount;
        }
        if (dueAmount > 0.0)
        //if(etPayment.getText().toString().equalsIgnoreCase("0"))
        {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("outlet_id", TempData.OutletID);
//		map.put("date",getCurrentDate() );
            map.put("date", getCurrentDate());
            map.put("memo_number", memoNo);
            map.put("memo_value", TempData.InvoiceTotal);
//		map.put("collect_date", getCurrentDate());
            map.put("collection_date", getCurrentDate());
            map.put("instrument_type", "1");
            map.put(SR_ID, getPreference(SR_ID));
            map.put("due_amount", String.valueOf(dueAmount));
            map.put("paid_amount", TempData.advance_Collection);
            map.put("is_Pushed", "0");
            map.put(Tables.MESSAGE_CREATED_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "credit_collection");

        }
        double paymentAmount = 0.0;


        if (paid_amount != 0.0)
        //if(!etPayment.getText().toString().equalsIgnoreCase("")&&!etPayment.getText().toString().equalsIgnoreCase("0"))
        {
            Log.e("Check!", "Checkede!");

            java.text.DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
            Calendar cal = Calendar.getInstance();
            String paymentTempId = "P" + getPreference("SO") + dateFormat.format(cal.getTime());

            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put("payment_temp_id", paymentTempId);
            map2.put("outlet_id", TempData.OutletID);
            map2.put("memo_no", memoNo);
            map2.put("memo_value", TempData.gross_value);
            map2.put("due_amount", "0");


            if (paid_amount != 0.0) {
                if (paid_amount > memo_value)
                    paymentAmount = memo_value;
                else
                    paymentAmount = paid_amount;
            } else {
                paymentAmount = 0.0;
            }
            Log.e("PAYMENT_AMOUNT", "......" + paymentAmount);

            map2.put("paid_amount", String.valueOf(paymentAmount));
            map2.put("inst_type", "1");
            map2.put("bank_branch_id", "");
//		map2.put("collect_date", getCurrentDate());
            map2.put("collect_date", getCurrentDate());
//		map2.put("memo_date",getCurrentDate() );
            map2.put("memo_date", getCurrentDate());
            map2.put("is_pushed", "0");

            db.InsertTable(map2, "payments");


        }
    }

    public String SalesMemoNO() {
        final Calendar c = Calendar.getInstance();
        int year1 = c.get(Calendar.YEAR);
        int month1 = c.get(Calendar.MONTH);
        int day1 = c.get(Calendar.DAY_OF_MONTH);

        String day = "", month = "", year;


        String year_ = String.valueOf(year1);
        year = year_.substring(2, 4);


        if (day1 < 10)
            day = "0" + String.valueOf(day1);
        else
            day = String.valueOf(day1);
        if (month1 + 1 < 10)
            month = "0" + String.valueOf(month1 + 1);
        else
            month = String.valueOf(month1 + 1);
        String date = year1 + "-" + month + "-" + day;
        Log.e("OutletID", TempData.OutletID);
        Log.e("SO", getPreference("SO"));
        Log.e("Day", day);
        Log.e("year", year);
        Log.e("Month", month);
        Log.e("Hour", "" + c.get(c.HOUR_OF_DAY));
        Log.e("Minute", "" + c.get(c.MINUTE));
        Log.e("SECOND", "" + c.get(c.SECOND));


        memoNo = "M" + getPreference(SR_ID) + year + month + day + getCurrentTime24ForMemo();
        Log.e("MemoNo", memoNo);
        TempData.memoNumber = memoNo;

        return memoNo;
    }


    public long printDifference(Date startDate, Date endDate) {
        //milliseconds
        long different = endDate.getTime() - startDate.getTime();

        System.out.println("startDate : " + startDate);
        System.out.println("endDate : " + endDate);
        System.out.println("different : " + different);

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;


        return elapsedDays;
    }

    private void SaleAbleList() {
        Data = new ArrayList<HashMap<String, String>>();
        Data.clear();
        Log.e("data_view_ustiki", String.valueOf(Data.size()));
        String memo_id = "";
        if (SALE_STATE != MEMO_EDITING)
            memo_id = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + ", " + PRODUCT_PRICE_PRICE + ",vat FROM " + TABLE_NAME_ORDER_DETAILS + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "' AND product_type='0'";
        else
            memo_id = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + ", " + PRODUCT_PRICE_PRICE + ",vat , serial_number FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "' AND product_type='0'";

        Log.e("MemoDetails", memo_id);

        Cursor cursor = db.rawQuery(memo_id);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String product_name = "";
                do {
                    Cursor c1 = db.rawQuery("SELECT " + PRODUCT_PRODUCT_NAME + " From " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_ID + "='" + cursor.getString(0) + "'");
                    if (c1 != null) {
                        if (c1.moveToFirst()) {
                            do {
                                product_name = c1.getString(0);
                            } while (c1.moveToNext());
                        }

                    }
                    map = new HashMap<String, String>();
                    map.put(PRODUCT_PRODUCT_NAME, product_name);
                    map.put(PRODUCT_BOOLEAN_QUANTITY, String.valueOf(cursor.getDouble(1)));

                    Log.e("pricellists", roundTwoDecimals(cursor.getDouble(2)));
                    map.put(PRODUCT_PRICE_PRICE, roundTwoDecimals(cursor.getDouble(2)));
                    String tPrice = roundTwoDecimals(cursor.getDouble(1) * cursor.getDouble(2));

                    map.put("total_price", String.valueOf(tPrice));
                    map.put("vat", roundTwoDecimals(cursor.getDouble(3)) + "%");
                    map.put(PRODUCT_ID, cursor.getString(0));
                    if (SALE_STATE == MEMO_EDITING)
                        map.put(MEMO_DETAILS_Serial_NUMBER, cursor.getString(4));
                    Data.add(map);
                    subtotal = subtotal + Double.parseDouble(tPrice);
                } while (cursor.moveToNext());
            }
        }
        total_price.setText(subtotal + "");
        TempData.InvoiceTotal = String.valueOf(subtotal);

        Log.e("Size", String.valueOf(Data.size()));
        AdapterForMemoDetails adapter;
        AdapterForOrderDetails adapter1;
        if (SALE_STATE == MEMO_EDITING) {
            adapter = new AdapterForMemoDetails(DetailsOrderReport.this, Data);
            list.setAdapter(adapter);
        } else {
            adapter1 = new AdapterForOrderDetails(DetailsOrderReport.this, Data);
            list.setAdapter(adapter1);
        }

        TempData.INVOICE_DETAILS = Data;

        getDiscount(subtotal + "");

    }

    private void getDiscount(String s) {
        int distype = 0;
        Double discountp = 0.0;
        Cursor cursor = db.rawQuery("select discount_percent,discount_type from discounts where memo_value <=" + s + " and date_from <= '" + TempData.MemoDate + "' and date_to>='" + TempData.MemoDate + "' order by memo_value DESC limit 1");
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            discountp = cursor.getDouble(0);
            distype = TempData.DISTYPE = cursor.getInt(1);
        }
        Double discount = 0.0;
        Double memoValue = Double.parseDouble(s);
        if (distype == 1) {
            TempData.DISCOUNTP = discountp;
            discount = (memoValue * discountp) / 100;
        } else if (distype == 2) {

            discount = discountp;
        }
        discountt.setText(roundTwoDecimals(discount));
        sub_total.setText(roundTwoDecimals(memoValue - discount));


    }

    private void SaleAbleListForAll() {
        // TODO Auto-generated method stub


        ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();
        Data.clear();
        String memo_id = "";
        if (SALE_STATE != MEMO_EDITING)
            memo_id = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + ", " + PRODUCT_PRICE_PRICE + " FROM " + TABLE_NAME_ORDER_DETAILS + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "'";
        else
            memo_id = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + ", " + PRODUCT_PRICE_PRICE + " FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "'";

        Log.e("MemoDetails", memo_id);

        Cursor cursor = db.rawQuery(memo_id);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String product_name = "";
                do {
                    String productId = cursor.getString(0);
                    Log.e("productId: ", productId);
                    Cursor c1 = db.rawQuery("SELECT " + PRODUCT_PRODUCT_NAME + " From " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_ID + "='" + productId + "'");
                    Log.e("raw Query : ", "SELECT product_name From products WHERE product_id='" + productId + "'");
                    if (c1 != null) {
                        if (c1.moveToFirst()) {
                            do {
                                product_name = c1.getString(0);
                            } while (c1.moveToNext());
                        }

                    }
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put(PRODUCT_PRODUCT_NAME, product_name);
                    map.put(PRODUCT_BOOLEAN_QUANTITY, String.valueOf(cursor.getDouble(1)));
                    map.put(PRODUCT_PRICE_PRICE, roundTwoDecimals(cursor.getDouble(2)));
                    String tPrice = roundTwoDecimals(cursor.getDouble(1) * cursor.getDouble(2));
                    map.put("total_price", String.valueOf(tPrice));
                    map.put(PRODUCT_ID, cursor.getString(0));
                    Data.add(map);

                } while (cursor.moveToNext());
            }
        }
        Log.e("Size", String.valueOf(Data.size()));

	/*	adapter=new AdapterForMemoDetails(MemoDetails.this,Data);
		ListView inst_list=(ListView)findViewById(R.id.inst_list);
		inst_list.setAdapter(adapter);*/

        TempData.INVOICE_DETAILS_FOR_ALL_PRODUCT = Data;


    }

    private void Bonus() {
        Log.e("T1", TempData.TempBonus + "no data");
        TempData.TempBonus = "";
        Log.e("T22", TempData.TempBonus + "no data");

        // TODO Auto-generated method stub
        ArrayList<HashMap<String, String>> bonusList = new ArrayList<HashMap<String, String>>();

        String memoQuery = "";
        if (SALE_STATE != MEMO_EDITING) {
            memoQuery = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + " FROM " + TABLE_NAME_ORDER_DETAILS + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "' AND product_type='2'";
        } else {
            memoQuery = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + " , serial_number FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "' AND product_type='2'";
        }

        Log.e("memoQuery", memoQuery);
        Cursor cursor = db.rawQuery(memoQuery);
        Log.e("memoQueryCount", String.valueOf(cursor.getCount()));
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String product_name = "";
                do {
                    String productId = cursor.getString(0);
                    Cursor c1 = db.rawQuery("SELECT " + PRODUCT_PRODUCT_NAME + " From " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_ID + "='" + productId + "'");
                    if (c1 != null) {
                        if (c1.moveToFirst()) {
                            do {
                                product_name = c1.getString(0);
                            } while (c1.moveToNext());
                        }
                    }

                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("product_id", cursor.getString(0));
                    map.put("product_name", product_name);
                    map.put("quantity", cursor.getString(1));
                    if (SALE_STATE == MEMO_EDITING) {
                        map.put("serial_number", cursor.getString(2));
                        savePreference("serial_number" + cursor.getString(0), cursor.getString(2));
                    }
                    savePreference("bonus" + cursor.getString(0), cursor.getString(1));

                    bonusList.add(map);
                } while (cursor.moveToNext());
            }
        }

        String bonus = "";
        TempData.BonusShowList.clear();
        for (int i = 0; i < bonusList.size(); i++) {

            HashMap<String, String> map = bonusList.get(i);
            bonus = bonus + map.get("product_name") + "(" + map.get("quantity") + ")";

            HashMap<String, String> product_map = new HashMap<String, String>();
            Log.e("product_name", map.get("product_name"));
            product_map.put("product_name", map.get("product_name"));
            product_map.put("quantity", map.get("quantity"));
            product_map.put("serial_number", map.get("serial_number"));


//            BonusItemList.add(product_map);
            TempData.BonusShowList.add(product_map);

            if (i != bonusList.size() - 1)
                bonus = bonus + ", ";
        }
        if (bonus.length() > 0) {
            TempData.TempBonus = "";
            txtBonus.setText(bonus);
            TempData.TempBonus = bonus;
        } else {
            LinearLayout llBonus = (LinearLayout) findViewById(R.id.llBonus);
            llBonus.setVisibility(View.GONE);
        }
        //txtBonus.setText("Nill");
        //BonusArrayList=bonusList;

        Log.e("T", TempData.TempBonus + "no data");
    }

    public void SwitchToSaleOrder() {


        savePreference("OutletID", TempData.OutletID);
        Log.e("BonusArrayList:", "..........." + BonusArrayList.toString());
        Log.e("GiftArrayList:", "..........." + TempData.GiftArrayList.toString());


        new productToBolleanTable().execute();


    }

    @Override
    public void OnLocalDBdataRetrive(final String json) {

        runOnUiThread(new Runnable() {
            public void run() {
                bf.getResponceData(URL.CREATE_MEMO, json, 102);

            }
        });


    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }


    public class productToBolleanTable extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(DetailsOrderReport.this);
            progressDialog.setTitle("Please Wait.....");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            Cursor c3 = db.rawQuery("SELECT product_id,product_name,product_category_id,product_type_id FROM product  group by product_id,product_name,product_category_id,product_type_id ORDER BY product_order ASC");
            if (c3 != null) {
                if (c3.moveToFirst()) {
                    do {


                        String product_id = c3.getString(c3.getColumnIndex("product_id"));
                        String product_name = c3.getString(c3.getColumnIndex("product_name"));

                        String product_category_id = c3.getString(c3.getColumnIndex("product_category_id"));
                        String product_type_id = c3.getString(c3.getColumnIndex("product_type_id"));

                        boolean is_checked = false;


                        int saleable = getIndexSaleAble(product_id);
                        int gift = getIndexGift(product_id);
                        int bonus = getIndexBonus(product_id);
                        String product_serial = "";
                        String product_quantity = "0.00";
                        if (saleable >= 0) {
                            is_checked = true;
                            product_quantity = TempData.INVOICE_DETAILS.get(saleable).get("quantity");
                            product_serial = TempData.INVOICE_DETAILS.get(saleable).get(MEMO_DETAILS_Serial_NUMBER);

                        } else if (gift >= 0) {
                            is_checked = true;
                            product_quantity = TempData.GiftArrayList.get(gift).get("quantity");
                            product_serial = TempData.INVOICE_DETAILS.get(gift).get(MEMO_DETAILS_Serial_NUMBER);

                        }/*else if (bonus>=0){
                            is_checked=true;
                            product_quantity=TempData.BonusArrayList.get(bonus).get("quantity");
                            Log.e("type_id",product_type_id);


                        }*/

                        //TotalBonusProductList = BonusArrayList;


                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("outlet_id", getPreference("OutletID"));
                        map.put("product_id", product_id);
                        map.put("quantity", product_quantity);
                        map.put("boolean", is_checked + "");
                        map.put("product_category_id", product_category_id);
                        map.put("product_type_id", product_type_id);
                        map.put(MEMO_DETAILS_Serial_NUMBER, product_serial);

                        db.InsertTable(map, TABLE_NAME_PRODUCT_BOOLEAN);


                    } while (c3.moveToNext());

                }
            }

            setBonus();
            progressDialog.dismiss();
            Intent idn = new Intent(DetailsOrderReport.this, Sales_Memo.class);
            idn.putExtra("flag", 1);

            startActivity(idn);
            finish();


            return null;
        }
    }

    private void setBonus() {
        //prTotalBonusProductList
    }


    int getIndexSaleAble(String value) {
        int pos = -1;
        for (int i = 0; i < TempData.INVOICE_DETAILS.size(); i++) {
            HashMap<String, String> map = TempData.INVOICE_DETAILS.get(i);
            // Log.e("PRODUCT_ID salable", map.get("product_id"));
            if (map.get("product_id").equalsIgnoreCase(value))
                pos = i;
        }

        return pos;
    }

    int getIndexBonus(String value) {
        int pos = -1;
        for (int i = 0; i < BonusArrayList.size(); i++) {
            HashMap<String, String> map = BonusArrayList.get(i);
            // Log.e("PRODUCT_ID bonus", map.get("product_id"));
            if (map.get("product_id").equalsIgnoreCase(value))
                pos = i;
        }

        return pos;
    }

    int getIndexGift(String value) {
        int pos = -1;
        for (int i = 0; i < TempData.GiftArrayList.size(); i++) {
            HashMap<String, String> map = TempData.GiftArrayList.get(i);
            //Log.e("PRODUCT_ID gift", map.get("product_id"));
            if (map.get("product_id").equalsIgnoreCase(value))
                pos = i;
        }

        return pos;
    }

    private void Gift() {
        // TODO Auto-generated method stub
        ArrayList<HashMap<String, String>> giftList = new ArrayList<HashMap<String, String>>();

        String memoGIFTQuery = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + " FROM " + TABLE_NAME_ORDER_DETAILS + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "' AND product_type='1'";
        if (SALE_STATE == MEMO_EDITING) {
            memoGIFTQuery = "SELECT " + PRODUCT_ID + ", " + PRODUCT_BOOLEAN_QUANTITY + " , " + MEMO_DETAILS_Serial_NUMBER + " FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "' AND product_type='1'";
        }
        Log.e("memoGIFTQuery", memoGIFTQuery);
        Cursor cursor = db.rawQuery(memoGIFTQuery);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                String product_name = "";
                do {

                    String productId = cursor.getString(0);
                    Cursor c1 = db.rawQuery("SELECT " + PRODUCT_PRODUCT_NAME + " From " + TABLE_NAME_PRODUCT + " WHERE " + PRODUCT_ID + "='" + productId + "'");

                    if (c1 != null) {
                        if (c1.moveToFirst()) {
                            do {
                                product_name = c1.getString(0);

                            } while (c1.moveToNext());
                        }

                    }
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("product_id", cursor.getString(0));
                    map.put("product_name", product_name);
                    map.put("quantity", cursor.getString(1));
                    if (SALE_STATE == MEMO_EDITING)
                        map.put("serial_number", cursor.getString(2));
                    giftList.add(map);
                } while (cursor.moveToNext());
            }
        }

        Log.e("giftList : ", giftList.toString());

        String gift = "";
        for (int i = 0; i < giftList.size(); i++) {
            HashMap<String, String> map = giftList.get(i);
            gift = gift + map.get("product_name") + "(" + map.get("quantity") + ")";
            if (i != giftList.size() - 1)
                gift = gift + ", ";

            Log.e("gift: ", gift);
        }
        if (gift.length() > 0) {
            txtGift.setText(gift);
            TempData.TempGift = gift;
        } else {
            LinearLayout llGift = (LinearLayout) findViewById(R.id.llGift);
            llGift.setVisibility(View.GONE);
        }
        TempData.GiftArrayList = giftList;
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (i == 111) {

            db.excQuery("delete from " + TABLE_NAME_ORDER + " where " + ORDER_order_number + " ='" + TempData.orderNumber + "'");
            db.excQuery("delete from " + TABLE_NAME_ORDER_DETAILS + " where " + ORDER_order_number + " ='" + TempData.orderNumber + "'");
            finish();
        } else if (i == 102) {


            try {
                if (jsonObject.getJSONObject("memo").getString("status").equalsIgnoreCase("1")) {
                    db.excQuery("update ORDER_table set is_complete='1' , status = '2' WHERE order_number = '" + TempData.orderNumber + "'");
                    Toast.makeText(this, jsonObject.getJSONObject("memo").getString("message"), Toast.LENGTH_LONG).show();
                    cancel.setVisibility(View.GONE);

                    SALE_STATE = MEMO_EDITING;
                    Back = 1;


                } else {

                    db.excQuery("delete from memos  WHERE order_number = '" + memoNo + "'");
                    db.excQuery("delete from memo_details  WHERE order_number = '" + memoNo + "'");
                    Toast.makeText(this, jsonObject.getJSONObject("memo").getString("message"), Toast.LENGTH_LONG).show();


                }
            } catch (JSONException e) {
                e.printStackTrace();
            }


            Log.e("json", jsonObject.toString());

        } else if (SALE_STATE == Converting_TO_MEMO) {
            startActivity(new Intent(DetailsOrderReport.this, DeliveryReport.class));
            finish();
            Log.e("memoredirect", "Order_Report_Activity");
        }


    }


    @Override
    public void OnConnetivityError() {

    }


    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (Back == 1) {
                startActivity(new Intent(DetailsOrderReport.this, DeliveryReport.class));
                finish();
            }
            if (SALE_STATE == MEMO_EDITING) {
                startActivity(new Intent(DetailsOrderReport.this, MemoReport.class));
                finish();

            } else if (SALE_STATE == Converting_TO_MEMO) {
                startActivity(new Intent(DetailsOrderReport.this, DeliveryReport.class));
                finish();

            } else if (SALE_STATE == 0) {

                startActivity(new Intent(DetailsOrderReport.this, Order_Report_Activity.class));
                finish();
            }

        }
        return super.onKeyDown(keyCode, event);

    }


}
