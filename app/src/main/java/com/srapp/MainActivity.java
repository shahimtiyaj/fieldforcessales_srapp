package com.srapp;

import android.Manifest;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.bxl.config.editor.BXLConfigLoader;
import com.google.android.material.textfield.TextInputLayout;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Difine;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.Parent;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.srapp.kotlin.GPSTracking;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import NewPrint.BixolonPrinter;
import kotlin.jvm.JvmMultifileClass;

import static com.srapp.Db_Actions.Tables.Allfild;
import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MARKETS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_OUTLETS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ROOT;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_THANA;

public class MainActivity extends Parent implements BasicFunctionListener, DBListener {

    BasicFunction basicFunction;

    TextView textDummyHintUsername;
    TextView textDummyHintPassword;
    EditText editUsername;
    EditText editPassword;
    ImageView login_button;

    Data_Source ds;
    ArrayList<String> data, bbData;
    private int portType = BXLConfigLoader.DEVICE_BUS_USB;
    private String logicalName = "SRP-E302";
    private String address = "";
    static BixolonPrinter printer;
    TextInputLayout usertextinput, passwordtextinput;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        basicFunction = new BasicFunction(this, this);
        ds = new Data_Source(this, this, this);

        printer = new BixolonPrinter(this);
        textDummyHintUsername = (TextView) findViewById(R.id.text_dummy_hint_username);
        textDummyHintPassword = (TextView) findViewById(R.id.text_dummy_hint_password);
        editUsername = (EditText) findViewById(R.id.edit_username);
        editPassword = (EditText) findViewById(R.id.edit_password);
        TextView version = findViewById(R.id.version);
        version.setText(URL.VERSION_txt);

        usertextinput = (TextInputLayout) findViewById(R.id.userTextinputLayout);
        passwordtextinput = (TextInputLayout) findViewById(R.id.passwordTextinputLayout);
        if (checkForPermission()) {
            TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            String MAC_Number = telephonyManager.getDeviceId();
            basicFunction.savePreference("mac", MAC_Number);
        }

        if (!basicFunction.getPreference("sr_name").equalsIgnoreCase("null")) {

            editUsername.setText(basicFunction.getPreference("sr_name"));
            editUsername.setEnabled(false);
        }

        login_button = findViewById(R.id.login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                login_button.setEnabled(false);

                JSONObject jsonObject = new JSONObject();
                try {

                    Log.e("permission", checkForPermission() + "");
                    if (checkForPermission()) {

                        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
                        String MAC_Number = telephonyManager.getDeviceId();

                        basicFunction.savePreference("mac", MAC_Number);
                        jsonObject.put(Difine.USERENAME, editUsername.getText().toString().trim());
                        jsonObject.put("password", editPassword.getText().toString().trim());
                        jsonObject.put("mac", basicFunction.getPreference("mac"));
                        jsonObject.put("version", URL.VERSION);
                        basicFunction.getResponceData(URL.Login, jsonObject.toString(), 101);
                        Log.e("map : ", jsonObject.toString());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }


        });

        editUsername.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                            textDummyHintUsername.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    // Required to show/hide white background behind floating label during focus change
                    if (editUsername.getText().length() > 0)
                        textDummyHintUsername.setVisibility(View.VISIBLE);
                    else
                        textDummyHintUsername.setVisibility(View.INVISIBLE);
                }
            }
        });

        // Password
        editPassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //passwordtextinput.setBackgroundResource(R.drawable.focused);
                            // Show white background behind floating label
                            textDummyHintPassword.setVisibility(View.VISIBLE);
                        }
                    }, 100);
                } else {
                    // Required to show/hide white background behind floating label during focus change
                    if (editPassword.getText().length() > 0)
                        textDummyHintPassword.setVisibility(View.VISIBLE);
                    else
                        textDummyHintPassword.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int RequestCode) {
        Log.e("Json_data_after_login", jsonObject.toString());
        try {
            switch (RequestCode) {
                case 101:
                    login_button.setEnabled(true);
                    if (jsonObject.getJSONArray("response").getJSONObject(0).getString("status").equalsIgnoreCase("1")) {

                        basicFunction.savePreference("sr_name", editUsername.getText().toString().trim());
                        basicFunction.savePreference("password", editPassword.getText().toString().trim());
                        basicFunction.savePreference("office_id", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("office_id"));
                        basicFunction.savePreference("territory_id", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("territory_id"));
                        //basicFunction.savePreference("store_id",jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("store_id")); // does not for new version -accepted from bakend-10-01-2020
                        basicFunction.savePreference("sales_person_id", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("sales_person_id"));
                        basicFunction.savePreference("office_name", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("office_name"));
                        basicFunction.savePreference("office_address", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("office_address"));
                        basicFunction.savePreference("office_phone", jsonObject.getJSONArray("response").getJSONObject(0).getJSONObject("user_info").getString("office_phone"));

                       /* //db Info-----------------------------------------------
                        basicFunction.savePreference("db_id",jsonObject.getJSONArray("response").getJSONObject(1).getJSONObject("db_info").getString("db_id"));
                        basicFunction.savePreference("db_name",jsonObject.getJSONArray("response").getJSONObject(1).getJSONObject("db_info").getString("db_name"));
                        basicFunction.savePreference("db_store_id",jsonObject.getJSONArray("response").getJSONObject(1).getJSONObject("db_info").getString("db_store_id"));
*/
                        Log.e("office_name", getPreference("office_name"));

                        ds.getlastupdateddate();

                    } else {
                        Toast.makeText(this, jsonObject.getJSONArray("response").getJSONObject(0).getString("message"), Toast.LENGTH_LONG).show();
                    }


                    break;
                case 102:
                    ds.excQuery("delete  from product_history");
                    ds.excQuery("delete  from instrument_type");
                    ds.excQuery("delete  from location");
                    ds.excQuery("delete  from materials");
                    ds.excQuery("delete  from stock_info");
                    ds.excQuery("delete  from product_serials");
                    ds.excQuery("delete from memos");
                    ds.excQuery("delete from memo_details");

                    //i added here
                    ds.excQuery("delete from fiscal_year");

                    ds.excQuery("delete from " + Tables.TABLE_NAME_Bonus_Card_type);

                    //--------clear for duplicate data -17-01-2020----------
                    ds.excQuery("delete from " + Tables.TABLE_NAME_THANA);
                    ds.excQuery("delete from " + Tables.TABLE_NAME_OUTLETS);
                    ds.excQuery("delete from " + Tables.TABLE_NAME_ROOT);
                    ds.excQuery("delete from " + Tables.TABLE_NAME_MARKETS);
                    ds.excQuery("delete from product");
                    ds.excQuery("delete from product_categories");
                    ds.excQuery("delete from outlets");
                    ds.excQuery("delete from outlet_categories");
                    ds.excQuery("delete from bonuses");
                   // ds.excQuery("delete from order_details");//----------------
                   // ds.excQuery("delete from order_table");//------------------

                    ds.insertData(jsonObject.getJSONObject("response").toString());
                    //basicFunction.getResponceData(URL.Log,jsonObject.getJSONObject("response").toString(),11);
                    Log.e("json_latest", jsonObject.getJSONObject("response").toString());
                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Log.e("jexp", e.getMessage());
        }

        // startActivity(new Intent(MainActivity.this, Dashboard.class));
    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(this, "NO Internet Connection", Toast.LENGTH_SHORT);

    }

    public static String SO_Tracking(String so_id,String mac)
    {

        String json = "";

        // 3. build jsonObject
        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("so_id", so_id);
            jsonObject.put("mac", mac);

        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        json = jsonObject.toString();

        return json;
    }

    @Override
    public void OnLocalDBdataRetrive(final String json) {
        Log.e("Json_after_login", json);


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (!json.equalsIgnoreCase("done")) {
                        JSONObject jsonObject = new JSONObject(json);
                        jsonObject.put("mac", basicFunction.getPreference("mac"));
                        basicFunction.getResponceData(URL.PULL, jsonObject.toString(), 102);
                    }

                    if (json.equalsIgnoreCase("done")) {
                        startActivity(new Intent(MainActivity.this, Dashboard.class));
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //basicFunction.getResponceData(URL.Log,createJWT("push",json), 101);
                //generateNoteOnSD(MainActivity.this,"log.txt",json);
            }
        });

    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }


    public void generateNoteOnSD(Context context, String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory() + File.separator + "log");
            if (!root.exists()) {
                root.mkdirs();
            }

            File gpxfile = new File(root, sFileName);
            FileWriter writer = new FileWriter(gpxfile);
            writer.append(sBody);
            writer.flush();
            writer.close();
            Toast.makeText(context, "Saved" + gpxfile.getAbsolutePath(), Toast.LENGTH_LONG).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private boolean checkForPermission() {
        //  Log.e("tag", "Permission");
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
            }

            return false;
        } else {
            return true;
        }

    }

}
