package com.srapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.AdapterForProductWiseSaleReport;
import com.srapp.Adapter.AdapterForSalesReport;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.Parent;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.srapp.Db_Actions.Tables.PRODUCT_PRODUCT_NAME;
import static com.srapp.Db_Actions.Tables.SR_ID;

public class ProductWiseDeliveryReport extends Parent implements BasicFunctionListener {
    ImageView img;
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    Button startdate, enddate;
    HashMap<String, ArrayList<String>> outlate = new HashMap<>();
    HashMap<String, ArrayList<String>> outlet_catagary = new HashMap<>();
    ListView listview;
    Data_Source db;
    TextView total_price,ec,oc,tqty;
    Double sum=0.0;
    BasicFunction bf;
    Spinner outlatesp, outlet_catagary_spinner;
    ArrayList<HashMap<String, String>> list;
    boolean onResunme=false;
    ImageView homeBtn,backBtn;
    TextView userIdTV,titleTV ;
    Spinner productsp;
    String product_id="0";
    ArrayList<String> productIdList = new ArrayList<>();
    ArrayList<String> productNameList = new ArrayList<>();

    //Distributor filtering--------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorSpinner;
    BasicFunction basicFunction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_wise_delivery_report);

        bf = new BasicFunction(this,this);
        db = new Data_Source(this);

        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();

        //View model factory class
        basicFunction = new BasicFunction(this,this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {
                distList.add("Select Distributor");
                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);
        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);
        ec = findViewById(R.id.ec);
        oc = findViewById(R.id.oc);
        tqty = findViewById(R.id.tqty);
        productsp = findViewById(R.id.productsp);
        setProductSp();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        total_price = findViewById(R.id.total_price);
        startdate = findViewById(R.id.start_date);
        list = new ArrayList<>();
        enddate = findViewById(R.id.end_date);
        listview = findViewById(R.id.list_view);
        img = findViewById(R.id.home);
        setDateTimeField();
        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductWiseDeliveryReport.this, Dashboard.class));
            }
        });
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePickerDialog.show();
            }
        });

        DataView();

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( ProductWiseDeliveryReport.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( ProductWiseDeliveryReport.this, Reports_Activity.class));
                finish();
            }
        });

        distributorSpinner = findViewById(R.id.distributor_spinner);

         distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                //Log.e("distributorId_from_spin", distributorStoreId);

                if (distributorStoreId!=null){

                }

                else {
                  //  Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        productsp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {
                TextView textView = (TextView)arg0.getChildAt(0);
                if (textView!=null) {
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0, 0, 0, 0);
                }

                product_id = productIdList.get(arg2);

                Log.e("product_id_b", product_id);

                if (!product_id.equals("0") && distributorStoreId!=null){
                    DataView();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
    }


    private void setProductSp() {
        Cursor c = db.rawQuery("select product_id, product_name from product");

        c.moveToFirst();
        if (c!=null && c.getCount()>0){
            do{
                productIdList.add(c.getString(0));
                productNameList.add(c.getString(1));


            }while (c.moveToNext());

        }
            productNameList.add(0,"Select Product");
            productIdList.add(0,"0");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(ProductWiseDeliveryReport.this, R.layout.spinner_text, productNameList);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        productsp.setAdapter(dataAdapter);

    }


    private void setDateTimeField() {
        bf.savePreference("start_Date", bf.getCurrentDate());
        bf.savePreference("end_date", bf.getCurrentDate());
        startdate.setText(bf.getCurrentDate());
        enddate.setText(bf.getCurrentDate());


        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("start_Date", dateFormatter.format(newDate.getTime()));
                startdate.setText(bf.getPreference("start_Date"));
                DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));




        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("end_date", dateFormatter.format(newDate.getTime()));
                enddate.setText(bf.getPreference("end_date"));
                DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));



    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (i==101){
            try {
                list.clear();
                JSONArray jsonArray = jsonObject.getJSONArray("product_order_report");
                JSONObject object = jsonArray.getJSONObject(0);
                ec.setText(object.getString("total_ec"));
                oc.setText(object.getString("total_oc"));
                tqty.setText(object.getString("total_qty"));
                JSONArray jsonArray1 = object.getJSONArray("report_details");

                for (int pos = 0 ; pos<jsonArray1.length(); pos++){

                    HashMap<String,String>  map = new HashMap<>();

                    map.put("dist_order_no",jsonArray1.getJSONObject(pos).getString("order_no"));
                    map.put("order_date",jsonArray1.getJSONObject(pos).getString("order_date"));
                    map.put("outlet_name",jsonArray1.getJSONObject(pos).getString("outlet_name"));
                    map.put("sales_qty",jsonArray1.getJSONObject(pos).getString("sales_qty"));
                    list.add(map);


                }


                AdapterForProductWiseSaleReport adapterForProductWiseSaleReport = new AdapterForProductWiseSaleReport(this,list);
                listview.setAdapter(adapterForProductWiseSaleReport);

            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("jsone",e.getMessage());
            }

        }
    }

    @Override
    public void OnConnetivityError() {



    }

    private void DataView() {

        if (!product_id.equalsIgnoreCase("0")) {

            Log.e("DataView", "DataView");

            if (bf.isInternetOn()) {

                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("start_date", startdate.getText().toString().trim());
                    jsonObject.put("end_date", enddate.getText().toString().trim());
                    jsonObject.put(SR_ID, bf.getPreference(SR_ID));
                    jsonObject.put("mac", bf.getPreference("mac"));
                    jsonObject.put("product_id", product_id);
                    jsonObject.put("db_id", distributorStoreId);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                bf.getResponceData(URL.PRODUCT_WISE_SALES, jsonObject.toString(), 101);
            } else {
                Toast.makeText(this, "NO Internet Connection", Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(ProductWiseDeliveryReport.this,"Select A Product First",Toast.LENGTH_SHORT).show();
        }



    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( ProductWiseDeliveryReport.this, Reports_Activity.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
