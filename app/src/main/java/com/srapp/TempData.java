package com.srapp;

import java.security.Provider;
import java.util.AbstractCollection;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

public class TempData {

    public static  Double DISCOUNTP =0.0 ;
    public static  int DISTYPE =0 ;
    public static  Double DISCOUNT =0.0 ;
    public static  Double VAT =0.0 ;
    public static ArrayList<HashMap<String,String>> INVOICE_DETAILS_FOR_ALL_PRODUCT =new ArrayList<>() ;
    public static String editMemo="";
    public static String orderNumber ="";
    public static String memoNumber ="";
    public static String sales_person_id="20394";
    public static String TargetCustomer="";
    public static String MemoDateTime="";
    public static String InstituteID="";
    public static int dist_selection_id=0;


    public static String OutletCatagoryID="";
    public static String SalesUPricePosition="";
    public static String SalesUPrice="";
    public static String Test="";
    public static ArrayList<HashMap<String,String>> BonusShowList = new ArrayList<>();
    public static ArrayList<HashMap<String,String>> INVOICE_DETAILS = new ArrayList<>();
    public static ArrayList<HashMap<String,String>> INVOICE_DETAILS_DRAFT_PRINT = new ArrayList<>();
    public static ArrayList<HashMap<String,String>> TotalBonusProductList = new ArrayList<>();
    public static ArrayList<HashMap<String,String>> BonusProductQuantity = new ArrayList<>();
    public static ArrayList<String> BonusChxSelected = new ArrayList<>();
    public static String OutletID="";
    public static String DB_ID="";
    public static String DB_ID_ORDER_REPORT="";
    public static String OutletName="";
    public static String SaleScreen="";
    public static String MarketID="";
    public static String tempThana="";
    public static String tempMarket="";


    public static String TempBonus="";
    public static String TempExtraBonus="";
    public static String From_App="";
    public static String for_memo_delete="";
    public static String MemoDate="";
    public static String DayCloseMemoEditable="";
    public static String InvoiceTotal="";
    public static String TempGift="";
    public static String CurrentInventoryID="";
    public static String SelectedOrderID;
    public static String MemoTotalView;
    public static String isPushed;
    public static String TeritoryID;
    public static String gross_value;
    public static ArrayList<HashMap<String, String>> BonusArrayList=new ArrayList<>();
    public static ArrayList<HashMap<String, String>> GiftArrayList=new ArrayList<>();
    public static int ORDER_STATUE;
    public static int ORDER_PlanVisit;
    public static double Advance_collection;
    public static int ORDER_plan_id;
    public static int ORDER_TO_MEMO;
    public static int SALE_STATE=0;
    public static int Converting_TO_MEMO=111;
    public static int MEMO_EDITING=101;
    public static boolean MEMO_EDIT;
    public static String InvoicePayment;
    public static String InvoiceGrand;
    public static String printBlankContent;
    public static String printTopContent;
    public static String PrintOutlate;
    public static String PrintBody;
    public static String printBottomContent;
    public static String printName;
    public static String AllprintName;
    public static String printboldthank;
    public static String printThanku;
    public static String AllprintValue;
    public static String printTopContentProductReceive;
    public static String SO_Stock_print;
    public static String printBottonText;
    public static int Back;

    public static String advance_Collection;
    static public boolean ismemo = false;
}
