package com.srapp.kotlin

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class GPSTracking {

    @SerializedName("tracking_list")
    @Expose
    private var trackingList: List<TrackingList?>? = null

    fun getTrackingList(): List<TrackingList?>? {
        return trackingList
    }

    fun setTrackingList(trackingList: List<TrackingList?>?) {
        this.trackingList = trackingList
    }

    class TrackingList {
        @SerializedName("LiveSalesTrack")
        @Expose
        private var liveSalesTrack: LiveSalesTrack? = null

        fun getLiveSalesTrack(): LiveSalesTrack? {
            return liveSalesTrack
        }

        fun setLiveSalesTrack(liveSalesTrack: LiveSalesTrack?) {
            this.liveSalesTrack = liveSalesTrack
        }
    }

    class LiveSalesTrack {
        @SerializedName("id")
        @Expose
        var id: String? = null

        @SerializedName("start_time")
        @Expose
        var startTime: String? = null

        @SerializedName("end_time")
        @Expose
        var endTime: String? = null

        @SerializedName("interval")
        @Expose
        var interval: String? = null
    }

}