package com.srapp.kotlin

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.srapp.Model.Market
import com.srapp.Model.Product
import okhttp3.Route

@JsonIgnoreProperties(ignoreUnknown = true)
class Distributer {
    @SerializedName("response")
    @Expose
    private var response: Response? = null

    fun getResponse(): Response? {
        return response
    }

    fun setResponse(response: Response?) {
        this.response = response
    }

    class Response {
        @SerializedName("user_info")
        @Expose
        var userInfo: UserInfo? = null

        @SerializedName("db_info")
        @Expose
        var dbInfo: List<DbInfo>? = null

        @SerializedName("message")
        @Expose
        var message: List<Any>? = null

        @SerializedName("current_promotion_products")
        @Expose
        var currentPromotionProducts: List<Any>? = null

        @SerializedName("product")
        @Expose
        var product: List<Product>? = null


       /* @SerializedName("product_categories")
        @Expose
        var productCategories: List<ProductCategory>? = null
*/
        @SerializedName("product_price")
        @Expose
        var productPrice: List<Any>? = null

     /*   @SerializedName("product_type")
        @Expose
        var productType: List<ProductType>? = null*/

        @SerializedName("product_combinations")
        @Expose
        var productCombinations: List<Any>? = null

        @SerializedName("territories")
        @Expose
        var territories: List<Any>? = null

        @SerializedName("thana")
        @Expose
        var thana: List<Any>? = null

        @SerializedName("route")
        @Expose
        var route: List<Route>? = null

        @SerializedName("markets")
        @Expose
        var markets: List<Market>? = null

        @SerializedName("outlets")
        @Expose
        var outlets: List<Any>? = null

  /*      @SerializedName("bank_branch")
        @Expose
        var bankBranch: List<BankBranch>? = null

        @SerializedName("product_history")
        @Expose
        var productHistory: List<ProductHistory>? = null

        @SerializedName("instrument_type")
        @Expose
        var instrumentType: List<InstrumentType>? = null

        @SerializedName("location")
        @Expose
        var location: List<Location>? = null

        @SerializedName("materials")
        @Expose
        var materials: List<Material>? = null

        @SerializedName("sales_week")
        @Expose
        var salesWeek: List<SalesWeek>? = null*/

        @SerializedName("unit")
        @Expose
        var unit: List<Any>? = null

        @SerializedName("orders")
        @Expose
        var orders: List<Any>? = null

        @SerializedName("memos")
        @Expose
        var memos: List<Any>? = null

        @SerializedName("general_notice")
        @Expose
        var generalNotice: List<Any>? = null

      /*  @SerializedName("outlet_categories")
        @Expose
        var outletCategories: List<OutletCategory>? = null

        @SerializedName("bonuses")
        @Expose
        var bonuses: List<Bonuse>? = null

        @SerializedName("discounts")
        @Expose
        var discounts: List<Discount>? = null*/

        @SerializedName("visit_list")
        @Expose
        var visitList: List<Any>? = null

        @SerializedName("stock_info")
        @Expose
        var stockInfo: List<Any>? = null

        @SerializedName("product_serials")
        @Expose
        var productSerials: List<Any>? = null

        @SerializedName("bonus_card_types")
        @Expose
        var bonusCardTypes: List<Any>? = null

       /* @SerializedName("fiscal_year")
        @Expose
        var fiscalYear: List<FiscalYear>? = null*/

        class DbInfo {
            @SerializedName("db_id")
            @Expose
            private var dbId: String? = null

            @SerializedName("db_name")
            @Expose
            private var dbName: String? = null

            @SerializedName("db_store_id")
            @Expose
            private var dbStoreId: String? = null

            fun getDbId(): String? {
                return dbId
            }

            fun setDbId(dbId: String?) {
                this.dbId = dbId
            }

            fun getDbName(): String? {
                return dbName
            }

            fun setDbName(dbName: String?) {
                this.dbName = dbName
            }

            fun getDbStoreId(): String? {
                return dbStoreId
            }

            fun setDbStoreId(dbStoreId: String?) {
                this.dbStoreId = dbStoreId
            }

        }


        class UserInfo {
            @SerializedName("sales_person_id")
            @Expose
            private var salesPersonId: Any? = null

            @SerializedName("office_id")
            @Expose
            private var officeId: Any? = null

            @SerializedName("territory_id")
            @Expose
            private var territoryId: Any? = null

            @SerializedName("sr_id")
            @Expose
            private var srId: Any? = null

            @SerializedName("company_id")
            @Expose
            private var companyId: Any? = null

            fun getSalesPersonId(): Any? {
                return salesPersonId
            }

            fun setSalesPersonId(salesPersonId: Any?) {
                this.salesPersonId = salesPersonId
            }

            fun getOfficeId(): Any? {
                return officeId
            }

            fun setOfficeId(officeId: Any?) {
                this.officeId = officeId
            }

            fun getTerritoryId(): Any? {
                return territoryId
            }

            fun setTerritoryId(territoryId: Any?) {
                this.territoryId = territoryId
            }

            fun getSrId(): Any? {
                return srId
            }

            fun setSrId(srId: Any?) {
                this.srId = srId
            }

            fun getCompanyId(): Any? {
                return companyId
            }

            fun setCompanyId(companyId: Any?) {
                this.companyId = companyId
            }
        }

    }


}