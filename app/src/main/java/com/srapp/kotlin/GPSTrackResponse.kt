package com.srapp.kotlin

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName


class GPSTrackResponse {
    @SerializedName("response")
    @Expose
    private var response: Response? = null
    fun getResponse(): Response? {
        return response
    }

    fun setResponse(response: Response?) {
        this.response = response
    }

    class Response {
        @SerializedName("status")
        @Expose
        private var status: Int? = null

        @SerializedName("message")
        @Expose
        private var message: String? = null

        fun getStatus(): Int? {
            return status
        }

        fun setStatus(status: Int?) {
            this.status = status
        }

        fun getMessage(): String? {
            return message
        }

        fun setMessage(message: String?) {
            this.message = message
        }

    }
}