package com.srapp.kotlin

import android.util.Log
import androidx.lifecycle.MutableLiveData
import com.google.gson.JsonObject
import com.srapp.apiService.ApiClient
import com.srapp.apiService.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DataRepository() {

    val distributorList = MutableLiveData<List<Distributer.Response.DbInfo>>()
    val gpsTrackingList = MutableLiveData<List<GPSTracking.TrackingList>>()
    val gpsPushStatus = MutableLiveData<String>()

    fun getDistributorData(data: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getDistributorData(data)
        val emptyList = ArrayList<Distributer.Response.DbInfo>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<Distributer> {
            override fun onResponse(call: Call<Distributer>, response: Response<Distributer>) {
                if (response.isSuccessful) {
                    Log.d("onResponse", "Dist:" + response.body())
                    distributorList.postValue(response.body()!!.getResponse()?.dbInfo as List<Distributer.Response.DbInfo>)
                } else {
                    distributorList.postValue(emptyList)
                }
            }

            override fun onFailure(call: Call<Distributer>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

    fun getGPSTrackingData(data: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.getGPSTrackingData(data)
        val emptyList = ArrayList<GPSTracking.TrackingList>()
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<GPSTracking> {
            override fun onResponse(call: Call<GPSTracking>, response: Response<GPSTracking>) {
                if (response.isSuccessful) {
                    Log.d("onResponse", "Tracking List:" + response.body())
                    gpsTrackingList.postValue(response.body()!!.getTrackingList() as List<GPSTracking.TrackingList>)
                } else {
                    gpsTrackingList.postValue(emptyList)
                }
            }

            override fun onFailure(call: Call<GPSTracking>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }


    fun postGPSTrackingData(data: JsonObject) {
        val service = ApiClient.getClient?.create(ApiInterface::class.java)
        val call = service?.postGPSTrackingData(data)
        //calling the api ---------------------------------------------
        call?.enqueue(object : Callback<GPSTrackResponse> {
            override fun onResponse(call: Call<GPSTrackResponse>, response: Response<GPSTrackResponse>) {
                if (response.isSuccessful) {
                    Log.d("onResponse", "Map Data send successful:" + response.body()?.getResponse()?.getMessage())
                    gpsPushStatus.postValue(response.body()?.getResponse()?.getStatus().toString())
                } else {
                    Log.d("onResponse", "Map data not send:" + response.body())
                }
            }

            override fun onFailure(call: Call<GPSTrackResponse>, t: Throwable) {
                Log.d("onFailure", t.toString())
            }
        })
    }

}