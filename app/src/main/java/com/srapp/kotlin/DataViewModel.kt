package com.srapp.kotlin

import android.app.Application
import android.util.Log
import androidx.lifecycle.*
import com.google.gson.JsonObject


class DataViewModel(application: Application) : AndroidViewModel(application){

    private val dataRepository = DataRepository()
     val distributorList = dataRepository.distributorList
     val gpsTrackingList = dataRepository.gpsTrackingList
     private var gpsPushStatus = dataRepository.gpsPushStatus

    lateinit var distributorId: String
    lateinit var distributorStoreId: String

    fun getCurrentStatus(): MutableLiveData<String> {
        if (gpsPushStatus == null) {
            gpsPushStatus = MutableLiveData()
        }
        return gpsPushStatus
    }

    fun getDistributorData(data: JsonObject) {
        dataRepository.getDistributorData(data)
    }

    var tempSectionList: LiveData<java.util.ArrayList<String>> = Transformations.map(distributorList) {
        getTempDistList(it)
    }

    private fun getTempDistList(sectionList: List<Distributer.Response.DbInfo>): ArrayList<String> {
        val tempSectionList = ArrayList<String>()
        tempSectionList.add("Select Distributor")
        sectionList.forEach { section -> tempSectionList.add(section.getDbName().toString()) }
        return tempSectionList
    }

    fun getDistributorId(distributorName: String) {
        distributorList.value!!.forEach { distributor ->
            if (distributor.getDbName()!!.equals(distributorName, true)) {
                distributorId = distributor.getDbStoreId()!!.toString()

                Log.e("dist_id_viewmodel", distributorId);
            }
        }
    }

    fun getDistributorStoreId(distributorStoreName: String) {
        distributorList.value!!.forEach { distributor ->
            if (distributor.getDbName()!!.equals(distributorStoreName, true)) {
                distributorStoreId = distributor.getDbId()!!.toString()

                Log.e("dist_Store_id_viewmodel", distributorStoreId);
            }
        }
    }


    fun getGPSTrackingData(data: JsonObject) {
        dataRepository.getGPSTrackingData(data)
    }

    fun postGPSTrackingData(data: JsonObject) {
        dataRepository.postGPSTrackingData(data)
    }

}

    class DataViewModelFactory(var application: Application) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(DataViewModel::class.java)) {
                return DataViewModel(application) as T
            }

            throw IllegalStateException("Unknown ViewModel class")
        }
    }
