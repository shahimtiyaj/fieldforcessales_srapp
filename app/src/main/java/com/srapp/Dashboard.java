package com.srapp;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Util.Parent;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.GPSTracking;
import com.srapp.service.MyServiceStart;
import com.srapp.service.MyServiceStop;
import com.srapp.service.TimerService;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class Dashboard extends Parent implements BasicFunctionListener {

    Button reportBtn, stockBtn, accountBtn, syncBtn, toolsBtn, deliveryBtn;
    ImageView backBtn, homeBtn;
    LinearLayout order_or_delivery;
    TextView cash_number, oc_value, userIdTV, titleTV;
    Data_Source ds;
    BasicFunction basicFunction;
    ArrayList<String> datelist;

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
            builder.setCancelable(true);
            builder.setTitle("App close");
            builder.setMessage("Do You Want to Exit?");
            builder.setPositiveButton("yes",
                    new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            AlertDialog dialog = builder.create();
            dialog.show();

            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        basicFunction = new BasicFunction(this, this);
        ds = new Data_Source(this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getGPSTrackingData(ds.getGPSJson(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<GPSTracking.TrackingList>> distributorObserver = new Observer<List<GPSTracking.TrackingList>>() {
            @Override
            public void onChanged(List<GPSTracking.TrackingList> dbInfos) {

                dbInfos.get(0).getLiveSalesTrack().getStartTime();
                dbInfos.get(0).getLiveSalesTrack().getEndTime();
                dbInfos.get(0).getLiveSalesTrack().getInterval();

                savePreference("start_time", dbInfos.get(0).getLiveSalesTrack().getStartTime());
                savePreference("end_time",  dbInfos.get(0).getLiveSalesTrack().getEndTime());
                savePreference("interval",   dbInfos.get(0).getLiveSalesTrack().getInterval());
                savePreference("gps_bts", "TEST");

                Log.e("gps_start_date", dbInfos.get(0).getLiveSalesTrack().getStartTime());
                Log.e("gps_end_date", dbInfos.get(0).getLiveSalesTrack().getEndTime());
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getGpsTrackingList().observe(this, distributorObserver);

        //----------------------------GPS Tracker Start---------------------------
        datelist = new ArrayList<String>();
        SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy");
        for (int i = 0; i < 7; i++) {
            Calendar calendar = new GregorianCalendar();
            calendar.add(Calendar.DATE, i);
            String day = sdf2.format(calendar.getTime());
            datelist.add(day);
        }

        for (int i = 0; i < datelist.size(); i++) {


            Calendar c = Calendar.getInstance();
            SimpleDateFormat sdf4 = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            String strDate = sdf4.format(c.getTime());

            Log.e("--------date2---------", "" + strDate);
            Log.e("--------date2---------", "" + strDate.substring(14, 16));
            int CurrentTime = Integer.parseInt(strDate.substring(11, 13) + strDate.substring(14, 16));
            Log.e("CurrentTime--------", "" + CurrentTime);
            Log.e("CurrentTime--------", "" + getPreference("end_time"));

            //savePreference("end_time","");

            int EndTime = 0;
            try {
                EndTime = Integer.parseInt(getPreference("end_time").substring(0, 2) + strDate.substring(3, 5));
            } catch (Exception e) {

            }

            Log.e("EndTime---------", "" + EndTime);

            Log.e("Service check()-------", "" + check());

            if (check() != true) {
                if (CurrentTime < EndTime) {

                    Intent intentStart = new Intent(this, MyServiceStart.class);
                    PendingIntent pendingIntentStart = PendingIntent.getService(Dashboard.this, i, intentStart, 0);

                    AlarmManager alarmStart = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

                    try {
                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                        String dateInString = datelist.get(i) + " " + getPreference("start_time");
                        Date date = sdf.parse(dateInString);
                        Log.e("dateInString---------", "" + dateInString);
                        Log.e("date------------", "" + date.getTime());
                        alarmStart.set(AlarmManager.RTC_WAKEUP, date.getTime(), pendingIntentStart);
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }

            }
        }

        //----------------------------GPS Tracker Stop--------------------------

        for (int i = 0; i < datelist.size(); i++) {

            Intent intentStop = new Intent(this, MyServiceStop.class);
            PendingIntent pendingIntentStop = PendingIntent.getService(Dashboard.this, i, intentStop, 0);
            AlarmManager alarmStop = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                String dateInString = datelist.get(i) + " " + getPreference("end_time");
                Date date = sdf.parse(dateInString);
                Log.e("dateInString------", "" + dateInString);
                Log.e("date------------", "" + date.getTime());
                alarmStop.set(AlarmManager.RTC_WAKEUP, date.getTime(), pendingIntentStop);

            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        startService(new Intent(this, TimerService.class));

        final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();
        }

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);
        reportBtn = findViewById(R.id.reportBtn);
        cash_number = findViewById(R.id.cash_number);
        oc_value = findViewById(R.id.oc_value);
        stockBtn = findViewById(R.id.stockBtn);
        accountBtn = findViewById(R.id.accountBtn);
        syncBtn = findViewById(R.id.sync_btn);
        toolsBtn = findViewById(R.id.toolBtn);
        deliveryBtn = findViewById(R.id.deliveryBtn);
        backBtn = findViewById(R.id.back);
        homeBtn = findViewById(R.id.home);

        order_or_delivery = findViewById(R.id.logo);
        reportBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Reports_Activity.class);
                startActivity(intent);
                finish();
            }
        });

        deliveryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, DeliveryReport.class);
                startActivity(intent);
                finish();
            }
        });

        stockBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, SrStoreStatus.class);
                startActivity(intent);
                finish();
            }
        });

        accountBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, SR_Account_Activity.class);
                startActivity(intent);
                finish();
            }
        });


        syncBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, SyncActivity.class);
                startActivity(intent);
                finish();
            }
        });

        //more button
        toolsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, Tools.class);
                startActivity(intent);
                finish();
            }
        });

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder = new AlertDialog.Builder(Dashboard.this);
                builder.setCancelable(true);
                builder.setTitle("Logout");
                builder.setMessage("Do You Want to Logout?");
                builder.setPositiveButton("yes",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(Dashboard.this, MainActivity.class);
                                startActivity(intent);
                                finishAffinity();
                                finish();
                            }
                        });
                builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();

            }
        });
        //outlets button
        order_or_delivery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runtime.getRuntime().gc();
                Intent i = new Intent(Dashboard.this, Create_New_Memo.class);
                i.putExtra("flag1", 1);
                startActivity(i);
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


            }
        });


        cash_number.setText(ds.getTotalCashOfCurrentDay());
        oc_value.setText(ds.getTotalOCofCurrentDay());


    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {

    }

    @Override
    public void OnConnetivityError() {

    }

    public boolean check() {
        ActivityManager manager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {

            if ("com.srapp.GPS.GPSUpdateService".equals(service.service.getClassName())) {
                Log.e("Service already","running");
                Log.e("service_already","running");

                return true;
            }

        }
        return false;
    }

}
