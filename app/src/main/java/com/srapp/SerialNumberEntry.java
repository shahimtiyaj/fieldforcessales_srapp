package com.srapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.AdapterForSelial;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Util.Parent;

import java.util.ArrayList;
import java.util.HashMap;

public class SerialNumberEntry extends Parent {

    TagEditText edit;
    Button add;
    String contactName;
    TextView textView;
    ListView lisy;
    Button next;
    public static ArrayList<ArrayList<String>> serial;
    AdapterForSelial adapterForSelial;
    ArrayList<HashMap<String, String>> arrayList;
    Data_Source db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sirial_number_entry);
        serial = new ArrayList<>();
        lisy = findViewById(R.id.lisy);
        next = findViewById(R.id.next);
        arrayList = new ArrayList<>();
        db = new Data_Source(this);

        Loaddata();

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (sirialValidation()) {

                   // Intent i = new Intent(SerialNumberEntry.this, ProductSalesForMemo.class);
                    Intent i = new Intent(SerialNumberEntry.this, ProductSalesForMemo.class);
                    startActivity(i);
                } else {

                    Toast.makeText(SerialNumberEntry.this, "Please Check Serial Number", Toast.LENGTH_LONG).show();
                }

            }
        });


    }

    private boolean sirialValidation() {
        boolean istrue = true;

        Cursor c = db.rawQuery("select p.product_id , pb.serial_number,pb.quantity from product_boolean as pb inner join product as p on p.product_id = pb.product_id where p.maintain_serial=1 and pb.boolean = 'true'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            for (int i = 0; i < c.getCount(); i++) {
                if (SirialValidation2(c.getString(1),c.getString(2))) {
                    istrue = true;
                }else {
                    istrue = false;
                }

                if (!istrue){
                    return istrue;
                }
                c.moveToNext();
            }
        }
        return istrue;
    }


    private boolean SirialValidation2(String serial, String quantity) {
        boolean istrue = true;
            if (serial==null){
                return false;
            }
            double serialQuantity = serial.split(",").length;

            if (serialQuantity == Double.parseDouble(quantity)) {

                istrue = true;
            } else {

                istrue = false;
            }




        return istrue;
    }

    private void Loaddata() {

        String query = "Select p.product_name,pb.product_id,pb.quantity, pb.serial_number from product_boolean as pb inner join product as p on pb.product_id=p.product_id where outlet_id='" + getPreference("OutletID") + "' and boolean='true' and p.maintain_serial='1'";

        Cursor c = db.rawQuery(query);
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {
                HashMap<String, String> map = new HashMap<>();
                map.put("product_name", c.getString(0));
                map.put("product_id", c.getString(1));
                map.put("quantity", c.getString(2));
                map.put("sirial_number", c.getString(3));
                arrayList.add(map);

                serial.add(getserialList(c.getString(1)));

            } while (c.moveToNext());


        }

        adapterForSelial = new AdapterForSelial(this, arrayList);
        lisy.setAdapter(adapterForSelial);


    }


    private ArrayList<String> getserialList(String product_id) {

        ArrayList<String> serial = new ArrayList<>();
        Log.e("product_id", product_id);

        Log.e("log", "select serial_no from " + Tables.TABLE_NAME_PRODUCT_SERIALS + " where product_id='" + product_id + "' and is_used='0'");
        Cursor c = db.rawQuery("select serial_no from " + Tables.TABLE_NAME_PRODUCT_SERIALS + " where product_id='" + product_id + "' and is_used='0'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {
                serial.add(c.getString(0));

            } while (c.moveToNext());

        }

        return serial;
    }


}
