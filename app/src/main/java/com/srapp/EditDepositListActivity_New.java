package com.srapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;


import com.srapp.Adapter.AdapterForEditDepositList;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Model.SR_Account;
import com.srapp.Util.ParentActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class EditDepositListActivity_New extends ParentActivity implements OnClickListener {

    private ImageView btnBack, btnHome;
    TextView HeaderTitleTv, CodeNoTv;

    Button buttonLogin;
    ArrayList<HashMap<String, String>> ItemListFromDB = new ArrayList<HashMap<String, String>>();

    ListView list;
    AdapterForEditDepositList adapterProductReceive12;

    Button FromDateEd, ToDateEd;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private SimpleDateFormat dateFormatter;
    float total = 0;

    TextView txtSaleTotal;
    Data_Source db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.edit_depost_list);

        db = new Data_Source(this);
        //TextView txtUser=(TextView)findViewById(R.id.txtUser);
        //txtUser.setText(getPreference("UserName"));


        txtSaleTotal = (TextView) findViewById(R.id.txtSaleTotal);

        initiateButtons(btnBack, R.id.back);
        initiateButtons(btnHome, R.id.home);

        FromDateEd = (Button) findViewById(R.id.FromDateEd);
        ToDateEd = (Button) findViewById(R.id.ToDateEd);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels / 18;


        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutBg);
        LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        params.setMargins(width, 0, width, 0);
        layout.setLayoutParams(params);


        Date today = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(today);

        calendar.add(Calendar.MONTH, 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.add(Calendar.DATE, -1);

        Date lastDayOfMonth = calendar.getTime();

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Today            : " + sdf.format(today));
        System.out.println("Last Day of Month: " + sdf.format(lastDayOfMonth));

        String[] parts = sdf.format(lastDayOfMonth).split("/", 3);
        String Month = parts[1];  // 004
        String Year = parts[2];  // 004

        FromDateEd.setText(getCurrentDate());
        ToDateEd.setText(getCurrentDate());

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        setDateTimeField();
        FilteringProduct();

    }


    public void FilteringProduct() {

        txtSaleTotal.setText("");


        ItemListFromDB.clear();
        list = (ListView) findViewById(R.id.listView);


        Cursor c = db.rawQuery("SELECT DP.deposit_date,DP.deposit_slip_no,BB.bank_banch_name,DP.deposit_amount, IT.instrument_type_name, DP._id, DP.deposit_id, DP.payment_id FROM deposits DP LEFT JOIN bank_banch BB ON(DP.bank_banch_id=BB.bank_banch_id) LEFT JOIN instrument_type IT ON(DP.instrument_type_id=IT.instrument_type_id) where DP.deposit_date>=" + "'" + FromDateEd.getText().toString() + "'" + " and DP.deposit_date <=" + "'" + ToDateEd.getText().toString() + "' ORDER BY DP._id DESC");
//		Cursor c =db.rawQuery("SELECT DP.deposit_date,DP.deposit_slip_no,BB.bank_banch_name,DP.deposit_amount, IT.instrument_type_name, DP._id, DP.deposit_id, DP.payment_id FROM deposits DP LEFT JOIN bank_banch BB ON(DP.bank_banch_id=BB.bank_banch_id) LEFT JOIN instrument_type IT ON(DP.instrument_type_id=IT.instrument_type_id) where DP.deposit_date>="+"'"+FromDateEd.getText().toString()+"'"+" and DP.deposit_date <="+"'"+ToDateEd.getText().toString()+"' GROUP BY DP.deposit_slip_no ORDER BY DP._id DESC");
//		Cursor c =db.rawQuery("SELECT DP.deposit_date,DP.deposit_slip_no,BB.bank_banch_name,DP.deposit_amount, IT.instrument_type_name, DP._id FROM deposits DP LEFT JOIN bank_banch BB ON(DP.bank_banch_id=BB.bank_banch_id) LEFT JOIN instrument_type IT ON(DP.instrument_type_id=IT.instrument_type_id) ORDER BY DP._id DESC");

        Log.e("---------------", "---deposit_date---" + "SELECT DP.deposit_date,DP.deposit_slip_no,BB.bank_banch_name,DP.deposit_amount, IT.instrument_type_name, DP._id, DP.deposit_id, DP.payment_id FROM deposits DP LEFT JOIN bank_banch BB ON(DP.bank_banch_id=BB.bank_banch_id) LEFT JOIN instrument_type IT ON(DP.instrument_type_id=IT.instrument_type_id) where DP.deposit_date>=" + "'" + FromDateEd.getText().toString() + "'" + " and DP.deposit_date <=" + "'" + ToDateEd.getText().toString() + "' ORDER BY DP._id DESC");
        Log.e("Count", "" + c.getCount());
        if (c != null) {
            if (c.moveToFirst()) {
                do {


                    String date = c.getString(0);
                    String particulars = c.getString(1);
                    String sales = c.getString(2);
                    Double deposits = c.getDouble(3);
                    String type = c.getString(4);
                    String Id = c.getString(5);
                    String deposit_id = c.getString(6);
                    String payment_id = c.getString(7);


                    HashMap<String, String> product_list_map = new HashMap<String, String>();

                    product_list_map.put("date", DateFormatedConverter(date));
                    product_list_map.put("particulars", particulars);
                    product_list_map.put("sales", sales);
                    product_list_map.put("deposits", roundTwoDecimals(deposits));
                    product_list_map.put("Id", Id);
                    product_list_map.put("deposit_id", deposit_id);
                    product_list_map.put("payment_id", payment_id);

                    if (type.equalsIgnoreCase("Cash"))
                        product_list_map.put("type", "CSH");
                    else
                        product_list_map.put("type", "INST.");

	

					/* total=total+Float.parseFloat(deposits);
				     txtSaleTotal.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(""+total))));*/


                    ItemListFromDB.add(product_list_map);


                } while (c.moveToNext());
            }


        }

        txtSaleTotal.setText("");
        adapterProductReceive12 = new AdapterForEditDepositList(this, ItemListFromDB);

        list.setAdapter(adapterProductReceive12);

    }

    private class ListAdapter extends BaseAdapter {


        LayoutInflater inflater;
        ViewHolder viewHolder;
        Activity activity;
        ArrayList<HashMap<String, String>> itemListFromDB5;

        public ListAdapter(ArrayList<HashMap<String, String>> itemListFromDB12, Activity act) {
            // TODO Auto-generated constructor stub

            inflater = LayoutInflater.from(getBaseContext());
            this.itemListFromDB5 = itemListFromDB12;
            this.activity = act;
        }

        @Override
        public int getCount() {
            return itemListFromDB5.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.edit_deposit_row, null);

            viewHolder = new ViewHolder();

            viewHolder.Datetxt = (TextView) vi.findViewById(R.id.Datetxt);
            viewHolder.Particulerstxt = (TextView) vi.findViewById(R.id.Particulerstxt);
            viewHolder.Saletxt = (TextView) vi.findViewById(R.id.Saletxt);
            viewHolder.Deposittxt = (TextView) vi.findViewById(R.id.Deposittxt);
            viewHolder.txtType = (TextView) vi.findViewById(R.id.txtType);
            viewHolder.ActionBtn = (ImageView) vi.findViewById(R.id.ActionBtn);


            final HashMap<String, String> map = itemListFromDB5.get(position);


            viewHolder.Datetxt.setText(map.get("date"));
            viewHolder.Particulerstxt.setText(map.get("particulars"));
            viewHolder.Saletxt.setText(map.get("sales"));
            viewHolder.Deposittxt.setText(map.get("deposits"));
            viewHolder.txtType.setText(map.get("type"));


            return vi;
        }

    }

    private class ViewHolder {
        TextView Datetxt;
        TextView Particulerstxt;
        TextView Saletxt;
        TextView Deposittxt;
        TextView txtType;
        ImageView ActionBtn;


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent idd = new Intent(EditDepositListActivity_New.this, Tools.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


    public void initiateButtons(ImageView btn, int id) {
        btn = (ImageView) findViewById(id);
        btn.setOnClickListener((OnClickListener) this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

        if (v.getId() == R.id.back) {

            Intent idd = new Intent(EditDepositListActivity_New.this, Tools.class);
            startActivity(idd);
            finish();
        } else if (v.getId() == R.id.home) {
            Intent intent = new Intent(EditDepositListActivity_New.this, MainActivity.class);
            startActivity(intent);
            finish();
        }
        if (v == FromDateEd) {
            fromDatePickerDialog.show();
            // MSC_Products();
        } else if (v == ToDateEd) {
            toDatePickerDialog.show();

        }


    }

    private void setDateTimeField() {
        FromDateEd.setOnClickListener(this);
        ToDateEd.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                FromDateEd.setText(dateFormatter.format(newDate.getTime()));
                FilteringProduct();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                ToDateEd.setText(dateFormatter.format(newDate.getTime()));
                FilteringProduct();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));



	       /* Intent idn = new Intent(SO_TargetActivity.this, SO_TargetActivity.class);
			 startActivity(idn);
			 finish();*/
    }

}
