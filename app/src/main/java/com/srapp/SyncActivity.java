package com.srapp;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.srapp.Adapter.AdapterForSyncSummery;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.Parent;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.srapp.Db_Actions.Tables.Allfild;
import static com.srapp.Db_Actions.Tables.SR_ID;


public class SyncActivity extends Parent implements DBListener, BasicFunctionListener {

    Button sync_btn, viewSummery;
    TextView pendingMemo;
    Data_Source ds;
    int Flag = 0;
    ListView listView;
    BasicFunction bf;
    ImageView homeBtn, backBtn;
    TextView userIdTV, titleTV, date;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);

        // Create the observer which updates the UI.
        final Observer<String> statusObserver = new Observer<String>() {
            @Override
            public void onChanged(@Nullable final String status) {
                // Update the UI, in this case, a TextView.
                if (status.equals("1")){
                    ds.excQuery("UPDATE gps_tracker SET is_pushed=1");
                }
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getCurrentStatus().observe(this, statusObserver);

        bf = new BasicFunction(this, this);
        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);
        date = findViewById(R.id.date);
        if (!bf.getPreference("lsyncTime").equalsIgnoreCase("null")) {
            date.setText(bf.getPreference("lsyncTime"));
        }

        ds = new Data_Source(this, this, this);
        sync_btn = (Button) findViewById(R.id.sync_btn);
        viewSummery = (Button) findViewById(R.id.viewSummery);
        pendingMemo = findViewById(R.id.pendingMemo);

        pendingMemo.setText(ds.getPendingOrderCount());
        viewSummery.setVisibility(View.GONE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SyncActivity.this, Dashboard.class));
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SyncActivity.this, Dashboard.class));
                finishAffinity();
            }
        });


        sync_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               Flag = 2;

                try {

                    JsonObject ob = new JsonObject();
                    JsonArray arrJson = new JsonArray();

                    String query = "SELECT *FROM gps_tracker WHERE is_pushed=0";
                    Cursor c2 = ds.rawQuery(query);

                    if (c2 != null) {
                        if (c2.moveToFirst()) {
                            do {

                                JsonObject ob1 = new JsonObject();

                                String _id = c2.getString(0);
                               // String so_id = c2.getString(1);
                                String latitude = c2.getString(1);
                                String longitude = c2.getString(2);
                                String updated_at = c2.getString(6);

                                    ob1.addProperty("id", _id);
                                    ob1.addProperty("sales_person_id", getPreference("sales_person_id"));
                                    ob1.addProperty("latitude", latitude);
                                    ob1.addProperty("longitude", longitude);
                                    ob1.addProperty("created", updated_at);

                                    arrJson.add(ob1);

                            } while (c2.moveToNext());
                        }

                    }

                   // ob.addProperty("sales_person_id", "21695");
                     ob.addProperty("sales_person_id", getPreference("sales_person_id"));
                     ob.addProperty("mac", "null");
                     ob.add("gps_tracking", arrJson);

                    Log.e("gps_post_json", "GPS-tracking: " + ob.toString());
                    dataViewModel.postGPSTrackingData(ob);


                } catch (Exception e) {
                    Log.e("response tracking", "" + "error");
                }

                ds.generatePushJson();
                bf.savePreference("lsyncTime",DateFormatedConverter(getCurrentDate())+" "+getCurrentTime());
            }
        });

        viewSummery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    showDetailsDailog();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }


    @Override
    public void OnLocalDBdataRetrive(final String json) {


        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    if (json.toString().equalsIgnoreCase("done")) {
                        pendingMemo.setText(ds.getPendingOrderCount());
                        viewSummery.setVisibility(View.VISIBLE);
                    }

                    JSONObject jsonObject = new JSONObject(json);
                    jsonObject.put("mac", bf.getPreference("mac"));

                    if (Flag == 1) {

                        bf.getResponceData(URL.PULL, jsonObject.toString(), 102);
                    } else if (Flag == 2) {
                        bf.getResponceData(URL.ORDERPUSH, jsonObject.toString(), 103);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //basicFunction.getResponceData(URL.Log,createJWT("push",json), 101);
                //generateNoteOnSD(MainActivity.this,"log.txt",json);
            }
        });


    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (jsonObject.toString().equalsIgnoreCase("done")) {
            startActivity(new Intent(SyncActivity.this, SyncActivity.class));
            finish();
        }

        if (i == 102) {
            try {
                ds.excQuery("delete  from product_serials");
                ds.excQuery("delete from fiscal_year");
                //-------------------------------------------
                ds.excQuery("delete  from product_history");
                ds.excQuery("delete  from instrument_type");
                ds.excQuery("delete  from location");
                ds.excQuery("delete  from materials");
                ds.excQuery("delete  from stock_info");
                ds.excQuery("delete  from product_serials");
                ds.excQuery("delete from memos");
                ds.excQuery("delete from memo_details");

                //i added here
                ds.excQuery("delete from fiscal_year");
                ds.excQuery("delete from " + Tables.TABLE_NAME_Bonus_Card_type);
                //--------clear for duplicate data -17-01-2020----------
                ds.excQuery("delete from " + Tables.TABLE_NAME_THANA);
                ds.excQuery("delete from " + Tables.TABLE_NAME_OUTLETS);
                ds.excQuery("delete from " + Tables.TABLE_NAME_ROOT);
                ds.excQuery("delete from " + Tables.TABLE_NAME_MARKETS);
                ds.excQuery("delete from product");
                ds.excQuery("delete from product_categories");
                ds.excQuery("delete from outlets");
                ds.excQuery("delete from outlet_categories");
              //  ds.excQuery("delete from "+TABLE_NAME_OUTLET_CATEGORY);
                ds.insertData(jsonObject.getJSONObject("response").toString());
                ds.savePreference("dataforsummery", jsonObject.getJSONObject("response").toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (i == 103) {
            ds.updatePushStatus();
            Flag = 1;
            ds.getlastupdateddate();

        }
    }

    @Override
    public void OnConnetivityError() {

        Toast.makeText(SyncActivity.this, "No Internet Connection", Toast.LENGTH_SHORT).show();

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            startActivity(new Intent(SyncActivity.this, Dashboard.class));
            finishAffinity();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


    private void showDetailsDailog() throws JSONException {
        ArrayList<HashMap<String,String>> list = new ArrayList<>();
        LayoutInflater inflater = LayoutInflater.from(SyncActivity.this);
        final View vv = inflater.inflate(R.layout.dialog_sync_summery, null);
        listView = vv.findViewById(R.id.details);
        JSONObject jsonObject = new JSONObject(bf.getPreference("dataforsummery"));

        for (int i = 0; i < Allfild.length; i++) {
            HashMap<String,String> map = new HashMap<>();
            if (jsonObject.has(Allfild[i][0])) {

                JSONArray jsonArray = jsonObject.getJSONArray(Allfild[i][0]);

                if (jsonArray.length()>0){
                    map.put("dataname",Allfild[i][0]);
                    map.put("dataQty",jsonArray.length()+"");
                    list.add(map);
                }

            }
        }

        AdapterForSyncSummery adapter = new AdapterForSyncSummery(SyncActivity.this,list);
        listView.setAdapter(adapter);


        final AlertDialog.Builder alert = new AlertDialog.Builder(
                SyncActivity.this);
        alert.setView(vv);
        //alert.setCancelable(false);


        final AlertDialog dialog = alert.create();
        dialog.show();


    }
}
