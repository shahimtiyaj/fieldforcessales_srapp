package com.srapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.Window;

import com.srapp.Util.Parent;

public class SplashActivity extends Parent {
	

	  public void onCreate(Bundle savedInstanceState) {
	        super.onCreate(savedInstanceState);

	        requestWindowFeature(Window.FEATURE_NO_TITLE);
	        setContentView(R.layout.splash);


		//savePreference("get_url","http://202.126.123.157/smc_sales/api_data168_retrives/");
		 //savePreference("get_url","http://182.160.103.236:8079/api_data168_retrives/");
	        Handler handler = new Handler();

	        handler.postDelayed(new Runnable() {
	 
	            @Override
	            public void run() {
	            	
	            	Log.e("HHHHHHHHHHHHHHHHHHHHH", "HHHHHHHH  getPreference(SO)  HHHHHHHHHHH"+getPreference("SO"));
	            	
	            	finish();
           		 	Intent intent = new Intent(SplashActivity.this, MainActivity.class);
	               startActivity(intent);
	            	
	            }
	 
	        }, 2000);
	  }
}
