package com.srapp;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.AdapterForOrderReadyForDalivery;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.srapp.print.PrintAllMemosActivity;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class DeliveryReport extends AppCompatActivity implements BasicFunctionListener {
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    BasicFunction bf;
    Data_Source ds;
    Spinner route;
    Button start_date,end_date,all,print;
    ListView daliveryList;
    ArrayList<String> ThanaID=new ArrayList<String>();
    ArrayList<String> ThanaName=new ArrayList<String>();
    String _ThanaID;
    ArrayList<HashMap<String, String>> list;
    ImageView homeBtn,backBtn;
    TextView userIdTV,titleTV ;
    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorCreateSpinner;
    Data_Source db;
    BasicFunction basicFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delivery_report);

        db = new Data_Source(this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();
        basicFunction = new BasicFunction(this,this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorCreateSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent( DeliveryReport.this, Dashboard.class));
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent( DeliveryReport.this, Dashboard.class));
                finishAffinity();
            }
        });


        route= findViewById(R.id.route);
        print= findViewById(R.id.print);
        start_date= findViewById(R.id.start_date);
        end_date= findViewById(R.id.end_date);
        all= findViewById(R.id.all);
        list= new ArrayList<>();
        daliveryList= findViewById(R.id.daliveryList);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        bf = new BasicFunction(this,this);
        ds = new Data_Source(this);
        bf.savePreference("Droute_id","0");

       // routeSpinnerSetup();
        setDateTimeField();

        // first time called does not disable progress dialog

//        JSONObject jsonObject = new JSONObject();
//        try {
//            jsonObject.put("mac",bf.getPreference("mac"));
//            jsonObject.put("start_Date",bf.getPreference("Dstart_Date"));
//            jsonObject.put("end_date",bf.getPreference("Dend_date"));
//            jsonObject.put("route_id",bf.getPreference("Droute_id"));
//            jsonObject.put(SR_ID,bf.getPreference(SR_ID));
//            bf.getResponceData(URL.PROCESS_ORDER_LIST_FOR_MEMO,jsonObject.toString(),111);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }


        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent idn = new Intent(DeliveryReport.this, PrintAllMemosActivity.class);
                idn.putExtra("FromDate",start_date.getText().toString());
                idn.putExtra("ToDate",end_date.getText().toString());
                startActivity(idn);
                finish();

            }
        });

        all.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("mac",bf.getPreference("mac"));
                    jsonObject.put("start_Date","0");
                    jsonObject.put("end_date","0");
                    jsonObject.put("route_id","0");
                    jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                    bf.getResponceData(URL.PROCESS_ORDER_LIST_FOR_MEMO,jsonObject.toString(),112);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        start_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        end_date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePickerDialog.show();
            }
        });

        distributorCreateSpinner = findViewById(R.id.distributor_spinner);
        distributorCreateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                try {
                    dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                    distributorStoreId = dataViewModel.distributorStoreId;
                    Log.e("distributorId_from_spin", distributorStoreId);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (distributorStoreId!=null){
                    Log.e("found_dist_id", distributorStoreId);
                    routeSpinnerSetup();
                }

                else {
                    Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });



        route.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                if (textView!=null) {
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0, 0, 0, 0);
                }

                bf.savePreference("Droute_id",ThanaID.get(position));
                DataView();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (i==111){
            ds.updateWithServerProcessed(jsonObject);
            list = ds.getProcessedOrder(bf.getPreference("Dstart_Date"), bf.getPreference("Dend_date"), bf.getPreference("Droute_id"), distributorStoreId);
            AdapterForOrderReadyForDalivery adapter = new AdapterForOrderReadyForDalivery(DeliveryReport.this,list);
            daliveryList.setAdapter(adapter);

        }else if(i==112){
            ds.updateWithServerProcessed(jsonObject);
            list = ds.getProcessedOrder("0", "0", "0", "0");
            AdapterForOrderReadyForDalivery adapter = new AdapterForOrderReadyForDalivery(DeliveryReport.this,list);
            daliveryList.setAdapter(adapter);
        }
    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(this,"NO Internet Connection",Toast.LENGTH_SHORT);

    }

    private void routeSpinnerSetup()
    {
        ThanaID.clear();
        ThanaName.clear();
       // Cursor c = ds.rawQuery("SELECT * FROM route ORDER BY route_name ASC");
        Cursor c = ds.rawQuery("SELECT * FROM route where db_id="+distributorStoreId+" ORDER BY route_name ASC"); // change for filterring distributor wise data
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String Thana_Id = c.getString(c.getColumnIndex("route_id"));
                    String Thana_Name = c.getString(c.getColumnIndex("route_name"));

                    ThanaID.add(Thana_Id);
                    ThanaName.add(Thana_Name);

                } while (c.moveToNext());
            }
            ThanaID.add(0,"0");
            ThanaName.add(0,"ALL");
            /*ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(DeliveryReport.this,R.layout.spinner_text, Route_name);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);*/
            SpinnerAdapter dataAdapter = new SpinnerAdapter(this, R.layout
                    .spinner_item, ThanaName);
            route.setAdapter(dataAdapter);

        }

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            Intent idd = new Intent(DeliveryReport.this, Dashboard.class);
            startActivity(idd);
            finishAffinity();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    private void setDateTimeField() {


        Log.e("dstart",bf.getPreference("Dstart_Date"));
        Log.e("dend",bf.getPreference("Dend_date"));

        bf.savePreference("Dstart_Date", bf.getCurrentDate());
        bf.savePreference("Dend_date", bf.getCurrentDate());


        start_date.setText(bf.getCurrentDate());
        end_date.setText(bf.getCurrentDate());

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("Dstart_Date", dateFormatter.format(newDate.getTime()));
                start_date.setText(bf.getPreference("Dstart_Date"));
                if (distributorStoreId!=null)
                DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("Dend_date", dateFormatter.format(newDate.getTime()));
                end_date.setText(bf.getPreference("Dend_date"));
                if (distributorStoreId!=null)
                    DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

    }

    private void DataView() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mac",bf.getPreference("mac"));
            jsonObject.put("start_Date",bf.getPreference("Dstart_Date"));
            jsonObject.put("end_date",bf.getPreference("Dend_date"));
            jsonObject.put("route_id",bf.getPreference("Droute_id"));
            jsonObject.put("db_id", distributorStoreId);
            jsonObject.put(SR_ID,bf.getPreference(SR_ID));

            Log.e("jsonojb",jsonObject.toString());

            bf.getResponceData(URL.PROCESS_ORDER_LIST_FOR_MEMO,jsonObject.toString(),111);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


}
