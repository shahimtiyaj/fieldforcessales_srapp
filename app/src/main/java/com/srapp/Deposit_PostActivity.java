package com.srapp;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.Parent;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.crypto.Mac;

import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.URL.SUBMIT_DEPOSITS;

public class Deposit_PostActivity extends Parent implements  OnClickListener, BasicFunctionListener {
	
	private ImageView btnBack, btnHome;
	TextView HeaderTitleTv, CodeNoTv;
	Spinner spInstrumentType,spInstrumentNo,spSalesWeek,spBankBanch,spAccountNo,spIns;
	EditText etSlipNo;
	EditText etDepositAmount,etInstrumentRefNo;
	Button btnSubmit,etInstrumentDate,etSlipDate;
	BasicFunction bf;
	ArrayList<HashMap<String, String>> listInstrumentType;
	ArrayList<HashMap<String, String>> listInstrumentNo;
	ArrayList<HashMap<String, String>> listBankBanch;
	ArrayList<HashMap<String, String>> listBankAccount;

    int Deposit_mode;

	ArrayList<String> InsID=new ArrayList<String>();
	ArrayList<String> InsName=new ArrayList<String>();
	ArrayList<String> paymentId=new ArrayList<String>();
	ArrayList<String> AccountNO=new ArrayList<String>();

	String _InsID,_paymentId;
	
	
	ArrayList<String> SalesWeekID=new ArrayList<String>();
	ArrayList<String> SalesWeekName=new ArrayList<String>();
	
	String _SalesWeekID;
	
	ArrayList<String> BankBanchID=new ArrayList<String>();
	ArrayList<String> BankBanchName=new ArrayList<String>();
	
	String _BankBanchID;

	
	 static final int DATE_PICKER_ID1 = 1111; 
	 static final int DATE_PICKER_ID2 = 2222; 

	 private int year1;
	 private int month1;
	 private int day1;
	 
	 private int year2;
	 private int month2;
	 private int day2;
	 List<String> list1,list2,list3,list4;
	 private static int RESULT_LOAD_IMAGE = 1;
	 
	 ArrayList<HashMap<String,String>> DataList=new ArrayList<HashMap<String,String>>();
	 ArrayList<String> list=new ArrayList<String>();
	 
	 LinearLayout  llInstrumentDate,llInsturmentNo;
	 
	 String instrumentDate="";
	 String slipDate="";
	 Data_Source db;
	 
	 Double totalDiff = 0.0, paymentCheck = 0.0;

	LinearLayout InstrumentRefNoLayout, InsLayout;

	ArrayList<String> InstID=new ArrayList<String>();
	ArrayList<String> InstName=new ArrayList<String>();
	String _InstID;
	double Total=0.0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.post_deposit);
		TextView txtUser=(TextView)findViewById(R.id.txtUser);
		txtUser.setText(getPreference("UserName"));

		db=new Data_Source(this);
		llInstrumentDate=(LinearLayout)findViewById(R.id.llInstrumentDate);
		llInsturmentNo=(LinearLayout)findViewById(R.id.llInsturmentNo);
        bf = new BasicFunction(this,this);

		initiateButtons(btnBack, R.id.back);
		initiateButtons(btnHome, R.id.home);
		
		TextView SlipNoTV=(TextView)findViewById(R.id.SlipNoTV);
		addAsMandetory(SlipNoTV, "Reference No:");
		
		TextView DepositAmountTV=(TextView)findViewById(R.id.DepositAmountTV);
		addAsMandetory(DepositAmountTV, "Deposit Amount:");
		
		TextView InstrumentTypeTV=(TextView)findViewById(R.id.InstrumentTypeTV);
		addAsMandetory(InstrumentTypeTV, "Type:");
		
		
		TextView SalesWeekTV=(TextView)findViewById(R.id.SalesWeekTV);
		addAsMandetory(SalesWeekTV, "Sales Week:");
		
		TextView BankBanchTV=(TextView)findViewById(R.id.BankBanchTV);
		 addAsMandetory(BankBanchTV, "Bank Banch:");
		

		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels/15;
		
		ScrollView layout = (ScrollView)findViewById(R.id.layoutBg12);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);            
		params.setMargins(width, 0, width, 0); 
		layout.setLayoutParams(params);
		ViewInitalizationAndAction();
		
	}
	
	
	private void ViewInitalizationAndAction() {
		// TODO Auto-generated method stub
		spInstrumentType=(Spinner)findViewById(R.id.spInstrumentType);
		spInstrumentNo=(Spinner)findViewById(R.id.spInstrumentNo);
		spSalesWeek=(Spinner)findViewById(R.id.spSalesWeek);
		spBankBanch=(Spinner)findViewById(R.id.spBankBanch);
		spAccountNo=(Spinner)findViewById(R.id.spAccountNo);
		spIns=(Spinner)findViewById(R.id.InsSp);


		etInstrumentDate=(Button)findViewById(R.id.etInstrumentDate);
		etSlipNo=(EditText)findViewById(R.id.etSlipNo);
		etSlipDate=(Button)findViewById(R.id.etSlipDate);
		etDepositAmount=(EditText)findViewById(R.id.etDepositAmount);
		etInstrumentRefNo=(EditText)findViewById(R.id.etInstrumentRefNo);

		btnSubmit=(Button)findViewById(R.id.btnSubmit);
		
		listInstrumentType=db.AllDataFromTableForInstrumentType("instrument_type");
//		listInstrumentNo=db.AllDataFromTable("instrument_no");
//		listSalesWeek=db.AllDataFromTable("sales_weeks");
//		listBankBanch=db.AllDataFromTable("bank_banch");

		InsLayout = (LinearLayout)findViewById(R.id.InsLayout);
		InstrumentRefNoLayout = (LinearLayout)findViewById(R.id.InstrumentRefNoLayout);

		SalesWeek_TableParse();
		BankTableParse();

//		list1.add("Select Instrument Type");
		 list1 = new ArrayList<String>();
		 list2 = new ArrayList<String>();
		for(int i=0;i<listInstrumentType.size();i++)
		{
			HashMap<String, String> map=listInstrumentType.get(i);
			list1.add(map.get("name"));
			list2.add(map.get("id"));
		}
	

		ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<String>(this,
			R.layout.spinner_text, list1);
		dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spInstrumentType.setAdapter(dataAdapter1);

	
		
//		Log.e("BankBanch:", listBankBanch.toString());
		Log.e("SO:", getPreference("SO"));


		spIns.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
									   long arg3) {
				// TODO Auto-generated method stub

				_InstID = InstID.get(arg2);
				Log.e("-----","_InstID: "+_InstID);
//				_InstID = InstID.get(InsSp.getSelectedItemPosition()).get(arg2);


			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});
		
	

	spInstrumentType.setOnItemSelectedListener(new OnItemSelectedListener() {
		
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1,
				int arg2, long arg3) {
			// TODO Auto-generated method stub
			
			 String spInstrumentType =""+list2.get(arg2);
			
			 Log.e("RRRRRRRRRRR", "RRRRRRRRRRRRRR   "+spInstrumentType);
			
		
			 if(spInstrumentType.equals("2"))

				{

				    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                        jsonObject.put("mac",bf.getPreference("mac"));
                        jsonObject.put("type","2");

                        bf.getResponceData(URL.GET_DEPOSITS,jsonObject.toString(),111);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
					
				else
				{

                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                        jsonObject.put("mac",bf.getPreference("mac"));
                        jsonObject.put("type","1");

                        bf.getResponceData(URL.GET_DEPOSITS,jsonObject.toString(),112);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


				}
			
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	
		
	spInstrumentNo.setOnItemSelectedListener(new OnItemSelectedListener() {

		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			
			
			String d=roundTwoDecimals(Double.parseDouble(DataList.get(arg2).get("payment")));
			
			paymentCheck = Double.parseDouble(d);
			etDepositAmount.setText(String.valueOf(d));
			
		}

		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	spSalesWeek.setOnItemSelectedListener(new OnItemSelectedListener() {
		
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			_SalesWeekID = SalesWeekID.get(arg2);
			
			 savePreference("SalesWeekID", _SalesWeekID);
			
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
	
	spBankBanch.setOnItemSelectedListener(new OnItemSelectedListener() {
		
		@Override
		public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			// TODO Auto-generated method stub
			_BankBanchID = BankBanchID.get(arg2);
						
		}
		
		@Override
		public void onNothingSelected(AdapterView<?> arg0) {
			// TODO Auto-generated method stub
			
		}
	});
		
		/* list3 = new ArrayList<String>();
		for(int i=0;i<listSalesWeek.size();i++)
		{
			HashMap<String, String> map=listSalesWeek.get(i);
			list3.add(map.get("name"));
		}
		
		ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<String>(this,
			R.layout.spinner_text, list3);
		dataAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spSalesWeek.setAdapter(dataAdapter3);*/
//.......................................................................................		
//		list4.add("Select Bank Banch"); 
		/* list4 = new ArrayList<String>();
		for(int i=0;i<listBankBanch.size();i++)
		{
			HashMap<String, String> map=listBankBanch.get(i);
			list4.add(map.get("name"));
		}
		
		ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<String>(this,
			R.layout.spinner_text, list4);
		dataAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spBankBanch.setAdapter(dataAdapter4);*/
		
		    final Calendar c = Calendar.getInstance();
	        year1  = c.get(Calendar.YEAR);
	        month1 = c.get(Calendar.MONTH);
	        day1   = c.get(Calendar.DAY_OF_MONTH);


		String day="",month="";
		if(day1<10)
			day="0"+String.valueOf(day1);
		else
			day=String.valueOf(day1);

		Log.e("month", "-----"+month2);
		if(month1==0 ||month1==1 || month1==2||month1==3||month1==4||month1==5||month1==6||month1==7||month1==8)
			month="0"+String.valueOf(month1+1);
		else if(month1==9 ||month1==10 || month1==11)
			month=String.valueOf(month1+1);


	        /*String day="",month="";
	        if(day1<10)
	        day="0"+String.valueOf(day1);
	        else
	        day=String.valueOf(day1);	
	        if(month1<10)
	        	month="0"+String.valueOf(month1+1);
	        else
	        month=String.valueOf(month1+1);*/

	        Log.e("day","Day"+ String.valueOf(day));
	        Log.e("month", String.valueOf(month));
             instrumentDate=new StringBuilder().append(year1 )
                     .append("-").append(month).append("-").append(day)
                     .append(" ").toString();
             slipDate=new StringBuilder().append(year1 )
                     .append("-").append(month).append("-").append(day)
                     .append(" ").toString();
	        
		   etInstrumentDate.setText(DateFormatedConverter(instrumentDate));
//		   etSlipDate.setText(DateFormatedConverter(instrumentDate));
		   etSlipDate.setText(slipDate);

		etInstrumentDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				// TODO Auto-generated method stub
				   final Calendar c = Calendar.getInstance();
			        year1  = c.get(Calendar.YEAR);
			        month1 = c.get(Calendar.MONTH);
			        day1   = c.get(Calendar.DAY_OF_MONTH);
				    showDialog(DATE_PICKER_ID1);
			}
		});
		
		etSlipDate.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final Calendar c = Calendar.getInstance();
		        year2  = c.get(Calendar.YEAR);
		        month2 = c.get(Calendar.MONTH);
		        day2   = c.get(Calendar.DAY_OF_MONTH);
			    showDialog(DATE_PICKER_ID2);
			}
		});
		
		Button buttonLoadImage = (Button) findViewById(R.id.Browsebtn);
		
		 buttonLoadImage.setOnClickListener(new OnClickListener() {

	            @Override
	            public void onClick(View arg0) {

	                Intent i = new Intent(
	                        Intent.ACTION_PICK,
	                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

	                startActivityForResult(i, RESULT_LOAD_IMAGE);
	            }
	        });


		btnSubmit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
				Calendar cal = Calendar.getInstance();
				String DepositID  ="D"+getPreference("SO")+dateFormat.format(cal.getTime());
				Log.e("spInstrumentType",list2.get(spInstrumentType.getSelectedItemPosition()));
				if(CheckValidation())
				{
//					btnSubmit.setEnabled(false);
					
				//Log.e("spInstrumentType",""+spInstrumentType.getSelectedItem().toString());
				if((list2.get(spInstrumentType.getSelectedItemPosition()).equals("2")))
				{
					 if(Double.parseDouble(etDepositAmount.getText().toString())>paymentCheck)
					{
						Toast.makeText(getApplicationContext(), "Enter Correct Deposit Amount!", Toast.LENGTH_LONG).show();
					}
					 else if (!etSlipNo.getText().toString().matches("[a-zA-Z0-9- ]*"))
					 {
						 Toast.makeText(getApplicationContext(), "Enter Correct Reference No.!", Toast.LENGTH_LONG).show();
					 }
					else
					{
						    Log.e("memo_no:", DataList.get(spInstrumentNo.getSelectedItemPosition()).get("memo_no"));
						 	Log.e("memo_no:", DataList.get(spInstrumentNo.getSelectedItemPosition()).get("memo_no"));

						 	JSONObject map=new JSONObject();
                        try {

                            map.put("deposit_id", DepositID);
                            map.put("type", "2");
                            map.put("post_deposit_id", "2");
							map.put("instrument_type_id", listInstrumentType.get(spInstrumentType.getSelectedItemPosition()).get("id"));
							map.put("inst_id", "");
							map.put("instrument_date", etInstrumentDate.getText().toString());
							map.put("instrument_no_id",DataList.get(spInstrumentNo.getSelectedItemPosition()).get("instrument_no") );
							map.put("instrument_ref_no",DataList.get(spInstrumentNo.getSelectedItemPosition()).get("instrument_no"));
							map.put("deposit_slip_no", etSlipNo.getText().toString().trim());
							map.put("deposit_slip_date", slipDate);
							map.put("sales_week_id",_SalesWeekID);
							map.put("deposit_amount", etDepositAmount.getText().toString());
//							map.put("bank_banch_id", listBankBanch.get(spBankBanch.getSelectedItemPosition()).get("id"));
							map.put("bank_banch_id", _BankBanchID);
							map.put(SR_ID,getPreference(SR_ID) );
							map.put("memo_no", DataList.get(spInstrumentNo.getSelectedItemPosition()).get("memo_no"));
							map.put("isPushed","0" );
							map.put("deposit_date", etSlipDate.getText().toString().trim());
							map.put("payment_id", DataList.get(spInstrumentNo.getSelectedItemPosition()).get("payment_id"));
							map.put("collection_id", DataList.get(spInstrumentNo.getSelectedItemPosition()).get("ID"));
							map.put("mac", bf.getPreference("mac"));
							Log.e("Deposit_json",map.toString());

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
						bf.getResponceData(SUBMIT_DEPOSITS,map.toString(),1000);


						}
				   }

				else
				{
					if (!etSlipNo.getText().toString().matches("[a-zA-Z0-9 ]*"))
					{
						Toast.makeText(getApplicationContext(), "Enter Correct Reference No.!", Toast.LENGTH_LONG).show();
					}
//					 else if(Double.parseDouble(etDepositAmount.getText().toString())<=totalDiff)
					else
					{
						JSONObject map=new JSONObject();
                        try {
                            map.put("deposit_id", DepositID);

                        map.put("post_deposit_id", "1");
							map.put("instrument_type_id", "1");
							map.put("inst_id", _InstID);
							map.put("type", "1");
							map.put("instrument_date", "");
							map.put("instrument_no_id","");
							map.put("instrument_ref_no",etInstrumentRefNo.getText().toString());
							map.put("deposit_slip_no", etSlipNo.getText().toString().trim());
							map.put("deposit_slip_date", slipDate);
							map.put("sales_week_id",_SalesWeekID);
							map.put("deposit_amount", etDepositAmount.getText().toString());
//							map.put("bank_banch_id", listBankBanch.get(spBankBanch.getSelectedItemPosition()).get("id"));
							map.put("bank_banch_id", _BankBanchID);
							map.put(SR_ID,getPreference(SR_ID) );
							map.put("memo_no", "0");
							map.put("isPushed","0" );
							map.put("deposit_date", etSlipDate.getText().toString().trim());
							map.put("payment_id", "");
                            map.put("mac", bf.getPreference("mac"));
                            Log.e("Deposit_json",map.toString());


                            bf.getResponceData(SUBMIT_DEPOSITS,map.toString(),1000);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }


					}
					/* else
						 Toast.makeText(getApplicationContext(), "Enter Correct Deposit Amount!", Toast.LENGTH_LONG).show();*/
					}
				}
			}
		});
		
	}

	private void setUpForInstrument(JSONObject jsonObject) throws JSONException {

        etDepositAmount.setEnabled(false);
        etDepositAmount.setText("");
        llInstrumentDate.setVisibility(View.VISIBLE);
        llInsturmentNo.setVisibility(View.VISIBLE);
        list.clear();DataList.clear();
        InsLayout.setVisibility(View.GONE);
        InstrumentRefNoLayout.setVisibility(View.GONE);

        JSONArray jsonArray = jsonObject.getJSONArray("deposit_list");

        for (int i = 0 ; i<jsonArray.length(); i++){

            HashMap<String,String> map=new HashMap<String,String>();
            map.put("ID", jsonArray.getJSONObject(i).getString("id"));
            map.put("memo_no", jsonArray.getJSONObject(i).getString("memo_no"));
            map.put("instrument_type",jsonArray.getJSONObject(i).getString("instrument_type"));
            map.put("instrument_no", jsonArray.getJSONObject(i).getString("instrument_no"));
            map.put("payment",jsonArray.getJSONObject(i).getString("collectionAmount"));





            list.add(jsonArray.getJSONObject(i).getString("instrument_no"));
            DataList.add(map);

        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spInstrumentNo.setAdapter(dataAdapter);


    }
	private void setUpForCash(Double total){


            etDepositAmount.setEnabled(true);
            Double totalPayment = 0.0;
            Double totalDeposit =0.0;

            InsLayout.setVisibility(View.VISIBLE);
            InstrumentRefNoLayout.setVisibility(View.VISIBLE);
            instrument_TableParse();


//
            Total = total;
//
            etDepositAmount.setText(roundTwoDecimals(Total));


            llInstrumentDate.setVisibility(View.GONE);
            llInsturmentNo.setVisibility(View.GONE);

            //Ins_TableParse();
            list.clear();DataList.clear();
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, list);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spInstrumentNo.setAdapter(dataAdapter);
        }




	public Boolean CheckValidation()
	{
		/*Double deposit_amount = 0.0;
		if(etDepositAmount.getText().toString().length()>0)
		{
			deposit_amount = Double.parseDouble(etDepositAmount.getText().toString());
		}else {
			Toast.makeText(getApplicationContext(), "Enter Deposit Amount!", 500).show();;
			return false;
		}*/
		

		if(etDepositAmount.getText().toString().length()==0)
		{
			Toast.makeText(getApplicationContext(), "Enter Deposit Amount!", Toast.LENGTH_LONG).show();;
			return false;
 		}
		
		if(etSlipNo.getText().toString().length()<=0)
		{
			Toast.makeText(getApplicationContext(), "Enter Slip No!", Toast.LENGTH_LONG).show();;
			return false;
 		}
		
		if(spSalesWeek.getSelectedItem().equals("Select Sales Week"))
		{
			Toast.makeText(getApplicationContext(), "Please Select Sales Week!", Toast.LENGTH_LONG).show();;
			return false;
		}
		
		if(_BankBanchID.equals("0"))
		{
			Toast.makeText(getApplicationContext(), "Select Bank Branch!", Toast.LENGTH_LONG).show();
			return false;
		}

		Log.e("--------------","-----etSlipDate-------"+slipDate);
		Log.e("--------------","-----getCurrentDate()-------"+getCurrentDate());
		Log.e("--------------","-----getPrvious3monthsDate()-------"+getPrvious3monthsDate());

		Date currentDate = null,depositDate = null,previousDate = null;

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		try {
			 currentDate = sdf.parse(getCurrentDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			 depositDate = sdf.parse(slipDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		try {
			 previousDate = sdf.parse(getPrvious3monthsDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}

		Log.e("------hhhh--------","-----etSlipDate-------"+depositDate);
		Log.e("-------hhhh-------","-----getCurrentDate()-------"+currentDate);
		Log.e("--------h-hhh-----","-----getPrvious3monthsDate()-------"+previousDate);

		if(depositDate.compareTo(currentDate)>0)
		{
			Toast.makeText(getApplicationContext(), "Select Correct Date!", Toast.LENGTH_LONG).show();
			return false;
		}


		if(depositDate.compareTo(previousDate)<0)
		{
			Toast.makeText(getApplicationContext(), "You can select only current and last month!", Toast.LENGTH_LONG).show();
			return false;
		}



		
         return true;
	}
	


	private void Ins_TableParse()
	{
		InsID.clear();
		InsName.clear();
		paymentId.clear();

		InsID.add("00");
		InsName.add("Select Ins No.");
		
		Cursor c = db.rawQuery("SELECT * FROM instrument_no");
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					
					String ins_id = c.getString(c.getColumnIndex("instrument_no_id"));
					String ins_name = c.getString(c.getColumnIndex("instrument_no_name"));
					String payment_id = c.getString(c.getColumnIndex("payment_id"));

					InsID.add(ins_id);
					InsName.add(ins_name);
					paymentId.add(payment_id);

				} while (c.moveToNext());
			}
			
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, InsName);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spInstrumentNo.setAdapter(dataAdapter);
		}
		
	}
	
	private void BankTableParse()
	{
		BankBanchID.clear();
		BankBanchName.clear();
		
		BankBanchID.add("0");
		BankBanchName.add("Select Bank Branch");
		
		Cursor c = db.rawQuery("SELECT * FROM bank_branch");
		c.moveToFirst();
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					
					String bank_banch_id = c.getString(c.getColumnIndex("bank_branch_id"));
					String bank_banch_name = c.getString(c.getColumnIndex("bank_branch_name"));
					
					BankBanchID.add(bank_banch_id);
					BankBanchName.add(bank_banch_name);
					
				} while (c.moveToNext());
			}
			
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, BankBanchName);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spBankBanch.setAdapter(dataAdapter);
		}
		
	}
	
	private void SalesWeek_TableParse()
	{
		SalesWeekID.clear();
		SalesWeekName.clear();
		
		SalesWeekID.add("0");
		SalesWeekName.add("Select Sales Week");
		
		Cursor c = db.rawQuery("SELECT * FROM sales_week");
		if (c != null) {
			if (c.moveToFirst()) {
				do {
					
					String sales_week_id = c.getString(c.getColumnIndex("sales_week_id"));
					String sales_week_name = c.getString(c.getColumnIndex("sales_week_name"));
					
					SalesWeekID.add(sales_week_id);
					SalesWeekName.add(sales_week_name);
					
				} while (c.moveToNext());
			}
			
			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, SalesWeekName);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spSalesWeek.setAdapter(dataAdapter);
			
			
			 if(!getPreference("SalesWeekID").equalsIgnoreCase("NO PREFERENCE")&&!getPreference("SalesWeekID").equalsIgnoreCase(""))
			    {
				   int SelectedPos=SalesWeekID.indexOf(getPreference("SalesWeekID"));
				   Log.e("SalesWeek POS:", ".........."+SelectedPos);
				   spSalesWeek.setSelection(SelectedPos);
			    }

		}
		
	}
	
	
	 @Override
	    protected Dialog onCreateDialog(int id) {
	       
		      if(id==DATE_PICKER_ID1)
		      return new DatePickerDialog(this, pickerListener1, year1, month1,day1);
		      if(id==DATE_PICKER_ID2)
		      return  new DatePickerDialog(this, pickerListener2, year2, month2,day2);
			
		      
		      return null;
			
			
	        
	       
	    }
	 
	 private DatePickerDialog.OnDateSetListener pickerListener1 = new DatePickerDialog.OnDateSetListener() {
		 
	        // when dialog box is closed, below method will be called.
	        @Override
	        public void onDateSet(DatePicker view, int selectedYear,
	                int selectedMonth, int selectedDay) {
	             
	            year1  = selectedYear;
	            month1 = selectedMonth;
	            day1  = selectedDay;
	 
	            String day="",month="";
	            if(day1<10)
	            day="0"+String.valueOf(day1);
	            else
	            day=String.valueOf(day1);	
	            if(month1+1<10)
	            	month="0"+String.valueOf(month1+1);
	            else
	            month=String.valueOf(month1+1);
	            Log.e("day","Day"+ String.valueOf(day));
	            Log.e("month", String.valueOf(month));

	            instrumentDate=new StringBuilder().append(year1 )
	                     .append("-").append(month).append("-").append(day)
	                     .append(" ").toString();
		        
			   etInstrumentDate.setText(DateFormatedConverter(instrumentDate));
			  
	            
	          
	     
	           }
	        };

	 
	    private DatePickerDialog.OnDateSetListener pickerListener2 = new DatePickerDialog.OnDateSetListener() {
	 
	        // when dialog box is closed, below method will be called.
	        @Override
	        public void onDateSet(DatePicker view, int selectedYear,
	                int selectedMonth, int selectedDay) {
	            
	        	
	        	
	        	
	            year2  = selectedYear;
	            month2 = selectedMonth;
	            day2   = selectedDay;
	            
	            String day="",month="";
	            if(day2<10)
	            day="0"+String.valueOf(day2);
	            else
	            day=String.valueOf(day2);

				Log.e("month", "-----"+month2);
	            if(month2==0 ||month2==1 || month2==2||month2==3||month2==4||month2==5||month2==6||month2==7||month2==8)
	            	month="0"+String.valueOf(month2+1);
	            else if(month2==9 ||month2==10 || month2==11)
	            month=String.valueOf(month2+1);

	            Log.e("day","Day"+ String.valueOf(day));
	            Log.e("month", String.valueOf(month));

	            slipDate=new StringBuilder().append(year2 )
	                     .append("-").append(month).append("-").append(day)
	                     .append(" ").toString();
		        
//	            etSlipDate.setText(DateFormatedConverter(slipDate));
	            etSlipDate.setText(slipDate);

	           }
	        };

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(Deposit_PostActivity.this, DepositActivity.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            ImageView imageView = (ImageView) findViewById(R.id.BrowseIm);
            imageView.setImageBitmap(BitmapFactory.decodeFile(picturePath));

        }


    }
	
	public void initiateButtons(ImageView btn, int id)
	{ 
		btn = (ImageView) findViewById(id);
		btn.setOnClickListener((OnClickListener) this);  
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.back)
		{

			Intent idd = new Intent(Deposit_PostActivity.this, DepositActivity.class);
			startActivity(idd);
			finish();
		}

		else if(v.getId()==R.id.home){
			Intent intent = new Intent(Deposit_PostActivity.this, Dashboard.class);
			startActivity(intent);
			finish();  
		}
	}

	private void instrument_TableParse()
	{
		InstID.clear();
		InstName.clear();

		InstID.add("0");
		InstName.add("Select Inst.Type");

		Cursor c = db.rawQuery("SELECT * FROM instrument_type where instrument_type_id!=1 and instrument_type_id!=2");
		if (c != null) {
			if (c.moveToFirst()) {
				do {

					String instrument_type_id = c.getString(c.getColumnIndex("instrument_type_id"));
					String instrument_type_name = c.getString(c.getColumnIndex("instrument_type_name"));

					InstID.add(instrument_type_id);
					InstName.add(instrument_type_name);

				} while (c.moveToNext());
			}

			ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Deposit_PostActivity.this,R.layout.spinner_text, InstName);
			dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spIns.setAdapter(dataAdapter);


			/*if(!getPreference("InstID").equalsIgnoreCase("NO PREFERENCE")&&!getPreference("SalesWeekID").equalsIgnoreCase(""))
			{
				int SelectedPos=InstID.indexOf(getPreference("SalesWeekID"));
				Log.e("SalesWeek POS:", ".........."+SelectedPos);
				spSalesWeek.setSelection(SelectedPos);
			}*/

		}

	}

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {

		if (i==1000){

			startActivity(new Intent(Deposit_PostActivity.this,DepositActivity.class));
			finish();
		}


	    if (i==111){
            try {
                setUpForInstrument(jsonObject);
                Deposit_mode=111;
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else{

            try {
                setUpForCash(Double.parseDouble(String.valueOf(jsonObject.getJSONArray("deposit_list").getJSONObject(0).getString("collectionAmount"))));
                Deposit_mode=112;
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("error",e.getMessage());
            }
        }

    }

    @Override
    public void OnConnetivityError() {

    }
}
