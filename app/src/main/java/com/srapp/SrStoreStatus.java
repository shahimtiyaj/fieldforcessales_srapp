package com.srapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Adapter.SrStoreStatusAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Model.OrderDetailsModel;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class SrStoreStatus extends AppCompatActivity implements BasicFunctionListener {

    RecyclerView recyclerView;
    SrStoreStatusAdapter statusAdapter;
    ArrayList<String>productNameList,unitNameList,quantityList,bookedQtyList,bonusqty;
    OrderDetailsModel obj;
    Spinner productTypeSpinner, productCategorySpinner;
    ImageView img;
    Data_Source ds;
    HashMap<String,ArrayList<String>> productCategoryList;
    HashMap<String,ArrayList<String>> productTypeList;
    HashMap<String,ArrayList<String>> stock;
    String typeId, categoryId;
    int filter= 0, byTypeId = 1, byCatId =2, byBoth = 3;
    boolean isLoading = false;
    ImageView homeBtn,backBtn;
    TextView userIdTV,titleTV ;

   //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner disSpinner;
    BasicFunction basicFunction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sr_store_status);
        basicFunction = new BasicFunction(this,this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();
        ds = new Data_Source(this);

        //View model factory class
        basicFunction = new BasicFunction(this,this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(ds.getDistributorFullData(basicFunction.getPreference(SR_ID)));


        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }
                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                disSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);


        recyclerView = findViewById(R.id.sr_status_list_recycler);
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(),DividerItemDecoration.VERTICAL));

        ds = new Data_Source(this);

        productCategoryList = new HashMap<>();
        productCategoryList = ds.getProductCategoryeList();
        productTypeList =new HashMap<>();
        productTypeList = ds.getProductTypeList();

        productTypeSpinner = findViewById(R.id.thana_spinner);
        disSpinner = findViewById(R.id.db_spinner);
        productCategorySpinner = findViewById(R.id.market_spinner);

        if (productTypeList != null && productTypeList.get(Tables.PPRODCUT_TYPE_PRODUCT_NAME).size()>0) {

            productTypeList.get(Tables.PPRODCUT_TYPE_PRODUCT_NAME).add(0, "All");
            productTypeList.get(Tables.PRODUCT_TYPE_ID).add(0, "x0x");

            SpinnerAdapter typeData = new SpinnerAdapter(this, R.layout
                    .spinner_item, productTypeList.get(Tables.PPRODCUT_TYPE_PRODUCT_NAME));
            productTypeSpinner.setAdapter(typeData);

            productCategoryList.get(Tables.PPRODUCT_CATEGORY_C_NAME).add(0, "All");
            productCategoryList.get(Tables.PPRODUCT_CATEGORY_C_id).add(0, "x0x");

            final SpinnerAdapter categoryData = new SpinnerAdapter(this, R.layout
                    .spinner_item, productCategoryList.get(Tables.PPRODUCT_CATEGORY_C_NAME));
            productCategorySpinner.setAdapter(categoryData);
        }

        disSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                try {
                    typeId = productTypeList.get(Tables.PRODUCT_TYPE_ID).get(position);
                } catch (IndexOutOfBoundsException e) {
                    e.printStackTrace();
                }
                Log.e("type & cat--->", "onTypeSelect: "+typeId+"  "+categoryId );

              // if (position == 0) {
                    dataViewModel.getDistributorId(parent.getItemAtPosition(position).toString());
                     distributorId = dataViewModel.distributorId;
                    Log.e("distributorId_from_spin", distributorId);

                    if (typeId != null) {

                        // only filter by type id
                        if (!typeId.equals("x0x")) {
                            // call api by type id only

                            if (categoryId != null && !categoryId.equals("x0x")) {
                                JSONObject primaryData = new JSONObject();
                                try {
                                    Log.e("if---->", "onItemSelected: " + typeId);
                                    primaryData.put("mac", basicFunction.getPreference("mac"));
                                    primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                    primaryData.put("store_id", distributorId);
                                    primaryData.put("product_type_id", typeId);
                                    primaryData.put("product_category_id", categoryId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                            } else {

                                JSONObject primaryData = new JSONObject();
                                try {
                                    Log.e("if---->", "onItemSelected: " + typeId);
                                    primaryData.put("mac", basicFunction.getPreference("mac"));
                                    primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                    primaryData.put("store_id", distributorId);
                                    primaryData.put("product_type_id", typeId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                            }
                        } else {
                            if (categoryId != null && !categoryId.equals("x0x")) {
                                JSONObject primaryData = new JSONObject();
                                try {
                                    Log.e("if---->", "onItemSelected: " + typeId);
                                    primaryData.put("mac", basicFunction.getPreference("mac"));
                                    primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                    primaryData.put("store_id", distributorId);
                                    primaryData.put("product_category_id", categoryId);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                            } else if (categoryId != null && categoryId.equals("x0x")) {
                                JSONObject primaryData = new JSONObject();
                                try {
                                    Log.e("if---->", "onItemSelected: type " + typeId + " cat " + categoryId);
                                    primaryData.put("mac", basicFunction.getPreference("mac"));
                                    primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                    primaryData.put("store_id", distributorId);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                            }

                        }
                    }
                }


           /*    else {
                    dataViewModel.subjectId = null;
                }

            }*/

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        productTypeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                typeId = productTypeList.get(Tables.PRODUCT_TYPE_ID).get(position);
                Log.e("type & cat--->", "onTypeSelect: "+typeId+"  "+categoryId );
                if (typeId != null) {

                    // only filter by type id
                    if ( !typeId.equals("x0x")) {
                        // call api by type id only

                        if (categoryId != null && !categoryId.equals("x0x")) {
                            JSONObject primaryData = new JSONObject();
                            try {
                                Log.e("if---->", "onItemSelected: " + typeId);
                                primaryData.put("mac", basicFunction.getPreference("mac"));
                                primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                primaryData.put("store_id", distributorId);

                                primaryData.put("product_type_id", typeId);
                                primaryData.put("product_category_id", categoryId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                        }
                        else {

                            JSONObject primaryData = new JSONObject();
                            try {
                                Log.e("if---->", "onItemSelected: " + typeId);
                                primaryData.put("mac", basicFunction.getPreference("mac"));
                                primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                primaryData.put("store_id", distributorId);
                                primaryData.put("product_type_id", typeId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                        }
                    }
                    else {
                        if (categoryId != null && !categoryId.equals("x0x")) {
                            JSONObject primaryData = new JSONObject();
                            try {
                                Log.e("if---->", "onItemSelected: " + typeId);
                                primaryData.put("mac", basicFunction.getPreference("mac"));
                                primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                primaryData.put("store_id", distributorId);
                                primaryData.put("product_category_id", categoryId);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                        }
                        else if (categoryId != null && categoryId.equals("x0x")){
                            JSONObject primaryData = new JSONObject();
                            try {
                                Log.e("if---->", "onItemSelected: type " + typeId+" cat "+categoryId);
                                primaryData.put("mac", basicFunction.getPreference("mac"));
                                primaryData.put("sales_person_id", basicFunction.getPreference("sales_person_id"));
                                primaryData.put("store_id", distributorId);

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData), 101);

                        }

                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        productCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                categoryId = productCategoryList.get(Tables.PPRODUCT_CATEGORY_C_id).get(position);
                Log.e("type & cat--->", "onCatSelect: "+typeId+"  "+categoryId );

                if ( typeId.equals("x0x") && categoryId.equals("x0x")){
                    //call api for all data in very 1st
                    JSONObject primaryData = new JSONObject();
                    try {
                        primaryData.put("mac",basicFunction.getPreference("mac"));
                        primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                        primaryData.put("store_id", distributorId);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData),101);

                }else {
                    if (typeId.equals("x0x") && !categoryId.equals("x0x")){
                        // call api by category id
                        JSONObject primaryData = new JSONObject();
                        try {
                            primaryData.put("mac",basicFunction.getPreference("mac"));
                            primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                            primaryData.put("store_id", distributorId);
                            primaryData.put("product_category_id",categoryId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData),101);

                    }
                    else if (!typeId.equals("x0x") && !categoryId.equals("x0x")){
                        //call api by type and cat id
                        JSONObject primaryData = new JSONObject();
                        try {
                            primaryData.put("mac",basicFunction.getPreference("mac"));
                            primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                            primaryData.put("store_id", distributorId);
                            primaryData.put("product_category_id",categoryId);
                            primaryData.put("product_type_id",typeId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData),101);

                    }
                    else if (!typeId.equals("x0x") && categoryId.equals("x0x")){
                        //call api by type and cat id
                        JSONObject primaryData = new JSONObject();
                        try {
                            primaryData.put("mac",basicFunction.getPreference("mac"));
                            primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                            primaryData.put("store_id", distributorId);
                            primaryData.put("product_type_id",typeId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData),101);

                    }

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        Log.e("type & cat--->", "onCreate: "+typeId+"  "+categoryId );
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent( SrStoreStatus.this, Dashboard.class));
                finishAffinity();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent( SrStoreStatus.this, Dashboard.class));
                finishAffinity();
            }
        });
    }

    @Override
    protected void onDestroy() {

        super.onDestroy();
    }



    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        Log.e("json response", "OnServerResponce: ---> "+jsonObject );
        if (i==101){
            productNameList = new ArrayList<>();
            unitNameList = new ArrayList<>();
            quantityList = new ArrayList<>();
            bookedQtyList = new ArrayList<>();
            bonusqty = new ArrayList<>();
            stock = new HashMap<>();
            try {
                if (jsonObject.getJSONArray("stock_info").length()>0){
                    for (int x = 0; x < jsonObject.getJSONArray("stock_info").length();x++){
                        productNameList.add( jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("product_name"));
                        bonusqty.add(jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("bonus_quantity"));
                        String id = jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("unit");
                        unitNameList.add(ds.getUnitName(id));
                        double stock, book, available,bonusQty,bonusBookQty,bonusAvailable;
                        if (!jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("quantity").equalsIgnoreCase("null")) {
                            stock = Double.parseDouble(jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("quantity"));
                        }else {
                            stock=0.00;
                        }
                        if (!jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("booking_quantity").equalsIgnoreCase("null")) {
                            book = Double.parseDouble(jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("booking_quantity"));
                        }else {
                            book=0.00;
                        }

                        available = stock - book;
                        quantityList.add( String.valueOf(available));
                      //  bonusQty = Double.parseDouble(jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("bonus_quantity"));
                       // bonusBookQty = Double.parseDouble(jsonObject.getJSONArray("stock_info").getJSONObject(x).getString("bonus_booking_quantity"));
                        //bonusAvailable = bonusQty - bonusBookQty;

                      //  bookedQtyList.add( String.valueOf(bonusAvailable));
                    }

                    stock.put(Tables.PRODUCT_PRODUCT_NAME,productNameList);
                    stock.put(Tables.UNIT_UNAME,unitNameList);
                    stock.put(Tables.Quantity,quantityList);
                    stock.put(Tables.BookedQuantity,bookedQtyList);
                    stock.put("bonusqty",bonusqty);

                }
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("jsex",e.getMessage());
            }

            Log.e("json", "OnServerResponce: got res");
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            if (stock != null && stock.size()>0 ) {
                Log.e("inside if---->", "OnServerResponce: "+stock );
                statusAdapter = new SrStoreStatusAdapter();
                statusAdapter.bindData(stock);
                recyclerView.setAdapter(statusAdapter);
            }else {
                recyclerView.setAdapter(null);
            }

        }

    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(SrStoreStatus.this,"No Internet Connection",Toast.LENGTH_SHORT).show();

    }

    public void loadMoreData(int page){
        isLoading= true;
        Log.e("page number......", ".......>>loadMoreData: "+page );
        JSONObject primaryData = new JSONObject();
        try {
            primaryData.put("mac",basicFunction.getPreference("mac"));
            primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
            if (filter == byTypeId){
                primaryData.put("product_type_id",typeId);

            }else if (filter == byCatId){
                primaryData.put("product_category_id",categoryId);
            }else if (filter == byBoth){
                primaryData.put("product_type_id",typeId);
                primaryData.put("product_category_id",categoryId);
            }
            primaryData.put("page",page);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        basicFunction.getResponceData(URL.Stock, String.valueOf(primaryData),102);

    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode==KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( SrStoreStatus.this, Dashboard.class));
            finishAffinity();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
