package com.srapp.Model;

import android.media.Image;

public class NCP_Collection {
    String name;
    Image image;

    public NCP_Collection(String name, Image image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }
}
