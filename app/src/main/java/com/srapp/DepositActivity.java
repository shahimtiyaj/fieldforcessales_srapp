package com.srapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import com.srapp.Util.Parent;

import java.util.ArrayList;
import java.util.List;

public class DepositActivity extends Parent implements  OnClickListener, OnItemClickListener{

	public static final String[] titles = new String[] { "Post Deposit", 
		"Deposit List"
	};


	private ListView listView;
	private List<RowItem> rowItems;

	private ImageView btnBack, btnHome;
	TextView HeaderTitleTv, CodeNoTv;

	String Distributor_Id="",RSO_Id="",RSO_Code="";
	public static final String MyPREFERENCES = "MyPrefs" ;
	SharedPreferences sharedpreferences;

	@Override 
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.outlet_account_details);
		
		TextView txtUser=(TextView)findViewById(R.id.txtUser);
		txtUser.setText(getPreference("UserName"));




		initiateButtons(btnBack, R.id.back);
		initiateButtons(btnHome, R.id.home);
		
		
		DisplayMetrics dm = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(dm);
		int width=dm.widthPixels/15;
		
		
		LinearLayout layout = (LinearLayout)findViewById(R.id.layoutListview1);
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.MATCH_PARENT);            
		params.setMargins(width, 0, width, 0); 
		layout.setLayoutParams(params);
		
		
		listView = (ListView) findViewById(R.id.listViewForOutletAccount);
		
		rowItems = new ArrayList<RowItem>();


		for (int i = 0; i < titles.length; i++) {
			RowItem item = new RowItem(titles[i]);
			rowItems.add(item);
		}

		
		CustomAdapter adapter = new CustomAdapter(this,
				R.layout.list_item_leaf, rowItems);
		listView.setAdapter(adapter); 
		listView.setOnItemClickListener(this);


		/*HomeBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(OutletAccountDetailsActivity.this,DashboardActivity.class);
				startActivity(idn);
				finish();
			}
		});
		
		BackBtn.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Intent idn = new Intent(OutletAccountDetailsActivity.this,DashboardActivity.class);
				startActivity(idn);
				finish();
			}
		});*/

	}





	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		// TODO Auto-generated method stub
		Intent intent;

		switch (position) {
		case 0:

			intent = new Intent(DepositActivity.this, Deposit_PostActivity.class);
			startActivity(intent);
			finish();  
			break;

		case 1:
			intent = new Intent(DepositActivity.this, LedgerActivity_New.class);
			startActivity(intent);
			finish();  
			break;
	

			/*case 3:
			intent = new Intent(OutletAccountDetailsActivity.this, Current_Offer_List_Activation.class);
			startActivity(intent);
			finish();  
			break;

		case 4:
			intent = new Intent(OutletAccountDetailsActivity.this, RSO_Target_Activity.class);
			startActivity(intent);
			finish();  
			break;
		case 5:
			intent = new Intent(OutletAccountDetailsActivity.this, RSO_Commissions_Activity.class);
			startActivity(intent);
			finish();  
			break;
		case 6:
			intent = new Intent(OutletAccountDetailsActivity.this, Market_Photo_upload_Activation.class);
			startActivity(intent);
			finish();  
			break;
		case 7:
			intent = new Intent(OutletAccountDetailsActivity.this, TopBottom_List_Activity.class);
			startActivity(intent);
			finish();  
			break;

		case 8:
			intent = new Intent(OutletAccountDetailsActivity.this, New_Outlet_Create_Activation.class);
			startActivity(intent);
			finish();  
			break;
			
		case 9:
			intent = new Intent(OutletAccountDetailsActivity.this, Pending_Market_Upload_Activity.class);
			startActivity(intent);
			finish();  
			break;
			
		case 10:
			intent = new Intent(OutletAccountDetailsActivity.this, PendingSMSRequest_Activity.class);
			startActivity(intent);
			finish();  
			break;


		default:
			break;*/
		}
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if(keyCode==KeyEvent.KEYCODE_BACK)
		{
			Intent idd = new Intent(DepositActivity.this, Tools.class);
			startActivity(idd);
			finish();
			return true;
		}
		return super.onKeyDown(keyCode, event);

	}
	
	public void initiateButtons(ImageView btn, int id)
	{ 
		btn = (ImageView) findViewById(id);
		btn.setOnClickListener((OnClickListener) this);  
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.back)
		{

			Intent idd = new Intent(DepositActivity.this, Tools.class);
			startActivity(idd);
			finish();
		}

		else if(v.getId()==R.id.home){
			Intent intent = new Intent(DepositActivity.this, Dashboard.class);
			startActivity(intent);
			finish();  
		}
	}
}
