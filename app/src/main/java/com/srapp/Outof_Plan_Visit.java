package com.srapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.JsonReader;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Model.SR_Account;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.URL.ORDERS_FOR_PROCESS;

public class Outof_Plan_Visit extends AppCompatActivity implements BasicFunctionListener {
    Spinner routesp, marketSpinner;
    ArrayList<String> Route_id = new ArrayList<String>();
    ArrayList<String> Route_name = new ArrayList<String>();
    ArrayList<String> MarketID = new ArrayList<String>();
    ArrayList<String> MarketName = new ArrayList<String>();
    Data_Source db;
    Button date,saveBtn;
    BasicFunction bf;
    ImageView homeBtn,backBtn;
    private DatePickerDialog DatePickerDialog;
    private SimpleDateFormat dateFormatter, dateFormatter2;
    String route_id, market_id,  datet;
    EditText remarks;
    TextView userIdTV;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorSpinner;
    BasicFunction basicFunction;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outof__plan__visit);

        db = new Data_Source(this);
        bf = new BasicFunction(this, this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();
        basicFunction = new BasicFunction(this,this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        routesp = findViewById(R.id.thana_spinner);
        saveBtn = findViewById(R.id.sendMessageBtn);
        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);
        userIdTV = findViewById(R.id.user_txt_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);
        remarks = findViewById(R.id.remarks);
        date = findViewById(R.id.date);
        marketSpinner = findViewById(R.id.market_spinner);
        dateFormatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.US);
        dateFormatter2 = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        date.setText(bf.getCurrentDate());
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Outof_Plan_Visit.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Outof_Plan_Visit.this, SR_Account_Activity.class));
                finish();
            }
        });
        Calendar newCalendar = Calendar.getInstance();
        DatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("start_Date", dateFormatter.format(newDate.getTime()));
                date.setText(bf.getPreference("start_Date"));
                datet = dateFormatter2.format(newDate.getTime());

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


     /*   ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, );*/
     //   RouteParse();
        distributorSpinner = findViewById(R.id.distributor_spinner);

        distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.fontBlackEnable));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                Log.e("dbId_cancel_order", distributorStoreId);

                if (distributorStoreId!=null){
                    Log.e("dbId_cancel_o_select", distributorStoreId);
                    RouteParse();

                }
                else {
                    Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        routesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                route_id = Route_id.get(position);
                MarketParse(route_id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        marketSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                market_id = MarketID.get(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DatePickerDialog.show();
            }
        });
        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("visit_date",datet);
                    jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                    jsonObject.put("route_id",route_id);
                    jsonObject.put("market_id",market_id);
                    jsonObject.put("db_id",distributorStoreId);
                    jsonObject.put("mac",bf.getPreference("mac"));
                    jsonObject.put("remarks",remarks.getText().toString().trim());
                    bf.getResponceData(URL.OUT_OF_PLAN_VISIT,jsonObject.toString(),100);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( Outof_Plan_Visit.this, SR_Account_Activity.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    private void RouteParse() {
        Route_id.clear();
        Route_name.clear();
        Cursor c = db.rawQuery("SELECT * FROM route where db_id= " + distributorStoreId);
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String Thana_Id = c.getString(c.getColumnIndex("route_id"));
                    String Thana_Name = c.getString(c.getColumnIndex("route_name"));

                    Route_id.add(Thana_Id);
                    Route_name.add(Thana_Name);

                } while (c.moveToNext());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Outof_Plan_Visit.this, R.layout.spinner_text, Route_name);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            routesp.setAdapter(dataAdapter);

        }

    }

    private void MarketParse(String Route_ID) {
        MarketID.clear();
        MarketName.clear();
        Cursor c = db.rawQuery("SELECT * FROM markets where route_id=" + "'" + Route_ID + "' and is_active='1' ORDER BY market_name COLLATE NOCASE ASC");

        Log.e("querymarket", "SELECT * FROM markets where route_id=" + "'" + Route_ID + "' and is_active!='1' ORDER BY market_name COLLATE NOCASE ASC");
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String Market_Id = c.getString(c.getColumnIndex("market_id"));
                    String Market_Name = c.getString(c.getColumnIndex("market_name"));

                    MarketID.add(Market_Id);
                    MarketName.add(Market_Name);


                } while (c.moveToNext());
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(Outof_Plan_Visit.this, R.layout.spinner_text, MarketName);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            marketSpinner.setAdapter(dataAdapter);


        }

    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        if (i==100){
            try {
                JSONObject jsonObject1 = jsonObject.getJSONObject("visit_plan");
                Toast.makeText(this,jsonObject1.getString("message"),Toast.LENGTH_SHORT).show();
                Intent ii = new Intent(Outof_Plan_Visit.this, SR_Account_Activity.class);
                startActivity(ii);
                finish();

            } catch (JSONException e) {


            }
        }

    }

    @Override
    public void OnConnetivityError() {

    }
}
