package com.srapp;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.AdapterForSalesReport;
import com.srapp.Adapter.OrderAdapter;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Model.Order_Report;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;
import com.weiwangcn.betterspinner.library.material.MaterialBetterSpinner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static com.srapp.Db_Actions.Tables.OUTLETS;
import static com.srapp.Db_Actions.Tables.OUTLETS_CATAGORY_ID;
import static com.srapp.Db_Actions.Tables.OUTLET_CATAGORY;
import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.TempData.DB_ID;
import static com.srapp.TempData.DB_ID_ORDER_REPORT;

public class Order_Report_Activity extends AppCompatActivity implements View.OnClickListener, BasicFunctionListener {
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    String[] SPINNERLIST = {"Rahim Store", "Jamal Electronics", "Jakir Pharmacy"};
    ListView listview;
    Button startdate, enddate;
    HashMap<String, ArrayList<String>> outlate = new HashMap<>();
    HashMap<String, ArrayList<String>> outlet_catagary = new HashMap<>();
    ListView Order_Report_recycleView;
    Data_Source db;
    BasicFunction bf;
    Spinner outlatesp, outlet_catagary_spinner;
    ArrayList<HashMap<String, String>> list;
    boolean onResunme=false;
    boolean onstart=false;

    ImageView homeBtn,backBtn;
    TextView userIdTV,titleTV  ;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorCreateSpinner;
    BasicFunction basicFunction;

    int flag=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order__report_);

        db=new Data_Source(getApplicationContext());
        basicFunction = new BasicFunction(this, this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));
        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {
                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }
                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorCreateSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        bf = new BasicFunction(this, this);
        Order_Report_recycleView = findViewById(R.id.Order_Report_recycleView);
        startdate = findViewById(R.id.startdate);
        enddate = findViewById(R.id.enddate);
        list = new ArrayList<>();
        db = new Data_Source(this);
        outlate = db.getAccessories(false, "00", OUTLETS, "no");
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        outlate.get(OUTLETS[3]).add(0, "All");
        outlate.get(OUTLETS[2]).add(0, "0");

       /* ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,
                R.layout.spinner_item, outlate.get(OUTLETS[3]));*/

        SpinnerAdapter arrayAdapter = new SpinnerAdapter(this, R.layout
                .spinner_item, outlate.get(OUTLETS[3]));


        outlatesp = findViewById(R.id.outlet_spinner);
        outlet_catagary_spinner = findViewById(R.id.outlet_catagary_spinner);
        distributorCreateSpinner = (Spinner) findViewById(R.id.distributor_spinner);

        outlet_catagary = db.getAccessories(false, "00", OUTLET_CATAGORY, "no");

        /*ArrayAdapter<String> arrayAdapter_catagory = new ArrayAdapter<String>(this,
                R.layout.spinner_item, outlet_catagary.get(OUTLET_CATAGORY[3]));*/

        SpinnerAdapter arrayAdapter_catagory = new SpinnerAdapter(this, R.layout
                .spinner_item, outlet_catagary.get(OUTLET_CATAGORY[3]));

        outlet_catagary_spinner.setAdapter(arrayAdapter_catagory);

        /*SpinnerAdapter arrayAdapter_catagory = new SpinnerAdapter(this, R.layout
                .spinner_item, outlet_catagary.get(OUTLET_CATAGORY[3]));*/

        outlatesp.setAdapter(arrayAdapter);

        setDateTimeField();

        bf.savePreference("outlet_id", "0");
        bf.savePreference("outlet_category_id_report", "0");

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePickerDialog.show();
            }
        });


        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Order_Report_Activity.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Order_Report_Activity.this, Reports_Activity.class));
                finish();
            }
        });


        // Distributor dropdown data------------------------------------------------------------------
         distributorCreateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                // TODO Auto-generated method stub
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                Log.e("distributorId_from_spin", distributorStoreId);

                if (distributorStoreId!=null){

                    DB_ID_ORDER_REPORT=distributorStoreId;

                    DataView();

                    outlate = db.getAccessories1(distributorStoreId, "0");
                    outlate.get(OUTLETS[3]).add(0, "All");
                    outlate.get(OUTLETS[2]).add(0, "0");
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Order_Report_Activity.this, R.layout.spinner_item, outlate.get(OUTLETS[3]));
                    outlatesp.setAdapter(arrayAdapter);
                }
                else {
                    Log.e("routEmpty", distributorStoreId);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {

            }
        });

        outlet_catagary_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView)parent.getChildAt(0);
                if (textView!=null) {
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0, 0, 0, 0);
                }

                    // outlate = db.getAccessories(true, outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position), OUTLETS, OUTLETS_CATAGORY_ID);

                try {
                    if (!outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position).equals("0")){

                        Log.e("outlet_category_id", outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));
                        outlate.clear();
                        outlate = db.getAccessories1(distributorStoreId, outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));

                        outlate.get(OUTLETS[3]).add(0, "All");
                        outlate.get(OUTLETS[2]).add(0, "0");

                        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Order_Report_Activity.this, R.layout.spinner_item, outlate.get(OUTLETS[3]));
                        outlatesp.setAdapter(arrayAdapter);
                        bf.savePreference("outlet_category_id_report", outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));


                        DataView();
                    }

                    else {
                        if (position==0){
                            bf.savePreference("outlet_category_id_report", "0");
                            DataView();
                        }

                        Log.e("outletCategory_id", String.valueOf(outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

               /* if (onstart) {
                    DataView();
                    onstart=false;
                }*/

    /*            try {
                    TextView textView = (TextView)parent.getChildAt(0);
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0,0,0,0);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                outlate.clear();
                try {
                    // outlate = db.getAccessories1(true, outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position), OUTLETS, OUTLETS_CATAGORY_ID, "294");
                      outlate = db.getAccessories1(distributorStoreId, outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));
                    //outlate.get(OUTLETS[3]).add(0, "All"); // chnagees 13/01/2020
                    //outlate.get(OUTLETS[2]).add(0, "0");
                    SpinnerAdapter arrayAdapter = new SpinnerAdapter(Order_Report_Activity.this, R.layout
                            .spinner_item, outlate.get(OUTLETS[3]));
                    outlatesp.setAdapter(arrayAdapter);
                    bf.savePreference("outlet_category_id_report",outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));
                    *//*ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(Order_Report_Activity.this, R.layout.spinner_item, outlate.get(OUTLETS[3]));*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }
                 if (outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position)!=null || !outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position).equals("0")){
                     DataView();
                 }

                 else {
                     Log.e("outletcategory", outlet_catagary.get(OUTLETS_CATAGORY_ID).get(position));
                 }

              *//*  if (onstart) {
                   // DataView();
                    onstart=false;
                }*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        // outlatesp.setOnItemSelectedListener()
        outlatesp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView)parent.getChildAt(0);
                if (textView!=null) {
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0, 0, 0, 0);
                }
                bf.savePreference("outlet_id", outlate.get(OUTLETS[2]).get(position));
                if(!outlate.get(OUTLETS[2]).get(position).equals("0")){
                    DataView();
                    Log.e("outlet_id_send", outlate.get(OUTLETS[2]).get(position));
                }
                else {

                }
                if (onstart) {
                    //  DataView();
                }

            /*    TextView textView = (TextView)parent.getChildAt(0);
                if (textView!=null) {
                    textView.setTextColor(getResources().getColor(R.color.background_card));
                    textView.setPadding(0, 0, 0, 0);
                  }
                try {
                    bf.savePreference("outlet_id", outlate.get(OUTLETS[2]).get(position));
                    if (outlate.get(OUTLETS[2]).get(position)!=null || !outlate.get(OUTLETS[2]).get(position).equals("0")){
                        DataView();
                    }
                    else {
                        Log.e("outletcategory", outlate.get(OUTLETS[2]).get(position));
                    }
                    *//*if (onstart) {
                        DataView();
                    }*//*
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        listview = findViewById(R.id.Order_Report_recycleView);


        //DataView();

        //onstart=true;

    }


    @Override
    public void onClick(View v) {

    }


    private void setDateTimeField() {
        bf.savePreference("start_Date", bf.getCurrentDate());
        bf.savePreference("end_date", bf.getCurrentDate());
        startdate.setText(bf.getCurrentDate());
        enddate.setText(bf.getCurrentDate());
        startdate.setOnClickListener(this);
        enddate.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("start_Date", dateFormatter.format(newDate.getTime()));
                startdate.setText(bf.getPreference("start_Date"));
               // if (onstart)
               // if (distributorStoreId!=null)
                    //DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("end_date", dateFormatter.format(newDate.getTime()));
                enddate.setText(bf.getPreference("end_date"));
               // if (distributorStoreId!=null)
                   // DataView();

               /* if (onstart) {
                    DataView();
                }*/

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


	       /* Intent idn = new Intent(SO_TargetActivity.this, SO_TargetActivity.class);
			 startActivity(idn);
			 finish();*/
    }


    @Override
    protected void onResume() {
        super.onResume();
      /*  if (onResunme)
        //DataView();
        onResunme=true;*/

    }

    private void  DataView() {

        Log.e("DataView", "DataView");

        if (bf.isInternetOn()) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("start_date", bf.getPreference("start_Date"));
                jsonObject.put("end_date", bf.getPreference("end_date"));
                jsonObject.put("outlet_id", bf.getPreference("outlet_id"));
                if (!bf.getPreference("outlet_category_id_report").equals("0")){
                    jsonObject.put("outlet_category_id", bf.getPreference("outlet_category_id_report"));
                }
                else {
                    jsonObject.put("outlet_category_id", 0);
                }
                jsonObject.put("db_id", distributorStoreId);
                jsonObject.put(SR_ID, bf.getPreference(SR_ID));
                jsonObject.put("mac", bf.getPreference("mac"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            try {
                bf.getResponceData(URL.GET_LAST_RECORD, jsonObject.toString(), 101);
            } catch (Exception e) {

            }
            Log.e("order_report_data",jsonObject.toString());
        }else {
            TextView txtTotalAmount = findViewById(R.id.TotalAmount);
            TextView TotalEC= findViewById(R.id.TotalEC);
            txtTotalAmount.setText("0.0");
            TotalEC.setText("0.0");
            list = db.getNotPushedOrder(bf.getPreference("start_Date"), bf.getPreference("end_date"), bf.getPreference("outlet_id"),bf.getPreference("outlet_category_id_report"), distributorStoreId);
            AdapterForSalesReport adapter = new AdapterForSalesReport(this, list);
            listview.setAdapter(adapter);
        }
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        onstart=true;
        TextView txtTotalAmount = findViewById(R.id.TotalAmount);
        TextView TotalEC= findViewById(R.id.TotalEC);
        txtTotalAmount.setText("0.0");
        TotalEC.setText("0.0");
        //db.excQuery("delete  from order_table");

        db.updateWithServer(jsonObject);

        Log.e("jsonObject_update", jsonObject.toString());

        list = db.getNotPushedOrder(bf.getPreference("start_Date"), bf.getPreference("end_date"), bf.getPreference("outlet_id"),bf.getPreference("outlet_category_id_report"), distributorStoreId);
        AdapterForSalesReport adapter = new AdapterForSalesReport(this, list);
        listview.setAdapter(adapter);


    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        /*if ( progressDialog!=null && progressDialog.isShowing() ){
            progressDialog.cancel();
        }*/
    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(this,"No Internet Connetion",Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( Order_Report_Activity.this, Reports_Activity.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }


}
