package com.srapp;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.srapp.Adapter.AdapterForOrderProcessShow;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Util.Parent;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.URL.Log;
import static com.srapp.Db_Actions.URL.ORDERS_FOR_PROCESS;

public class OrderProcess extends Parent implements BasicFunctionListener {

    Button date,processorder,clear;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog fromDatePickerDialog;
    ListView order_list;
    AdapterForOrderProcessShow  adapter;
    ArrayList<HashMap<String,String>> arrayList;
    BasicFunction bf;
    ImageView homeBtn,backBtn;
    TextView userIdTV,titleTV;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorCreateSpinner;
    Data_Source db;
    BasicFunction basicFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_process);

        db = new Data_Source(this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();
        basicFunction=new BasicFunction(this, this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorCreateSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( OrderProcess.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( OrderProcess.this, Tools.class));
                finish();
            }
        });

        date = findViewById(R.id.date);
        arrayList = new ArrayList<>();
        processorder = findViewById(R.id.processorder);
        order_list = findViewById(R.id.order_list);
        clear = findViewById(R.id.clear);
        bf = new BasicFunction(this,this);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                fromDatePickerDialog.show();
            }
        });
        processorder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



            }
        });

       /* JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put(SR_ID,bf.getPreference(SR_ID));
            jsonObject.put("mac",bf.getPreference("mac"));
            jsonObject.put("db_id", distributorStoreId);

            //  jsonObject.put("order_date","all");
            bf.getResponceData(ORDERS_FOR_PROCESS,jsonObject.toString(),100);
        } catch (JSONException e) {
            e.printStackTrace();
        }*/

        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                    jsonObject.put("mac",bf.getPreference("mac"));
                    jsonObject.put("db_id", distributorStoreId);

                    //  jsonObject.put("order_date","all");
                    bf.getResponceData(ORDERS_FOR_PROCESS,jsonObject.toString(),100);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("Date", dateFormatter.format(newDate.getTime()));
                date.setText(bf.getPreference("Date"));
                try {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                    jsonObject.put("mac",bf.getPreference("mac"));
                    jsonObject.put("order_date",bf.getPreference("Date"));
                    jsonObject.put("db_id", distributorStoreId);

                    bf.getResponceData(ORDERS_FOR_PROCESS,jsonObject.toString(),100);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        distributorCreateSpinner = findViewById(R.id.distributor_spinner);
        distributorCreateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;

                if (distributorStoreId!=null){
                    JSONObject jsonObject = new JSONObject();

                    try {
                        jsonObject.put(SR_ID,bf.getPreference(SR_ID));
                        jsonObject.put("mac",bf.getPreference("mac"));
                        jsonObject.put("db_id", distributorStoreId);

                        //  jsonObject.put("order_date","all");
                        bf.getResponceData(ORDERS_FOR_PROCESS,jsonObject.toString(),100);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                else {

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


    }


    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        arrayList.clear();
        android.util.Log.e("json",jsonObject.toString());


        try {
            JSONArray jsonArray = jsonObject.getJSONArray("orders");
            for (int j = 0 ; j<jsonArray.length(); j++){
                HashMap<String,String> map = new HashMap<>();

                map.put("order_number",jsonArray.getJSONObject(j).getString("order_number"));
                map.put("outlet_name",jsonArray.getJSONObject(j).getString("outlet_name"));
                map.put("gross_value",jsonArray.getJSONObject(j).getString("gross_value"));
                map.put("order_date",jsonArray.getJSONObject(j).getString("order_date"));
                map.put("status","2");

                arrayList.add(map);
            }

            adapter = new AdapterForOrderProcessShow(this,arrayList);
            order_list.setAdapter(adapter);


        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(OrderProcess.this,"No Internet Connection", Toast.LENGTH_SHORT).show();


    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( OrderProcess.this, Tools.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
