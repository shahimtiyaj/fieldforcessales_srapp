package com.srapp;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Db_Actions.Tables;
import com.srapp.Util.Parent;

public class GPS_UPdate extends Parent {

    TextView location,clocation;
    Button update_loc;
    private static  final int REQUEST_LOCATION=1;
    String lattitude,longitude;
    LocationManager locationManager;
    ImageView backBtn, homeBtn;

    TextView userIdTV,titleTV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gps__update);
        location = findViewById(R.id.location);
        clocation = findViewById(R.id.clocation);
        update_loc = findViewById(R.id.update_loc);

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( GPS_UPdate.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( GPS_UPdate.this, Create_New_Memo.class));
                finish();
            }
        });

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);
        
        
        location.setText(getPreference("lat")+" ,\n"+getPreference("lon"));
        clocation.setText("Not Updated");
        
        
        update_loc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                locationManager=(LocationManager) getSystemService(Context.LOCATION_SERVICE);

                //Check gps is enable or not

                if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER))
                {
                    //Write Function To enable gps
                    OnGPS();
                }
                else
                {
                    //GPS is already On then
                    getLocation();
                }
                
                
            }
        });
        
    }

    private void getLocation() {

        //Check Permissions again

        if (ActivityCompat.checkSelfPermission(GPS_UPdate.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(GPS_UPdate.this,

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this,new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        }
        else
        {
            android.location.Location LocationGps= locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            android.location.Location LocationNetwork=locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            android.location.Location LocationPassive=locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps !=null)
            {
                double lat=LocationGps.getLatitude();
                double longi=LocationGps.getLongitude();
                lattitude=String.valueOf(lat);
                longitude=String.valueOf(longi);

                Log.e("Location", "getLocation: +"+"Your Location:"+"\n"+"Latitude= "+lattitude+"\n"+"Longitude= "+longitude);
                Toast.makeText(GPS_UPdate.this,"GPS Updated Successfully", Toast.LENGTH_LONG).show();
            }
            else if (LocationNetwork !=null)
            {
                double lat=LocationNetwork.getLatitude();
                double longi=LocationNetwork.getLongitude();

                lattitude=String.valueOf(lat);
                longitude=String.valueOf(longi);

                clocation.setText(getPreference("lat")+" ,\n"+getPreference("lon"));
                Log.e("Loc", "getLocation: "+"Your Location:"+"\n"+"Latitude= "+lattitude+"\n"+"Longitude= "+longitude);
                Toast.makeText(GPS_UPdate.this,"GPS Updated Successfully", Toast.LENGTH_LONG).show();

                savePreference("lat",lattitude);
               savePreference("lon",longitude);
            }
            else if (LocationPassive !=null)
            {
                double lat=LocationPassive.getLatitude();
                double longi=LocationPassive.getLongitude();

                lattitude=String.valueOf(lat);
                longitude=String.valueOf(longi);
               savePreference("lat",lattitude);
                savePreference("lon",longitude);
                clocation.setText(getPreference("lat")+" ,\n"+getPreference("lon"));

                Log.e("Loc", "getLocation: "+"Your Location:"+"\n"+"Latitude= "+lattitude+"\n"+"Longitude= "+longitude);
                // Toast.makeText(GPS_UPdate.this,"Lat : "+lattitude+" Long: "+longitude, Toast.LENGTH_LONG).show();
            }
            else
            {
                Toast.makeText(this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

            //Thats All Run Your App
        }

    }

    private void OnGPS() {

        final AlertDialog.Builder builder= new AlertDialog.Builder(this);

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final AlertDialog alertDialog=builder.create();
        alertDialog.show();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent idd = new Intent(GPS_UPdate.this, Create_New_Memo.class);
            startActivity(idd);
            finish();
        }
        return super.onKeyDown(keyCode, event);

    }
    
}
