package com.srapp.apiService

import com.google.gson.JsonObject
import com.srapp.kotlin.Distributer
import com.srapp.kotlin.GPSTrackResponse
import com.srapp.kotlin.GPSTracking
import retrofit2.Call
import retrofit2.http.*
import java.util.*


interface ApiInterface {

    @Headers("Content-Type: application/json")
    @POST("dist_data_pull.json")
    fun getDistributorData(@Body jsonObject: JsonObject): Call<Distributer>

    @Headers("Content-Type: application/json")
    @POST("live_sales_tracking.json")
    fun getGPSTrackingData(@Body jsonObject: JsonObject): Call<GPSTracking>


    @Headers("Content-Type: application/json")
    @POST("map_sales_tracking.json")
    fun postGPSTrackingData(@Body jsonObject: JsonObject): Call<GPSTrackResponse>

}


