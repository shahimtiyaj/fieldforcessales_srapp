package com.srapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Util.ParentActivity;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.URL.GET_CREDIT_COLLECTION;

public class CreditHistoryActivity extends ParentActivity implements View.OnClickListener, BasicFunctionListener {

    Button buttonLogin;
    ArrayList<HashMap<String, String>> ItemListFromDB = new ArrayList<HashMap<String, String>>();

    ListView list;
    ListAdapter adapterMarketUpload;

    private ImageView btnBack, btnHome;
    TextView HeaderTitleTv, CodeNoTv;

    Data_Source db;
BasicFunction bf;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_credit_collection);
        db = new Data_Source(this);
        bf = new BasicFunction(this,this);
        TextView txtUser = (TextView) findViewById(R.id.txtUser);
        txtUser.setText(getPreference("UserName"));
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("mac", bf.getPreference("mac"));
            jsonObject.put(Tables.OUTLET_ID,bf.getPreference("OutletID"));
            jsonObject.put(SR_ID, bf.getPreference(SR_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("json",jsonObject.toString());

        bf.getResponceData(GET_CREDIT_COLLECTION,jsonObject.toString(),111);

        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels / 15;


        LinearLayout layout = (LinearLayout) findViewById(R.id.layoutBg);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        params.setMargins(width, 0, width, 0);
        layout.setLayoutParams(params);


        initiateButtons(btnBack, R.id.back);
        initiateButtons(btnHome, R.id.home);


        list = (ListView) findViewById(R.id.list);

        Log.e("prefOutletid", getPreference("OutletID"));





    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent idd = new Intent(CreditHistoryActivity.this, Create_New_Memo.class);
            startActivity(idd);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

    public void initiateButtons(ImageView btn, int id) {
        btn =  findViewById(id);
        btn.setOnClickListener((View.OnClickListener) this);
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        if (v.getId() == R.id.back) {

            Intent idd = new Intent(CreditHistoryActivity.this, Create_New_Memo.class);
            startActivity(idd);
            finish();
        } else if (v.getId() == R.id.home) {
            Intent intent = new Intent(CreditHistoryActivity.this, Dashboard.class);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {

        try {
            JSONArray jsonArray = jsonObject.getJSONArray("credit_collection_list");
            for (int j=0 ; j <jsonArray.length() ; j++){
                HashMap<String, String> product_list_map = new HashMap<String, String>();

                product_list_map.put("outlet_id", jsonArray.getJSONObject(j).getString("outlet_id"));
                product_list_map.put("date", jsonArray.getJSONObject(j).getString("date"));
                product_list_map.put("memo_no",jsonArray.getJSONObject(j).getString("memo_no"));
                product_list_map.put("memo_value",jsonArray.getJSONObject(j).getString("memo_value"));
                product_list_map.put("collect_date", jsonArray.getJSONObject(j).getString("collect_date"));
                product_list_map.put("due_amount", jsonArray.getJSONObject(j).getString("due_ammount"));
                product_list_map.put("paid_amount", jsonArray.getJSONObject(j).getString("paid_ammount"));
               // Log.e("paid_amount", "....." + c.getString(c.getColumnIndex("paid_amount")));

//					 product_list_map.put("inst_type",inst_type);

                Log.e("productList", product_list_map.toString());
                ItemListFromDB.add(product_list_map);

            }

            adapterMarketUpload = new ListAdapter(ItemListFromDB, this);

            list.setAdapter(adapterMarketUpload);
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void OnConnetivityError() {

    }


    private class ListAdapter extends BaseAdapter {


        LayoutInflater inflater;
        ViewHolder viewHolder;
        Activity activity;
        ArrayList<HashMap<String, String>> itemListFromDB5;

        public ListAdapter(ArrayList<HashMap<String, String>> itemListFromDB12, Activity act) {
            // TODO Auto-generated constructor stub

            inflater = LayoutInflater.from(getBaseContext());
            this.itemListFromDB5 = itemListFromDB12;
            this.activity = act;
        }

        @Override
        public int getCount() {
            return itemListFromDB5.size();
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }


        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            View vi = convertView;
            if (convertView == null)
                vi = inflater.inflate(R.layout.credit_history_row, null);

            viewHolder = new ViewHolder();

            viewHolder.dateTv = (TextView) vi.findViewById(R.id.txtDale);
            viewHolder.memo_noTv = (TextView) vi.findViewById(R.id.txtMemoNo);
            viewHolder.memo_valueTv = (TextView) vi.findViewById(R.id.txtMemoValue);
            viewHolder.col_dateTv = (TextView) vi.findViewById(R.id.txtCallDate);
            viewHolder.Action = (ImageView) vi.findViewById(R.id.ActionBtn);


			 /*product_list_map.put("date",date);
			 product_list_map.put("memo_no",memo_no);
			 product_list_map.put("memo_value",memo_value);
			 product_list_map.put("collect_date",collect_date);
			 product_list_map.put("inst_type",inst_type);*/

            final HashMap<String, String> map = itemListFromDB5.get(position);

            final String _outlet_id = map.get("outlet_id");
            final String _date = map.get("date");
            final String _memo_no = map.get("memo_no");
            final String _memo_value = map.get("memo_value");
            final String _collect_date = map.get("collect_date");
            final String due_amount = map.get("due_amount");
            final String paid_amount = map.get("paid_amount");

            Log.e("paid_amount", "" + paid_amount);
            Log.e("due_amount", "" + due_amount);

            viewHolder.dateTv.setText(_date);
            viewHolder.memo_noTv.setText(_memo_no);
            viewHolder.memo_valueTv.setText(_memo_value);
            viewHolder.col_dateTv.setText(map.get("due_amount"));

            viewHolder.Action.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    Intent idn = new Intent(CreditHistoryActivity.this, CreditHistoryPayActivity.class);
                    idn.putExtra("outlet_id", _outlet_id);
                    idn.putExtra("date", _date);
                    idn.putExtra("memo_no", _memo_no);
                    idn.putExtra("memo_value", _memo_value);
                    idn.putExtra("collect_date", _collect_date);
                    idn.putExtra("due_amount", due_amount);
                    idn.putExtra("paid_amount", paid_amount);

                    startActivity(idn);
                    finish();
                }
            });


            return vi;
        }

    }

    private class ViewHolder {
        TextView dateTv;
        TextView memo_noTv;
        TextView memo_valueTv;
        TextView col_dateTv;
        //		TextView memo_valueTv;
        ImageView Action;

    }
}