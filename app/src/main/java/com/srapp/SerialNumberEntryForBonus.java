package com.srapp;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.srapp.Adapter.AdapterForBonusSerial;
import com.srapp.Adapter.AdapterForSelial;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Util.Parent;

import java.util.ArrayList;
import java.util.HashMap;

import static com.srapp.TempData.TotalBonusProductList;

public class SerialNumberEntryForBonus extends Parent {

    TagEditText edit;
    Button add;
    String contactName;
    TextView textView;
    ListView lisy;
    Button next;
    public static HashMap<String, ArrayList<String>> serial;
    AdapterForBonusSerial adapterForSelial;
    ArrayList<HashMap<String, String>> arrayList;
    Data_Source db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sirial_number_entry_for_bonus);
        serial = new HashMap<>();
        lisy = findViewById(R.id.lisy);
       // next = findViewById(R.id.next);
        arrayList = new ArrayList<>();
        db = new Data_Source(this);

        Loaddata();


            //next.setText("next");
    }

    private void Loaddata() {

        Log.e("size", TotalBonusProductList.size() + "");

        for (int i = 0; i < TotalBonusProductList.size(); i++) {

            Cursor c = db.rawQuery("select maintain_serial from product where product_id='" + TotalBonusProductList.get(i).get("product_id") + "'");
            c.moveToFirst();
            if (c != null && c.getCount() > 0) {
                if (c.getInt(0) == 1) {
                    HashMap<String, String> map = new HashMap<>();
                    map.put("product_name", TotalBonusProductList.get(i).get("product_name"));
                    map.put("product_id", TotalBonusProductList.get(i).get("product_id"));
                    map.put("quantity", TotalBonusProductList.get(i).get("quantity"));
                    if (TotalBonusProductList.get(i).get("sirial_number")==null)
                    map.put("sirial_number", "");
                    else
                    map.put("sirial_number", TotalBonusProductList.get(i).get("sirial_number"));
                    arrayList.add(map);

                    serial.put(TotalBonusProductList.get(i).get("product_id"), getserialList(TotalBonusProductList.get(i).get("product_id")));
                }else {
                    arrayList.add(TotalBonusProductList.get(i));
                }
            }else {

                arrayList.add(TotalBonusProductList.get(i));
            }
        }
        adapterForSelial = new AdapterForBonusSerial(this, arrayList);
        lisy.setAdapter(adapterForSelial);


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(SerialNumberEntryForBonus.this,BonusProductListActivityForMemo.class);
            startActivity(intent);
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
    private ArrayList<String> getserialList(String product_id) {

        ArrayList<String> serial = new ArrayList<>();
        Log.e("product_id", product_id);

        Log.e("log", "select serial_no from " + Tables.TABLE_NAME_PRODUCT_SERIALS + " where product_id='" + product_id + "' and is_used='0'");
        Cursor c = db.rawQuery("select serial_no from " + Tables.TABLE_NAME_PRODUCT_SERIALS + " where product_id='" + product_id + "' and is_used='0'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            do {
                serial.add(c.getString(0));

            } while (c.moveToNext());

        }

        return serial;
    }


}
