package com.srapp;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.srapp.Adapter.PriceListAdapter;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static android.widget.LinearLayout.VERTICAL;
import static com.srapp.Util.ParentActivity.getCurrentDate;

public class PriceList extends AppCompatActivity implements BasicFunctionListener, DBListener {
    ImageView img;
    RecyclerView recyclerView;
    BasicFunction basicFunction;
    int initialPageIndex=1;
    ArrayList<String[]> priceList;
    PriceListAdapter priceListAdapter;
    ArrayList<String>productNameList,priceList1,priceList2,minQtyList;
    HashMap<String, ArrayList<ArrayList<String>>> priceLishHashMap;
    boolean isLoading= false;
    Data_Source ds;
    Button update_price;
    String date;
    ImageView backBtn, homeBtn;

    TextView userIdTV,titleTV ;

    int Flag = 0;
    BasicFunction bf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_list);
        basicFunction = new BasicFunction(this,this);
        //bf = new BasicFunction(this, this);
        ds = new Data_Source(this,this,this);

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        userIdTV = findViewById(R.id.user_txt_view);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        date = sdf.format(new Date());

        recyclerView = findViewById(R.id.price_list_rec);
        update_price = findViewById(R.id.update_price);
        //priceList = new ArrayList<>();
        priceLishHashMap = new HashMap<>();


        Log.e("----", "onCreate: price list-------->"+priceLishHashMap );
        Log.e("----", "onCreate: price list-------->"+date );

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( PriceList.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( PriceList.this, Tools.class));
                finish();
            }
        });

        //........use this section in update price button..........//
        update_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                JSONObject primaryData = new JSONObject();
                try {
                    primaryData.put("mac",basicFunction.getPreference("mac"));
                    primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                    //primaryData.put("last_update","1995-01-18");
                    primaryData.put("last_update","all");

                    //primaryData.put("page",initialPageIndex);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.e("<-------->", "onCreate: primarydata---------> "+ primaryData);
                basicFunction.getResponceData(URL.PriceList,primaryData.toString(),101);

//                Flag = 2;
//                ds.generatePushJson();
//                bf.savePreference("lsyncTime",DateFormatedConverter(getCurrentDate())+" "+getCurrentTime());



            }
        });

        //......................xxxxxxxxxxxxxxxxxxxxxxx..............................//
        priceLishHashMap = ds.getSlapWisePriceList(date);
        Log.e("hashmap_size", String.valueOf(priceLishHashMap.size()));
        Log.e("pricehashmap", "onCreate: slap wise1---------> "+priceLishHashMap );

        if (priceLishHashMap != null && priceLishHashMap.size()>0) {
            priceListAdapter = new PriceListAdapter(priceLishHashMap, this);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
            recyclerView.setAdapter(priceListAdapter);
        }
        recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL));


    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public String getCurrentTime (){
        Calendar cal = Calendar.getInstance();
        cal.set(cal.get(Calendar.HOUR), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND));
        Date date = cal.getTime();
        Time tme = new Time(cal.get(Calendar.HOUR),cal.get(Calendar.MINUTE),0);
        //seconds by
        // default set to zero
        Format formatter;
        formatter = new SimpleDateFormat("h:mm a");
        String time = formatter.format(tme);
        Log.e("time",time);
        String string[] = time.toString().split(" ");
        if (cal.get(Calendar.AM_PM)==Calendar.AM){
            time=string[0]+" am";
        }else {

            time=string[0]+" pm";
        } Log.e("time",time);
        return time;

    }

    public String DateFormatedConverter(String oldDateString)
    {
        DateFormat originalFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        DateFormat targetFormat = new SimpleDateFormat("dd-MMM-yyyy");
        Date date;
        String formattedDate="";
        try {
            date = originalFormat.parse(oldDateString);
            formattedDate = targetFormat.format(date);
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return formattedDate;

    }
    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        Log.e("respResponse---->", "OnServerResponce: "+i+"---- " +jsonObject );
        if (i==101) {

            try {

                ds.excQuery("delete  from product_price");
                ds.excQuery("delete  from product_combinations");

                Log.e("updatecall","called");

                Log.e("called",jsonObject.toString());

                Log.e("jsond",jsonObject.getJSONObject("dist_product_price").toString());

                ds.insertData(jsonObject.getJSONObject("dist_product_price").toString());


            } catch (JSONException e) {
                e.printStackTrace();
            }
        }



    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(PriceList.this,"No Internet Connection",Toast.LENGTH_SHORT).show();

    }



    @Override
    public void OnLocalDBdataRetrive(String json) {
        if (json.equalsIgnoreCase("done")) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    priceLishHashMap = ds.getSlapWisePriceList(date);
                    priceListAdapter = new PriceListAdapter(priceLishHashMap,PriceList.this);
                    recyclerView.setLayoutManager(new LinearLayoutManager(PriceList.this));
                    recyclerView.setAdapter(priceListAdapter);
                    recyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), VERTICAL));
                }
            });
        }

    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( PriceList.this, Tools.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }

}
