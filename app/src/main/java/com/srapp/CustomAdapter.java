package com.srapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class CustomAdapter extends ArrayAdapter<RowItem> {

	Context context;
	int layoutID;

	public CustomAdapter(Context context, int layoutID, List<RowItem> items) 
	{
		super(context, layoutID, items);
		this.context = context;
		this.layoutID = layoutID;
	}
 
	private class ViewHolder 
	{
		ImageView imageView;
		TextView txtTitle;
		TextView txtDesc;
		
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) 
	{
		ViewHolder holder = null;
		RowItem rowItem = getItem(position);

		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		if (convertView == null) 
		{
			convertView = mInflater.inflate(layoutID, null);
			holder = new ViewHolder(); 
			
			holder.txtTitle = (TextView) convertView.findViewById(R.id.title);
			holder.imageView = (ImageView) convertView.findViewById(R.id.icon);
		
			convertView.setTag(holder);
		}
		else
			holder = (ViewHolder) convertView.getTag();
 
			holder.txtTitle.setText(rowItem.getTitle());
			holder.imageView.setImageResource(rowItem.getImageId());
			
			
			

			return convertView;
	}
}