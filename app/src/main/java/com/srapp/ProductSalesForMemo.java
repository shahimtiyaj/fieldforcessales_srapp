package com.srapp;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;

import com.srapp.Adapter.SalesOrderDetailsAdaperForMemo;
import com.srapp.Db_Actions.DBListener;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Model.OrderDetailsModel;
import com.srapp.Util.Parent;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.srapp.Db_Actions.Tables.CREDIT_COLLECTION;
import static com.srapp.Db_Actions.Tables.MEMOS;
import static com.srapp.Db_Actions.Tables.MEMOS_memo_number;
import static com.srapp.Db_Actions.Tables.MEMO_DETAILS;
import static com.srapp.Db_Actions.Tables.MESSAGE_UPDATED_AT;
import static com.srapp.Db_Actions.Tables.NCP_RETURN_CREATE_AT;
import static com.srapp.Db_Actions.Tables.ORDER_DETAILS_is_bonus;
import static com.srapp.Db_Actions.Tables.ORDER_order_number;
import static com.srapp.Db_Actions.Tables.PAYMENTS;
import static com.srapp.Db_Actions.Tables.SR_ID;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_GIFT_ISSUE;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MEMOS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_MEMO_DETAILS;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER_DETAILS;
import static com.srapp.TempData.Converting_TO_MEMO;
import static com.srapp.TempData.DB_ID_ORDER_REPORT;
import static com.srapp.TempData.MEMO_EDITING;
import static com.srapp.TempData.SALE_STATE;
import static com.srapp.TempData.editMemo;
import static com.srapp.TempData.memoNumber;

public class ProductSalesForMemo extends Parent implements BasicFunctionListener, DBListener {

    ImageView imageView;

    ListView listView;

    OrderDetailsModel orderDetailsModel;
    ArrayList<HashMap<String, String>> productList;

    ArrayList<HashMap<String, String>> Data = new ArrayList<HashMap<String, String>>();
    private DatePickerDialog CurrentDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private int year1;
    private int month1;
    private int day1;
    int urlcall;
    SalesOrderDetailsAdaperForMemo Adapter;
    ListView list_add_vehicle_details;
    ImageView homeBtn, backBtn;
    Button SaveBtn;
    TextView TotalPrice, discount, total_price, CreditTv, txtGift, txtBonus, txtBonusExtra, vattv;
    ArrayList<HashMap<String, String>> product_history;
    ArrayList<Integer> num = new ArrayList<Integer>();
    String price = "";
    String orderNo = "", lattitude, longitude;
    String memoNo = "";
    String FromApp = "";
    String for_memo_delete = "";
    String OrderDate = "";
    String orderdateTime = "";
    TextView button_with_icon;
    String date = "";
    BasicFunction bf;
    String editableAllow = "0";
    Data_Source db;
    private static final int REQUEST_LOCATION = 1;
    ArrayList<HashMap<String, String>> ItemListFromDB = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> GiftItem = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> Bonus = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> orderList = new ArrayList<HashMap<String, String>>();
    private String ForEditMeno;
    private String memodate;
    TextView addBonusBtn, ddate;
    LocationManager locationManager;
    String is_out_of_plan = "0", plan_id = "0";
    int f = 0;
    TextView userIdTV,titleTV;

    String memoUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_sales);
        userIdTV = findViewById(R.id.user_txt_view);
        titleTV = findViewById(R.id.title_tv);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        db = new Data_Source(this, this, this);
        bf = new BasicFunction(this, this);
        Bundle b = getIntent().getExtras();
        if (b != null) {
            ForEditMeno = b.getString("ForEditMeno");
        }

        memoUpdate= b.getString("memo", "0");

        if (TempData.editMemo.equalsIgnoreCase("true")) {
            date = TempData.MemoDateTime;
        } else {
            date = getCurrentDate();
        }

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);
        list_add_vehicle_details = (ListView) findViewById(R.id.product_sales_rec);
        list_add_vehicle_details.setItemsCanFocus(true);
 /*
        list_add_vehicle_details.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e("positionlis", position + "");
                TextView serial_number = view.findViewById(R.id.serial_number);
                if (serial_number.getVisibility() == View.VISIBLE)
                    serial_number.setVisibility(View.GONE);
                else {
                    serial_number.setVisibility(View.VISIBLE);
                }

            }
        });
*/

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductSalesForMemo.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProductSalesForMemo.this, Sales_Memo.class));
                finishAffinity();

            }
        });
        if (!editMemo.equalsIgnoreCase("true"))
            orderdata();


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        //Check gps is enable or not

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Write Function To enable gps
            OnGPS();
        } else {
            //GPS is already On then
            getLocation();
        }
        Log.e("Date:", String.valueOf(date));
        Log.e("Date:", TempData.orderNumber + " no");
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        Date datevalue = null;
        try {
            datevalue = simpleDateFormat.parse(String.valueOf(date));

            simpleDateFormat.applyPattern(pattern);
            memodate = simpleDateFormat.format(datevalue);

            //memodate = datevalue.toString();
            Log.e("MemoDate:", String.valueOf(memodate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int width = dm.widthPixels / 15;


        Log.e("INSTRITUTE-ID", "..........." + TempData.InstituteID);


        TempData.INVOICE_DETAILS.clear();


        txtGift = (TextView) findViewById(R.id.txtGift);
        txtBonus = (TextView) findViewById(R.id.txtBonus);
        txtBonusExtra = (TextView) findViewById(R.id.txtBonusExtra);

        LinearLayout BonusLayout = (LinearLayout) findViewById(R.id.BonusLayout);
        addBonusBtn = findViewById(R.id.addBonusBtn);
        button_with_icon = findViewById(R.id.button_with_icon);
        button_with_icon.setText(DateFormatedConverter(getCurrentDate()));

        ddate = findViewById(R.id.ddate);
        ddate.setText(DateFormatedConverter(getCurrentDate()));

        setDeliveryDate();
       // ddate.setText(db.getNextVisitday(bf.getPreference("thanaID")));


        TotalPrice = (TextView) findViewById(R.id.sub_total);
        discount = (TextView) findViewById(R.id.discount);
        total_price = (TextView) findViewById(R.id.total_price);
        vattv = (TextView) findViewById(R.id.vat);


        SaveBtn = (Button) findViewById(R.id.SaveButton);


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        BonusButtonShow();

        ProduSctDisplayListView();

        bonus_show_WithoutDB();


        addBonusBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent idn = new Intent(ProductSalesForMemo.this, BonusProductListActivityForMemo.class);
                startActivity(idn);
//				finish();
            }
        });


        BonusLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //  TempData.TempBonus=txtBonus.getText().toString();
                TempData.TempExtraBonus = txtBonus.getText().toString();
                Log.e("++++++", "TempData.TempBonus: " + TempData.TempBonus);
                Log.e("++++++", "TempData.TempExtraBonus: " + TempData.TempExtraBonus);
                Log.e("++++++", "TempData.BonusShowList " + TempData.BonusShowList);

                Intent idn = new Intent(ProductSalesForMemo.this, BonusShowListActivity.class);
                startActivity(idn);

            }
        });

       /* DraftPrintButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Cursor c2 = db.rawQuery("SELECT * FROM product ORDER BY product_order ASC");
                if (c2 != null) {
                    if (c2.moveToFirst()) {
                        do {

                            String product_id = c2.getString(c2.getColumnIndex("product_id"));
                            savePreference(product_id, "0.0");

                        } while (c2.moveToNext());
                    }
                }

                if(!TotalPrice.getText().toString().equals("0.00"))
                {
                    bonus_Draft_WithoutDB();
                    DaftPrintDataToTable();

                } }
        });*/


        SaveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  SavePrintButton.setEnabled(false);
                SaveBtn.setEnabled(false);


                Cursor c1 = db.rawQuery("SELECT * FROM memos where from_app='1' order by _id DESC limit 1");
                c1.moveToFirst();
                if (c1.getCount() > 0) {
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
                    Date date = null;
                    try {
                        date = format.parse(c1.getString(c1.getColumnIndex("memo_date")));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Calendar timetcal = Calendar.getInstance();

                    Date currentDate = timetcal.getTime();

                    Log.e("date", String.valueOf(date));
                    if (TempData.editMemo.equalsIgnoreCase("false")) {

                        SaveBtn.setEnabled(false);

                        if (!currentDate.after(date)) {
                            Log.e("date", c1.getString(c1.getColumnIndex("memo_date")));
                            Toast.makeText(ProductSalesForMemo.this, "Invalide Date", Toast.LENGTH_LONG).show();
                            return;
                        }

                    }
                }


                Cursor c2 = db.rawQuery("SELECT * FROM product ORDER BY product_order ASC");
                if (c2 != null) {
                    if (c2.moveToFirst()) {
                        do {

                            String product_id = c2.getString(c2.getColumnIndex("product_id"));
                            savePreference(product_id, "0.0");

                        } while (c2.moveToNext());
                    }
                }

                /*  Log.e("PaymmentText",""+etPayment.getText().toString());*/

               // if (SirialValidation()) {
                    if (EmptyPaymentValidation()) {
                        if (TempData.editMemo.equalsIgnoreCase("true"))
                            AlertDialog();
                        else {
                            if (injectable_product_check().equalsIgnoreCase("0")) {
                                if (TempData.editMemo.equalsIgnoreCase("true"))
                                    AlertDialog();
                                else {
                                    if (!total_price.getText().toString().equals("0.00")) {

                                        new SaveData().execute();
                                    }
                                }

                            } else if (injectable_product_check().equalsIgnoreCase("1") && getPreference("Is_WithinGroup").equalsIgnoreCase("1")) {
                                if (TempData.editMemo.equalsIgnoreCase("true"))
                                    AlertDialog();
                                else {
                                    if (!total_price.getText().toString().equals("0.00")) {

                                        new SaveData().execute();
                                    }
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Not Saleable Product!", Toast.LENGTH_LONG).show();
                            }
                        }
                    }
               // }
               else {
                    Toast.makeText(ProductSalesForMemo.this,"Please Input Bonus Serial Number",Toast.LENGTH_LONG).show();
                    SaveBtn.setEnabled(true);
                }
            }
        });

    }

    private boolean SirialValidation() {
        boolean istrue = true;
        for (int i = 0; i < TempData.TotalBonusProductList.size(); i++) {
            if (isSerialMaintain(TempData.TotalBonusProductList.get(i).get("product_id"))) {
                if (TempData.TotalBonusProductList.get(i).get("sirial_number")==null){
                    istrue=false;
                    return false;
                }
                double serialQuantity = TempData.TotalBonusProductList.get(i).get("sirial_number").split(",").length;

                if (serialQuantity == Double.parseDouble(TempData.TotalBonusProductList.get(i).get("quantity"))) {

                    istrue = true;
                } else {

                    istrue = false;
                }

                if (!istrue) {
                    return istrue;
                }


            }
        }

        return istrue;
    }

    private boolean isSerialMaintain(String product_id) {
        Cursor c = db.rawQuery("select maintain_serial from product where product_id='" + product_id + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            if (c.getInt(0) == 1) {
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    private void setDeliveryDate() {
        boolean loop=true;
        int  current_salesWeek  = getWeekId();
        int [] weekid={1,2,3,4};

        int [] days_of_week={1,2,3,4,5,6,7};
        Calendar calendar = Calendar.getInstance();
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        Log.e("dayOfWeek",dayOfWeek+"");
        for (int i = current_salesWeek; i<=weekid.length;i++){


            Cursor c= db.rawQuery("select * from visit_list where route_id='"+bf.getPreference("thanaID")+"' and week_id='"+(i)+"'");
            Log.e("Queryt"+i,"select * from visit_list where route_id='"+bf.getPreference("thanaID")+"' and week_id='"+(i)+"' "+c.getCount());
            c.moveToFirst();
            if (c!=null && c.getCount()>0){
                int j = 0;
                if (i==current_salesWeek){
                    j=dayOfWeek;
                    Log.e("J",j+"");
                }
                for ( ; j<days_of_week.length;j++){
                    if (i==current_salesWeek && dayOfWeek==days_of_week[j]){
                        continue;
                    }
                    Log.e("J+1",j+"");
                    if (c.getInt(days_of_week[j]+1)==1){
                        calculateDate(i,days_of_week[j],current_salesWeek);
                        break;


                    }

                }
                break;
            }

            if (i==4 && loop){
                i=0;
                loop=false;
            }

        }



    }

    private void calculateDate(int week_id, int day_of_week, int current_sales_week) {

        Log.e("week_id", week_id + " ," + day_of_week + " ," + current_sales_week);

        Calendar c = Calendar.getInstance();
        if (week_id < current_sales_week)
            c.set(Calendar.MONTH, c.get(Calendar.MONTH) + 1);
        if (c.get(Calendar.MONTH) > 11) {
            c.set(Calendar.YEAR, c.get(Calendar.YEAR) + 1);
        }

        c.set(Calendar.DAY_OF_WEEK, day_of_week);
        c.set(Calendar.WEEK_OF_MONTH, week_id);
        Date date = c.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Log.e("Calender", sdf.format(date));
        //ddate.setText(sdf.format(date));

    }


    void orderdata() {
        Calendar cal = Calendar.getInstance();
        cal.get(Calendar.DAY_OF_WEEK);
        Cursor c = db.rawQuery("select * from visit_list where route_id='" + bf.getPreference("thanaID") + "' and week_id='" + getWeekId() + "'");
        c.moveToFirst();
        Log.e("datofweek", cal.get(Calendar.DAY_OF_WEEK) + "  " + "select * from visit_list where route_id='" + bf.getPreference("thanaID") + "' and week_id='" + getWeekId() + "'");
        if (c != null & c.getCount() > 0) {

            Log.e("test", cal.get(Calendar.DAY_OF_WEEK) + 1 + " 0");
            if (c.getString(cal.get(Calendar.DAY_OF_WEEK) + 1).equalsIgnoreCase("1")) {

                is_out_of_plan = "1";
                plan_id = c.getString(1);
            }

        }

    }


    private int getWeekId() {

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int weekNumber = calendar.get(Calendar.WEEK_OF_MONTH) - 1;

        if (day <= 7) {
            weekNumber = 1;
        } else if (day > 7 && day <= 13) {
            weekNumber = 2;
        } else if (day > 13 && day <= 20) {
            weekNumber = 3;
        } else {
            weekNumber = 4;
        }

        return weekNumber;
    }

    private int getstartdate(int weekNumber) {

        int day;

        if (weekNumber == 1) {
            day = 1;
        } else if (weekNumber == 2) {
            day = 8;
        } else if (weekNumber == 3) {
            day = 14;
        } else {
            day = 21;
        }

        return day;
    }


    @Override
    public void OnLocalDBdataRetrive(final String json) {
        runOnUiThread(new Runnable() {
            public void run() {
                if (urlcall == 102) {
                    bf.getResponceData(URL.CREATE_MEMO, json, 102);
                } else
                    bf.getResponceData(URL.ORDERPUSH, json, 101);
            }
        });

    }

    @Override
    public void OnLocalDBdataRetrive(ArrayList<HashMap<String, String>> arrayList) {

    }

    @Override
    public void OnLocalDBdataRetrive(HashMap<String, String> hasmap) {

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        try {
            if (i == 102) {


                if (jsonObject.getJSONObject("memo").getString("status").equalsIgnoreCase("1")) {
                    db.excQuery("update ORDER_table set is_complete='1' , status = '2' WHERE order_number = '" + orderNo + "'");
                    Toast.makeText(this, jsonObject.getJSONObject("memo").getString("message"), Toast.LENGTH_LONG).show();
                    if(f==0){
                        saveMemos();
                        f=1;
                    }

                } else {

                    db.excQuery("delete from temp_memos");
                    db.excQuery("delete from temp_memo_details");
                    db.excQuery("delete from temp_payments");
                    db.excQuery("delete from temp_credit_collection");
                    Toast.makeText(this, jsonObject.getJSONObject("memo").getString("message"), Toast.LENGTH_LONG).show();
                    SaveBtn.setEnabled(true);

                }


                Log.e("json", jsonObject.toString());
                if (SALE_STATE == MEMO_EDITING) {
                    startActivity(new Intent(ProductSalesForMemo.this, MemoReport.class));
                    finish();
                    Log.e("memoredirect", "MemoReport");
                } else if (SALE_STATE == Converting_TO_MEMO) {
                    startActivity(new Intent(ProductSalesForMemo.this, DeliveryReport.class));
                    finish();
                    Log.e("memoredirect", "Order_Report_Activity");
                }


            } else if (i == 101) {

                startActivity(new Intent(ProductSalesForMemo.this, Order_Report_Activity.class));
                Log.e("memoredirect", "DeliveryReport");
                finishAffinity();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveMemos() {

        Log.e("memoNumber", "nomemo" + memoNo + " " + memoNumber + " " + TempData.memoNumber);

        if (SALE_STATE == MEMO_EDITING) {
            orderNo = TempData.orderNumber;
            db.excQuery("DELETE FROM " + TABLE_NAME_MEMOS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "'");
            db.excQuery("DELETE FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "'");
            UpdateSerials();
            db.excQuery("DELETE  FROM gift_issue WHERE gift_issue_id='" + TempData.orderNumber + "'");
            db.excQuery("DELETE  FROM gift_issue_details WHERE gift_issue_id='" + TempData.orderNumber + "'");
        }

        Cursor c1 = db.rawQuery("SELECT * FROM temp_memos  WHERE " + MEMOS_memo_number + "='" + memoNo + "'");
        Log.e("query", "SELECT * FROM temp_memos  WHERE " + MEMOS_memo_number + "='" + memoNo + "' " + c1.getCount());
        c1.moveToFirst();
        if (c1 != null && c1.getCount() > 0) {
            //Log.e("memoNumber","nomemo"+memoNo+" "+memoNumber+" "+ TempData.memoNumber);
            HashMap<String, String> map = new HashMap<>();
            for (int i = 2; i < MEMOS.length - 2; i++) {

                map.put(MEMOS[i], c1.getString(c1.getColumnIndex(MEMOS[i])));
            }
            map.put(NCP_RETURN_CREATE_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "memos");

        }

        Cursor c2 = db.rawQuery(" SELECT * FROM temp_memo_details  WHERE " + MEMOS_memo_number + "='" + memoNo + "'");

        c2.moveToFirst();
        if (c2 != null && c2.getCount() > 0) {

            for (int j = 0; j < c2.getCount(); j++) {
                UpdateDbForSeliarPS(c2.getString(c2.getColumnIndex(Tables.MEMO_DETAILS_Serial_NUMBER)), "1");

                HashMap<String, String> map = new HashMap<>();
                for (int i = 2; i < MEMO_DETAILS.length - 2; i++) {

                    map.put(MEMO_DETAILS[i], c2.getString(c2.getColumnIndex(MEMO_DETAILS[i])));
                }
                map.put(NCP_RETURN_CREATE_AT, getCurrentDateTime());
                map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
                db.InsertTable(map, "memo_details");

                c2.moveToNext();
            }

        }


        Cursor c3 = db.rawQuery(" SELECT * FROM temp_payments  WHERE " + " memo_no ='" + memoNo + "'");

        c3.moveToFirst();
        if (c3 != null && c3.getCount() > 0) {

            HashMap<String, String> map = new HashMap<>();
            for (int i = 2; i < PAYMENTS.length - 2; i++) {

                map.put(PAYMENTS[i], c3.getString(c3.getColumnIndex(PAYMENTS[i])));
            }
            map.put(NCP_RETURN_CREATE_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "payments");

        }


        Cursor c4 = db.rawQuery(" SELECT * FROM temp_credit_collection  WHERE " + MEMOS_memo_number + "='" + memoNo + "'");

        c4.moveToFirst();
        if (c4 != null && c4.getCount() > 0) {

            HashMap<String, String> map = new HashMap<>();
            for (int i = 2; i < CREDIT_COLLECTION.length - 2; i++) {

                map.put(CREDIT_COLLECTION[i], c4.getString(c4.getColumnIndex(CREDIT_COLLECTION[i])));
            }
            map.put(NCP_RETURN_CREATE_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "credit_collection");

        }


        //db.excQuery("delete from temp_memos");
        //db.excQuery("delete from temp_memo_details");
        //db.excQuery("delete from temp_payments");
        //db.excQuery("delete from temp_credit_collection");
    }

    private void UpdateSerials() {

        Cursor c = db.rawQuery("Select serial_number FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            for (int i = 0; i < c.getCount(); i++) {
                UpdateDbForSeliarPS(c.getString(0), "0");
                c.moveToNext();
            }
        }


        db.excQuery("DELETE  FROM " + TABLE_NAME_MEMO_DETAILS + " WHERE " + MEMOS_memo_number + "='" + TempData.memoNumber + "'");
    }


    @Override
    public void OnConnetivityError() {
        Toast.makeText(this, "NO Internet Connection", Toast.LENGTH_SHORT);
    }


    public class SaveData extends AsyncTask<Void, Void, Void> {


        @RequiresApi(api = Build.VERSION_CODES.M)
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                SaveDataToTable();
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }


    @RequiresApi(api = Build.VERSION_CODES.M)
    public void SaveDataToTable() {
      //  db.excQuery("delete from memo_details");

        orderList = Adapter.getAdapterHashMapList();
        Log.e("ReturnAdapter ", "ReturnAdapter " + orderList.toString());
        double MemoQuantity = 0.0;

        String query = "SELECT product_id, quantity FROM product_boolean WHERE outlet_id='" + getPreference("OutletID") + "' AND boolean='true'";
        Cursor c = db.rawQuery(query);
        int count = c.getCount();
        int VanQtyCheckFlag = 1;
        Log.e("QUERY COUNT:", "..............." + count);
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String productId = c.getString(0);
                    double Quantity = c.getDouble(1);

                    Log.e("productId: ", "" + productId);
                    Log.e("Quantity: ", "" + Quantity);
                    Log.e("orderNumber: ", "" + TempData.orderNumber);

                    String Memoquery = "SELECT SUM(quantity) FROM memo_details WHERE product_id='" + productId + "' and " + Tables.MEMOS_memo_number + "='" + TempData.orderNumber + "'";
                    Cursor c3 = db.rawQuery(Memoquery);
                    int count3 = c3.getCount();

                    if (c3 != null) {
                        if (c3.moveToFirst()) {
                            do {

                                MemoQuantity = c3.getDouble(0);

                            } while (c3.moveToNext());
                        }
                    }
                } while (c.moveToNext());
            }
        }

        VanQtyCheckFlag = 1;
        if (VanQtyCheckFlag == 1) {
            VanQtyCheckFlag = 1;

            SimpleDateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
            Calendar cal = Calendar.getInstance();


            final LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                buildAlertMessageNoGps();
            } else {
                if (TempData.editMemo.equalsIgnoreCase("true")) {
                    Log.e("order", TempData.orderNumber + "");
                    orderNo = TempData.orderNumber;
                    if (SALE_STATE == MEMO_EDITING) {
                        memoNo = TempData.memoNumber;
                    }
                    FromApp = TempData.From_App;
                    for_memo_delete = TempData.for_memo_delete;
                    OrderDate = TempData.MemoDate;
                    orderdateTime = TempData.MemoDateTime;
                    //new EditMemoDatabaseHelper().UpdateVan(ProductSalesForMemo.this, orderNo);
                    SalesOrderNO(false);
                    editableAllow = TempData.DayCloseMemoEditable;
                    if (SALE_STATE == Converting_TO_MEMO) {
                        SalesMemoNO(true);
                    }

                } else {
                    FromApp = "1";
                    for_memo_delete = "0";
                    OrderDate = getCurrentDate();
                    orderdateTime = getCurrentDateTime();
                    SalesOrderNO(true);
                    editableAllow = "0";
                }

                Memo();

                Log.e("MEMO", orderNo);

                HashMap<String, String> map1 = new HashMap<String, String>();

                map1.put("outlet_id", TempData.OutletID);
                map1.put("gift_issue_id", "" + orderNo);
                map1.put("gift_issued_by", getPreference("SO"));
                map1.put("gift_issue_date", OrderDate);
                map1.put("order_number", orderNo);
                map1.put("is_Pushed", "0");
                db.InsertTable(map1, "gift_issue");


                Log.e("MEMO", orderNo);
                TempData.orderNumber = orderNo;
                TempData.INVOICE_DETAILS.clear();

                Log.e("SiZED IN SAVE button:", String.valueOf(orderList.size()));

                Bonus = Adapter.getBonusMapList();
                GiftItem = GiftItem;

                int currentMonth1 = Calendar.getInstance().get(Calendar.MONTH);

                Log.e("orderList size", orderList.size() + "");

                for (int i = 0; i < orderList.size(); i++) {
                    Log.e("TAG:", "target");
                    //SO TARGETS
                    double totalAmount = Double.parseDouble(orderList.get(i).get("quantity")) * Double.parseDouble(orderList.get(i).get("general_price"));
                    Log.e("totalAmount", "" + totalAmount);
                    Log.e("PRODUCT_ID(TARGET)", orderList.get(i).get("product_id"));
                    Log.e("SO TARGET query:", "UPDATE sales_targets SET achieve_quantity=achieve_quantity+" + Double.parseDouble(orderList.get(i).get("quantity")) + ", achieve_amount=achieve_amount+" + totalAmount + " WHERE product_id=" + orderList.get(i).get("product_id") + " AND month_id=" + currentMonth1 + " AND territory_id=" + getPreference("Territory_Id") + " AND so_id=" + getPreference("SO"));
//	                db.excQuery("UPDATE sales_targets SET achieve_quantity=achieve_quantity+"+Double.parseDouble(orderList.get(i).get("quantity"))+", achieve_amount=achieve_amount+"+totalAmount+" WHERE product_id="+orderList.get(i).get("product_id")+" AND month_id="+currentMonth1+" AND territory_id="+getPreference("Territory_Id")+" AND so_id="+getPreference("SO"));

                }
                for (int i = 0; i < orderList.size(); i++) {
                    //UpdateStock(orderList.get(i).get("product_id"),orderList.get(i).get("quantity"));
                    //UpdateVan(orderList.get(i).get("product_id"),orderList.get(i).get("quantity"));
                    Log.e("entry","gen_price"+orderList.get(i).get("general_price"));
                    MemoDetails("0", orderList.get(i).get("product_id"), orderList.get(i).get("quantity"), orderList.get(i).get("general_price"), orderList.get(i).get("vat"));
                    InvoiceTemporary(orderList.get(i).get("product_id"), orderList.get(i).get("product_name"), orderList.get(i).get("quantity"), orderList.get(i).get("general_price"));
                    //UpdateLastLifting(orderList.get(i).get("product_id"),orderList.get(i).get("product_name"),orderList.get(i).get("quantity"),orderList.get(i).get("general_price"));
                    db.excQuery("UPDATE product_boolean SET quantity ='', boolean = 'false' WHERE product_id = " + "'" + orderList.get(i).get("product_id") + "'" + "" + " and outlet_id=" + "'" + getPreference("OutletID") + "'");

                }

                for (int i = 0; i < Bonus.size(); i++) {
                    //UpdateStock(Bonus.get(i).get("product_id"),Bonus.get(i).get("quantity"));
                    // UpdateVan(Bonus.get(i).get("product_id"),Bonus.get(i).get("quantity"));
                    MemoDetails("2", Bonus.get(i).get("product_id"), Bonus.get(i).get("quantity"), "0", "0");
                }

                for (int i = 0; i < GiftItem.size(); i++) {
                    MemoDetails("1", GiftItem.get(i).get("product_id"), GiftItem.get(i).get("quantity"), "0", "0");
                    //  UpdateVan(GiftItem.get(i).get("product_id"),GiftItem.get(i).get("quantity"));
                    db.excQuery("UPDATE product_boolean SET quantity ='', boolean = 'false' WHERE product_id = " + "'" + GiftItem.get(i).get("product_id") + "'" + " and outlet_id=" + "'" + getPreference("OutletID") + "'");

                }


                for (int i = 0; i < TempData.TotalBonusProductList.size(); i++) {
                    MemoDetailsForBonus(TempData.TotalBonusProductList.get(i).get("product_id"), TempData.TotalBonusProductList.get(i).get("quantity"), TempData.TotalBonusProductList.get(i).get("sirial_number"));
                }


//			    db.excQuery("UPDATE memos_draft SET isPushed ='1' WHERE outlet_id='"+getPreference("OutletID")+"'");
                //db.deleteRowOFDraft("memos_draft", getPreference("OutletID"));


                TempData.InvoiceTotal = TotalPrice.getText().toString();


                TempData.TempGift = txtGift.getText().toString();
                TempData.TempBonus = txtBonus.getText().toString();
                TempData.TempExtraBonus = txtBonusExtra.getText().toString();
                TempData.TempGift = txtGift.getText().toString();


                writeToFile("smc");

                DeleteBefore3DaysMemos();

                if (TempData.editMemo.equalsIgnoreCase("true")) {
                    if (SALE_STATE == 0 && !TempData.isPushed.equalsIgnoreCase("1")) {
                      //  EntryForAdvanceCollection();
                    }
                    if (TempData.isPushed.equalsIgnoreCase("1") && SALE_STATE == 0) {
                        urlcall = 101;
                        Log.e("memoT2", memoNo);
                        generateMemoForPush(orderNo);
                       // EntryForAdvanceCollection();
                    } else if (SALE_STATE == Converting_TO_MEMO) {
                        Log.e("memoT1", memoNo);
                        urlcall = 102;
                        //EntryForCreditCollection();
                        db.generateSingleTempMemo(memoNo);
                    } else if (SALE_STATE == MEMO_EDITING) {
                        urlcall = 102;
                        Log.e("memoT", memoNo);
                       // EntryForCreditCollection();
                        db.generateSingleTempMemo(memoNo);

                    }

                    // TempData.orderNumber = "";
                    TempData.From_App = "";
                    tempClean();
                    TempBonus();
                    TempData.TotalBonusProductList.clear();
                    TempData.BonusChxSelected.clear();
                    TempData.BonusProductQuantity.clear();
                    TempData.TempBonus = "";
                    TempData.TempExtraBonus = "";
                    TempData.editMemo = "";
                    //db.deleteRowOFDraft("bonus_items", TempData.OutletID);
                    if (TempData.isPushed.equalsIgnoreCase("0") && SALE_STATE != MEMO_EDITING) {
                        startActivity(new Intent(ProductSalesForMemo.this, DetailsOrderReport.class));
                        finish();
                    }

                } else {
                    //EntryForAdvanceCollection();
                    tempClean();
                    TempBonus();
                    TempData.TotalBonusProductList.clear();
                    TempData.BonusChxSelected.clear();
                    TempData.BonusProductQuantity.clear();
                    TempData.TempBonus = "";
                    TempData.TempExtraBonus = "";
                    // db.deleteRowOFDraft("bonus_items", TempData.OutletID);
                    startActivity(new Intent(ProductSalesForMemo.this, Create_New_Memo.class));
                    finish();
                }
            }
        }
    }

    private void generateMemoForPush(String orderNo) {

        db.generateSingleOrder(orderNo);
    }


    public boolean EmptyPaymentValidation() {
       /* if(etPayment.getText().toString().equalsIgnoreCase("") || etPayment.getText().toString().equalsIgnoreCase("."))
        {
            Toast.makeText(getApplicationContext(), "Enter into payment field!", Toast.LENGTH_LONG).show();
            return false;
        }*/
        if (TempData.MarketID.equalsIgnoreCase("") || TempData.MarketID.equalsIgnoreCase
                ("null")) {
            Toast.makeText(getApplicationContext(), "No Found Market!", Toast.LENGTH_LONG).show();
            return false;
        } else if (TempData.OutletID.equalsIgnoreCase("") || TempData.OutletID.equalsIgnoreCase
                ("null")) {
            Toast.makeText(getApplicationContext(), "No Found Outlet", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public void Memo() {

        if (TempData.editMemo.equalsIgnoreCase("true") && SALE_STATE == 0) {
            orderNo = TempData.orderNumber;
            db.excQuery("DELETE FROM " + TABLE_NAME_ORDER + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "'");
            db.excQuery("DELETE FROM " + TABLE_NAME_ORDER_DETAILS + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "'");
            db.excQuery("DELETE FROM " + TABLE_NAME_GIFT_ISSUE + " WHERE " + ORDER_order_number + "='" + TempData.orderNumber + "'");
            db.excQuery("DELETE  FROM gift_issue WHERE gift_issue_id='" + TempData.orderNumber + "'");
            db.excQuery("DELETE  FROM gift_issue_details WHERE gift_issue_id='" + TempData.orderNumber + "'");
        }

      //  db.excQuery("DELETE  FROM temp_memos");
     //   db.excQuery("DELETE  FROM temp_memo_details");


        String lat = "", lng = "";


        lat = lattitude;
        lng = longitude;

        Log.e("lat: ", "" + lat);
        Log.e("lng: ", "" + lng);

        Log.e("----FromApp Memo----", FromApp);

        HashMap<String, String> map1 = new HashMap<String, String>();


        if (SALE_STATE == Converting_TO_MEMO) {
            Log.e("orderNo", orderNo);
            map1.put(Tables.ORDER_order_number, orderNo);
            map1.put(Tables.MEMOS_memo_number, memoNo);
            map1.put(Tables.MEMOS_memo_date, getCurrentDate());
            map1.put(Tables.MEMOS_memo_date_time, getCurrentDateTime());
            map1.put(Tables.SR_ID, getPreference(SR_ID));

            map1.put(Tables.MEMOS_sales_to, "0");

            map1.put(Tables.OUTLETS_ID, TempData.OutletID);
            map1.put(Tables.MARKET_ID, TempData.MarketID);
            map1.put(Tables.MEMOS_gross_value, total_price.getText().toString().trim());
            map1.put(Tables.MEMOS_is_active, "1");
            map1.put(Tables.MEMOS_latitude, lat);
            map1.put(Tables.MEMOS_longitude, lng);
            map1.put("cash_received", (Double.parseDouble(total_price.getText().toString().trim())) + "");

            map1.put(Tables.MEMOS_TOTAL_Vat, vattv.getText().toString().trim());
            map1.put(Tables.MEMOS_discount_amount, discount.getText().toString().trim());
            if (TempData.DISTYPE == 1) {
                map1.put(Tables.MEMOS_discount_Percentage, TempData.DISCOUNTP + "");
            } else if (TempData.DISTYPE == 2) {

                map1.put(Tables.MEMOS_discount_Percentage, discount.getText().toString().trim() + "");
            } else {
                map1.put(Tables.MEMOS_discount_Percentage, "0");
            }

            map1.put(Tables.MEMOS_DISCOUNT_TYPE, TempData.DISTYPE + "");
            String outlate = "0";
            if (TempData.OutletCatagoryID.equalsIgnoreCase("17")) {
                outlate = "1";
            }
            map1.put(Tables.MEMOS_is_distributor, outlate);
            map1.put(Tables.MEMOS_from_app, FromApp);
            map1.put(Tables.MEMOS_for_memo_delete, for_memo_delete);
            map1.put(Tables.MEMOS_editable, editableAllow);
            Log.e("ProductSalesUsingFor", "Order To Memo");
            map1.put(Tables.ORDER_is_pushed, "1");
            map1.put(Tables.ORDER_created_at, bf.getCurrentDateTime());
            map1.put(Tables.ORDER_updated_at, bf.getCurrentDateTime());
            db.InsertTable(map1, "temp_memos");

            Log.e("Product Sales For", "Order TO MEMo");

        } else if (SALE_STATE == 0 || !TempData.editMemo.equalsIgnoreCase("true")) {
            map1.put(Tables.ORDER_order_number, orderNo);
            map1.put(Tables.ORDER_order_date, OrderDate);
            map1.put(Tables.ORDER_order_date_time, orderdateTime);
            map1.put(Tables.SR_ID, getPreference(SR_ID));
            map1.put(Tables.ORDER_sales_to, "0");
            map1.put(Tables.ORDER_db_id, TempData.DB_ID_ORDER_REPORT);// Added when memo editing
            Log.e("order_report_db_id", DB_ID_ORDER_REPORT);
            map1.put(Tables.OUTLETS_ID, TempData.OutletID);
            map1.put(Tables.MARKET_ID, TempData.MarketID);
            Log.e("edit_memo", TempData.OutletID);
            if (TempData.editMemo.equalsIgnoreCase("true")) {
                plan_id = TempData.ORDER_plan_id + "";
                is_out_of_plan = TempData.ORDER_PlanVisit + "";
            }
            map1.put(Tables.ORDER_plan_id, plan_id);
            map1.put(Tables.ORDER_is_out_of_plan, is_out_of_plan);
            map1.put(Tables.ORDER_is_Complete, "0");
            map1.put(Tables.ORDER_STATUS, "0");
            map1.put(Tables.ORDER_TOTAL_VAT, vattv.getText().toString().trim());
            if (TempData.DISTYPE == 1) {
                map1.put(Tables.ORDER_discount_Percentage, TempData.DISCOUNTP + "");
            } else if (TempData.DISTYPE == 2) {

                map1.put(Tables.ORDER_discount_Percentage, discount.getText().toString().trim() + "");
            } else {
                map1.put(Tables.ORDER_discount_Percentage, "0");
            }

            map1.put(Tables.ORDER_discount_amount, discount.getText().toString().trim());
            map1.put(Tables.ORDER_gross_value, total_price.getText().toString());
            map1.put(Tables.ORDER_is_active, "1");
            map1.put(Tables.ORDER_ADVANCE_COLLECTION, "0");
            map1.put(Tables.ORDER_latitude, lat);
            map1.put(Tables.ORDER_longitude, lng);
            map1.put(Tables.MEMOS_DISCOUNT_TYPE, TempData.DISTYPE + "");
            String outlate = "0";
            if (TempData.OutletCatagoryID.equalsIgnoreCase("17")) {
                outlate = "1";
            }

            Log.e("ProductSalesUsingFor", "Memo Edit & Create");
            map1.put(Tables.ORDER_is_distributor, outlate);
            map1.put(Tables.ORDER_from_app, FromApp);
            map1.put(Tables.ORDER_for_order_delete, for_memo_delete);
            map1.put(Tables.ORDER_editable, editableAllow);

            map1.put(Tables.ORDER_is_pushed, TempData.isPushed);
            if (!TempData.editMemo.equalsIgnoreCase("true"))
                map1.put(Tables.ORDER_created_at, bf.getCurrentDateTime());
            map1.put(Tables.ORDER_updated_at, bf.getCurrentDateTime());

            db.InsertTable(map1, Tables.TABLE_NAME_ORDER);
            Log.e("Product Sales For", "Order Create");
        } else if (SALE_STATE == MEMO_EDITING) {
            try {
                Log.e("orderNo", orderNo);
                map1.put(Tables.ORDER_order_number, orderNo);
                map1.put(Tables.MEMOS_memo_number, memoNo);
                map1.put(Tables.MEMOS_memo_date, TempData.MemoDate);
                map1.put(Tables.MEMOS_memo_date_time, TempData.MemoDateTime);
                map1.put(Tables.SR_ID, getPreference(SR_ID));
                map1.put(Tables.MEMOS_sales_to, "0");
                map1.put(Tables.OUTLETS_ID, TempData.OutletID);
                map1.put(Tables.MARKET_ID, TempData.MarketID);
                map1.put(Tables.MEMOS_gross_value, total_price.getText().toString());
                map1.put(Tables.MEMOS_is_active, "1");
                map1.put(Tables.MEMOS_latitude, lat);
                map1.put("cash_received", "" + (Double.parseDouble(total_price.getText().toString().trim()) + setAdvanceText(TempData.orderNumber)));
                map1.put(Tables.MEMOS_longitude, lng);
                map1.put(Tables.MEMOS_discount_amount, discount.getText().toString().trim());
                if (TempData.DISTYPE == 1) {
                    map1.put(Tables.MEMOS_discount_Percentage, TempData.DISCOUNTP + "");
                } else if (TempData.DISTYPE == 2) {

                    map1.put(Tables.MEMOS_discount_Percentage, discount.getText().toString().trim() + "");
                } else {
                    map1.put(Tables.MEMOS_discount_Percentage, "0");
                }
                String outlate = "0";
                if (TempData.OutletCatagoryID.equalsIgnoreCase("17")) {
                    outlate = "1";
                }
                Log.e("ProductSalesUsingFor", "MemoEdit");
                map1.put(Tables.MEMOS_is_distributor, outlate);
                map1.put(Tables.MEMOS_from_app, FromApp);
                map1.put(Tables.MEMOS_for_memo_delete, for_memo_delete);
                map1.put(Tables.MEMOS_DISCOUNT_TYPE, TempData.DISTYPE + "");
                map1.put(Tables.MEMOS_editable, editableAllow);
                map1.put(Tables.MEMOS_TOTAL_Vat, vattv.getText().toString().trim());
                map1.put(Tables.ORDER_is_pushed, "1");
                map1.put(Tables.ORDER_created_at, TempData.MemoDateTime);
                map1.put(Tables.ORDER_updated_at, bf.getCurrentDateTime());
                db.InsertTable(map1, "temp_memos");
            }
            catch (Exception e) {
                e.printStackTrace();
            }

        }


        db.excQuery("UPDATE outlet_visit SET ispushed='3'  WHERE  date(updated_at)='" + getCurrentDate() + "' and outlet_id ='" + getPreference("OutletID") + "'");


        Log.e("Outside Check!", "Checkede!");
        double paymentAmount = 0.0;

        double memo_value = Double.parseDouble(TotalPrice.getText().toString());
        double paid_amount = memo_value;

    }

    private double setAdvanceText(String orderNumber) {
        double advance = 0.0;
        Cursor c = db.rawQuery("select advance_collection from order_table where order_number='" + orderNumber + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            advance = c.getDouble(0);
        }

        Log.e("advance", advance + "");
        if (TempData.editMemo.equalsIgnoreCase("true")) {

            return advance;
        } else

            return 0.0;


    }

    public void MemoDetails(String type, String product_id, String qunatity, String price, String vat) {

        //Toast.makeText(this, "sample", Toast.LENGTH_SHORT).show();
        Log.e("CurrentInventoryID: ", TempData.CurrentInventoryID);

        Log.e("SALE_STATE: ", String.valueOf(SALE_STATE));

        HashMap<String, String> map = new HashMap<String, String>();

        // i added here for open product price

//        if(type.equalsIgnoreCase("0")){
//            map.put(Tables.ORDER_DETAILS_order_number, orderNo);
//            map.put(Tables.ORDER_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
//            map.put(Tables.PRODUCT_ID, product_id);
//            map.put(Tables.ORDER_DETAILS_product_type, type);
//            map.put(Tables.ORDER_DETAILS_quantity, qunatity);
//            map.put(Tables.ORDER_DETAILS_price, price);
//            map.put(Tables.ORDER_DETAILS_order_date, OrderDate);
//            map.put(ORDER_DETAILS_is_bonus, "0");
//            map.put(Tables.MEMO_DETAILS_vat, vat);
//            map.put(Tables.ORDER_DETAILS_updated_at, getCurrentDateTime());
//            db.InsertTable(map, TABLE_NAME_ORDER_DETAILS);
//        }

        if (type.equalsIgnoreCase("1")) {
            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put("gift_issue_id", "" + orderNo);
            map2.put("product_id", product_id);
            map2.put("quantity", qunatity);
            db.InsertTable(map2, "gift_issue_details");
        }


        if (SALE_STATE == Converting_TO_MEMO) {

            map.put(Tables.MEMO_DETAILS_memo_number, memoNo);
            map.put(Tables.MEMO_DETAILS_order_number, orderNo);
            map.put(Tables.MEMO_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.MEMO_DETAILS_product_type, type);
            map.put(Tables.MEMO_DETAILS_quantity, qunatity);
            map.put(Tables.MEMO_DETAILS_price, price);
            map.put(Tables.MEMO_DETAILS_memo_date, getCurrentDate());
            map.put(Tables.MEMO_DETAILS_is_bonus, "0");
            map.put(Tables.MEMO_DETAILS_vat, vat);
            map.put(Tables.MEMO_DETAILS_Serial_NUMBER, getSirialS(product_id));
            map.put(Tables.MEMO_DETAILS_created_at, getCurrentDateTime());
            map.put(Tables.MEMO_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, "temp_memo_details");

        } else if (SALE_STATE == 0) {

            map.put(Tables.ORDER_DETAILS_order_number, orderNo);
            map.put(Tables.ORDER_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.ORDER_DETAILS_product_type, type);
            map.put(Tables.ORDER_DETAILS_quantity, qunatity);
            map.put(Tables.ORDER_DETAILS_price, price);
            map.put(Tables.ORDER_DETAILS_order_date, OrderDate);
            map.put(ORDER_DETAILS_is_bonus, "0");
            map.put(Tables.MEMO_DETAILS_vat, vat);
            map.put(Tables.ORDER_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, TABLE_NAME_ORDER_DETAILS);
        } else if (SALE_STATE == MEMO_EDITING) {

            map.put(Tables.MEMO_DETAILS_memo_number, memoNo);
            map.put(Tables.MEMO_DETAILS_order_number, orderNo);
            map.put(Tables.MEMO_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.MEMO_DETAILS_product_type, type);
            map.put(Tables.MEMO_DETAILS_quantity, qunatity);
            map.put(Tables.MEMO_DETAILS_price, price);
            map.put(Tables.MEMO_DETAILS_vat, vat);
            map.put(Tables.MEMO_DETAILS_memo_date, TempData.MemoDate);
            map.put(Tables.MEMO_DETAILS_is_bonus, "0");
            map.put(Tables.MEMO_DETAILS_Serial_NUMBER, getSirialS(product_id));
            map.put(Tables.MEMO_DETAILS_created_at, TempData.MemoDateTime);
            map.put(Tables.MEMO_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, "temp_memo_details");
            Log.e("ProductSaleUsiormemod", "MemoEdit");

            Log.e("UPDATESALE_STATE: ", String.valueOf(SALE_STATE));
        }

    }

    private String getSirialS(String product_id) {
        Cursor c = db.rawQuery("select serial_number from product_boolean where product_id='" + product_id + "' and Outlet_id='" + TempData.OutletID + "'");
        c.moveToFirst();
        if (c.getCount() > 0 && c != null) {

            return c.getString(0);
        }
        return "";
    }

    private void UpdateDbForSeliarPS(String serial, String is_used) {
        if (serial!=null && serial.length()>0) {
            String snumbers[] = serial.split(",");

            for (int i = 0; i < snumbers.length; i++) {

                db.excQuery("update product_serials set is_used='" + is_used + "' where serial_no = '" + snumbers[i] + "'");
                Log.e("rrrr", "update product_serials set is_used='" + is_used + "' where serial_no = '" + snumbers[i] + "'");
            }
        }

    }

    public void MemoDetailsForBonus(String product_id, String qunatity, String sirial_number) {
        Log.e("oyeah", sirial_number + " ");
        // UpdateDbForSeliarPS(sirial_number, "1");
//		db.excQuery("DELETE FROM bonus_items WHERE memo_no='"+TempData.orderNumber+"'");
        price = "0";
        //Toast.makeText(this, "total bonus product list", Toast.LENGTH_SHORT).show();
        HashMap<String, String> map = new HashMap<String, String>();
        if (SALE_STATE == Converting_TO_MEMO) {

            map.put(Tables.MEMO_DETAILS_memo_number, memoNo);
            map.put(Tables.MEMO_DETAILS_order_number, orderNo);
            map.put(Tables.MEMO_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.MEMO_DETAILS_product_type, "2");
            map.put(Tables.MEMO_DETAILS_quantity, qunatity);
            map.put(Tables.MEMO_DETAILS_price, price);
            map.put(Tables.MEMO_DETAILS_memo_date, getCurrentDate());
            map.put(Tables.MEMO_DETAILS_is_bonus, "1");
            map.put(Tables.MEMO_DETAILS_Serial_NUMBER, sirial_number);
            map.put(Tables.MEMO_DETAILS_created_at, getCurrentDateTime());
            map.put(Tables.MEMO_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, "temp_memo_details");

        } else if (SALE_STATE == 0) {

            map.put(Tables.ORDER_DETAILS_order_number, orderNo);
            map.put(Tables.ORDER_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.ORDER_DETAILS_product_type, "2");
            map.put(Tables.ORDER_DETAILS_quantity, qunatity);
            map.put(Tables.ORDER_DETAILS_price, price);
            map.put(Tables.ORDER_DETAILS_order_date, OrderDate);
            map.put(ORDER_DETAILS_is_bonus, "1");
            map.put(Tables.ORDER_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, TABLE_NAME_ORDER_DETAILS);
        } else if (SALE_STATE == MEMO_EDITING) {

            map.put(Tables.MEMO_DETAILS_memo_number, memoNo);
            map.put(Tables.MEMO_DETAILS_order_number, orderNo);
            map.put(Tables.MEMO_DETAILS_current_inventory_id, TempData.CurrentInventoryID);
            map.put(Tables.PRODUCT_ID, product_id);
            map.put(Tables.MEMO_DETAILS_product_type, "2");
            map.put(Tables.MEMO_DETAILS_quantity, qunatity);
            map.put(Tables.MEMO_DETAILS_price, price);
            map.put(Tables.MEMO_DETAILS_memo_date, TempData.MemoDate);
            map.put(Tables.MEMO_DETAILS_is_bonus, "1");
            map.put(Tables.MEMO_DETAILS_Serial_NUMBER, sirial_number);
            map.put(Tables.MEMO_DETAILS_created_at, TempData.MemoDateTime);
            map.put(Tables.MEMO_DETAILS_updated_at, getCurrentDateTime());
            db.InsertTable(map, "temp_memo_details");
            Log.e("ProductSalesUsingFor", "MemoEdit");
        }


    }

    public void SalesOrderNO(Boolean createMemo) {
        final Calendar c = Calendar.getInstance();
        year1 = c.get(Calendar.YEAR);
        month1 = c.get(Calendar.MONTH);
        day1 = c.get(Calendar.DAY_OF_MONTH);

        String day = "", month = "", year;


        String year_ = String.valueOf(year1);
        year = year_.substring(2, 4);


        if (day1 < 10)
            day = "0" + String.valueOf(day1);
        else
            day = String.valueOf(day1);
        if (month1 + 1 < 10)
            month = "0" + String.valueOf(month1 + 1);
        else
            month = String.valueOf(month1 + 1);
        date = year1 + "-" + month + "-" + day;
        Log.e("OutletID", TempData.OutletID);
        Log.e("SO", getPreference(SR_ID));
        Log.e("Day", day);
        Log.e("year", year);
        Log.e("Month", month);
        Log.e("Hour", "" + c.get(c.HOUR_OF_DAY));
        Log.e("Minute", "" + c.get(c.MINUTE));
        Log.e("SECOND", "" + c.get(c.SECOND));

        if (createMemo) {
            orderNo = "O" + getPreference(SR_ID) + year + day + month + getCurrentTime24ForMemo();
        }
    }

    public void SalesMemoNO(Boolean createMemo) {
        final Calendar c = Calendar.getInstance();
        year1 = c.get(Calendar.YEAR);
        month1 = c.get(Calendar.MONTH);
        day1 = c.get(Calendar.DAY_OF_MONTH);

        String day = "", month = "", year;


        String year_ = String.valueOf(year1);
        year = year_.substring(2, 4);


        if (day1 < 10)
            day = "0" + String.valueOf(day1);
        else
            day = String.valueOf(day1);
        if (month1 + 1 < 10)
            month = "0" + String.valueOf(month1 + 1);
        else
            month = String.valueOf(month1 + 1);
        date = year1 + "-" + month + "-" + day;
        Log.e("OutletID", TempData.OutletID);
        Log.e("SO", getPreference("SO"));
        Log.e("Day", day);
        Log.e("year", year);
        Log.e("Month", month);
        Log.e("Hour", "" + c.get(c.HOUR_OF_DAY));
        Log.e("Minute", "" + c.get(c.MINUTE));
        Log.e("SECOND", "" + c.get(c.SECOND));

        if (createMemo) {
            memoNo = "M" + getPreference(SR_ID) + year + month + day + getCurrentTime24ForMemo();
            Log.e("MemoNo", memoNo);
        }
    }

    public void InvoiceTemporary(String product_id, String product_name, String quantity, String price) {

        //Log.e("Cursor","null");
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("product_name", product_name);
        map.put("quantity", quantity);
        map.put("price", price);
        TempData.INVOICE_DETAILS.add(map);

    }


    private void ProduSctDisplayListView() {


        ItemListFromDB.clear();

        Cursor c = db.rawQuery("SELECT DISTINCT * FROM product_boolean where boolean=" + "'" + "true" + "'" + " AND outlet_id='" + getPreference("OutletID") + "'");
        if (c != null) {
            if (c.moveToFirst()) {
                do {

                    String product_id = c.getString(c.getColumnIndex("product_id"));
                    String is_bonus = "";
                    String products_query = "SELECT * FROM product_history WHERE start_date<=" + "'" + memodate + "'" + " and end_date>=" + "'" + memodate + "' and product_id='" + product_id + "'";
                    Log.e("products_query", products_query);
                    Cursor c5 = db.rawQuery(products_query);
                    Log.e("QUERY COUNT:", "..............." + products_query);
                    if (c5 != null) {
                        if (c5.moveToFirst()) {
                            do {
                                String product_id_ = c5.getString(c5.getColumnIndex("product_id"));
                                is_bonus = c5.getString(c5.getColumnIndex("is_bonus"));

                                if (TempData.editMemo.equalsIgnoreCase("true")) {
                                    Log.e("memonoprint", memoNumber);

                                    String Unitprice = "";
                                    Log.e("is it edit?", "YES");
                                    Log.e("salesrrrr", "SELECT * FROM " + TABLE_NAME_ORDER_DETAILS + " where product_id='" + product_id_ + "' and " + Tables.ORDER_order_number + "='" + TempData.orderNumber + "' and is_bonus='0' AND product_type='0'");
                                    Cursor c2 = db.rawQuery("SELECT * FROM " + TABLE_NAME_ORDER_DETAILS + " where product_id='" + product_id_ + "' and " + Tables.ORDER_order_number + "='" + TempData.orderNumber + "' and is_bonus='0'");
                                    Log.e("memo_query", "SELECT * FROM " + TABLE_NAME_ORDER_DETAILS + " where product_id='" + product_id_ + "' and " + Tables.ORDER_order_number + "='" + TempData.orderNumber + "' and is_bonus='0' AND quantity='25'");

                                    if (c2 != null) {
                                        if (c2.moveToFirst()) {
                                            do {

                                                Unitprice = c2.getString(c2.getColumnIndex("price"));
                                                Log.e("sale ", "UnitpriceMemo: " + Unitprice);
                                                if (!TextUtils.isEmpty(Unitprice))
                                                    savePreference("memo" + product_id, Unitprice);

                                            } while (c2.moveToNext());
                                        }
                                    }
                                }


                            } while (c5.moveToNext());
                        }
                    }


                    String product_name = "";
                    String query1 = "SELECT product_name FROM product WHERE product_id='" + product_id + "'";
                    Cursor c2 = db.rawQuery(query1);
                    if (c2 != null) {
                        if (c2.moveToFirst()) {
                            do {
                                product_name = c2.getString(0);
                            } while (c2.moveToNext());
                        }
                    }


                    String quantity = c.getString(c.getColumnIndex("quantity"));

                    Log.e("QUANTITY:", "  " + quantity);

                    String product_category_id = c.getString(c.getColumnIndex("product_category_id"));
                    String product_type_id = c.getString(c.getColumnIndex("product_type_id"));

                    if (!product_type_id.equalsIgnoreCase("3")) {
                        HashMap<String, String> product_list_map = new HashMap<String, String>();
                        product_list_map.put("product_id", product_id);
                        product_list_map.put("product_name", product_name);
                        product_list_map.put("quantity", quantity);
                        product_list_map.put("is_bonus", is_bonus);
                        product_list_map.put("product_category_id", product_category_id);
                        product_list_map.put("serial_number", c.getString(c.getColumnIndex("serial_number")));
                        product_list_map.put("vatt", vatCalculation(product_id, Double.parseDouble(quantity)) + "");

                        ItemListFromDB.add(product_list_map);
                    } else {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("Name", product_name);
                        map.put("quantity", quantity);
                        map.put("product_id", product_id);

                        GiftItem.add(map);
                    }


                } while (c.moveToNext());
            }


            Log.e("ItemListFromDB prd", "ItemListFromDB prd: " + ItemListFromDB);
            Adapter = new SalesOrderDetailsAdaperForMemo(this, ItemListFromDB, getPreference("SO"), getPreference("OutletID"), memoUpdate);
            //		  ListView listView = (ListView) findViewById(R.id.ProductListView);
            list_add_vehicle_details.setAdapter(Adapter);


//            if (SALE_STATE==Converting_TO_MEMO||SALE_STATE==MEMO_EDITING) {
//                SalesOrderDetailsAdaperForMemo adaper2 = new SalesOrderDetailsAdaperForMemo(this, ItemListFromDB, getPreference("SO"), getPreference("OutletID"));
//                list_add_vehicle_details.setAdapter(adaper2);
//            }else {
//                list_add_vehicle_details.setAdapter(Adapter);
//           }

            String gift = "";
            for (int i = 0; i < GiftItem.size(); i++) {
                HashMap<String, String> map = GiftItem.get(i);
                gift = gift + map.get("Name") + "(" + map.get("quantity") + ")";
                if (i != GiftItem.size() - 1)
                    gift = gift + ", ";
            }
            if (gift.length() > 0)
                txtGift.setText(gift);
            else
                txtGift.setText("Nill");

            Log.e("GIFT", gift);
        }

        Log.e("ProductSalesForMemo", " End");
    }

    private double vatCalculation(String product_id, Double quantity) {

        Double vatprice = 0.0, vatofvat = 0.0, price = 0.0, vat = 0.0;
        Cursor c = db.rawQuery("select vat from product_price where product_id='" + product_id + "' and vat!='null'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {


            return c.getDouble(0);
        }
        return vat;
    }

    public void writeToFile(String fileName) {
        FileOutputStream fos = null;

        try {
            final File dir = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/SMC_Print/");

            if (!dir.exists()) {
                if (!dir.mkdirs()) {
                    Log.e("ALERT", "could not create the directories");
                }
            }

            final File myFile = new File(dir, fileName + ".txt");

            if (!myFile.exists()) {
                myFile.createNewFile();
            }

            fos = new FileOutputStream(myFile);

            String data = TempData.INVOICE_DETAILS.toString() + TempData.TempGift.toString() + TempData.TempBonus.toString();
            String hexDump = hexDump(data.getBytes(), data);
            Log.e(" hext dump:", "..." + hexDump);
            fos.write(hexDump.getBytes());
            fos.close();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static String hexDump(byte[] data, String encoding) {
        StringBuilder sb = new StringBuilder();

        if (encoding == null) {
            encoding = System.getProperty("file.encoding");
        }

        final int bytesPerLine = 16;

        int i = 0;
        for (i = 0; i < data.length; i++) {
            if (i % bytesPerLine == 0) {
                if (i > 0) {
                    sb.append("  ");
                    try {
                        sb.append(new String(data, i - bytesPerLine, bytesPerLine, encoding));
                    } catch (UnsupportedEncodingException e) {

                    }
                    sb.append("\n");
                }
                sb.append(String.format("%08x:", i));
            } else if (i % 4 == 0) {
                sb.append(' ');
            }

            sb.append(String.format(" %02x", data[i]));
        }

//		    Log.v(Constants.LOG_TAG, "data size: " + data.length + ", dumped size: " + i);

        if (i % bytesPerLine != 0) {
            if (i / bytesPerLine > 0) {
                for (int j = i; j % bytesPerLine != 0; j++) {
                    if (j % 4 == 0) {
                        sb.append(" ");
                    }
                    sb.append("   ");
                }
            }
            sb.append("  ");
            try {
                sb.append(new String(data, i - i % bytesPerLine, i % bytesPerLine, encoding));
            } catch (UnsupportedEncodingException e) {

            }
            sb.append("\n");
        }

        return sb.toString();
    }

    public static String byteArrayToHex(byte[] a) {
        StringBuilder sb = new StringBuilder(a.length * 2);
        for (byte b : a)
            sb.append(String.format("%02x", b));
        return sb.toString();
    }

    public void AlertDialog() {
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(ProductSalesForMemo.this);
        builder.setMessage(" Are you sure?");
        builder.setCancelable(false)
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                        if (!total_price.getText().toString().equals("0.00")) {
                          //  db.excQuery("delete from memo_details");
                            db.excQuery("DELETE  FROM temp_memos");
                            db.excQuery("DELETE  FROM temp_memo_details");
                           // db.excQuery("DELETE  FROM memos");

                            Log.e("delete_edit_data", total_price.getText().toString());

                            new SaveData().execute();
                        }

                        // dialog.dismiss();

                    }


                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //  Action for 'NO' Button
                        ///SavePrintButton.setEnabled(true);
                        SaveBtn.setEnabled(true);
                        dialog.cancel();
                    }
                });

        //Creating dialog box
        android.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void DeleteBefore3DaysMemos() {
        String memo_no1 = "";
        String AllMemoNos = "";
        String not_in_condition = "";


        db.excQuery("DELETE FROM product_boolean WHERE outlet_id ='" + TempData
                .OutletID + "'");

        Cursor cd = db.rawQuery(" DELETE FROM product_boolean where strftime('%Y/%m/%d', updated_at) < ( strftime('%Y/%m/%d', date('now','-2 day')))");


        if (memo_no1.length() > 1) {
            AllMemoNos = memo_no1.substring(1, memo_no1.length());
            not_in_condition = "AND memo_no NOT IN (" + AllMemoNos + ")";
            Log.e("AllMemoNos ", "AllMemoNos: " + AllMemoNos);
//				Cursor c6 =db.rawQuery("SELECT memo_no FROM memos where outlet_id ='"+TempData.OutletID+"' and memo_date<=date('"+OrderDate+"') "+not_in_condition);
            String QRY = "SELECT memo_no FROM memos where outlet_id ='" + TempData.OutletID + "' and memo_date < (SELECT DATETIME('now', '-3  day')) " + not_in_condition + " and isPushed=1";
            Log.e("QRY ", "QRY " + QRY);
            Cursor c6 = db.rawQuery(QRY);
            if (c6 != null) {
                if (c6.moveToFirst()) {
                    do {
                        String memo_no6 = c6.getString(0);

                        Log.e("memo_no6 ", "memo_no6: " + memo_no6);

                        db.deleteRowOfMemo("memos", memo_no6);
                        db.deleteRowOfMemo("memo_details", memo_no6);
                        db.deleteRowOfMemo("credit_collections", memo_no6);
                        db.deleteRowOfMemo("payments", memo_no6);

                    } while (c6.moveToNext());
                }
            }
        } else {
            String QRY = "SELECT " + Tables.ORDER_order_number + " FROM order_table where outlet_id ='" + TempData.OutletID + "' and order_date < (SELECT DATETIME('now', '-3  day'))" + " and is_Pushed=1";
            Log.e("QRY ", "QRY " + QRY);
            Cursor c6 = db.rawQuery(QRY);
            if (c6 != null) {
                if (c6.moveToFirst()) {
                    do {
                        String memo_no6 = c6.getString(0);

                        Log.e("memo_no6 ", "memo_no6: " + memo_no6);

                        db.deleteRowOfMemo("memos", memo_no6);
                        db.deleteRowOfMemo("memo_details", memo_no6);
                        db.deleteRowOfMemo("credit_collections", memo_no6);
                        db.deleteRowOfMemo("payments", memo_no6);

                    } while (c6.moveToNext());
                }
            }
        }

    }

    private void tempClean() {
        orderList = Adapter.getAdapterHashMapList();
        for (int k = 0; k < orderList.size(); k++) {

            savePreference("up" + orderList.get(k).get("product_id"), "0");
//			savePreference(TempData.OutletID+orderList.get(k).get("product_id"),"0");
        }
    }

    private void TempBonus() {

        for (int k = 0; k < TempData.TotalBonusProductList.size(); k++) {
            savePreference("bonus" + TempData.TotalBonusProductList.get(k).get("product_id"), "0.0");
            Log.e("TempBonus()", "TempBonus()" + getPreference("bonus" + TempData.TotalBonusProductList.get(k).get("product_id")));
        }
    }

    private void bonus_show_WithoutDB() {
        Log.e("TotalBonusProductList", " TotalBonusProductList for Sales Order details: " + TempData.TotalBonusProductList);
        String bonus_value1 = "";
        Log.e("TempData.TotalBonusProductList.size()", TempData.TotalBonusProductList.size() + "");
        for (int i = 0; i < TempData.TotalBonusProductList.size(); i++) {

            HashMap<String, String> product_list_map2 = new HashMap<String, String>();

            product_list_map2 = TempData.TotalBonusProductList.get(i);

            Log.e("product_id: ", "" + product_list_map2.get("product_id"));
            Log.e("product_name: ", "" + product_list_map2.get("product_name"));
            Log.e("quantity: ", "" + product_list_map2.get("quantity"));

            String product_id = product_list_map2.get("product_id");
            String product_name = product_list_map2.get("product_name");
            String quantity = product_list_map2.get("quantity");

            String bonus_value2 = product_name + "(" + quantity + ")";
            bonus_value1 = bonus_value1 + "," + bonus_value2;
            String bonus_value = bonus_value1.substring(1, bonus_value1.length());

            Log.e("bonus_value", bonus_value);
            txtBonusExtra.setText(bonus_value);
            Log.e("txtBonusExtra", txtBonusExtra.getText().toString());
            txtBonusExtra.setVisibility(View.VISIBLE);

        }
    }

    private void BonusButtonShow() {
        double boolean_quantity = 0.0;
        double memo_details_quantity = 0.0;

        ArrayList<HashMap<String, String>> BonusItemListFromDB = new ArrayList<HashMap<String, String>>();
        BonusItemListFromDB.clear();
        Cursor c2 = null;
        c2 = db.rawQuery("SELECT product_id FROM product_history WHERE start_date<=" + "'" + memodate + "'" + " and end_date>=" + "'" + memodate + "' AND is_bonus=1");
        if (c2 != null) {
            if (c2.moveToFirst()) {
                do {

                    String product_id = c2.getString(0);
                    String EditQuery = " and quantity>0";
                    if (TempData.editMemo.equalsIgnoreCase("true")) {
                        EditQuery = "and quantity>=0";
                    }
                } while (c2.moveToNext());

				/*c2.close();
				db.close();*/
            }
            Log.e("BonusItemListFromDB--", "BonusItemListFromDB--" + BonusItemListFromDB.toString());

            Log.e("AddBonusBtn", memo_details_quantity + "-" + boolean_quantity
                    + "=" + (memo_details_quantity - boolean_quantity));

        }
    }

    public String injectable_product_check() {
        String is_injectable = "0";

        String outlet = getPreference("OutletID");
        Log.d("outlete", outlet);

        String query2 = "SELECT product_id FROM product_boolean WHERE outlet_id='" + getPreference("OutletID") + "' AND boolean='true'";
        Cursor c23 = db.rawQuery(query2);
        int count22 = c23.getCount();
        Log.e("QUERY COUNT:", "..............." + count22);
        if (c23 != null) {
            if (c23.moveToFirst()) {
                do {
                    String productId = c23.getString(0);
                    String query = "SELECT is_injectable FROM product WHERE product_id='" + productId + "'";
                    Log.e("QUERY:", "..............." + query);
                    Cursor c = db.rawQuery(query);
                    int count23 = c.getCount();
                    Log.e("QUERY COUNT:", "..............." + count23);

                    if (c != null) {
                        if (c.moveToFirst()) {
                            do {
                                is_injectable = c.getString(0);
                                Log.d("isinjectableoutuput", is_injectable + "productid" + productId);
                            } while (c.moveToNext());
                        }
                    }


                } while (c23.moveToNext());
            }
        }

        return is_injectable;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            Intent idd = new Intent(ProductSalesForMemo.this, Sales_Memo.class);
            startActivity(idd);
            finish();


        }
        return super.onKeyDown(keyCode, event);

    }

    @Override
    protected void onPause() {
        super.onPause();

      //  finish();
    }

    private void getLocation() {

        //Check Permissions again

        if (ActivityCompat.checkSelfPermission(ProductSalesForMemo.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(ProductSalesForMemo.this,

                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]
                    {Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_LOCATION);
        } else {
            android.location.Location LocationGps = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            android.location.Location LocationNetwork = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
            android.location.Location LocationPassive = locationManager.getLastKnownLocation(LocationManager.PASSIVE_PROVIDER);

            if (LocationGps != null) {
                double lat = LocationGps.getLatitude();
                double longi = LocationGps.getLongitude();
                lattitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                Log.e("Location", "getLocation: +" + "Your Location:" + "\n" + "Latitude= " + lattitude + "\n" + "Longitude= " + longitude);
                // Toast.makeText(ProductSalesForMemo.this,"Lat : "+lattitude+" Long: "+longitude, Toast.LENGTH_LONG).show();
            } else if (LocationNetwork != null) {
                double lat = LocationNetwork.getLatitude();
                double longi = LocationNetwork.getLongitude();

                lattitude = String.valueOf(lat);
                longitude = String.valueOf(longi);

                // Log.e("Loc", "getLocation: "+"Your Location:"+"\n"+"Latitude= "+lattitude+"\n"+"Longitude= "+longitude);
                //Toast.makeText(ProductSalesForMemo.this,"Lat : "+lattitude+" Long: "+longitude, Toast.LENGTH_LONG).show();

                bf.savePreference("lat", lattitude);
                bf.savePreference("lon", longitude);
            } else if (LocationPassive != null) {
                double lat = LocationPassive.getLatitude();
                double longi = LocationPassive.getLongitude();

                lattitude = String.valueOf(lat);
                longitude = String.valueOf(longi);
                bf.savePreference("lat", lattitude);
                bf.savePreference("lon", longitude);

                Log.e("Loc", "getLocation: " + "Your Location:" + "\n" + "Latitude= " + lattitude + "\n" + "Longitude= " + longitude);
                // Toast.makeText(ProductSalesForMemo.this,"Lat : "+lattitude+" Long: "+longitude, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Can't Get Your Location", Toast.LENGTH_SHORT).show();
            }

            //Thats All Run Your App
        }

    }

    private void OnGPS() {

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage("Enable GPS").setCancelable(false).setPositiveButton("YES", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
            }
        }).setNegativeButton("NO", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                dialog.cancel();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    /*public void EntryForCreditCollection() {

        Log.e("creditcollection", memoNo);
        if (TempData.editMemo.equalsIgnoreCase("true") && SALE_STATE == MEMO_EDITING) {
            ///memoNo=TempData.memoNumber;
            db.excQuery("DELETE FROM credit_collection WHERE memo_number='" + memoNo + "'");
        }
        Log.e("memoNo", memoNo);
        double dueAmount = 0.0;

        double memo_value = Double.parseDouble(total_price.getText().toString());
        double paid_amount = Double.parseDouble(receive.getText().toString()) + setAdvanceText(orderNo);

        Log.e("payment amount:", receive.getText().toString());

        Log.e("due_amount", String.valueOf(dueAmount)+" "+paid_amount+" "+setAdvanceText(orderNo));
        if (paid_amount == 0.0) {
            dueAmount = memo_value;
        } else {
            dueAmount = memo_value - paid_amount ;
        }
        if (dueAmount > 0.0)
        //if(etPayment.getText().toString().equalsIgnoreCase("0"))
        {
            Log.e("due_amount", String.valueOf(dueAmount));
            HashMap<String, String> map = new HashMap<String, String>();
            map.put("outlet_id", TempData.OutletID);
//		map.put("date",getCurrentDate() );
            map.put("date", memodate);
            map.put("memo_number", memoNo);
            map.put("memo_value", total_price.getText().toString());
//		map.put("collect_date", getCurrentDate());
            map.put("collection_date", memodate);
            map.put("instrument_type", "1");
            map.put(SR_ID, getPreference(SR_ID));
            map.put("due_amount", String.valueOf(dueAmount));
            map.put("paid_amount", receive.getText().toString().trim());
            map.put("is_Pushed", "0");
            map.put(Tables.MESSAGE_CREATED_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "temp_credit_collection");

        }
        double paymentAmount = 0.0;


        if (paid_amount != 0.0)
        //if(!etPayment.getText().toString().equalsIgnoreCase("")&&!etPayment.getText().toString().equalsIgnoreCase("0"))
        {
            Log.e("Check!", "Checkede!");

            java.text.DateFormat dateFormat = new SimpleDateFormat("MMddHHmmss");
            Calendar cal = Calendar.getInstance();
            String paymentTempId = "P" + getPreference("SO") + dateFormat.format(cal.getTime());

            HashMap<String, String> map2 = new HashMap<String, String>();
            map2.put("payment_temp_id", paymentTempId);
            map2.put("outlet_id", TempData.OutletID);
            map2.put("memo_no", memoNo);
            map2.put("memo_value", total_price.getText().toString());
            map2.put("due_amount", "0");


            if (paid_amount != 0.0) {
                if (paid_amount > memo_value)
                    paymentAmount = memo_value;
                else
                    paymentAmount = paid_amount;
            } else {
                paymentAmount = 0.0;
            }
            Log.e("PAYMENT_AMOUNT", "......" + paymentAmount);

            map2.put("paid_amount", String.valueOf(paymentAmount));
            map2.put("inst_type", "1");
            map2.put("bank_branch_id", "");
//		map2.put("collect_date", getCurrentDate());
            map2.put("collect_date", memodate);
//		map2.put("memo_date",getCurrentDate() );
            map2.put("memo_date", memodate);
            map2.put("is_pushed", "0");

            db.InsertTable(map2, "temp_payments");


        }
    }

    public void EntryForAdvanceCollection() {
        if (TempData.editMemo.equalsIgnoreCase("true")) {
            memoNo = TempData.memoNumber;
            db.excQuery("DELETE FROM advance_collection WHERE order_number='" + TempData.orderNumber + "'");
        }
        Log.e("memoNo", memoNo);
        double dueAmount = 0.0;

        double memo_value = Double.parseDouble(TotalPrice.getText().toString());
        double paid_amount = Double.parseDouble(receive.getText().toString());

        Log.e("payment amount:", receive.getText().toString());


        if (paid_amount == 0.0) {
            dueAmount = memo_value;
        } else {
            dueAmount = memo_value - paid_amount;
        }
        if (dueAmount > 0.0)
        //if(etPayment.getText().toString().equalsIgnoreCase("0"))
        {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("outlet_id", TempData.OutletID);
//		map.put("date",getCurrentDate() );
            map.put("date", memodate);
            map.put("order_number", orderNo);
            map.put("memo_value", total_price.getText().toString().trim());
//		map.put("collect_date", getCurrentDate());
            map.put("collection_date", memodate);
            map.put("instrument_type", "1");
            map.put(SR_ID, getPreference(SR_ID));
            map.put("due_amount", String.valueOf(dueAmount));
            map.put("paid_amount", receive.getText().toString());
            map.put("is_Pushed", "0");
            map.put(Tables.MESSAGE_CREATED_AT, getCurrentDateTime());
            map.put(MESSAGE_UPDATED_AT, getCurrentDateTime());
            db.InsertTable(map, "advance_collection");

        }


    }*/


}


