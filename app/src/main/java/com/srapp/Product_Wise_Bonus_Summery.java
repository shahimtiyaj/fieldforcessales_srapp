package com.srapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.AdapterForOrderSummery;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import static com.srapp.Db_Actions.Tables.SR_ID;

public class Product_Wise_Bonus_Summery extends AppCompatActivity implements BasicFunctionListener {
    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    ListView listview;
    Button startdate, enddate;
    BasicFunction bf;
    boolean onstart =false;
    ArrayList<HashMap<String,String>> list;
    ImageView homeBtn,backBtn;
    TextView userIdTV;

    //Distributor filtering-------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId = "";
    String distributorStoreId = "";
    ArrayList<String> distList = new ArrayList<>();
    Spinner distributorSpinner;
    Data_Source db;

    BasicFunction basicFunction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product__wise__bonus__summery);

        bf = new BasicFunction(this, this);
        db = new Data_Source(this);

        dBhelper = new DB_Helper(getApplicationContext());
        distList = new ArrayList<>();

        //View model factory class
        basicFunction = new BasicFunction(this,this);

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(db.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {
                distList.add("Select Distributor");
                for (int i = 0; i < dbInfos.size(); i++) {
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorSpinner.setAdapter(distributorData);
            }
        };

        distributorSpinner = findViewById(R.id.distributor_spinner);

        distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView) arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0, 0, 0, 0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                //Log.e("distributorId_from_spin", distributorStoreId);

                if (distributorStoreId != null) {

                    DataView();

                } else {
                    //  Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);


        startdate = findViewById(R.id.start_date);
        list= new ArrayList<>();
        listview = findViewById(R.id.list_view);
        enddate = findViewById(R.id.end_date);
        userIdTV = findViewById(R.id.user_txt_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);
        homeBtn = findViewById(R.id.home);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        backBtn = findViewById(R.id.back);
        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Product_Wise_Bonus_Summery.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( Product_Wise_Bonus_Summery.this, Reports_Activity.class));
                finish();
            }
        });
        bf = new BasicFunction(this,this);
        setDateTimeField();

        startdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fromDatePickerDialog.show();
            }
        });

        enddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toDatePickerDialog.show();
            }
        });
        DataView();
    }

    private void setDateTimeField() {

        bf.savePreference("start_Date", bf.getCurrentDate());
        bf.savePreference("end_date", bf.getCurrentDate());
        startdate.setText(bf.getCurrentDate());
        enddate.setText(bf.getCurrentDate());


        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("start_Date", dateFormatter.format(newDate.getTime()));
                startdate.setText(bf.getPreference("start_Date"));
                if (onstart)
                    DataView();

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));






        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                bf.savePreference("end_date", dateFormatter.format(newDate.getTime()));
                enddate.setText(bf.getPreference("end_date"));
                if (onstart) {
                    DataView();
                }

            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


	       /* Intent idn = new Intent(SO_TargetActivity.this, SO_TargetActivity.class);
			 startActivity(idn);
			 finish();*/
    }

    private void DataView() {
        if (bf.isInternetOn()) {

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("start_date", startdate.getText().toString().trim());
                jsonObject.put("end_date", enddate.getText().toString().trim());
                jsonObject.put(SR_ID, bf.getPreference(SR_ID));
                jsonObject.put("db_id", distributorStoreId);
                jsonObject.put("mac", bf.getPreference("mac"));
            } catch (JSONException e) {
                e.printStackTrace();
            }


            bf.getResponceData(URL.PRODUCT_WISE_BONUS_SUMMERY, jsonObject.toString(), 101);
        } else {
            Toast.makeText(this, "NO Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, int i) {
        list.clear();
        onstart=true;

        if (i==101){
            try {
                JSONArray jsonArray = jsonObject.getJSONArray("product_bonus_report");
                for (int j = 0 ; j<jsonArray.length(); j++ ){
                    if (Double.parseDouble(jsonArray.getJSONObject(j).getString("total_qty"))>0) {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("product_name", jsonArray.getJSONObject(j).getString("product_name"));
                        Log.println(1,"total_qty",jsonArray.getJSONObject(j).getString("total_qty"));
                        Log.e("total_qty",jsonArray.getJSONObject(j).getString("total_qty"));
                        map.put("EC", jsonArray.getJSONObject(j).getString("total_ec"));
                        map.put("OC", jsonArray.getJSONObject(j).getString("total_oc"));
                        map.put("qty", jsonArray.getJSONObject(j).getString("total_qty"));
                        //map.put("total_price", jsonArray.getJSONObject(j).getString("total_price"));
                        list.add(map);
                    }

                }
                AdapterForOrderSummery adapterForOrderSummery = new AdapterForOrderSummery(this,list);
                listview.setAdapter(adapterForOrderSummery);


            } catch (JSONException e) {
                e.printStackTrace();
            }

        }


    }

    @Override
    public void OnConnetivityError() {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            startActivity(new Intent( Product_Wise_Bonus_Summery.this, Reports_Activity.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
