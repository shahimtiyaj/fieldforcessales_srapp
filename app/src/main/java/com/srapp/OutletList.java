package com.srapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Adapter.OutletListAdapter;
import com.srapp.Adapter.SpinnerAdapter;
import com.srapp.Db_Actions.DB_Helper;
import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.URL;
import com.srapp.Util.RecyclerTouchListener;
import com.srapp.Util.StaticFlags;
import com.srapp.kotlin.DataViewModel;
import com.srapp.kotlin.DataViewModelFactory;
import com.srapp.kotlin.Distributer;
import com.tanvir.BasicFun.BasicFunction;
import com.tanvir.BasicFun.BasicFunctionListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.widget.LinearLayout.VERTICAL;
import static com.srapp.Db_Actions.Tables.SR_ID;

public class OutletList extends AppCompatActivity implements BasicFunctionListener {
    Spinner thanaSpinner, marketSpinner;
    RecyclerView recyclerView;
    ArrayList<String> outlets;
    OutletListAdapter outletListAdapter;
    Button createOutletBtn;
    ImageView imageView;
    Data_Source ds;
    BasicFunction basicFunction;
    private HashMap<String, ArrayList<String>> outletList;
    ArrayList<String>outletIdList;
    ArrayList<String>outletNameList;
    ArrayList<String>outletThanaIdList;
    ArrayList<String>outletMarketIdList;
    private HashMap<String, ArrayList<String>> markets,thanas;
    private String thanaId;
    private String marketId;
    ArrayList<String>all,blank;
    int thanSpinnerPosition=0, marketSpinnerPosition =0;
    Boolean isLoading = false;
    private final int THREAT_SHOT = 5;
    int initialPageIndex = 1;
    String nowCaling = "all_data";
    String WITH_MARKET_ID = "with_market_id";
    String WITH_THANA_ID = "with_thana_id";
    int thanaSpinResume = 0,marketSpinResume = 0;

    ImageView homeBtn,backBtn;
    int rcvIntent;
    TextView userIdTV;

    //Distributor filtering--------------------------------------------------------------------------
    private DB_Helper dBhelper;
    private SQLiteDatabase sqLiteDatabase;
    String distributorId="";
    String distributorStoreId="";
    ArrayList<String> distList=new ArrayList<>();
    Spinner distributorSpinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet_list);

        basicFunction = new BasicFunction(this,this);
        ds = new Data_Source(this);
        dBhelper = new DB_Helper(getApplicationContext());
        distList=new ArrayList<>();

        //View model factory class
        final DataViewModel dataViewModel  = new ViewModelProvider(this, new DataViewModelFactory(this.getApplication())).get(DataViewModel.class);
        dataViewModel.getDistributorData(ds.getDistributorFullData(basicFunction.getPreference(SR_ID)));

        // Create the observer which updates the UI.
        final Observer<List<Distributer.Response.DbInfo>> distributorObserver = new Observer<List<Distributer.Response.DbInfo>>() {
            @Override
            public void onChanged(List<Distributer.Response.DbInfo> dbInfos) {

                for (int i=0;  i<dbInfos.size(); i++){
                    distList.add(dbInfos.get(i).getDbName());
                }

                SpinnerAdapter distributorData = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, distList);
                distributorSpinner.setAdapter(distributorData);
            }
        };

        // Observe the LiveData, passing in this activity as the LifecycleOwner and the observer.
        dataViewModel.getDistributorList().observe(this, distributorObserver);

        all = new ArrayList<>();
        all.add("All");
        blank = new ArrayList<>();
        blank.add("");

        Intent intent = getIntent();
        rcvIntent = intent.getIntExtra("resume",0);

        distributorSpinner = findViewById(R.id.distributor_spinner);
        thanaSpinner = findViewById(R.id.thana_spinner);
        marketSpinner = findViewById(R.id.market_spinner);
        userIdTV = findViewById(R.id.user_txt_view);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String value = prefs.getString(Tables.SR_ID, "0");
        userIdTV.setText(value);

        markets = new HashMap<>();
        thanas = new HashMap<>();
        outletIdList = new ArrayList<>();
        outletNameList = new ArrayList<>();
        outletThanaIdList = new ArrayList<>();
        outletMarketIdList = new ArrayList<>();

        thanas = ds.getThanaList("no");


        Log.e("thana test", "onCreate: "+thanas+" size "+thanas.size());



        //add adapter to thana
        if (thanas != null && thanas.size()>0) {

            thanas.get(Tables.THANA_TH_id).add(0,"x0x");
            thanas.get(Tables.THANA_NAME).add(0,"All");


            SpinnerAdapter thanaData = new SpinnerAdapter(this, R.layout
                    .spinner_item, thanas.get(Tables.THANA_NAME));
            thanaSpinner.setAdapter(thanaData);
        }

        recyclerView = findViewById(R.id.outlet_list_recycler);
        DividerItemDecoration decoration = new DividerItemDecoration(getApplicationContext(), VERTICAL);
        recyclerView.addItemDecoration(decoration);


        outletListAdapter = new OutletListAdapter();


        outletList = new HashMap<>();
        outlets = new ArrayList<>();

        Log.e("------>", "onCreate: "+rcvIntent );

        if (rcvIntent == 1){
            thanaSpinner.setSelection(StaticFlags.OUTLET_THANA_SPINNER_POSITION);
        }else if (rcvIntent == 2){
            thanaSpinner.setSelection(getPosition(thanas.get(Tables.THANA_TH_id),StaticFlags.OUTLET_THANA_ID));
        }

        distributorSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int position, long id) {
                TextView textView = (TextView)arg0.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                // TODO Auto-generated method stub
                dataViewModel.getDistributorStoreId(arg0.getItemAtPosition(position).toString());
                distributorStoreId = dataViewModel.distributorStoreId;
                Log.e("distributorId_from_spin", distributorStoreId);

                if (distributorStoreId!=null){
                    thanas = ds.getThanaList(distributorStoreId);
                    Log.e("distributorId_select", distributorStoreId);
                    //thana spinner adapter setup
                    if (thanas != null && thanas.size()>0) {
                        SpinnerAdapter thanaSpinnerAdapter = new SpinnerAdapter(getApplicationContext(), R.layout.spinner_item, thanas.get(Tables.THANA_NAME));
                        thanaSpinner.setAdapter(thanaSpinnerAdapter);
                        Log.e("thana_all_data", thanas.toString());

                    }else {
                        thanaSpinner.setAdapter(null);
                        Log.e("thana_all_data_not", thanas.toString());

                    }                }

                else {
                    Log.e("routEmpty", distributorStoreId);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


        thanaSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);
                thanSpinnerPosition = position;

                thanaId = thanas.get(Tables.THANA_TH_id).get(position);
                Log.e("thana id seletion", "onItemSelected: "+thanaId );

                StaticFlags.OUTLET_THANA_SPINNER_POSITION = position;
                StaticFlags.OUTLET_THANA_ID = thanaId;

                if (!thanaId.equals("x0x") ){
                    //set adapter to market
                    markets = ds.getMarketDataOutlet(distributorStoreId);
                    Log.e(" thana id != xox", "onItemSelected: "+markets );
                    if (markets!=null && markets.size()>0) {
                        markets.get(Tables.MARKETS_market_id).add(0, "x0x");
                        markets.get(Tables.MARKETS_market_name).add(0, "All");
                        setSpinnerAdapter(markets, marketSpinner, Tables.MARKETS_market_id, Tables.MARKETS_market_name);
                        //if navigates from outlet view
                        if (rcvIntent == 1 ){
                            marketSpinner.setSelection(StaticFlags.OUTLET_MARKET_SPINNER_POSITION);
                        }else if (rcvIntent == 2){
                            marketSpinner.setSelection(getPosition(markets.get(Tables.MARKETS_market_id),StaticFlags.OUTLET_MARKET_ID));
                        }
                    }
                    else {
                        //markets = null;
                        Log.e("markets...", "onItemSelected: "+"entered to else for markets "+markets );
                        final SpinnerAdapter marketDataAdapter = new SpinnerAdapter(OutletList.this, R.layout
                                .spinner_item, blank);
                        marketSpinner.setAdapter(marketDataAdapter);

                    }

                }
                else {
                    // here is the for the very first adapter setup for market spinner
                    /*final SpinnerAdapter marketDataAdapter = new SpinnerAdapter(OutletList.this, R.layout
                            .spinner_item, all);
                    marketSpinner.setAdapter(marketDataAdapter);*/

                    markets = ds.getMarketData("no","no");
                    if (markets!=null && markets.size()>0) {
                        markets.get(Tables.MARKETS_market_id).add(0, "x0x");
                        markets.get(Tables.MARKETS_market_name).add(0, "All");
                        setSpinnerAdapter(markets, marketSpinner, Tables.MARKETS_market_id, Tables.MARKETS_market_name);

                        //if navigates from outlet view
                        if (rcvIntent == 1 ){
                            marketSpinner.setSelection(StaticFlags.OUTLET_MARKET_SPINNER_POSITION);
                        }else if (rcvIntent == 2){
                            marketSpinner.setSelection(getPosition(markets.get(Tables.MARKETS_market_id),StaticFlags.OUTLET_MARKET_ID));
                            Log.e("------->", "onItemSelected: market pos--------> "+getPosition(markets.get(Tables.MARKETS_market_id),StaticFlags.OUTLET_MARKET_ID) );
                        }
                    }
                    else {
                        //markets = null;
                        final SpinnerAdapter marketDataAdapter = new SpinnerAdapter(OutletList.this, R.layout
                                .spinner_item, blank);
                        marketSpinner.setAdapter(marketDataAdapter);

                    }


                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        marketSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                TextView textView = (TextView)parent.getChildAt(0);
                textView.setTextColor(getResources().getColor(R.color.background_card));
                textView.setPadding(0,0,0,0);

                marketSpinnerPosition = position;
                Log.e("market select pos", "onItemSelected: "+position );

                Log.e("crashhhh", "onItemSelected.......>>: thana id "+thanaId+" mrkt id "+marketId+" pos:"+position );
                Log.e("crashhhh", "onItemSelected.......>>: market id list "+markets.get(Tables.MARKETS_market_id) );

                /*if (!thanaId.equals("x0x") && position != 0) {
                    marketId = markets.get(Tables.MARKETS_market_id).get(position);
                }else if (!thanaId.equals("x0x") && position == 0){
                    marketId.equals("x0x");
                }*/
                if (markets != null & markets.size()>0) {
                    marketId = markets.get(Tables.MARKETS_market_id).get(position);
                }else {
                    marketId = "x0x";
                }

                StaticFlags.OUTLET_MARKET_SPINNER_POSITION = position;
                StaticFlags.OUTLET_MARKET_ID = marketId;

                //set adapter to recycler view for all filter
                if (thanaId.equals("x0x") && position==0 ) {

                    Log.e("market id crash...", "onItemSelected: "+marketId );
                    //data wii come from server here
                    /*outletList = ds.getOutletData("no", "no");
                    outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);*/

                    JSONObject primaryData = new JSONObject();
                    try {
                        primaryData.put("mac",basicFunction.getPreference("mac"));
                        primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                        primaryData.put("db_id",distributorStoreId);
                        primaryData.put("page",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    initialPageIndex = 1;


                    basicFunction.getResponceData(URL.OutletList, String.valueOf(primaryData),401);

                    isLoading=false;

                }


                // filter for thana
                else if (!thanaId.equals("x0x") && position == 0) {

                    /*outletList = ds.getOutletData(thanaId, "no");
                    outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);*/
                    //............. call api....................

                    JSONObject primaryData = new JSONObject();
                    try {
                        primaryData.put("mac",basicFunction.getPreference("mac"));
                        primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                        primaryData.put("thana_id",thanaId);
                        primaryData.put("db_id",distributorStoreId);
                        primaryData.put("page",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    nowCaling = WITH_THANA_ID;
                    initialPageIndex = 1;

                    Log.e("thana filter req", "onItemSelected: "+primaryData );


                    try {
                        basicFunction.getResponceData(URL.OutletList, String.valueOf(primaryData),401);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    //............api call end....................


                }

                //filter for market when thana id is selected
                else if (!thanaId.equals("x0x") && !marketId.equals("x0x")){


                    JSONObject primaryData = new JSONObject();
                    try {
                        primaryData.put("mac",basicFunction.getPreference("mac"));
                        primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                        primaryData.put("market_id",marketId);
                        primaryData.put("db_id",distributorStoreId);
                        primaryData.put("page",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    nowCaling = WITH_THANA_ID;
                    initialPageIndex = 1;

                    Log.e("thana filter req", "onItemSelected: "+primaryData );


                    basicFunction.getResponceData(URL.OutletList, String.valueOf(primaryData),401);

                }

                //filter for market when thana id is selected
                else if (thanaId.equals("x0x") && !marketId.equals("x0x")){


                    JSONObject primaryData = new JSONObject();
                    try {
                        primaryData.put("mac",basicFunction.getPreference("mac"));
                        primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
                        primaryData.put("market_id",marketId);
                        primaryData.put("db_id",distributorStoreId);
                        primaryData.put("page",1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.e("thana filter req", "onItemSelected: "+primaryData );

                    nowCaling = WITH_MARKET_ID;
                    initialPageIndex = 1;


                    basicFunction.getResponceData(URL.OutletList, String.valueOf(primaryData),401);

                }

         /*        else {

                        outletList = ds.getOutletData(thanaId, "no");
                        outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);

                        recyclerView.setLayoutManager(new LinearLayoutManager(OutletList.this));
                        if (outlets != null && outlets.size() > 0) {
                            outletListAdapter.setOutletNames(outlets);
                            recyclerView.setAdapter(outletListAdapter);
                        } else {
                            recyclerView.setAdapter(null);
                        }
                    }
*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }



        });



        /*outletListAdapter.setOnItemClickListener(new OutletListAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                // initialize in viewholder class ---> itemview.setOnclik listener  line edit.setOnclick
            }

            @Override
            public void onEditClick(int position) {
                Toast.makeText(OutletList.this,"Outlet Id"+outletList.get(Tables.OUTLETS_ID).get(position),Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(OutletList.this,CreateOutlet.class);
                intent.putExtra("outlet_id_byClick",outletList.get(Tables.OUTLETS_ID).get(position));
                startActivity(intent);

            }

            @Override
            public void onViewClick(int position) {
                Log.e("view btn click", "onViewClick: "+position );

                Intent intent = new Intent(OutletList.this,OutletView.class);
                intent.putExtra("outlet_id",outletList.get(Tables.OUTLETS_ID).get(position));
                startActivity(intent);


            }
        });*/

        homeBtn = findViewById(R.id.home);
        backBtn = findViewById(R.id.back);

        homeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( OutletList.this, Dashboard.class));
                finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent( OutletList.this, Tools.class));
                finish();
            }
        });

        createOutletBtn = findViewById(R.id.create_outlet_btn);
        createOutletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(OutletList.this, CreateOutlet.class));
                finish();
            }
        });


        /*recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                //Toast.makeText(OutletList.this,""+position,Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(OutletList.this,CreateOutlet.class);
                intent.putExtra("outlet_id_byClick",outletList.get(Tables.OUTLETS_ID).get(position));
                startActivity(intent);

                if (view.getId() == R.id.view_outlet_item){
                    Log.e("Recycle button check", "onClick: "+"View Buton it is" );
                }else {
                    Log.e("Recycle button check", "onClick: "+"not identified it is"+view.getId() );
            }


            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));*/

    }

    private void setSpinnerAdapter(HashMap<String,ArrayList<String> >dataMap, Spinner spinner, String idKey, String nameKey) {
        if (dataMap != null && dataMap.size()>0) {

            /*dataMap.get(idKey).add(0, "x0x");
            dataMap.get(nameKey).add(0, "All");*/

            //add adapter to market spinner

            final SpinnerAdapter marketDataAdapter = new SpinnerAdapter(OutletList.this, R.layout
                    .spinner_item, dataMap.get(nameKey));
            spinner.setAdapter(marketDataAdapter);
        }else {
            spinner.setAdapter(null);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();

        /*thanaSpinner.setSelection(thanSpinnerPosition);
        marketSpinner.setSelection(marketSpinnerPosition);
        if (thanSpinnerPosition == 0 && marketSpinnerPosition ==0){
            outletList = ds.getOutletData("no","no");
            outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);
        }else if (thanSpinnerPosition > 0 && marketSpinnerPosition ==0){
            outletList = ds.getOutletData(thanas.get(Tables.THANA_TH_id).get(thanSpinnerPosition),"no");
            outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);

        }else if (thanSpinnerPosition > 0 && marketSpinnerPosition > 0){
            outletList = ds.getOutletData(thanas.get(Tables.THANA_TH_id).get(thanSpinnerPosition),markets.get(Tables.MARKETS_market_id).get(marketSpinnerPosition));
            outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);

        }

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        if (outlets!=null && outlets.size()>0) {
            outletListAdapter.setOutletNames(outlets);
            recyclerView.setAdapter(outletListAdapter);
        }*/
    }

    @Override
    public void OnServerResponce(JSONObject jsonObject, final int i) {

        Log.e("response code....", "OnServerResponce: "+i );


        Log.e("json response", "OnServerResponce: "+jsonObject+"  size.."+jsonObject.length() );

        if (i == 401) {
            isLoading = false;

            outletListAdapter = new OutletListAdapter();
            outletList = new HashMap<>();
            outletIdList = new ArrayList<>();
            outletNameList = new ArrayList<>();
            outlets = new ArrayList<>();
            outletMarketIdList = new ArrayList<>();
            outletThanaIdList = new ArrayList<>();
            try {
                for (int j = 0; j < jsonObject.getJSONArray("outlets").length(); j++) {
                    if (!jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_name").equals("")) {
                        outletIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_id"));
                        outletNameList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_name"));
                        outletThanaIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("thana_id"));
                        outletMarketIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("market_id"));
                    }

                }

                outletList.put(Tables.OUTLETS_ID, outletIdList);
                outletList.put(Tables.OUTLETS_OUTLET_NAME, outletNameList);
                outletList.put(Tables.OUTLETS_THANA_ID, outletThanaIdList);
                outletList.put(Tables.OUTLETS_MARKET_ID, outletMarketIdList);
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("jsonx",e.getMessage());
            }

            try {
                Log.e("json response", "OnServerResponce:" + jsonObject.getJSONArray("outlets").length());
            } catch (JSONException e) {
                e.printStackTrace();

            }


            outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);

        /*if (i == 402) {
            linManager = (LinearLayoutManager) recyclerView.getLayoutManager();
            recyclerView.setLayoutManager(linManager);
        }*/

            if (outlets != null && outlets.size() > 0) {
                recyclerView.setLayoutManager(new LinearLayoutManager(OutletList.this));
                outletListAdapter.setOutletNames(outletNameList);
                recyclerView.setAdapter(outletListAdapter);
                if (rcvIntent>0) {
                    recyclerView.scrollToPosition(getPosition(outletIdList, StaticFlags.OUTLET_ID_FOR_SCROLL));
                }
                Log.e("50 data setes", "OnServerResponce: Adapter seted...");
            } else {
                recyclerView.setAdapter(null);
            }

            outletListAdapter.setOnItemClickListener(new OutletListAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(int position) {
                    // initialize in viewholder class ---> itemview.setOnclik listener  line edit.setOnclick
                }

                @Override
                public void onEditClick(int position) {
                   // Toast.makeText(OutletList.this,"Outlet Id"+outletList.get(Tables.OUTLETS_ID).get(position),Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(OutletList.this,CreateOutlet.class);
                    intent.putExtra("outlet_id_byClick",outletList.get(Tables.OUTLETS_ID).get(position));
                    startActivity(intent);
                    finish();

                }

                @Override
                public void onViewClick(int position) {
                    Log.e("view btn click", "onViewClick: "+position );

                    Intent intent = new Intent(OutletList.this,OutletView.class);
                    intent.putExtra("outlet_id",outletList.get(Tables.OUTLETS_ID).get(position));
                    startActivity(intent);
                    finish();


                }
            });



        }

        else if (i == 402){



            try {
                for (int j = 0; j < jsonObject.getJSONArray("outlets").length(); j++) {
                    if (!jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_name").equals("")) {
                        outletIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_id"));
                        outletNameList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("outlet_name"));
                        outletThanaIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("thana_id"));
                        outletMarketIdList.add(jsonObject.getJSONArray("outlets").getJSONObject(j).getString("market_id"));
                    }

                }

                outletList.put(Tables.OUTLETS_ID, outletIdList);
                outletList.put(Tables.OUTLETS_OUTLET_NAME, outletNameList);
                outletList.put(Tables.OUTLETS_THANA_ID, outletThanaIdList);
                outletList.put(Tables.OUTLETS_MARKET_ID, outletMarketIdList);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                Log.e("json response", "OnServerResponce:" + jsonObject.getJSONArray("outlets").length());
            } catch (JSONException e) {
                e.printStackTrace();
                Log.e("json response",e.getMessage());
            }

            outlets = outletList.get(Tables.OUTLETS_OUTLET_NAME);


            if (outlets != null && outlets.size() > 0) {


                recyclerView.getAdapter().notifyDataSetChanged();

                Log.e("50 data setes", "OnServerResponce: Adapter seted...");
            }




        }

        final LinearLayoutManager linManager = (LinearLayoutManager) recyclerView.getLayoutManager();


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                //Log.e("entered scrolled.....", "onScrolled: "+"On Scroll.." );


                if (!isLoading && linManager.getItemCount() - THREAT_SHOT== linManager.findLastVisibleItemPosition()){
                    Log.e("entr scrolled..0n 402", "onScrolled: "+"On Scroll.." );
                    initialPageIndex++;
                    loadMoreData(initialPageIndex);

                  //  Toast.makeText(OutletList.this," "+initialPageIndex,Toast.LENGTH_SHORT).show();

                    Log.e("last position", "onScrolled: "+linManager.findLastVisibleItemPosition() );
                }

                isLoading=true;
            }

        });

    }

    @Override
    public void OnConnetivityError() {
        Toast.makeText(OutletList.this,"No Internet Connection",Toast.LENGTH_SHORT).show();

    }

    public int getPosition(ArrayList<String>data,String id){

        int position = data.indexOf(id);

        return  position;
    }

    public void loadMoreData(int page){
        isLoading= true;
        Log.e("page number......", ".......>>loadMoreData: "+page );
        JSONObject primaryData = new JSONObject();
        try {
            primaryData.put("mac",basicFunction.getPreference("mac"));
            primaryData.put("sales_person_id",basicFunction.getPreference("sales_person_id"));
            primaryData.put("db_id", distributorStoreId);
            if (nowCaling.equals(WITH_MARKET_ID)){
                primaryData.put("market_id",marketId);

            }else if (nowCaling.equals(WITH_THANA_ID)){
                primaryData.put("thana_id",thanaId);
            }
            primaryData.put("page",page);
        } catch (JSONException e) {
            e.printStackTrace();
        }


        basicFunction.getResponceData(URL.OutletList, String.valueOf(primaryData),402);

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // TODO Auto-generated method stub
        if(keyCode== KeyEvent.KEYCODE_BACK)
        {
            StaticFlags.OUTLET_ID_FOR_SCROLL = null;
            StaticFlags.OUTLET_MARKET_ID = null;
            StaticFlags.OUTLET_THANA_ID = null;
            StaticFlags.OUTLET_MARKET_SPINNER_POSITION = 0;
            StaticFlags.OUTLET_SCROL_POSITION = 0;
            StaticFlags.OUTLET_THANA_SPINNER_POSITION = 0;
            startActivity(new Intent( OutletList.this, Tools.class));
            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);

    }
}
