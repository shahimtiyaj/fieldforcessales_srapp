package com.srapp.Adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.srapp.Db_Actions.Data_Source;
import com.srapp.EditDepositListActivity_New;
import com.srapp.Edit_Deposit_PostActivity;
import com.srapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class AdapterForEditDepositList extends BaseAdapter {

	// Declare Variables
	Context context;
	String SO_ID;
	Data_Source db;

	ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();
	public AdapterForEditDepositList(Context context, ArrayList<HashMap<String, String>> arraylistContent) {
		this.context = context;
		itemListContent = arraylistContent;
	}

	@Override
	public int getCount() {
		return itemListContent.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
//		return position;
	}

	
	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {

		db=new Data_Source(context);
		
		View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.edit_deposit_row, null);
		TextView Datetxt = (TextView)view2.findViewById(R.id.Datetxt);
		TextView Particulerstxt = (TextView)view2.findViewById(R.id.Particulerstxt);
		TextView Saletxt = (TextView)view2.findViewById(R.id.Saletxt);
		TextView Deposittxt = (TextView)view2.findViewById(R.id.Deposittxt);
		TextView txtType = (TextView)view2.findViewById(R.id.txtType);
		ImageView ActionBtn = (ImageView)view2.findViewById(R.id.ActionBtn);
		ImageView DeleteBtn = (ImageView)view2.findViewById(R.id.DeleteBtn);

		
			
		
		HashMap<String, String> mapContent = new HashMap<String, String>();
		mapContent = itemListContent.get(position);
		Datetxt.setText( mapContent.get("date"));
		Particulerstxt.setText( mapContent.get("particulars"));
		Saletxt.setText(mapContent.get("sales"));
		Deposittxt.setText(mapContent.get("deposits"));
		txtType.setText(mapContent.get("type"));
		final String Id = mapContent.get("Id");
		final String deposit_id = mapContent.get("deposit_id");
		final String payment_id = mapContent.get("payment_id");
		final String Type = mapContent.get("type");
		final String typeint = mapContent.get("typeint");

//		double payment_amount=roundTwoDecimals(Double.parseDouble(mapContent.get("payment_amount")));
		
		
	
		
		
		TextView txtSaleTotal=(TextView)((Activity) context).findViewById(R.id.txtSaleTotal);
		txtSaleTotal.setText("0.0");
		float sum=0;
		for(int i=0;i<itemListContent.size();i++)
		{
			sum=sum+Float.parseFloat(itemListContent.get(i).get("deposits"));
		}
		
		sum=(float) roundTwoDecimals(sum);
		txtSaleTotal.setText(String.valueOf(sum));
		
		ActionBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Intent idn = new Intent(context, Edit_Deposit_PostActivity.class);
				idn.putExtra("Id", Id);
				idn.putExtra("Type", Type);
				idn.putExtra("typeint", typeint);
				idn.putExtra("deposit_id", deposit_id);
				idn.putExtra("position", position);
				context.startActivity(idn);
				((Activity) context).finish();
				
			}
		});

		final HashMap<String, String> finalMapContent = mapContent;
		DeleteBtn.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {

				AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
				builder1.setMessage("Do you want delete this deposit?");
				builder1.setCancelable(true);

				builder1.setPositiveButton(
						"Yes",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {

								double depositBalance=0.0;
								String	DepositQuery = "SELECT deposit_balance_amount FROM deposit_balance";
								Cursor c2 =db.rawQuery(DepositQuery);
								if(c2!=null)
								{
									if(c2.moveToFirst())
									{
										do{
											depositBalance =c2.getDouble(0);

										}while(c2.moveToNext());
									}
								}


								double depositBalance2=0.0;
								String	DepositQuery2 = "SELECT deposit_amount FROM deposits " +
										"where deposit_id='"+itemListContent.get(position).get
										("deposit_id")+"'"+"and instrument_type_id=1";
								Cursor c1 =db.rawQuery(DepositQuery2);
								if(c1!=null)
								{
									if(c1.moveToFirst())
									{
										do{
											depositBalance2 =c1.getDouble(0);

										}while(c1.moveToNext());
									}
								}

								double Total = depositBalance+depositBalance2;

								String UpdateDepositBalanceQuery="UPDATE deposit_balance SET deposit_balance_amount='"+Total+"' WHERE _id=1";
								Log.e("----", "-UpdateDepositBalanceQuery-"+UpdateDepositBalanceQuery);
								db.excQuery(UpdateDepositBalanceQuery);

								String DepositId = itemListContent.get(position).get("deposit_id");
								String Sub_DepositIdt = DepositId.substring(0,1);
								Log.e("--------","++Sub_DepositIdt+++"+Sub_DepositIdt);
								if(Sub_DepositIdt.equalsIgnoreCase("D"))
								{
									//db.deleteDepodit("deposits",itemListContent.get(position).get("deposit_id"));
									db.excQuery("UPDATE instrument_no SET isUsed=0,isPushed=0 WHERE " +
											"payment_id='"+payment_id+"'");



								}
								else
								{
								//	db.deleteDepodit("deposits",itemListContent.get(position).get("deposit_id"));
									db.excQuery("UPDATE instrument_no SET isUsed=0,isPushed=0 WHERE " +
											"payment_id='"+payment_id+"'");


									HashMap<String,String> map=new HashMap<String,String>();
									map.put("deposit_id", itemListContent.get(position).get("deposit_id"));
									map.put("isPushed", "0");
									db.InsertTable(map,"deposit_delete");


								}





									Double.parseDouble(finalMapContent.get("deposits"));
								Intent idn = new Intent(context, EditDepositListActivity_New.class);
								context.startActivity(idn);
								((Activity) context).finish();

							}
						});

				builder1.setNegativeButton(
						"No",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

				AlertDialog alert11 = builder1.create();
				alert11.show();
			}
		});
	

		return view2;
	}

		

   


	
	 double roundTwoDecimals(double d)
	 {
	     DecimalFormat twoDForm = new DecimalFormat("#.######");
	     return Double.valueOf(twoDForm.format(d));
	 }	





}
