package com.srapp.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.srapp.R;
import com.srapp.TagEditText;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterForSerialShow extends BaseAdapter {

    // Declare Variables
    Activity context;


    ArrayList<HashMap<String, String>> BonusItemList = new ArrayList<HashMap<String, String>>();
    public AdapterForSerialShow(Activity context, ArrayList<HashMap<String, String>> arraylistContent) {
        this.context = context;
        BonusItemList = arraylistContent;
    }

    @Override
    public int getCount() {
        return BonusItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
//		return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.sirial_input_row, null);
        TextView txtName = (TextView)view2.findViewById(R.id.product_name);
        final AutoCompleteTextView sirial = view2.findViewById(R.id.sirial);
        final TagEditText tagEditText =  view2.findViewById(R.id.tags);

        txtName.setText(BonusItemList.get(position).get("product_name"));
        tagEditText.setText(BonusItemList.get(position).get("serial_number"));
        sirial.setVisibility(View.GONE);




        return view2;
    }








}
