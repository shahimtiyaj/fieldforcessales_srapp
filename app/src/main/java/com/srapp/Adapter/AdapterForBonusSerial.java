package com.srapp.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.EdittextListener;
import com.srapp.ProductSales;
import com.srapp.ProductSalesForMemo;
import com.srapp.R;
import com.srapp.SerialNumberEntryForBonus;
import com.srapp.TagEditText;
import com.srapp.TempData;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterForBonusSerial extends BaseAdapter  {

    // Declare Variables
    Activity context;
    Data_Source db;
    Button next;
    ArrayList<HashMap<String, String>> BonusItemList = new ArrayList<HashMap<String, String>>();
    public AdapterForBonusSerial(final Activity context, ArrayList<HashMap<String, String>> arraylistContent) {
        this.context = context;
        BonusItemList = arraylistContent;
        db = new Data_Source(context);
        Log.e("AdapterForBonusSerial","AdapterForBonusSerial");
        next = (Button)((Activity) context).findViewById(R.id.save_serial);
        next.setEnabled(true);
        next.setText("next12");

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("clicl","click");
                TempData.TotalBonusProductList.clear();
                TempData.TotalBonusProductList.addAll(BonusItemList);
                TempData.BonusProductQuantity.clear();
                TempData.BonusProductQuantity.addAll(BonusItemList);

                if (SirialValidation()) {

                    Intent i = new Intent(context, ProductSalesForMemo.class);
                    ((Activity) context).startActivity(i);
                }else {

                    Toast.makeText(context,"Please Input Serial Number Correctly",Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public int getCount() {
        return BonusItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
//		return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int listposition, View convertView, ViewGroup parent) {


        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.sirial_input_row, null);


        TextView txtName = (TextView)view2.findViewById(R.id.product_name);
        LinearLayout main = (LinearLayout)view2.findViewById(R.id.main);
        final AutoCompleteTextView sirial = view2.findViewById(R.id.sirial);
        final TagEditText tagEditText =  view2.findViewById(R.id.tags);
        if (TempData.editMemo.equalsIgnoreCase("true")) {
            if (TempData.SALE_STATE==TempData.MEMO_EDITING)
            BonusItemList.get(listposition).put("sirial_number",BonusItemList.get(listposition).get("sirial_number")+getPreference("serial_number"+BonusItemList.get(listposition).get("product_id")));
        }

        if (!isSerialMaintain(BonusItemList.get(listposition).get("product_id"))){

            main.setVisibility(View.GONE);
        }

        tagEditText.setText(BonusItemList.get(listposition).get("sirial_number"));
        tagEditText.setLeftTextListener(new EdittextListener() {
            @Override
            public void setleftText(String txt) {
                BonusItemList.get(listposition).put("sirial_number",txt);
                UpdateDbForSeliarPB(txt,BonusItemList.get(listposition).get("product_id"), TempData.OutletID);

            }

            @Override
            public void getDeletedText(String txt) {
                Log.e("deleted_sirial",txt);
                SerialNumberEntryForBonus.serial.get(BonusItemList.get(listposition).get("product_id")).add(txt);
                UpdateDbForSeliarPS(txt,"0");
            }
        });



        Log.e("","itemListContent"+BonusItemList.toString());

        HashMap<String, String> mapContent = new HashMap<String, String>();
        mapContent = BonusItemList.get(listposition);
        Log.e("","bonus_name"+mapContent.get("bonus_name"));
        txtName.setText(mapContent.get("product_name"));
        ArrayAdapter dataAdapter = new ArrayAdapter<String>(context, R.layout.outlet_dw, SerialNumberEntryForBonus.serial.get(BonusItemList.get(listposition).get("product_id")));
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        sirial.setAdapter(dataAdapter);
        sirial.setThreshold(1);
        sirial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sirial.showDropDown();

            }
        });

        final ArrayList<String> finalStrings = SerialNumberEntryForBonus.serial.get(BonusItemList.get(listposition).get("product_id"));
        final HashMap<String, String> finalMapContent = mapContent;
        sirial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String sirialtext = finalStrings.get(position)+",";
                BonusItemList.get(listposition).put("sirial_number",tagEditText.getText()+ sirialtext);
                tagEditText.setText(tagEditText.getText()+ sirialtext);
                UpdateDbForSeliarPB(tagEditText.getText().toString().trim(),finalMapContent.get("product_id"), TempData.OutletID);
                UpdateDbForSeliarPS(finalStrings.get(position),"1");
                sirial.setText("");
                finalStrings.remove(position);
                ArrayAdapter dataAdapter = new ArrayAdapter<String>(context, R.layout.outlet_dw, SerialNumberEntryForBonus.serial.get(BonusItemList.get(listposition).get("product_id")));
                dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                sirial.setAdapter(dataAdapter);
                sirial.setThreshold(1);
                sirial.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sirial.showDropDown();

                    }
                });
            }
        });


        return view2;
    }

    private boolean SirialValidation() {
        boolean istrue= true;
        for (int i=0 ; i< TempData.TotalBonusProductList.size(); i++){
            if (isSerialMaintain(TempData.TotalBonusProductList.get(i).get("product_id"))){
                double serialQuantity = TempData.TotalBonusProductList.get(i).get("sirial_number").split(",").length;

                if (serialQuantity==Double.parseDouble(TempData.TotalBonusProductList.get(i).get("quantity"))){

                    istrue= true;
                }else {

                    istrue = false;
                }

                if (!istrue){
                    return istrue;
                }


            }
        }

        return istrue;
    }

    private boolean isSerialMaintain(String product_id) {
        Cursor c = db.rawQuery("select maintain_serial from product where product_id='" + product_id + "'");
        Log.e("ttt","select maintain_serial from product where product_id='" + product_id + "'");
        c.moveToFirst();
        if (c!=null && c.getCount()>0){

            if (c.getInt(0)==1){
                return true;
            }else {
                return false;
            }
        }
        return false;
    }

    private void UpdateDbForSeliarPB(String serial, String product_id, String outletID) {
        Log.e("UpdateQuery","update product_boolean set serial_number='"+serial+"' where product_id = '"+product_id+"' and outlet_id='"+outletID+"'");

       // db.excQuery("update product_boolean set serial_number='"+serial+"' where product_id = '"+product_id+"' and outlet_id='"+outletID+"'");


    }private void UpdateDbForSeliarPS(String serial, String is_used) {

       // db.excQuery("update product_serials set is_used='"+is_used+"' where serial_no = '"+serial+"'");


    }

    public String getPreference(String key)
    {
        String value="";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "");

        return value;

    }



}
