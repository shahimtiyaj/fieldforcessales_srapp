package com.srapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;


import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.Db_Actions.Temp;
import com.srapp.R;
import com.srapp.TempData;
import com.srapp.Util.Parent;
import com.srapp.Util.ParentActivity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.srapp.Db_Actions.Tables.ORDER_DETAILS_order_number;
import static com.srapp.Db_Actions.Tables.TABLE_NAME_ORDER_DETAILS;

public class SalesOrderDetailsAdaperForMemo extends BaseAdapter {

    // Declare Variables
    Context context;
    String SO_ID;
    ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();
    //	ArrayList<HashMap<String, String>> itemListData = new ArrayList<HashMap<String, String>>();
    //HomeProjectListAdapter featuredProjectsListAdapter;
    ArrayList<HashMap<String, String>> returnContent = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> Bonus = new ArrayList<HashMap<String, String>>();
    ArrayList<Double> num = new ArrayList<Double>();
    String price = "";
    String TargetCustomer = "0";
    String InstietuteID = "";
    int open_price=0;
    ArrayList<HashMap<String, String>> BonusItemList = new ArrayList<HashMap<String, String>>();
    Double discountp = 0.0;
    TextView NameTv, TPrice, txtTotal,txtdiscount, subt,vattxt,vattv;
    EditText QuantityEd, UPrice;
    ImageView ActionBtn;
    String product_name = "", Quantity = "", product_id = "", has_combination = "", general_price = "", is_bonus="";
    Double sum = 0.0,vat=0.0;
    String outlet_ID = "";
    String date, memodate;
    Data_Source db;
    String memoUpdate;

    public SalesOrderDetailsAdaperForMemo(Context context, ArrayList<HashMap<String, String>> arraylistContent, String _SO_ID, String outlet_ID, String memoUpdate) {
        this.context = context;
        this.outlet_ID = outlet_ID;
        itemListContent = arraylistContent;
        this.memoUpdate=memoUpdate;
        db = new Data_Source(context);
        SO_ID = _SO_ID;


        if (TempData.editMemo.equalsIgnoreCase("true")) {
            date = TempData.MemoDate;
        } else {
            date = getCurrentDate();
        }


        Log.e("Date:", String.valueOf(date));
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        Date datevalue = null;
        try {
            datevalue = simpleDateFormat.parse(String.valueOf(date));

            simpleDateFormat.applyPattern(pattern);
            memodate = simpleDateFormat.format(datevalue);

            Log.e("MemoDate:", String.valueOf(memodate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        InstietuteID = TempData.InstituteID;
        Log.e("Target:" + TargetCustomer, "Institude: " + InstietuteID);
        sum = 0.0;
        txtTotal = (TextView) ((Activity) context).findViewById(R.id.total_price);
        txtdiscount = (TextView) ((Activity) context).findViewById(R.id.discount);
        subt = (TextView) ((Activity) context).findViewById(R.id.sub_total);
        vattxt = (TextView) ((Activity) context).findViewById(R.id.vat);




        if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
            UpdatePrice();
        else {
            UpdateDistPrice();
        }
    }

    @Override
    public int getCount() {
        return itemListContent.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
//		return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        returnContent = itemListContent;

        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_sales_item, null);


        NameTv = (TextView) view2.findViewById(R.id.product_name);
        UPrice = (EditText) view2.findViewById(R.id.price);
        vattv = view2.findViewById(R.id.vat);
        TPrice = (TextView) view2.findViewById(R.id.total_price);
        QuantityEd = (EditText) view2.findViewById(R.id.quantity);
        ActionBtn = (ImageView) view2.findViewById(R.id.dlt_product_sales_item);

        HashMap<String, String> mapContent = new HashMap<String, String>();
        mapContent = itemListContent.get(position);
        product_name = mapContent.get("product_name");
        Quantity = mapContent.get("quantity");
        product_id = mapContent.get("product_id");
        QuantityEd.setEnabled(true);
        String p_id=db.getProductId(product_id);
        Log.e("product_id_before", "....." + p_id);

        vattv.setText(mapContent.get("vatt")+"%");
        Log.e("vat",mapContent.get("vatt")+"%");
        has_combination = mapContent.get("has_combination");
        general_price = mapContent.get("general_price");
        is_bonus = mapContent.get("is_bonus");

        Log.e("general_price", "....." + general_price);
        Log.e("is_bonus_before", "....." + is_bonus);

        try {
            if (is_bonus.equalsIgnoreCase("0") || p_id.equals("0")) { // here is_bonus=0, open price
                Log.e("is_bonus_editable", mapContent.get("is_bonus")); //When open price =0 editable mood enable
                Log.e("is_bonus_editable","enable"); //When open price =0 editable mood enable
                UPrice.setEnabled(true);
            }

            else {
                Log.e("is_bonus_editable", mapContent.get("is_bonus"));
                Log.e("is_bonus_editable","disable");
                UPrice.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!Quantity.equals("")) {

            if (general_price == null) {
                NameTv.setText(product_name);
                QuantityEd.setText(Quantity);

            } else {
                Double _generalPrice = Double.parseDouble(general_price);
                Double _Quantity = Double.parseDouble(Quantity);
                Double T_Price = _generalPrice * _Quantity;

                NameTv.setText(product_name);
                if (Quantity.equalsIgnoreCase("0")) {
                    QuantityEd.setText("");
                    UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("0.00"))));

                } else {
                    Log.e("UPrice product_id: ", "" + getPreference("memo" + itemListContent.get(position).get("product_id")));
                    Log.e("SalesUPrice: ", TempData.SalesUPricePosition + "00");

                    if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("0")) {
                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(general_price))));
                        Log.e("SalesUPrice_: ", TempData.SalesUPricePosition + "00");

                    } else if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("")) {
                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(general_price))));
                        Log.e("SalesUPrice__: ", TempData.SalesUPricePosition + "00");

                    } else {
                        //UpdatePrice();

                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(getPreference("memo" + itemListContent.get(position).get("product_id"))))));
                        T_Price = Double.parseDouble(getPreference("memo" + itemListContent.get(position).get("product_id"))) * _Quantity;
                        Log.e("SalesUPrice___: ", TempData.SalesUPricePosition + "00");

                    }

                    QuantityEd.setText(Quantity);
                }

                Log.e("SCROLLING PRICE:", "......." + T_Price);
                Log.e("Valuetttt :", getPreference("memo" + mapContent.get("product_id")) + "   n");
                //TPrice.setText(""+T_Price);

                TPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("" + T_Price))));

               if (TempData.editMemo.equalsIgnoreCase("true")) { // if memo in edit moad then & open  price applied then open price will show from memo
                    Log.e("Valuettt", getPreference("memo" + mapContent.get("product_id")));

                    if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("0")) {
                        Log.e("Value", getPreference("memo" + mapContent.get("product_id")));
                        UPrice.setText(String.valueOf(((Parent) context).roundTwoDecimals(Double.parseDouble(general_price))));
                        Log.e("SalesUPrice_last: ", TempData.SalesUPricePosition + "00");

                    } else {

                        /* if ()*/
                        Log.e("SalesUPrice_befor: ", TempData.SalesUPricePosition + "00");

                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(getPreference("memo" + mapContent.get("product_id"))))));
                    }


                }

            }

        }

        // TODO Auto-generated method stub
        QuantityEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                // UPrice.setText("");
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Log.e("AFTER:", s.toString());

              /* db.excQuery("UPDATE product_boolean SET boolean = " + "'" + "false" + "'" + ", quantity=" + "'" + "" + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'");
                Log.e("ProductID_FromPosition", itemListContent.get(position).get("product_id"));
                savePreference(itemListContent.get(position).get("product_id"), "0.0");
                savePreference("memo" + itemListContent.get(position).get("product_id"), "0");
                //itemListContent.remove(position);
                notifyDataSetChanged();
                UpdatePrice();*/

                if (s.toString().length() == 0) {
                    itemListContent.get(position).put("quantity", "0.00");
                    String query = "UPDATE product_boolean SET quantity = " + "'" + s.toString() + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'";
                    db.excQuery(query);
                    Log.e("QUERY", query);
                    if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
                        UpdatePrice();
                    else {
                        UpdateDistPrice();                    }
                } else {
                    if (isDouble(s.toString())) {


                        String QUANTITY = "";

                        Log.e("quantity..........", "" + QUANTITY);
                        Log.e("QuantityEd quantity: ", "" + QuantityEd.getText().toString());
                        //QuantityEd.setText("0");
                        QUANTITY = s.toString();
                        String query = "UPDATE product_boolean SET quantity = " + "'" + QUANTITY + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'";
                        Log.e("QUERY", query);
                        db.excQuery(query);
                        itemListContent.get(position).put("quantity", QUANTITY);


                        showLimitExeednotification(Double.parseDouble(QUANTITY), itemListContent.get(position).get("product_id"));
                        if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
                            UpdatePrice();
                        else {
                            UpdateDistPrice();                        }
                    } else {
                        Toast.makeText(context, "Please enter correct number format!", Toast.LENGTH_LONG).show();
                        QuantityEd.setText("");

                    }
                }
            }
        });


        // TODO Auto-generated method stub
        UPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {

                if (s.toString().equals(".")) {


                }


                // TODO Auto-generated method stub
                Log.e("AFTER:", s.toString());
                Log.e("Position Logg:", "" + position);


                //assign position string value to a temp variable
                TempData.SalesUPricePosition = "" + position;

                TempData.SalesUPrice = "" + s.toString();
                savePreference("memo" + itemListContent.get(position).get("product_id"), s.toString());
                Log.e("product_id_Position", itemListContent.get(position).get("product_id"));
                Log.e("product_id_Sales", getPreference(itemListContent.get(position).get("product_id")));

                if (s.toString().length() == 0) {
                    if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
                        UpdatePrice();
                    else {
                        UpdateDistPrice();
                    }
                } else {
                    if (isDouble(s.toString())) {
                        HashMap<String, String> mapContent = new HashMap<String, String>();
                        mapContent = itemListContent.get(position);
                        //product_name= mapContent.get("product_name");
                        String Quantity = mapContent.get("quantity");

                        double giventQty = Double.parseDouble(Quantity);
                        double stockqty = 0;

                        String QUANTITY = "";

                        QUANTITY = "" + giventQty;

                        Log.e("quantity..........", "" + QUANTITY);
                        if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
                            UpdatePrice();
                        else {
                            UpdateDistPrice();                        }
                    } else {
                        Toast.makeText(context, "Please enter correct number format!", Toast.LENGTH_LONG).show();
                        QuantityEd.setText("");

                    }
                }
            }
        });

        ActionBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Log.e("positionclick", position + " size " + itemListContent.size());
                db.excQuery("UPDATE product_boolean SET boolean = " + "'" + "false" + "'" + ", quantity=" + "'" + "" + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'");
                Toast.makeText(context, "Product has deleted successfully", Toast.LENGTH_LONG).show();

                Log.e("ProductID_FromPosition", itemListContent.get(position).get("product_id"));

                savePreference(itemListContent.get(position).get("product_id"), "0.0");
                savePreference("memo" + itemListContent.get(position).get("product_id"), "0");
                itemListContent.remove(position);
                notifyDataSetChanged();
                if (!TempData.OutletCatagoryID.equalsIgnoreCase("17"))
                    UpdatePrice();
                else {
                    UpdateDistPrice();
                }

            }
        });


        return view2;
    }

    public boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void UpdatePrice() {
        ArrayList<ArrayList<String>> ArrCombine = new ArrayList<ArrayList<String>>();
        ArrayList<String> slaveID = new ArrayList<String>();
        ArrayList<String> generalID = new ArrayList<String>();
        ArrayList<String> combineID = new ArrayList<String>();
        ArrayList<String> inList = new ArrayList<String>();
        ArrayList<String> quantity = new ArrayList<String>();

        for (int i = 0; i < itemListContent.size(); i++) {
            inList.add(itemListContent.get(i).get("product_id"));
            Log.e("IDDDDDDDDDDDDDDDDDD:", itemListContent.get(i).get("product_id"));
        }
        for (int i = 0; i < itemListContent.size(); i++) {
            quantity.add(itemListContent.get(i).get("quantity"));
            savePreference(itemListContent.get(i).get("product_id"), itemListContent.get(i).get("quantity"));

            Log.e("quantity:", itemListContent.get(i).get("quantity"));
        }

        HashMap<String, String> priceMap = new HashMap<String, String>();

        String byPassNow = "YES";
        if (!TargetCustomer.equalsIgnoreCase("0")) {
            //for(int i=0;i<inList.size();i++)
            //priceMap.put(inList.get(i), NGO_price(inList.get(i),quantity.get(i)));
        }

        if (TargetCustomer.equalsIgnoreCase("0") || byPassNow == "YES") {
            InstietuteID = "0";

            for (int i = 0; i < inList.size(); i++) {
                Log.e("PRISING TYPE: ", inList.get(i) + ": " + CheckPriceType(inList.get(i)));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("COMBINE"))
                    combineID.add(inList.get(i));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("SLAVE"))
                    slaveID.add(inList.get(i));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("GENERAL"))
                    generalID.add(inList.get(i));
            }


            Log.e("COMBINE SIZE:", "" + combineID.size());
            String combination_id = "";
            for (int i = 0; i < combineID.size(); i++) {
                Cursor cursor = db.rawQuery("SELECT combination_id FROM product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + combineID.get(i) + "' AND effective_date<='" + memodate + "')");


                Log.e("OUTER_QUERY:", "SELECT combination_id FROM product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0");
                Log.e("OUTER_QUERY_COUNT", "" + cursor.getCount());
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            combination_id = cursor.getString(0);
                            Cursor cursor1 = db.rawQuery("SELECT * FROM product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY:", "SELECT * FROM product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY_COUNT:", "" + cursor1.getCount());
                            if (cursor1 != null) {
                                if (cursor1.moveToFirst()) {
                                    ArrayList<String> combinationList = new ArrayList<String>();
                                    do {
                                        //.....combine id added  to list.......
                                        if (!combinationList.contains(cursor1.getString(1)))
                                            combinationList.add(cursor1.getString(1));
                                        Log.e("CombineID", cursor1.getString(1));

                                    } while (cursor1.moveToNext());

                                    if (ArrCombine.size() == 0) {
                                        ArrCombine.add(combinationList);
                                        Log.e("added to combine list", "added");
                                    } else {
                                        if (ArrCombine.contains(combinationList))
                                            Log.e("Duplicate", "True");
                                        else
                                            ArrCombine.add(combinationList);
                                    }
                                }
                            }


                        } while (cursor.moveToNext());
                    }
                }
            }

            Log.e("COMBINE ARRAY:", ArrCombine.toString());
            Log.e("ArrCombine.size()", ArrCombine.size() + "");
            Log.e("QUANTITY ARRAY", quantity.toString());

            for (int i = 0; i < ArrCombine.size(); i++) {
                double QUANTITY = 0;

                ArrayList<String> arrCheck = ArrCombine.get(i);
                Log.e("ArraySize:", arrCheck.size() + "");
                String comProduct = combineID.get(i);
                int index = inList.indexOf(combineID.get(i));

                for (int j = 0; j < arrCheck.size(); j++) {

                    Log.e(".............", ".............");
                    Log.e("COMBINE ID:", arrCheck.get(j));
                    if (!getPreference(arrCheck.get(j)).equalsIgnoreCase(""))
                        QUANTITY = QUANTITY + Double.parseDouble(getPreference(arrCheck.get(j)));

                }
                Log.e("Given....QUANTITY", "" + QUANTITY);
                Log.e("INDi....inList", "" + inList.toString());
                Log.e("INDi....arrCheck", "" + arrCheck.toString());
                /// sql query to find combined quantity

                Double Combined_Quantity = 0.0;

                Cursor cursor = db.rawQuery("SELECT combination_id,min_quantity FROM product_combinations WHERE product_id= '" + arrCheck.get(0) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(0) + "' AND effective_date<='" + memodate + "')");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
//                            Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            if (QUANTITY >= Double.parseDouble(cursor.getString(1))) {
                                Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            }
                        } while (cursor.moveToNext());
                    }
                }

                Log.e("Combined_Quantity: ", "" + Combined_Quantity);

//
                if (QUANTITY >= Combined_Quantity) {


                    Log.e("QUANTITY111", "" + QUANTITY);

                    String ProductCombinationID = "";
//                    String query3="SELECT combination_id FROM product_combinations WHERE product_id='"+ArrCombine.get(i).get(0) +"' AND combination_id!=0  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='"+ArrCombine.get(i).get(0)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                    String query3 = "SELECT combination_id FROM product_combinations WHERE product_id='" + ArrCombine.get(i).get(0) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + "  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + ArrCombine.get(i).get(0) + "' AND effective_date<='" + memodate + "')";
                    Cursor cursor3 = db.rawQuery(query3);
                    if (cursor3.moveToFirst()) {
                        do {
                            ProductCombinationID = cursor3.getString(0);
                        } while (cursor3.moveToNext());
                    }

                    Log.e("ProductCombinationID", "..........." + ProductCombinationID);
                    if (CheckCombineQuantity(QUANTITY, ArrCombine.get(i).get(0), ProductCombinationID)) {
                        for (int j = 0; j < arrCheck.size(); j++) {
                            String combinationID = "";
//                            String query1="SELECT combination_id FROM product_combinations WHERE product_id='"+arrCheck.get(j) +"' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='"+arrCheck.get(j)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                            String query1 = "SELECT combination_id FROM product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + " AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "')";
                            Log.e("query1", query1);
                            Cursor cursor1 = db.rawQuery(query1);
                            if (cursor1.moveToFirst()) {
                                do {
                                    combinationID = cursor1.getString(0);
                                } while (cursor1.moveToNext());
                            }

                            Log.e("combinationID", combinationID);
                            String query2 = "SELECT price From product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id='" + combinationID + "' AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "') AND min_quantity<=" + QUANTITY + "  order by effective_date LIMIT 1";
                            Log.e("COMBINATION PRICE QRY:", query2);
                            Log.e("combination_price:", query2);
                            Log.e("combination_qnty:", String.valueOf(QUANTITY));

                            Cursor cursor2 = db.rawQuery(query2);
                            if (cursor2.moveToFirst()) {
                                do {
                                    Log.e("COMBINATION_Price:", cursor2.getString(0));
                                    priceMap.put(arrCheck.get(j), cursor2.getString(0));
                                } while (cursor2.moveToNext());
                            }
                        }
                    } else {
                        slaveID.addAll(ArrCombine.get(i));
                        //ArrCombine.remove(i);
                        Log.e("RETURN1", "return1");
                    }
                } else {
                    slaveID.addAll(ArrCombine.get(i));
                }


            }
        }

        Log.e("..........", "......................");


        for (int i = 0; i < inList.size(); i++)
            Log.e("Selected ID", inList.get(i));


        Log.e("SLIVE SIZE:", "" + slaveID.size());
        for (int i = 0; i < slaveID.size(); i++) {
            if (inList.contains(slaveID.get(i))) {
                int index = inList.indexOf(slaveID.get(i));
                Log.e("quqntity", quantity.get(index));
                double Quantity = Double.parseDouble(quantity.get(index));
                if (!priceMap.containsKey(slaveID.get(i)))
                    priceMap.put(slaveID.get(i), CheckSlapPrice(Quantity, slaveID.get(i)));
            }
        }

        for (int i = 0; i < generalID.size(); i++) {
            if (!priceMap.containsKey(generalID.get(i)))
                priceMap.put(generalID.get(i), General_price(generalID.get(i)));

        }

        for (int i = 0; i < inList.size(); i++) {
            Log.e("ID: " + inList.get(i), "Price:" + priceMap.get(inList.get(i)));
            Log.e("...........", "......................");
            Log.e("...........", "..........product_id............" + getPreference("memo" + itemListContent.get(i).get("product_id")));


            if (getPreference("memo" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("0")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else if (getPreference("memo" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else {
                itemListContent.get(i).put("general_price", getPreference("memo" + itemListContent.get(i).get("product_id")));
                Log.e("", "++++++++++ general_price  " + getPreference("memo" + itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  " + i);
            }

        }

        sum = 0.0;
        vat =0.0;

        for (int i = 0; i < itemListContent.size(); i++) {
            HashMap<String, String> map = itemListContent.get(i);

            sum = sum + (Double.parseDouble(map.get("general_price")) * Double.parseDouble(map.get("quantity")));

            vatCalculation(map.get("product_id"),Double.parseDouble(map.get("quantity")),Double.parseDouble(map.get("general_price")),i);
        }

        txtTotal.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("" + sum))));//;//String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum))));
        getDiscount(roundTwoDecimals(Double.parseDouble("" + sum)));
        //;//String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum))));
        vattxt.setText(roundTwoDecimals(vat));

        BonusCalculation(itemListContent);
        notifyDataSetChanged();
    }

    private double setAdvanceText(String orderNumber) {
        double advance = 0.0 ;
        Cursor c = db.rawQuery("select advance_collection from order_table where order_number='"+orderNumber+"'");
        c.moveToFirst();
        if (c!=null && c.getCount()>0){

            advance = c.getDouble(0);
        }

        Log.e("memos ",advance+"");
        if (TempData.editMemo.equalsIgnoreCase("true")) {

            return advance;
        }else

            return 0.0;


    }

    private void vatCalculation(String product_id, Double quantity,Double price,int pos) {
        Double vatprice=0.0,vatofvat=0.0;
        Cursor c = db.rawQuery("select vat from product_price where product_id='"+product_id+"' and vat!='null'");
        c.moveToFirst();
        if (c!=null && c.getCount()>0){
            vatprice =  (price*100)/(100+c.getDouble(0));
            vatofvat = (price-vatprice);
            itemListContent.get(pos).put("vat",c.getDouble(0)+"");
            vat=vat+(vatofvat*quantity);
        }

    }

    private void getDiscount(String s) {
        int distype=0;
        discountp = 0.0;
        Cursor cursor = db.rawQuery("select discount_percent,discount_type from discounts where memo_value <=" + s + " and date_from <= '" + memodate + "' and date_to>='" + memodate + "' order by memo_value DESC limit 1");
        Log.e("DisQuery","select discount_percent,discount_type from discounts where memo_value <=" + s + " and date_from <= '" + memodate + "' and date_to>='" + memodate + "' order by memo_value DESC limit 1");
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            discountp = cursor.getDouble(0);
            distype = TempData.DISTYPE = cursor.getInt(1);
        }
        Double discount = 0.0;
        Double memoValue = Double.parseDouble(s);
        if (distype==1) {
            TempData.DISCOUNTP = discountp;
            discount = (memoValue * discountp) / 100;
        }else if (distype==2){

            discount =discountp;
        }
        txtdiscount.setText(roundTwoDecimals(discount));
        subt.setText(roundTwoDecimals(memoValue - discount-setAdvanceText(TempData.orderNumber)));


    }

    public ArrayList<HashMap<String, String>> getAdapterHashMapList() {
        return returnContent;
    }

    public ArrayList<HashMap<String, String>> getBonusMapList() {
        return Bonus;
    }


    public void BonusCalculation(ArrayList<HashMap<String, String>> itemListContent) {
        TextView txtBonus = (TextView) ((Activity) context).findViewById(R.id.txtBonus);
        txtBonus.setText("");

        Bonus.clear();

        for (int i = 0; i < itemListContent.size(); i++) {
            String givenQty = itemListContent.get(i).get("quantity");
            String ProductID = itemListContent.get(i).get("product_id");
            Log.e("BONUS_QUANTITY------", givenQty);
            Log.e("PRODUCT_ID------", ProductID);

            //String currentDate=getCurrentDate();
            //Log.e("CURRENTDATE",currentDate);

            String query = "SELECT B.bonus_product_id, P.product_name, B.bonus_product_qty, B.mother_product_qty FROM bonuses B LEFT JOIN product P ON(B.bonus_product_id=P.product_id)  WHERE B.mother_product_id='" + ProductID + "' AND  B.mother_product_qty<='" + givenQty + "' AND '" + memodate + "'>=B.start_date  AND '" + memodate + "'<=B.end_date limit 1";
            Log.e("BONUS_QUERY:", query);

            Cursor cursor = db.rawQuery(query);
            Log.e("COURSOR_COUNT:", "" + cursor.getCount());
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("product_id", cursor.getString(0));
                        map.put("item", cursor.getString(1));

                        double bonusQty = Double.parseDouble(cursor.getString(2)); //1
                        double tableQty = Double.parseDouble(cursor.getString(3));//12

                        double qty = (Double.parseDouble(givenQty) / tableQty) * bonusQty; //4.66*1.0

                        qty = (int) qty; // 4
                        String QuantityCheck = "";

                        Log.e("QUANTITY_TABLE:", "" + tableQty);
                        Log.e("QUANTITY_GIVEN:", "" + givenQty);
                        Log.e("QUANTITY_PARSE:", "" + qty);

                     /*   double stockQty=0;
                        String stockQtyquey="SELECT SUM(quantity) as quantity FROM  van_stocks WHERE product_id='"+ProductID+ "'";
                        Log.e("STOCK QUERY:", stockQtyquey);
                        Cursor cursor1=db.rawQuery(stockQtyquey);
                        if(cursor1!=null)
                        {
                            if(cursor1.moveToFirst())
                            {
                                do{
//                                    stockQty=Double.parseDouble(cursor1.getString(0));  //0 edit
                                    stockQty=cursor1.getDouble(0);  //0 edit 14.05.2018

                                    if(TempData.editMemo.equalsIgnoreCase("true")){
                                        ///TempData.orderNumber memo no
                                        /// query and get all memo details from memo_details table with memo no and product id cursor.getString(0))
                                        // loop through the memo details records and get quantity for cursor.getString(0)) product

                                        String MemoTablequey="SELECT SUM(quantity) as quantity FROM  memo_details WHERE product_id='"+ProductID+ "' and memo_no='"+ TempData.orderNumber +"'";
                                        Cursor cur=db.rawQuery(MemoTablequey);
                                        if(cur!=null)
                                        {
                                            if(cur.moveToFirst())
                                            {
                                                do{
                                                    stockQty = stockQty + cur.getDouble(0);
                                                }while(cur.moveToNext());
                                            }
                                        }
                                    }

                                }while(cursor1.moveToNext());
                            }
                        }*/


                        if (getIndex(cursor.getString(0)) < 0) {
                            Log.e("String.valueOf(qty)", String.valueOf(qty));
                            map.put("quantity", String.valueOf(qty));
                            Bonus.add(map);
                        } else {
                            double QUANTITY = Double.parseDouble(Bonus.get(getIndex(cursor.getString(4))).get("quantity")) + qty;
                            Log.e("Quantity", String.valueOf(QUANTITY));
                            Bonus.get(getIndex(cursor.getString(4))).put("quantity", String.valueOf(QUANTITY));  ///// Why index 4 where as query has 3 indexes
                        }
                    } while (cursor.moveToNext());
                }
            }

        }

        String bonusString = "";
        TempData.BonusShowList.clear();
        for (int i = 0; i < Bonus.size(); i++) {
            bonusString = bonusString + Bonus.get(i).get("item") + "(" + Bonus.get(i).get("quantity") + ")";

            HashMap<String, String> product_map = new HashMap<String, String>();

            product_map.put("bonus_name", Bonus.get(i).get("item"));
            product_map.put("quantity", Bonus.get(i).get("quantity"));


//            maplist.add(product_map);
            TempData.BonusShowList.add(product_map);


            if (i != Bonus.size() - 1)
                bonusString = bonusString + ", ";
        }


        if (bonusString.length() > 0) {
            Log.e("bonusString", bonusString);
            txtBonus.setText(bonusString);
            txtBonus.setVisibility(View.VISIBLE);
            TempData.Test = BonusItemList.toString();
        } else
//            txtBonus.setText("");
            txtBonus.setVisibility(View.GONE);

    }


    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String CurrentDate = "" + dateFormat.format(date);

        return CurrentDate;

    }

    public String roundTwoDecimals(double d) {


        Log.e("tEST:", String.format("%.2f", d));


        Log.e("Double:", String.valueOf(d));
        Log.e("Modulus:", String.valueOf(d % 1));

		 /* if(d%1>0.0)
		  {

	     DecimalFormat twoDForm = new DecimalFormat("#.##");
	     String value= String.valueOf(twoDForm.format(d));
	     Log.e("value:", ""+value);
	     Log.e("value.indexOf('.'):", ""+value.indexOf('.'));
	     Log.e("value.length:", ""+value.length());
	     String subValue=value.substring(value.indexOf('.'),value.length());
	     Log.e("LENGTH:", subValue);
	              //if(subValue.length()>1)
	        	 //return String.valueOf(twoDForm.format(d));
	             // else
	 	        	 //return String.valueOf(twoDForm.format(d))+"0";


		  }
		  else
		  {

			     //return String.valueOf(d)+"0";
		  }*/

        return String.format("%.2f", d);
    }

    public void UpdateDistPrice() {

        ArrayList<ArrayList<String>> ArrCombine = new ArrayList<ArrayList<String>>();
        ArrayList<String> slaveID = new ArrayList<String>();
        ArrayList<String> generalID = new ArrayList<String>();
        ArrayList<String> combineID = new ArrayList<String>();
        ArrayList<String> inList = new ArrayList<String>();
        ArrayList<String> quantity = new ArrayList<String>();

        for (int i = 0; i < itemListContent.size(); i++) {
            inList.add(itemListContent.get(i).get("product_id"));
            Log.e("IDDDDDDDDDDDDDDDDDD:", itemListContent.get(i).get("product_id"));
        }
        for (int i = 0; i < itemListContent.size(); i++) {
            quantity.add(itemListContent.get(i).get("quantity"));
            savePreference(itemListContent.get(i).get("product_id"), itemListContent.get(i).get("quantity"));

            Log.e("quantity:", itemListContent.get(i).get("quantity"));
        }

        HashMap<String, String> priceMap = new HashMap<String, String>();

        String byPassNow = "YES";
        if (!TargetCustomer.equalsIgnoreCase("0")) {
            //for(int i=0;i<inList.size();i++)
            //priceMap.put(inList.get(i), NGO_price(inList.get(i),quantity.get(i)));
        }

        if (TargetCustomer.equalsIgnoreCase("0") || byPassNow == "YES") {
            InstietuteID = "0";

            for (int i = 0; i < inList.size(); i++) {
                Log.e("PRISING TYPE: ", inList.get(i) + ": " + DistCheckPriceType(inList.get(i)));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("COMBINE"))
                    combineID.add(inList.get(i));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("SLAVE"))
                    slaveID.add(inList.get(i));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("GENERAL"))
                    generalID.add(inList.get(i));
            }


            Log.e("COMBINE SIZE:", "" + combineID.size());
            String combination_id = "";
            for (int i = 0; i < combineID.size(); i++) {
                Cursor cursor = db.rawQuery("SELECT combination_id FROM distributor_product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + combineID.get(i) + "' AND effective_date<='" + memodate + "')");


                Log.e("OUTER_QUERY:", "SELECT combination_id FROM distributor_product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0");
                Log.e("OUTER_QUERY_COUNT", "" + cursor.getCount());
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            combination_id = cursor.getString(0);
                            Cursor cursor1 = db.rawQuery("SELECT * FROM distributor_product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY:", "SELECT * FROM distributor_product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY_COUNT:", "" + cursor1.getCount());
                            if (cursor1 != null) {
                                if (cursor1.moveToFirst()) {
                                    ArrayList<String> combinationList = new ArrayList<String>();
                                    do {
                                        //.....combine id added  to list.......
                                        if (!combinationList.contains(cursor1.getString(1)))
                                            combinationList.add(cursor1.getString(1));
                                        Log.e("CombineID", cursor1.getString(1));

                                    } while (cursor1.moveToNext());

                                    if (ArrCombine.size() == 0) {
                                        ArrCombine.add(combinationList);
                                        Log.e("added to combine list", "added");
                                    } else {
                                        if (ArrCombine.contains(combinationList))
                                            Log.e("Duplicate", "True");
                                        else
                                            ArrCombine.add(combinationList);
                                    }
                                }
                            }


                        } while (cursor.moveToNext());
                    }
                }
            }

            Log.e("COMBINE ARRAY:", ArrCombine.toString());
            Log.e("ArrCombine.size()", ArrCombine.size() + "");
            Log.e("QUANTITY ARRAY", quantity.toString());

            for (int i = 0; i < ArrCombine.size(); i++) {
                double QUANTITY = 0;

                ArrayList<String> arrCheck = ArrCombine.get(i);
                Log.e("ArraySize:", arrCheck.size() + "");
                String comProduct = combineID.get(i);
                int index = inList.indexOf(combineID.get(i));

                for (int j = 0; j < arrCheck.size(); j++) {

                    Log.e(".............", ".............");
                    Log.e("COMBINE ID:", arrCheck.get(j));
                    if (!getPreference(arrCheck.get(j)).equalsIgnoreCase(""))
                        QUANTITY = QUANTITY + Double.parseDouble(getPreference(arrCheck.get(j)));

                }
                Log.e("Given....QUANTITY", "" + QUANTITY);
                Log.e("INDi....inList", "" + inList.toString());
                Log.e("INDi....arrCheck", "" + arrCheck.toString());
                /// sql query to find combined quantity

                Double Combined_Quantity = 0.0;

                Cursor cursor = db.rawQuery("SELECT combination_id,min_quantity FROM distributor_product_combinations WHERE product_id= '" + arrCheck.get(0) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(0) + "' AND effective_date<='" + memodate + "')");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
//                            Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            if (QUANTITY >= Double.parseDouble(cursor.getString(1))) {
                                Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            }
                        } while (cursor.moveToNext());
                    }
                }

                Log.e("Combined_Quantity: ", "" + Combined_Quantity);

//		    	     if(inList.containsAll(arrCheck))
                if (QUANTITY >= Combined_Quantity) {

                    //Log.e("ArrCombine.get(i).get(index))",""+ArrCombine.get(i).get(index));
                    //Log.e("ArrCombine.get(index).get(i))",""+ArrCombine.get(index).get(i));
                    Log.e("QUANTITY111", "" + QUANTITY);

                    String ProductCombinationID = "";
//                    String query3="SELECT combination_id FROM distributor_product_combinations WHERE product_id='"+ArrCombine.get(i).get(0) +"' AND combination_id!=0  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='"+ArrCombine.get(i).get(0)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                    String query3 = "SELECT combination_id FROM distributor_product_combinations WHERE product_id='" + ArrCombine.get(i).get(0) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + "  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + ArrCombine.get(i).get(0) + "' AND effective_date<='" + memodate + "')";
                    Cursor cursor3 = db.rawQuery(query3);
                    if (cursor3.moveToFirst()) {
                        do {
                            ProductCombinationID = cursor3.getString(0);
                        } while (cursor3.moveToNext());
                    }

                    Log.e("ProductCombinationID", "..........." + ProductCombinationID);
                    if (DistCheckCombineQuantity(QUANTITY, ArrCombine.get(i).get(0), ProductCombinationID)) {
                        for (int j = 0; j < arrCheck.size(); j++) {
                            String combinationID = "";
//                            String query1="SELECT combination_id FROM distributor_product_combinations WHERE product_id='"+arrCheck.get(j) +"' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='"+arrCheck.get(j)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                            String query1 = "SELECT combination_id FROM distributor_product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + " AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "')";
                            Log.e("query1", query1);
                            Cursor cursor1 = db.rawQuery(query1);
                            if (cursor1.moveToFirst()) {
                                do {
                                    combinationID = cursor1.getString(0);
                                } while (cursor1.moveToNext());
                            }

                            Log.e("combinationID", combinationID);
                            String query2 = "SELECT price From distributor_product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id='" + combinationID + "' AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "') AND min_quantity<=" + QUANTITY + "  order by effective_date LIMIT 1";
                            Log.e("COMBINATION PRICE QRY:", query2);
                            Cursor cursor2 = db.rawQuery(query2);
                            if (cursor2.moveToFirst()) {
                                do {
                                    Log.e("COMBINATION_Price:", cursor2.getString(0));
                                    priceMap.put(arrCheck.get(j), cursor2.getString(0));
                                } while (cursor2.moveToNext());
                            }
                        }
                    } else {
                        slaveID.addAll(ArrCombine.get(i));
                        //ArrCombine.remove(i);
                        Log.e("RETURN1", "return1");
                    }
                } else {
                    slaveID.addAll(ArrCombine.get(i));
                    //ArrCombine.remove(i);
                }


            }
        }

        Log.e("..........", "......................");


        for (int i = 0; i < inList.size(); i++)
            Log.e("Selected ID", inList.get(i));


        Log.e("SLIVE SIZE:", "" + slaveID.size());
        for (int i = 0; i < slaveID.size(); i++) {
            if (inList.contains(slaveID.get(i))) {
                int index = inList.indexOf(slaveID.get(i));
                double Quantity = Double.parseDouble(quantity.get(index));
                if (!priceMap.containsKey(slaveID.get(i)))
                    priceMap.put(slaveID.get(i), DistCheckSlapPrice(Quantity, slaveID.get(i)));
            }

        }

        for (int i = 0; i < generalID.size(); i++) {
            if (!priceMap.containsKey(generalID.get(i)))
                priceMap.put(generalID.get(i), DistGeneral_price(generalID.get(i)));
            //priceMap.put(generalID.get(i), "0");
        }

        for (int i = 0; i < inList.size(); i++) {
            Log.e("ID: " + inList.get(i), "Price:" + priceMap.get(inList.get(i)));
            Log.e("...........", "......................");
            Log.e("...........", "..........product_id............" + getPreference("up" + itemListContent.get(i).get("product_id")));

            /*if(!getPreference("up"+itemListContent.get(i).get("product_id")).equalsIgnoreCase("0"))
            {
                itemListContent.get(i).put("general_price", getPreference("up"+itemListContent.get(i).get("product_id")));
                Log.e("", "++++++++++ general_price  "+getPreference("up"+itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  "+i);
            }else {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  "+i);
            }*/


            if (getPreference("up" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("0")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else if (getPreference("up" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else {
                itemListContent.get(i).put("general_price", getPreference("up" + itemListContent.get(i).get("product_id")));
                Log.e("", "++++++++++ general_price  " + getPreference("up" + itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  " + i);
            }


        }
        sum = 0.0;

        for (int i = 0; i < itemListContent.size(); i++) {
            HashMap<String, String> map = itemListContent.get(i);
            Log.e("Tgeneral price list", map.get("general_price"));
            Log.e("", "XXXXXXXXXXXXXXXXXXXXXXXXX  general_price  " + map.get("general_price"));
            Log.e("", "XXXXXXXXXXXXXXXXXXXXXXXXX  quantity " + map.get("quantity"));
            sum = sum + (Double.parseDouble(map.get("general_price")) * Double.parseDouble(map.get("quantity")));
        }
        txtTotal.setText(String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble("" + sum))));//;//String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum))));
        getDiscount(roundTwoDecimals(Double.parseDouble("" + sum)));

        /*if(TempData.InstituteID.equalsIgnoreCase("0")|| TempData.InstituteID.equalsIgnoreCase(""))
            etPayment.setText(txtTotal.getText().toString());

        double paymentAmunt=0.0;
        double totalAmount=0.0;
        totalAmount= Double.parseDouble(txtTotal.getText().toString());

        paymentAmunt= Double.parseDouble(etPayment.getText().toString());
        if(paymentAmunt>totalAmount)
        {
            txtDueChanged.setText("Change:");
            //String String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum)));
            txtCredit.setText(String.valueOf(paymentAmunt-totalAmount));


        }
        else
        {
            txtDueChanged.setText("Due:");
            txtCredit.setText(String.valueOf(totalAmount-paymentAmunt));
        }*/

        DistBonusCalculation(itemListContent);
        notifyDataSetChanged();
    }

    public void DistBonusCalculation(ArrayList<HashMap<String, String>> itemListContent) {
        TextView txtBonus = (TextView) ((Activity) context).findViewById(R.id.txtBonus);
        txtBonus.setText("");

        Bonus.clear();

        for (int i = 0; i < itemListContent.size(); i++) {
            String givenQty = itemListContent.get(i).get("quantity");
            String ProductID = itemListContent.get(i).get("product_id");
            Log.e("BONUS_QUANTITY------", givenQty);
            Log.e("PRODUCT_ID------", ProductID);

            //String currentDate=getCurrentDate();
            //Log.e("CURRENTDATE",currentDate);

            String query = "SELECT B.bonus_product_id, P.product_name, B.bonus_product_qty, B.mother_product_qty FROM bonuses B LEFT JOIN products P ON(B.bonus_product_id=P.product_id)  WHERE B.mother_product_id='" + ProductID + "' AND  B.mother_product_qty<='" + givenQty + "' AND '" + memodate + "'>=B.start_date  AND '" + memodate + "'<=B.end_date limit 1";
            Log.e("BONUS_QUERY:", query);

            Cursor cursor = db.rawQuery(query);
            Log.e("COURSOR_COUNT:", "" + cursor.getCount());
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("product_id", cursor.getString(0));
                        map.put("item", cursor.getString(1));

                        double bonusQty = Double.parseDouble(cursor.getString(2)); //1
                        double tableQty = Double.parseDouble(cursor.getString(3));//12

                        double qty = (Double.parseDouble(givenQty) / tableQty) * bonusQty; //4.66*1.0

                        qty = (int) qty; // 4
                        String QuantityCheck = "";

                        Log.e("QUANTITY_TABLE:", "" + tableQty);
                        Log.e("QUANTITY_GIVEN:", "" + givenQty);
                        Log.e("QUANTITY_PARSE:", "" + qty);

                        double stockQty = 0;
                        String stockQtyquey = "SELECT SUM(quantity) as quantity FROM  van_stocks WHERE product_id='" + ProductID + "'";
                        Log.e("STOCK QUERY:", stockQtyquey);
                        Cursor cursor1 = db.rawQuery(stockQtyquey);
                        if (cursor1 != null) {
                            if (cursor1.moveToFirst()) {
                                do {
//                                    stockQty=Double.parseDouble(cursor1.getString(0));  //0 edit
                                    stockQty = cursor1.getDouble(0);  //0 edit 14.05.2018

                                    if (TempData.editMemo.equalsIgnoreCase("true")) {
                                        ///TempData.orderNumber memo no
                                        /// query and get all memo details from memo_details table with memo no and product id cursor.getString(0))
                                        // loop through the memo details records and get quantity for cursor.getString(0)) product

                                        String MemoTablequey = "SELECT SUM(quantity) as quantity FROM  memo_details WHERE product_id='" + ProductID + "' and memo_no='" + TempData.orderNumber + "'";
                                        Cursor cur = db.rawQuery(MemoTablequey);
                                        if (cur != null) {
                                            if (cur.moveToFirst()) {
                                                do {
                                                    stockQty = stockQty + cur.getDouble(0);
                                                } while (cur.moveToNext());
                                            }
                                        }
                                    }

                                } while (cursor1.moveToNext());
                            }
                        }

                        double GivenAndBonusQty = Double.parseDouble(givenQty) + qty;  //22 edit
                        Log.e("stockQty", "" + stockQty);
                        Log.e("GivenAndBonusQty", "" + GivenAndBonusQty);


                        if (GivenAndBonusQty > stockQty) {
                            qty = stockQty - Double.parseDouble(givenQty);
                            Log.e("quantityvalue", String.valueOf(qty));
                        }


                        if (getIndex(cursor.getString(0)) < 0) {
                            Log.e("String.valueOf(qty)", String.valueOf(qty));
                            map.put("quantity", String.valueOf(qty));
                            Bonus.add(map);
                        } else {
                            double QUANTITY = Double.parseDouble(Bonus.get(getIndex(cursor.getString(4))).get("quantity")) + qty;
                            Log.e("Quantity", String.valueOf(QUANTITY));
                            Bonus.get(getIndex(cursor.getString(4))).put("quantity", String.valueOf(QUANTITY));  ///// Why index 4 where as query has 3 indexes
                        }
                    } while (cursor.moveToNext());
                }
            }

        }

        String bonusString = "";
        TempData.BonusShowList.clear();
        for (int i = 0; i < Bonus.size(); i++) {
            bonusString = bonusString + Bonus.get(i).get("item") + "(" + Bonus.get(i).get("quantity") + ")";

            HashMap<String, String> product_map = new HashMap<String, String>();

            product_map.put("bonus_name", Bonus.get(i).get("item"));
            product_map.put("quantity", Bonus.get(i).get("quantity"));


//            maplist.add(product_map);
            TempData.BonusShowList.add(product_map);


            if (i != Bonus.size() - 1)
                bonusString = bonusString + ", ";
        }


        if (bonusString.length() > 0) {
            Log.e("bonusString", bonusString);
            txtBonus.setText(bonusString);
            TempData.Test = BonusItemList.toString();
        } else
//            txtBonus.setText("");
            txtBonus.setVisibility(View.GONE);

    }


    public String DistGeneral_price(String product_id) {
        String query2 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date DESC, _id DESC LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            String query3 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer=0 AND price !='0.0' AND effective_date<='" + memodate + "' order by effective_date, _id DESC LIMIT 1";
            Cursor cursor3 = db.rawQuery(query3);
            if (cursor3.getCount() > 0) {
                if (cursor3.moveToFirst()) {
                    do {
                        return cursor3.getString(0);
                    } while (cursor3.moveToNext());
                }
            }
        }
        return "0.0";
    }

    public String DistCheckSlapPrice(double quantity, String product_id) {
        num.clear();
        //String query1="SELECT min_quantity From distributor_product_combinations WHERE product_id='"+product_id+"' AND combination_id=0 AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
        String query1 = "SELECT min_quantity From distributor_product_combinations WHERE product_id='" + product_id + "' AND combination_id=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') order by min_quantity DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query1);
        Log.e("QUERY", query1);
        Log.e("COUNT", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    num.add(Double.parseDouble(cursor.getString(0)));
                } while (cursor.moveToNext());
            }
        }
        num.add(0, 0.0);
        num.add(quantity);
        num.add(8080.0);
        Collections.sort(num);


        int i = 0;
        for (int k = 0; k < num.size(); k++) {
            Log.e("NUM", " " + num.get(k));
            if (num.get(k) == quantity)
                i = k;
        }


        //String query="SELECT price From distributor_product_combinations WHERE product_id="+String.valueOf(product_id)+" AND combination_id=0 " +" AND "+ "min_quantity>="+String.valueOf(num.get(i-1))+" AND "+ "min_quantity <"+String.valueOf(num.get(i+1));
        //String query="SELECT price From distributor_product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";
        //String query="SELECT price From distributor_product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";

        String query = "SELECT price From distributor_product_combinations WHERE product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND min_quantity <=" + String.valueOf(num.get(i)) + "  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') ORDER BY  min_quantity DESC LIMIT 1";

        Log.e("QUERY PRICE VALUE: ", query);
        Cursor cursor1 = db.rawQuery(query);
        Log.e("Second Cursor:", "" + cursor1.getCount());
        if (cursor1.getCount() > 0) {
            try {
                if (cursor1.moveToFirst()) {
                    do {
                        price = cursor1.getString(0);
                        Log.e("Slave price:", price);
                        return price;
                    } while (cursor1.moveToNext());
                }

            } finally {
                try {
                    cursor1.close();
                } catch (Exception ignore) {
                }
            }

        } else {
            //String query2="SELECT price From distibutor_product_prices WHERE product_id='"+product_id+ "' AND target_custommer=0 AND price!='0.0' AND effective_date=(SELECT max(effective_date) from distibutor_product_prices where product_id='"+product_id+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
            String query2 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer=0 AND price!='0.0' AND effective_date<='" + memodate + "' ORDER by effective_date DESC , _id DESC limit 1";

            Log.e("query2", query2);
            Cursor cursor2 = db.rawQuery(query2);
            if (cursor2.getCount() == 0) {
                return "0.0";
            }

            if (cursor2.moveToFirst()) {
                do {
                    Log.e("General price:", cursor2.getString(0));
                    return cursor2.getString(0);
                    //return "0";
                } while (cursor2.moveToNext());
            }
        }
        return "0";
    }


    public boolean DistCheckCombineQuantity(double quantity, String product_id, String combination_id) {

        Log.e("Calling this method:", "called for " + product_id);
        Log.e("quantity", "" + quantity);
        Log.e("product_id", "" + product_id);
        Log.e("combination_id", "" + combination_id);

        //Cursor cursor=db.rawQuery("SELECT *FROM distributor_product_combinations WHERE product_id= '"+product_id+"' AND combination_id='"+combination_id+"'", null);
        Cursor cursor = db.rawQuery("SELECT *FROM distributor_product_combinations WHERE combination_id='" + combination_id + "'");
        Log.e("CheckCombineQtyCount", "" + cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                double qty = Double.parseDouble(cursor.getString(3));
                Log.e("Pass Quantity:", "" + quantity);
                Log.e("Quantity:", "" + qty);
                if (quantity >= qty) {
                    Log.e("TRUE", "true");
                    return true;
                } else {
                    Log.e("FALSE", "false");
                    return false;
                }

            } while (cursor.moveToNext());
        }
        return false;
    }

    public String DistCheckPriceType(String ID) {


        Cursor cursor = db.rawQuery("SELECT * FROM distributor_product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from distributor_product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        Log.e("PRICE TYPE QUERY:", "SELECT * FROM distributor_product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from distributor_product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        if (cursor.getCount() == 0) {
            db.close();
            cursor.close();
            return "GENERAL";
        } else {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        Log.e("COMBINATION ID", cursor.getString(2));
                        if (!cursor.getString(2).equalsIgnoreCase("0")) {
                            db.close();
                            cursor.close();
                            return "COMBINE";
                        } else {
                            db.close();
                            cursor.close();
                            return "SLAVE";
                        }
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
            }
        }

        db.close();
        cursor.close();
        return "";
    }


    public boolean CheckCombineQuantity(double quantity, String product_id, String combination_id) {

        Log.e("Calling this method:", "called for " + product_id);
        Log.e("quantity", "" + quantity);
        Log.e("product_id", "" + product_id);
        Log.e("combination_id", "" + combination_id);

        //Cursor cursor=db.rawQuery("SELECT *FROM product_combinations WHERE product_id= '"+product_id+"' AND combination_id='"+combination_id+"'", null);
        Cursor cursor = db.rawQuery("SELECT *FROM product_combinations WHERE combination_id='" + combination_id + "'");
        Log.e("CheckCombineQtyCount", "" + cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                double qty = Double.parseDouble(cursor.getString(3));
                Log.e("Pass Quantity:", "" + quantity);
                Log.e("Quantity:", "" + qty);
                if (quantity >= qty) {
                    Log.e("TRUE", "true");
                    return true;
                } else {
                    Log.e("FALSE", "false");
                    return false;
                }

            } while (cursor.moveToNext());
        }
        return false;
    }

    public String CheckPriceType(String ID) {


        Cursor cursor = db.rawQuery("SELECT * FROM product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        Log.e("PRICE TYPE QUERY:", "SELECT * FROM product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        if (cursor.getCount() == 0) {
            db.close();
            cursor.close();
            return "GENERAL";
        } else {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        Log.e("COMBINATION ID", cursor.getString(2));
                        if (!cursor.getString(2).equalsIgnoreCase("0")) {
                            db.close();
                            cursor.close();
                            return "COMBINE";
                        } else {
                            db.close();
                            cursor.close();
                            return "SLAVE";
                        }
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
            }
        }

        db.close();
        cursor.close();
        return "";
    }

    public String General_price(String product_id) {
        String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date DESC LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            String query3 = "SELECT price From " + Tables.TABLE_NAME_PRODUCT_PRICE + " WHERE product_id='" + product_id + "' AND target_custommer=0 AND price !='0.0' AND effective_date<='" + memodate + "' order by effective_date LIMIT 1";
            Cursor cursor3 = db.rawQuery(query3);
            if (cursor3.getCount() > 0) {
                if (cursor3.moveToFirst()) {
                    do {
                        return cursor3.getString(0);
                    } while (cursor3.moveToNext());
                }
            }
        }
        return "0.0";
    }


    public String NGO_price(String product_id, String quantity) {
        String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            return CheckSlapPrice(Double.parseDouble(quantity), product_id);
/*			   String query3="SELECT price From product_price WHERE id='"+product_id+"' AND target_custommer=0 AND price !='0.0' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
			   Cursor  cursor3= db.rawQuery(query3, null);
			   if(cursor3.getCount() > 0)
			   {
			   if(cursor3.moveToFirst())
			     {
			    	 do{
			    		 return cursor3.getString(0);
			    	 }while(cursor3.moveToNext());
			     }
			   }
*/
        }
        return "0.0";
    }

    private int get_is_bonus(String product_id) {

        String products_query="SELECT is_bonus FROM product_history WHERE start_date<="+"'"+memodate+"'"+" and end_date>="+"'"+memodate+"' and product_id='" +product_id +"' and is_bonus='0'";
        Cursor c5=db.rawQuery(products_query);
        //	Log.e("sales order Unitprice: ","Unitprice: "+c5.getCount());
        Log.e("QUERY COUNT:", "..............."+products_query);
        if(c5!=null) {
            if (c5.moveToFirst()) {
                do {
                    Log.e("bonus",product_id+" "+c5.getInt(0));
                    return c5.getInt(0);
                }while (c5.moveToNext());
            }
        }

        return 5;
    }

    public String CheckSlapPrice(double quantity, String product_id) {
        num.clear();
        //String query1="SELECT min_quantity From product_combinations WHERE product_id='"+product_id+"' AND combination_id=0 AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
        String query1 = "SELECT min_quantity From product_combinations WHERE product_id='" + product_id + "' AND combination_id=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') order by min_quantity DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query1);
        Log.e("QUERY", query1);
        Log.e("COUNT", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    num.add(Double.parseDouble(cursor.getString(0)));
                } while (cursor.moveToNext());
            }
        }
        num.add(0, 0.0);
        num.add(quantity);
        num.add(8080.0);
        Collections.sort(num);


        int i = 0;
        for (int k = 0; k < num.size(); k++) {
            Log.e("NUM", " " + num.get(k));
            if (num.get(k) == quantity)
                i = k;
        }


        //String query="SELECT price From product_combinations WHERE product_id="+String.valueOf(product_id)+" AND combination_id=0 " +" AND "+ "min_quantity>="+String.valueOf(num.get(i-1))+" AND "+ "min_quantity <"+String.valueOf(num.get(i+1));
        //String query="SELECT price From product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";
        //String query="SELECT price From product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";

        String query = "SELECT price From product_combinations WHERE product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND min_quantity <=" + String.valueOf(num.get(i)) + "  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') ORDER BY  min_quantity DESC LIMIT 1";

        Log.e("QUERY PRICE VALUE: ", query);
        Cursor cursor1 = db.rawQuery(query);
        Log.e("Second Cursor:", "" + cursor1.getCount());
        if (cursor1.getCount() > 0) {
            try {
                if (cursor1.moveToFirst()) {
                    do {
                        price = cursor1.getString(0);
                        Log.e("Slave price:", price);
                        if (TempData.editMemo.equalsIgnoreCase("true")){

                            if (get_is_bonus(product_id)!=0) {
                                Log.e("upb", getPreference("up" + product_id));
                                savePreference("up" + product_id, "0");
                                Log.e("up1", getPreference("up" + product_id));
                            }
                        }
                        return price;
                    } while (cursor1.moveToNext());
                }

            } finally {
                try {
                    cursor1.close();
                } catch (Exception ignore) {
                }
            }

        } else {
            //String query2="SELECT price From product_price WHERE product_id='"+product_id+ "' AND target_custommer=0 AND price!='0.0' AND effective_date=(SELECT max(effective_date) from product_price where product_id='"+product_id+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
            String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer=0 AND price!='0.0' AND effective_date<='" + memodate + "' ORDER by effective_date DESC limit 1";

            Log.e("query2", query2);
            Cursor cursor2 = db.rawQuery(query2);
            if (cursor2.getCount() == 0) {
                return "0.0";
            }

            if (cursor2.moveToFirst()) {
                do {
                    if (TempData.editMemo.equalsIgnoreCase("true")){

                        if (get_is_bonus(product_id)!=0) {
                            Log.e("upb", getPreference("up" + product_id));
                            savePreference("up" + product_id,"0");
                            Log.e("up1", getPreference("up" + product_id));
                        }
                    }
                    return cursor2.getString(0);
                    //return "0";
                } while (cursor2.moveToNext());
            }
        }
        return "0";
    }


    int getIndex(String value) {
        int pos = -1;
        for (int i = 0; i < Bonus.size(); i++) {
            HashMap<String, String> map = Bonus.get(i);
            Log.e("PRODUCT_ID", map.get("product_id"));
            if (map.get("product_id").equalsIgnoreCase(value))
                pos = i;
        }

        return pos;
    }

    public void savePreference(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key) {
        String value = "";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "0");

        return value;

    }

    public String roundTwoDecimal(double d) {

        Log.e("Double:", String.valueOf(d));
        Log.e("Modulus:", String.valueOf(d % 1));

        if (d % 1 > 0.0) {

            DecimalFormat twoDForm = new DecimalFormat("#.##");
            String value = String.valueOf(twoDForm.format(d));
            String subValue = value.substring(value.indexOf('.'), value.length() - 1);
            Log.e("LENGTH:", subValue);
            if (subValue.length() > 1)
                return String.valueOf(twoDForm.format(d));
            else
                return String.valueOf(twoDForm.format(d)) + "0";


        } else {

            return String.valueOf(d) + "0";
        }
    }


    private void showLimitExeednotification(double giventQty, String product_id) {

        Cursor c = db.rawQuery("SELECT quantity-booking_quantity>" + giventQty + " as p from stock_info WHERE  product_id = '" + product_id + "'");
        Log.e("ProductSales", "SELECT quantity-booking_quantity>" + giventQty + " as p from stock_info WHERE  product_id = '" + product_id + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            if (c.getInt(0) == 0) {
                Toast.makeText(context, "Your Stock Limit Exceeded", Toast.LENGTH_SHORT).show();
            }
        }


    }
}

/*
package com.srapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.database.Cursor;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.Db_Actions.Tables;
import com.srapp.R;
import com.srapp.TagEditText;
import com.srapp.TempData;
import com.srapp.Util.Parent;
import com.srapp.Util.ParentActivity;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

public class SalesOrderDetailsAdaperForMemo extends BaseAdapter {

    // Declare Variables
    Context context;
    String SO_ID;
    ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();
    //	ArrayList<HashMap<String, String>> itemListData = new ArrayList<HashMap<String, String>>();
    //HomeProjectListAdapter featuredProjectsListAdapter;
    ArrayList<HashMap<String, String>> returnContent = new ArrayList<HashMap<String, String>>();
    ArrayList<HashMap<String, String>> Bonus = new ArrayList<HashMap<String, String>>();
    ArrayList<Double> num = new ArrayList<Double>();
    String price = "";
    String TargetCustomer = "0";
    String InstietuteID = "";

    ArrayList<HashMap<String, String>> BonusItemList = new ArrayList<HashMap<String, String>>();
    Double discountp = 0.0;
    TextView NameTv, TPrice, txtTotal, txtdiscount, subt, vattxt, vattv;
    EditText QuantityEd, UPrice;
    TagEditText serial_number;
    ImageView ActionBtn, expand;
    String product_name = "", Quantity = "", product_id = "", has_combination = "", general_price = "", is_bonus = "";
    Double sum = 0.0, vat = 0.0;
    String outlet_ID = "";
    String date, memodate;
    Data_Source db;
    // LinearLayout linear_layout;

    public SalesOrderDetailsAdaperForMemo(Context context, ArrayList<HashMap<String, String>> arraylistContent, String _SO_ID, String outlet_ID) {
        this.context = context;
        this.outlet_ID = outlet_ID;
        itemListContent = arraylistContent;

        db = new Data_Source(context);
        SO_ID = _SO_ID;


        if (TempData.editMemo.equalsIgnoreCase("true")) {
            date = TempData.MemoDate;
        } else {
            date = getCurrentDate();
        }


        Log.e("Date:", String.valueOf(date));
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, Locale.getDefault());
        Date datevalue = null;
        try {
            datevalue = simpleDateFormat.parse(String.valueOf(date));

            simpleDateFormat.applyPattern(pattern);
            memodate = simpleDateFormat.format(datevalue);

            Log.e("MemoDate:", String.valueOf(memodate));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        InstietuteID = TempData.InstituteID;
        Log.e("Target:" + TargetCustomer, "Institude: " + InstietuteID);
        sum = 0.0;
        txtTotal = (TextView) ((Activity) context).findViewById(R.id.total_price);
        txtdiscount = (TextView) ((Activity) context).findViewById(R.id.discount);
        subt = (TextView) ((Activity) context).findViewById(R.id.sub_total);
        vattxt = (TextView) ((Activity) context).findViewById(R.id.vat);

        UpdatePrice();
    }

    @Override
    public int getCount() {
        return itemListContent.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {


        returnContent = itemListContent;

        final View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_sales_item_for_memo, null);
        view2.setClickable(true);
        view2.setFocusable(true);
        NameTv = (TextView) view2.findViewById(R.id.product_name);
        UPrice = (EditText) view2.findViewById(R.id.price);
        vattv = view2.findViewById(R.id.vat);
        TPrice = (TextView) view2.findViewById(R.id.total_price);
        QuantityEd = (EditText) view2.findViewById(R.id.quantity);
        ActionBtn = (ImageView) view2.findViewById(R.id.dlt_product_sales_item);
        //expand = (ImageView) view2.findViewById(R.id.expand);
        //serial_number = view2.findViewById(R.id.serial_number);
        //linear_layout = view2.findViewById(R.id.linear_layout);
        //serial_number.setVisibility(View.GONE);
        HashMap<String, String> mapContent = new HashMap<String, String>();
        mapContent = itemListContent.get(position);
        product_name = mapContent.get("product_name");
        Quantity = mapContent.get("quantity");

*/
/*        view2.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                serial_number = view2.findViewById(R.id.serial_number);
                if (serial_number.getVisibility() == View.VISIBLE)
                    serial_number.setVisibility(View.GONE);
                else {
                    serial_number.setVisibility(View.VISIBLE);
                }
            }
        });
        expand.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (serial_number.getVisibility() == View.VISIBLE)
                    serial_number.setVisibility(View.GONE);
                else {
                    serial_number.setVisibility(View.VISIBLE);
                }
            }
        });*//*


        product_id = mapContent.get("product_id");
        QuantityEd.setEnabled(false);
        vattv.setText(mapContent.get("vatt") + "%");
        //serial_number.setText(mapContent.get("serial_number"));
        Log.e("vat", mapContent.get("vatt") + "%");
        has_combination = mapContent.get("has_combination");
        general_price = mapContent.get("general_price");
        Log.e("general_price_first", "....." + general_price);

        // Product bonus issues-------------------------------------------------------
        is_bonus = mapContent.get("is_bonus");
        String p_id = db.getProductId(product_id);
        Log.e("product_id_before", "....." + p_id);

        try {
            if (is_bonus.equalsIgnoreCase("0") || p_id.equals("0")) { // here is_bonus=0, open price
                Log.e("is_bonus_editable", mapContent.get("is_bonus")); //When open price =0 editable mood enable
                Log.e("is_bonus_editable", "enable"); //When open price =0 editable mood enable
                UPrice.setEnabled(true);
            } else {
                Log.e("is_bonus_editable", mapContent.get("is_bonus"));
                Log.e("is_bonus_editable", "disable");
                UPrice.setEnabled(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        if (!Quantity.equals("")) {

            if (general_price == null) {
                NameTv.setText(product_name);
                QuantityEd.setText(Quantity);

            } else {
                Double _generalPrice = Double.parseDouble(general_price);
                Double _Quantity = Double.parseDouble(Quantity);
                Double T_Price = _generalPrice * _Quantity;

                NameTv.setText(product_name);
                if (Quantity.equalsIgnoreCase("0")) {
                    QuantityEd.setText("");
                    UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("0.00"))));

                } else {
                    Log.e("UPrice product_id: ", "" + getPreference("memo" + itemListContent.get(position).get("product_id")));
                    Log.e("SalesUPrice: ", TempData.SalesUPricePosition + "00");

                    if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("0")) {
                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(general_price))));

                    } else if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("")) {
                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(general_price))));

                    } else {
                        UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(getPreference("memo" + itemListContent.get(position).get("product_id"))))));
                        T_Price = Double.parseDouble(getPreference("memo" + itemListContent.get(position).get("product_id"))) * _Quantity;

                        Log.e("total_price", itemListContent.get(position).get("product_id"));
                    }

                    QuantityEd.setText(Quantity);
                }

                Log.e("SCROLLING PRICE:", "......." + T_Price);
                Log.e("Valuettt :", getPreference("memo" + mapContent.get("product_id")) + "   n");

                TPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("" + T_Price))));

                if (TempData.editMemo.equalsIgnoreCase("true")) { // if memo in edit moad then & open  price applied then open price will show from memo
                    Log.e("Valuettt", getPreference("memo" + mapContent.get("product_id")));
                    if (getPreference("memo" + itemListContent.get(position).get("product_id")).equalsIgnoreCase("0")) {
                        Log.e("Value", getPreference("up" + mapContent.get("product_id")));
                        UPrice.setText(String.valueOf(((Parent) context).roundTwoDecimals(Double.parseDouble(general_price))));

                    } else {
                        Log.e("SalesUPrice_befor: ", TempData.SalesUPricePosition + "00");

                       UPrice.setText(String.valueOf(roundTwoDecimals(Double.parseDouble(getPreference("memo" + mapContent.get("product_id"))))));
                    }

                }

            }

        }

        // TODO Auto-generated method stub
        QuantityEd.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
                Log.e("AFTER:", s.toString());

                if (s.toString().length() == 0) {
                    itemListContent.get(position).put("quantity", "0.00");
                    String query = "UPDATE product_boolean SET quantity = " + "'" + s.toString() + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'";
                    db.excQuery(query);
                    Log.e("QUERY", query);
                    UpdatePrice();
                } else {
                    if (isDouble(s.toString())) {

                        String QUANTITY = "";

                        Log.e("quantity..........", "" + QUANTITY);
                        Log.e("QuantityEd quantity: ", "" + QuantityEd.getText().toString());
                        QUANTITY = s.toString();
                        String query = "UPDATE product_boolean SET quantity = " + "'" + QUANTITY + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'";
                        Log.e("QUERY_Quantity", query);
                        db.excQuery(query);
                        itemListContent.get(position).put("quantity", QUANTITY);
                        showLimitExeednotification(Double.parseDouble(QUANTITY), itemListContent.get(position).get("product_id"));
                        UpdatePrice();
                    } else {
                        Toast.makeText(context, "Please enter correct number format!", Toast.LENGTH_LONG).show();
                        QuantityEd.setText("");

                    }
                }
            }
        });


        // TODO Auto-generated method stub
        UPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().equals(".")) {
                    Log.e("after_edit_text_change", s.toString());
                }

             */
/*   Log.e("after_edit_text_change", price.toString());
                  if (price.toString().length() == 0) {
                  String query = "UPDATE product_combinations SET price = " + "'" + price.toString() + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'";
                    db.excQuery(query);
                    Log.e("QUERY", query);
                    UpdatePrice();
                }

                 else {
                    if (isDouble(price.toString())) {

                        String UPrice = "";

                        Log.e("UPrice..........", "" + UPrice);
                        Log.e("QuantityEd quantity: ", "" + QuantityEd.getText().toString());

                        UPrice = price.toString();

                        String query = "UPDATE product_combinations SET price = " + "'" + UPrice + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'";
                        Log.e("QUERY_UPrice", query);
                        db.excQuery(query);
                        itemListContent.get(position).put("price", UPrice);

                        showLimitExeednotification(Double.parseDouble(UPrice), itemListContent.get(position).get("product_id"));

                        UpdatePrice();

                    } else {
                        Toast.makeText(context, "Please enter correct number format!", Toast.LENGTH_LONG).show();
                        QuantityEd.setText("");
                    }
                }*//*


                // TODO Auto-generated method stub
                Log.e("AFTER:", s.toString());
                Log.e("Position Logg:", "" + position);


                //assign position string value to a temp variable
                TempData.SalesUPricePosition = "" + position;

                TempData.SalesUPrice = "" + s.toString();
                savePreference("memo" + itemListContent.get(position).get("product_id"), s.toString());
                Log.e("product_id_Position", itemListContent.get(position).get("product_id"));
                Log.e("product_id_Sales", getPreference(itemListContent.get(position).get("product_id")));
                Log.e("product__price", TempData.SalesUPrice);

                if (s.toString().length() == 0) {
                    UpdatePrice();
                } else {
                    if (isDouble(s.toString())) {
                        HashMap<String, String> mapContent = new HashMap<String, String>();
                        mapContent = itemListContent.get(position);
                        //product_name= mapContent.get("product_name");
                        String Quantity = mapContent.get("quantity");

                        double giventQty = Double.parseDouble(Quantity);
                        double stockqty = 0;

                        String QUANTITY = "";

                        QUANTITY = "" + giventQty;

                        Log.e("quantity..........", "" + price.toString());

                        UpdatePrice();

                    } else {
                        Toast.makeText(context, "Please enter correct number format!", Toast.LENGTH_LONG).show();
                        QuantityEd.setText("");
                    }
                }
            }
        });

        ActionBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Log.e("positionclick", position + " size " + itemListContent.size());
                db.excQuery("UPDATE product_boolean SET boolean = " + "'" + "false" + "'" + ", quantity=" + "'" + "" + "'" + " WHERE product_id = " + "'" + itemListContent.get(position).get("product_id") + "'" + " AND outlet_id='" + outlet_ID + "'");
                Toast.makeText(context, "Product has deleted successfully", Toast.LENGTH_LONG).show();

                Log.e("ProductID_FromPosition", itemListContent.get(position).get("product_id"));

                savePreference(itemListContent.get(position).get("product_id"), "0.0");
                savePreference("memo" + itemListContent.get(position).get("product_id"), "0");
                itemListContent.remove(position);
                notifyDataSetChanged();

                UpdatePrice();

            }
        });

        return view2;
    }

    public boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void UpdatePrice() {

        ArrayList<ArrayList<String>> ArrCombine = new ArrayList<ArrayList<String>>();
        ArrayList<String> slaveID = new ArrayList<String>();
        ArrayList<String> generalID = new ArrayList<String>();
        ArrayList<String> combineID = new ArrayList<String>();
        ArrayList<String> inList = new ArrayList<String>();
        ArrayList<String> quantity = new ArrayList<String>();

        for (int i = 0; i < itemListContent.size(); i++) {
            inList.add(itemListContent.get(i).get("product_id"));
            Log.e("IDDDDDDDDDDDDDDDDDD:", itemListContent.get(i).get("product_id"));
        }
        for (int i = 0; i < itemListContent.size(); i++) {
            quantity.add(itemListContent.get(i).get("quantity"));
            savePreference(itemListContent.get(i).get("product_id"), itemListContent.get(i).get("quantity"));

            Log.e("quantity:", itemListContent.get(i).get("quantity"));
        }

        HashMap<String, String> priceMap = new HashMap<String, String>();

        String byPassNow = "YES";
        if (!TargetCustomer.equalsIgnoreCase("0")) {
            //for (int i = 0; i < inList.size(); i++)
            //    priceMap.put(inList.get(i), NGO_price(inList.get(i), quantity.get(i)));
        }

        if (TargetCustomer.equalsIgnoreCase("0") || byPassNow == "YES") {
            InstietuteID = "0";

            for (int i = 0; i < inList.size(); i++) {
                Log.e("PRISING TYPE: ", inList.get(i) + ": " + CheckPriceType(inList.get(i)));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("COMBINE"))
                    combineID.add(inList.get(i));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("SLAVE"))
                    slaveID.add(inList.get(i));
                if (CheckPriceType(inList.get(i)).equalsIgnoreCase("GENERAL"))
                    generalID.add(inList.get(i));
            }


            Log.e("COMBINE SIZE:", "" + combineID.size());
            String combination_id = "";
            for (int i = 0; i < combineID.size(); i++) {
                Cursor cursor = db.rawQuery("SELECT combination_id FROM product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + combineID.get(i) + "' AND effective_date<='" + memodate + "')");


                Log.e("OUTER_QUERY:", "SELECT combination_id FROM product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0");
                Log.e("OUTER_QUERY_COUNT", "" + cursor.getCount());
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            combination_id = cursor.getString(0);
                            Cursor cursor1 = db.rawQuery("SELECT * FROM product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY:", "SELECT * FROM product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY_COUNT:", "" + cursor1.getCount());
                            if (cursor1 != null) {
                                if (cursor1.moveToFirst()) {
                                    ArrayList<String> combinationList = new ArrayList<String>();
                                    do {
                                        //.....combine id added  to list.......
                                        if (!combinationList.contains(cursor1.getString(1)))
                                            combinationList.add(cursor1.getString(1));
                                        Log.e("CombineID", cursor1.getString(1));

                                    } while (cursor1.moveToNext());

                                    if (ArrCombine.size() == 0) {
                                        ArrCombine.add(combinationList);
                                        Log.e("added to combine list", "added");
                                    } else {
                                        if (ArrCombine.contains(combinationList))
                                            Log.e("Duplicate", "True");
                                        else
                                            ArrCombine.add(combinationList);
                                    }
                                }
                            }


                        } while (cursor.moveToNext());
                    }
                }
            }

            Log.e("COMBINE ARRAY:", ArrCombine.toString());
            Log.e("ArrCombine.size()", ArrCombine.size() + "");
            Log.e("QUANTITY ARRAY", quantity.toString());

            for (int i = 0; i < ArrCombine.size(); i++) {
                double QUANTITY = 0;

                ArrayList<String> arrCheck = ArrCombine.get(i);
                Log.e("ArraySize:", arrCheck.size() + "");
                String comProduct = combineID.get(i);
                int index = inList.indexOf(combineID.get(i));

                for (int j = 0; j < arrCheck.size(); j++) {

                    Log.e(".............", ".............");
                    Log.e("COMBINE ID:", arrCheck.get(j));
                    if (!getPreference(arrCheck.get(j)).equalsIgnoreCase(""))
                        QUANTITY = QUANTITY + Double.parseDouble(getPreference(arrCheck.get(j)));

                }
                Log.e("Given....QUANTITY", "" + QUANTITY);
                Log.e("INDi....inList", "" + inList.toString());
                Log.e("INDi....arrCheck", "" + arrCheck.toString());
                /// sql query to find combined quantity

                Double Combined_Quantity = 0.0;

                Cursor cursor = db.rawQuery("SELECT combination_id,min_quantity FROM product_combinations WHERE product_id= '" + arrCheck.get(0) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(0) + "' AND effective_date<='" + memodate + "')");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
//                            Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            if (QUANTITY >= Double.parseDouble(cursor.getString(1))) {
                                Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            }
                        } while (cursor.moveToNext());
                    }
                }

                Log.e("Combined_Quantity: ", "" + Combined_Quantity);

                if (QUANTITY >= Combined_Quantity) {

                    Log.e("QUANTITY111", "" + QUANTITY);

                    String ProductCombinationID = "";
//                    String query3="SELECT combination_id FROM product_combinations WHERE product_id='"+ArrCombine.get(i).get(0) +"' AND combination_id!=0  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='"+ArrCombine.get(i).get(0)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                    String query3 = "SELECT combination_id FROM product_combinations WHERE product_id='" + ArrCombine.get(i).get(0) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + "  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + ArrCombine.get(i).get(0) + "' AND effective_date<='" + memodate + "')";
                    Cursor cursor3 = db.rawQuery(query3);
                    if (cursor3.moveToFirst()) {
                        do {
                            ProductCombinationID = cursor3.getString(0);
                        } while (cursor3.moveToNext());
                    }

                    Log.e("ProductCombinationID", "..........." + ProductCombinationID);
                    if (CheckCombineQuantity(QUANTITY, ArrCombine.get(i).get(0), ProductCombinationID)) {
                        for (int j = 0; j < arrCheck.size(); j++) {
                            String combinationID = "";
//                            String query1="SELECT combination_id FROM product_combinations WHERE product_id='"+arrCheck.get(j) +"' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='"+arrCheck.get(j)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                            String query1 = "SELECT combination_id FROM product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + " AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "')";
                            Log.e("query1", query1);
                            Cursor cursor1 = db.rawQuery(query1);
                            if (cursor1.moveToFirst()) {
                                do {
                                    combinationID = cursor1.getString(0);
                                } while (cursor1.moveToNext());
                            }

                            Log.e("combinationID", combinationID);
                             String query2 = "SELECT price From product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id='" + combinationID + "' AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "') AND min_quantity<=" + QUANTITY + "  order by effective_date LIMIT 1";
                           // String query2 = "SELECT price From order_details WHERE product_id='" + arrCheck.get(j);
                            Log.e("COMBINATION PRICE QRY:", query2);
                            Cursor cursor2 = db.rawQuery(query2);
                            if (cursor2.moveToFirst()) {
                                do {
                                    Log.e("COMBINATION_Price:", cursor2.getString(0));
                                    savePreference("memo" + arrCheck.get(j), "0");

                                    priceMap.put(arrCheck.get(j), cursor2.getString(0));
                                } while (cursor2.moveToNext());
                            }
                        }
                    } else {
                        slaveID.addAll(ArrCombine.get(i));
                        //ArrCombine.remove(i);
                        Log.e("RETURN1", "return1");
                    }
                } else {
                    slaveID.addAll(ArrCombine.get(i));
                }


            }
        }

        Log.e("..........", "......................");


        for (int i = 0; i < inList.size(); i++)
            Log.e("Selected ID", inList.get(i));


        Log.e("SLIVE SIZE:", "" + slaveID.size());
        for (int i = 0; i < slaveID.size(); i++) {
            if (inList.contains(slaveID.get(i))) {
                int index = inList.indexOf(slaveID.get(i));
                Log.e("quqntity", quantity.get(index));
                double Quantity = Double.parseDouble(quantity.get(index));
                if (!priceMap.containsKey(slaveID.get(i)))
                    priceMap.put(slaveID.get(i), CheckSlapPrice(Quantity, slaveID.get(i)));
            }

        }

        for (int i = 0; i < generalID.size(); i++) {
            if (!priceMap.containsKey(generalID.get(i)))
                priceMap.put(generalID.get(i), General_price(generalID.get(i)));

        }

        for (int i = 0; i < inList.size(); i++) {
            Log.e("ID: " + inList.get(i), "Price:" + priceMap.get(inList.get(i)));
            Log.e("...........", "......................");
            Log.e("...........", "..........product_id............" + getPreference("memo" + itemListContent.get(i).get("product_id")));


            if (getPreference("memo" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("0")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));

                Log.e("vvv_gen_update", "vvv value=  " + itemListContent.get(i).put("general_price", priceMap.get(inList.get(i))));
            } else if (getPreference("memo" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));

                Log.e("vvv", "vvv value=  " + i);
                Log.e("vvv_gen_updat", "vvv value=  " + itemListContent.get(i).put("general_price", priceMap.get(inList.get(i))));

            } else {
                itemListContent.get(i).put("general_price", getPreference("memo" + itemListContent.get(i).get("product_id")));

                Log.e("", "++++++++++ general_price  " + getPreference("memo" + itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  " + i);
                Log.e("vvv_gen_upda", "vvv value=  " + itemListContent.get(i).put("general_price", priceMap.get(inList.get(i))));

            }

        }

        sum = 0.0;
        vat = 0.0;

        for (int i = 0; i < itemListContent.size(); i++) {
            HashMap<String, String> map = itemListContent.get(i);

            sum = sum + (Double.parseDouble(map.get("general_price")) * Double.parseDouble(map.get("quantity")));

            vatCalculation(map.get("product_id"), Double.parseDouble(map.get("quantity")), Double.parseDouble(map.get("general_price")), i);
        }

        txtTotal.setText(String.valueOf(roundTwoDecimals(Double.parseDouble("" + sum))));//;//String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum))));
        getDiscount(roundTwoDecimals(Double.parseDouble("" + sum)));

        vattxt.setText(roundTwoDecimal(vat));

        BonusCalculation(itemListContent);
        notifyDataSetChanged();
    }

    private double setAdvanceText(String orderNumber) {
        double advance = 0.0;
        Cursor c = db.rawQuery("select advance_collection from order_table where order_number='" + orderNumber + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {

            advance = c.getDouble(0);
        }

        Log.e("memos ", advance + "");
        if (TempData.editMemo.equalsIgnoreCase("true")) {

            return advance;
        } else

            return 0.0;

    }

    private void vatCalculation(String product_id, Double quantity, Double price, int pos) {
        Double vatprice = 0.0, vatofvat = 0.0;
        Cursor c = db.rawQuery("select vat from product_price where product_id='" + product_id + "' and vat!='null'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            vatprice = (price * 100) / (100 + c.getDouble(0));
            vatofvat = (price - vatprice);
            itemListContent.get(pos).put("vat", c.getDouble(0) + "");
            vat = vat + (vatofvat * quantity);
        }

    }

    private void getDiscount(String s) {
        int distype = 0;
        discountp = 0.0;
        Cursor cursor = db.rawQuery("select discount_percent,discount_type from discounts where memo_value <=" + s + " and date_from <= '" + memodate + "' and date_to>='" + memodate + "' order by memo_value DESC limit 1");
        Log.e("DisQuery", "select discount_percent,discount_type from discounts where memo_value <=" + s + " and date_from <= '" + memodate + "' and date_to>='" + memodate + "' order by memo_value DESC limit 1");
        cursor.moveToFirst();
        if (cursor != null && cursor.getCount() > 0) {

            discountp = cursor.getDouble(0);
            distype = TempData.DISTYPE = cursor.getInt(1);
        }
        Double discount = 0.0;
        Double memoValue = Double.parseDouble(s);
        if (distype == 1) {
            TempData.DISCOUNTP = discountp;
            discount = (memoValue * discountp) / 100;
        } else if (distype == 2) {

            discount = discountp;
        }
        txtdiscount.setText(roundTwoDecimals(discount));
        subt.setText(roundTwoDecimals(memoValue - discount - setAdvanceText(TempData.orderNumber)));
    }

    public ArrayList<HashMap<String, String>> getAdapterHashMapList() {
        return returnContent;
    }

    public ArrayList<HashMap<String, String>> getBonusMapList() {
        return Bonus;
    }


    public void BonusCalculation(ArrayList<HashMap<String, String>> itemListContent) {
        TextView txtBonus = (TextView) ((Activity) context).findViewById(R.id.txtBonus);
        txtBonus.setText("");

        Bonus.clear();

        for (int i = 0; i < itemListContent.size(); i++) {
            String givenQty = itemListContent.get(i).get("quantity");
            String ProductID = itemListContent.get(i).get("product_id");
            Log.e("BONUS_QUANTITY------", givenQty);
            Log.e("PRODUCT_ID------", ProductID);

            //String currentDate=getCurrentDate();
            //Log.e("CURRENTDATE",currentDate);

            String query = "SELECT B.bonus_product_id, P.product_name, B.bonus_product_qty, B.mother_product_qty FROM bonuses B LEFT JOIN product P ON(B.bonus_product_id=P.product_id)  WHERE B.mother_product_id='" + ProductID + "' AND  B.mother_product_qty<='" + givenQty + "' AND '" + memodate + "'>=B.start_date  AND '" + memodate + "'<=B.end_date limit 1";
            Log.e("BONUS_QUERY:", query);

            Cursor cursor = db.rawQuery(query);
            Log.e("COURSOR_COUNT:", "" + cursor.getCount());
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("product_id", cursor.getString(0));
                        map.put("item", cursor.getString(1));

                        double bonusQty = Double.parseDouble(cursor.getString(2)); //1
                        double tableQty = Double.parseDouble(cursor.getString(3));//12

                        double qty = (Double.parseDouble(givenQty) / tableQty) * bonusQty; //4.66*1.0

                        qty = (int) qty; // 4
                        String QuantityCheck = "";

                        Log.e("QUANTITY_TABLE:", "" + tableQty);
                        Log.e("QUANTITY_GIVEN:", "" + givenQty);
                        Log.e("QUANTITY_PARSE:", "" + qty);

                     */
/*   double stockQty=0;
                        String stockQtyquey="SELECT SUM(quantity) as quantity FROM  van_stocks WHERE product_id='"+ProductID+ "'";
                        Log.e("STOCK QUERY:", stockQtyquey);
                        Cursor cursor1=db.rawQuery(stockQtyquey);
                        if(cursor1!=null)
                        {
                            if(cursor1.moveToFirst())
                            {
                                do{
//                                    stockQty=Double.parseDouble(cursor1.getString(0));  //0 edit
                                    stockQty=cursor1.getDouble(0);  //0 edit 14.05.2018

                                    if(TempData.editMemo.equalsIgnoreCase("true")){
                                        ///TempData.orderNumber memo no
                                        /// query and get all memo details from memo_details table with memo no and product id cursor.getString(0))
                                        // loop through the memo details records and get quantity for cursor.getString(0)) product

                                        String MemoTablequey="SELECT SUM(quantity) as quantity FROM  memo_details WHERE product_id='"+ProductID+ "' and memo_no='"+ TempData.orderNumber +"'";
                                        Cursor cur=db.rawQuery(MemoTablequey);
                                        if(cur!=null)
                                        {
                                            if(cur.moveToFirst())
                                            {
                                                do{
                                                    stockQty = stockQty + cur.getDouble(0);
                                                }while(cur.moveToNext());
                                            }
                                        }
                                    }

                                }while(cursor1.moveToNext());
                            }
                        }*//*



                        if (getIndex(cursor.getString(0)) < 0) {
                            Log.e("String.valueOf(qty)", String.valueOf(qty));
                            map.put("quantity", String.valueOf(qty));
                            Bonus.add(map);
                        } else {
                            double QUANTITY = Double.parseDouble(Bonus.get(getIndex(cursor.getString(4))).get("quantity")) + qty;
                            Log.e("Quantity", String.valueOf(QUANTITY));
                            Bonus.get(getIndex(cursor.getString(4))).put("quantity", String.valueOf(QUANTITY));  ///// Why index 4 where as query has 3 indexes
                        }
                    } while (cursor.moveToNext());
                }
            }

        }

        String bonusString = "";
        TempData.BonusShowList.clear();
        for (int i = 0; i < Bonus.size(); i++) {
            bonusString = bonusString + Bonus.get(i).get("item") + "(" + Bonus.get(i).get("quantity") + ")";

            HashMap<String, String> product_map = new HashMap<String, String>();

            product_map.put("bonus_name", Bonus.get(i).get("item"));
            product_map.put("quantity", Bonus.get(i).get("quantity"));


//            maplist.add(product_map);
            TempData.BonusShowList.add(product_map);


            if (i != Bonus.size() - 1)
                bonusString = bonusString + ", ";
        }


        if (bonusString.length() > 0) {
            Log.e("bonusString", bonusString);
            txtBonus.setText(bonusString);
            txtBonus.setVisibility(View.VISIBLE);
            TempData.Test = BonusItemList.toString();
        } else
//            txtBonus.setText("");
            txtBonus.setVisibility(View.GONE);

    }


    public String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String CurrentDate = "" + dateFormat.format(date);

        return CurrentDate;

    }

    public String roundTwoDecimals(double d) {


        Log.e("tEST:", String.format("%.2f", d));


        Log.e("Double:", String.valueOf(d));
        Log.e("Modulus:", String.valueOf(d % 1));

		 */
/* if(d%1>0.0)
		  {

	     DecimalFormat twoDForm = new DecimalFormat("#.##");
	     String value= String.valueOf(twoDForm.format(d));
	     Log.e("value:", ""+value);
	     Log.e("value.indexOf('.'):", ""+value.indexOf('.'));
	     Log.e("value.length:", ""+value.length());
	     String subValue=value.substring(value.indexOf('.'),value.length());
	     Log.e("LENGTH:", subValue);
	              //if(subValue.length()>1)
	        	 //return String.valueOf(twoDForm.format(d));
	             // else
	 	        	 //return String.valueOf(twoDForm.format(d))+"0";


		  }
		  else
		  {

			     //return String.valueOf(d)+"0";
		  }*//*


        return String.format("%.2f", d);
    }

    public void UpdateDistPrice() {
        ArrayList<ArrayList<String>> ArrCombine = new ArrayList<ArrayList<String>>();
        ArrayList<String> slaveID = new ArrayList<String>();
        ArrayList<String> generalID = new ArrayList<String>();
        ArrayList<String> combineID = new ArrayList<String>();
        ArrayList<String> inList = new ArrayList<String>();
        ArrayList<String> quantity = new ArrayList<String>();

        for (int i = 0; i < itemListContent.size(); i++) {
            inList.add(itemListContent.get(i).get("product_id"));
            Log.e("IDDDDDDDDDDDDDDDDDD:", itemListContent.get(i).get("product_id"));
        }
        for (int i = 0; i < itemListContent.size(); i++) {
            quantity.add(itemListContent.get(i).get("quantity"));
            savePreference(itemListContent.get(i).get("product_id"), itemListContent.get(i).get("quantity"));

            Log.e("quantity:", itemListContent.get(i).get("quantity"));
        }

        HashMap<String, String> priceMap = new HashMap<String, String>();

        String byPassNow = "YES";
        if (!TargetCustomer.equalsIgnoreCase("0")) {
            //for(int i=0;i<inList.size();i++)
            //priceMap.put(inList.get(i), NGO_price(inList.get(i),quantity.get(i)));
        }

        if (TargetCustomer.equalsIgnoreCase("0") || byPassNow == "YES") {
            InstietuteID = "0";

            for (int i = 0; i < inList.size(); i++) {
                Log.e("PRISING TYPE: ", inList.get(i) + ": " + DistCheckPriceType(inList.get(i)));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("COMBINE"))
                    combineID.add(inList.get(i));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("SLAVE"))
                    slaveID.add(inList.get(i));
                if (DistCheckPriceType(inList.get(i)).equalsIgnoreCase("GENERAL"))
                    generalID.add(inList.get(i));
            }


            Log.e("COMBINE SIZE:", "" + combineID.size());
            String combination_id = "";
            for (int i = 0; i < combineID.size(); i++) {
                Cursor cursor = db.rawQuery("SELECT combination_id FROM distributor_product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + combineID.get(i) + "' AND effective_date<='" + memodate + "')");


                Log.e("OUTER_QUERY:", "SELECT combination_id FROM distributor_product_combinations WHERE product_id= '" + combineID.get(i) + "' AND combination_id!=0");
                Log.e("OUTER_QUERY_COUNT", "" + cursor.getCount());
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
                            combination_id = cursor.getString(0);
                            Cursor cursor1 = db.rawQuery("SELECT * FROM distributor_product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY:", "SELECT * FROM distributor_product_combinations WHERE combination_id= '" + cursor.getString(0) + "'");
                            Log.e("INNER QUERY_COUNT:", "" + cursor1.getCount());
                            if (cursor1 != null) {
                                if (cursor1.moveToFirst()) {
                                    ArrayList<String> combinationList = new ArrayList<String>();
                                    do {
                                        //.....combine id added  to list.......
                                        if (!combinationList.contains(cursor1.getString(1)))
                                            combinationList.add(cursor1.getString(1));
                                        Log.e("CombineID", cursor1.getString(1));

                                    } while (cursor1.moveToNext());

                                    if (ArrCombine.size() == 0) {
                                        ArrCombine.add(combinationList);
                                        Log.e("added to combine list", "added");
                                    } else {
                                        if (ArrCombine.contains(combinationList))
                                            Log.e("Duplicate", "True");
                                        else
                                            ArrCombine.add(combinationList);
                                    }
                                }
                            }


                        } while (cursor.moveToNext());
                    }
                }
            }

            Log.e("COMBINE ARRAY:", ArrCombine.toString());
            Log.e("ArrCombine.size()", ArrCombine.size() + "");
            Log.e("QUANTITY ARRAY", quantity.toString());

            for (int i = 0; i < ArrCombine.size(); i++) {
                double QUANTITY = 0;

                ArrayList<String> arrCheck = ArrCombine.get(i);
                Log.e("ArraySize:", arrCheck.size() + "");
                String comProduct = combineID.get(i);
                int index = inList.indexOf(combineID.get(i));

                for (int j = 0; j < arrCheck.size(); j++) {

                    Log.e(".............", ".............");
                    Log.e("COMBINE ID:", arrCheck.get(j));
                    if (!getPreference(arrCheck.get(j)).equalsIgnoreCase(""))
                        QUANTITY = QUANTITY + Double.parseDouble(getPreference(arrCheck.get(j)));

                }
                Log.e("Given....QUANTITY", "" + QUANTITY);
                Log.e("INDi....inList", "" + inList.toString());
                Log.e("INDi....arrCheck", "" + arrCheck.toString());
                /// sql query to find combined quantity

                Double Combined_Quantity = 0.0;

                Cursor cursor = db.rawQuery("SELECT combination_id,min_quantity FROM distributor_product_combinations WHERE product_id= '" + arrCheck.get(0) + "' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(0) + "' AND effective_date<='" + memodate + "')");
                if (cursor != null) {
                    if (cursor.moveToFirst()) {
                        do {
//                            Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            if (QUANTITY >= Double.parseDouble(cursor.getString(1))) {
                                Combined_Quantity = Double.parseDouble(cursor.getString(1));
                            }
                        } while (cursor.moveToNext());
                    }
                }

                Log.e("Combined_Quantity: ", "" + Combined_Quantity);

//		    	     if(inList.containsAll(arrCheck))
                if (QUANTITY >= Combined_Quantity) {

                    //Log.e("ArrCombine.get(i).get(index))",""+ArrCombine.get(i).get(index));
                    //Log.e("ArrCombine.get(index).get(i))",""+ArrCombine.get(index).get(i));
                    Log.e("QUANTITY111", "" + QUANTITY);

                    String ProductCombinationID = "";
//                    String query3="SELECT combination_id FROM distributor_product_combinations WHERE product_id='"+ArrCombine.get(i).get(0) +"' AND combination_id!=0  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='"+ArrCombine.get(i).get(0)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                    String query3 = "SELECT combination_id FROM distributor_product_combinations WHERE product_id='" + ArrCombine.get(i).get(0) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + "  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + ArrCombine.get(i).get(0) + "' AND effective_date<='" + memodate + "')";
                    Cursor cursor3 = db.rawQuery(query3);
                    if (cursor3.moveToFirst()) {
                        do {
                            ProductCombinationID = cursor3.getString(0);
                        } while (cursor3.moveToNext());
                    }

                    Log.e("ProductCombinationID", "..........." + ProductCombinationID);
                    if (DistCheckCombineQuantity(QUANTITY, ArrCombine.get(i).get(0), ProductCombinationID)) {
                        for (int j = 0; j < arrCheck.size(); j++) {
                            String combinationID = "";
//                            String query1="SELECT combination_id FROM distributor_product_combinations WHERE product_id='"+arrCheck.get(j) +"' AND combination_id!=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='"+arrCheck.get(j)+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
                            String query1 = "SELECT combination_id FROM distributor_product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id!=0 AND min_quantity<=" + QUANTITY + " AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "')";
                            Log.e("query1", query1);
                            Cursor cursor1 = db.rawQuery(query1);
                            if (cursor1.moveToFirst()) {
                                do {
                                    combinationID = cursor1.getString(0);
                                } while (cursor1.moveToNext());
                            }

                            Log.e("combinationID", combinationID);
                            String query2 = "SELECT price From distributor_product_combinations WHERE product_id='" + arrCheck.get(j) + "' AND combination_id='" + combinationID + "' AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + arrCheck.get(j) + "' AND effective_date<='" + memodate + "') AND min_quantity<=" + QUANTITY + "  order by effective_date LIMIT 1";
                            Log.e("COMBINATION PRICE QRY:", query2);
                            Cursor cursor2 = db.rawQuery(query2);
                            if (cursor2.moveToFirst()) {
                                do {
                                    Log.e("COMBINATION_Price:", cursor2.getString(0));
                                    priceMap.put(arrCheck.get(j), cursor2.getString(0));
                                } while (cursor2.moveToNext());
                            }
                        }
                    } else {
                        slaveID.addAll(ArrCombine.get(i));
                        //ArrCombine.remove(i);
                        Log.e("RETURN1", "return1");
                    }
                } else {
                    slaveID.addAll(ArrCombine.get(i));
                    //ArrCombine.remove(i);
                }


            }
        }

        Log.e("..........", "......................");


        for (int i = 0; i < inList.size(); i++)
            Log.e("Selected ID", inList.get(i));


        Log.e("SLIVE SIZE:", "" + slaveID.size());
        for (int i = 0; i < slaveID.size(); i++) {
            if (inList.contains(slaveID.get(i))) {
                int index = inList.indexOf(slaveID.get(i));
                double Quantity = Double.parseDouble(quantity.get(index));
                if (!priceMap.containsKey(slaveID.get(i)))
                    priceMap.put(slaveID.get(i), DistCheckSlapPrice(Quantity, slaveID.get(i)));
            }

        }

        for (int i = 0; i < generalID.size(); i++) {
            if (!priceMap.containsKey(generalID.get(i)))
                priceMap.put(generalID.get(i), DistGeneral_price(generalID.get(i)));
            //priceMap.put(generalID.get(i), "0");
        }

        for (int i = 0; i < inList.size(); i++) {
            Log.e("ID: " + inList.get(i), "Price:" + priceMap.get(inList.get(i)));
            Log.e("...........", "......................");
            Log.e("...........", "..........product_id............" + getPreference("up" + itemListContent.get(i).get("product_id")));

            */
/*if(!getPreference("up"+itemListContent.get(i).get("product_id")).equalsIgnoreCase("0"))
            {
                itemListContent.get(i).put("general_price", getPreference("up"+itemListContent.get(i).get("product_id")));
                Log.e("", "++++++++++ general_price  "+getPreference("up"+itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  "+i);
            }else {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  "+i);
            }*//*



            if (getPreference("up" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("0")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else if (getPreference("up" + itemListContent.get(i).get("product_id")).equalsIgnoreCase("")) {
                itemListContent.get(i).put("general_price", priceMap.get(inList.get(i)));
                Log.e("vvv", "vvv value=  " + i);
            } else {
                itemListContent.get(i).put("general_price", getPreference("up" + itemListContent.get(i).get("product_id")));
                Log.e("", "++++++++++ general_price  " + getPreference("up" + itemListContent.get(i).get("product_id")));
                Log.e("", "i value=  " + i);
            }


        }
        sum = 0.0;

        for (int i = 0; i < itemListContent.size(); i++) {
            HashMap<String, String> map = itemListContent.get(i);
            Log.e("Tgeneral price list", map.get("general_price"));
            Log.e("", "XXXXXXXXXXXXXXXXXXXXXXXXX  general_price  " + map.get("general_price"));
            Log.e("", "XXXXXXXXXXXXXXXXXXXXXXXXX  quantity " + map.get("quantity"));
            sum = sum + (Double.parseDouble(map.get("general_price")) * Double.parseDouble(map.get("quantity")));
        }
        txtTotal.setText(String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble("" + sum))));//;//String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum))));
        getDiscount(roundTwoDecimals(Double.parseDouble("" + sum)));

        */
/*if(TempData.InstituteID.equalsIgnoreCase("0")|| TempData.InstituteID.equalsIgnoreCase(""))
            etPayment.setText(txtTotal.getText().toString());

        double paymentAmunt=0.0;
        double totalAmount=0.0;
        totalAmount= Double.parseDouble(txtTotal.getText().toString());

        paymentAmunt= Double.parseDouble(etPayment.getText().toString());
        if(paymentAmunt>totalAmount)
        {
            txtDueChanged.setText("Change:");
            //String String.valueOf(((ParentActivity) context).roundTwoDecimals(Double.parseDouble(""+sum)));
            txtCredit.setText(String.valueOf(paymentAmunt-totalAmount));


        }
        else
        {
            txtDueChanged.setText("Due:");
            txtCredit.setText(String.valueOf(totalAmount-paymentAmunt));
        }*//*


        // DistBonusCalculation(itemListContent);
        notifyDataSetChanged();
    }

    public void DistBonusCalculation(ArrayList<HashMap<String, String>> itemListContent) {
        TextView txtBonus = (TextView) ((Activity) context).findViewById(R.id.txtBonus);
        txtBonus.setText("");

        Bonus.clear();

        for (int i = 0; i < itemListContent.size(); i++) {
            String givenQty = itemListContent.get(i).get("quantity");
            String ProductID = itemListContent.get(i).get("product_id");
            Log.e("BONUS_QUANTITY------", givenQty);
            Log.e("PRODUCT_ID------", ProductID);

            //String currentDate=getCurrentDate();
            //Log.e("CURRENTDATE",currentDate);

            String query = "SELECT B.bonus_product_id, P.product_name, B.bonus_product_qty, B.mother_product_qty FROM bonuses B LEFT JOIN products P ON(B.bonus_product_id=P.product_id)  WHERE B.mother_product_id='" + ProductID + "' AND  B.mother_product_qty<='" + givenQty + "' AND '" + memodate + "'>=B.start_date  AND '" + memodate + "'<=B.end_date limit 1";
            Log.e("BONUS_QUERY:", query);

            Cursor cursor = db.rawQuery(query);
            Log.e("COURSOR_COUNT:", "" + cursor.getCount());
            if (cursor.getCount() > 0) {
                if (cursor.moveToFirst()) {
                    do {
                        HashMap<String, String> map = new HashMap<String, String>();
                        map.put("product_id", cursor.getString(0));
                        map.put("item", cursor.getString(1));

                        double bonusQty = Double.parseDouble(cursor.getString(2)); //1
                        double tableQty = Double.parseDouble(cursor.getString(3));//12

                        double qty = (Double.parseDouble(givenQty) / tableQty) * bonusQty; //4.66*1.0

                        qty = (int) qty; // 4
                        String QuantityCheck = "";

                        Log.e("QUANTITY_TABLE:", "" + tableQty);
                        Log.e("QUANTITY_GIVEN:", "" + givenQty);
                        Log.e("QUANTITY_PARSE:", "" + qty);

                        double stockQty = 0;
                        String stockQtyquey = "SELECT SUM(quantity) as quantity FROM  van_stocks WHERE product_id='" + ProductID + "'";
                        Log.e("STOCK QUERY:", stockQtyquey);
                        Cursor cursor1 = db.rawQuery(stockQtyquey);
                        if (cursor1 != null) {
                            if (cursor1.moveToFirst()) {
                                do {
//                                    stockQty=Double.parseDouble(cursor1.getString(0));  //0 edit
                                    stockQty = cursor1.getDouble(0);  //0 edit 14.05.2018

                                    if (TempData.editMemo.equalsIgnoreCase("true")) {
                                        ///TempData.orderNumber memo no
                                        /// query and get all memo details from memo_details table with memo no and product id cursor.getString(0))
                                        // loop through the memo details records and get quantity for cursor.getString(0)) product

                                        String MemoTablequey = "SELECT SUM(quantity) as quantity FROM  memo_details WHERE product_id='" + ProductID + "' and memo_no='" + TempData.orderNumber + "'";
                                        Cursor cur = db.rawQuery(MemoTablequey);
                                        if (cur != null) {
                                            if (cur.moveToFirst()) {
                                                do {
                                                    stockQty = stockQty + cur.getDouble(0);
                                                } while (cur.moveToNext());
                                            }
                                        }
                                    }

                                } while (cursor1.moveToNext());
                            }
                        }

                        double GivenAndBonusQty = Double.parseDouble(givenQty) + qty;  //22 edit
                        Log.e("stockQty", "" + stockQty);
                        Log.e("GivenAndBonusQty", "" + GivenAndBonusQty);


                        if (GivenAndBonusQty > stockQty) {
                            qty = stockQty - Double.parseDouble(givenQty);
                            Log.e("quantityvalue", String.valueOf(qty));
                        }


                        if (getIndex(cursor.getString(0)) < 0) {
                            Log.e("String.valueOf(qty)", String.valueOf(qty));
                            map.put("quantity", String.valueOf(qty));
                            Bonus.add(map);
                        } else {
                            double QUANTITY = Double.parseDouble(Bonus.get(getIndex(cursor.getString(4))).get("quantity")) + qty;
                            Log.e("Quantity", String.valueOf(QUANTITY));
                            Bonus.get(getIndex(cursor.getString(4))).put("quantity", String.valueOf(QUANTITY));  ///// Why index 4 where as query has 3 indexes
                        }
                    } while (cursor.moveToNext());
                }
            }

        }

        String bonusString = "";
        TempData.BonusShowList.clear();
        for (int i = 0; i < Bonus.size(); i++) {
            bonusString = bonusString + Bonus.get(i).get("item") + "(" + Bonus.get(i).get("quantity") + ")";

            HashMap<String, String> product_map = new HashMap<String, String>();

            product_map.put("bonus_name", Bonus.get(i).get("item"));
            product_map.put("quantity", Bonus.get(i).get("quantity"));


//            maplist.add(product_map);
            TempData.BonusShowList.add(product_map);


            if (i != Bonus.size() - 1)
                bonusString = bonusString + ", ";
        }


        if (bonusString.length() > 0) {
            Log.e("bonusString", bonusString);
            txtBonus.setText(bonusString);
            TempData.Test = BonusItemList.toString();
        } else
//            txtBonus.setText("");
            txtBonus.setVisibility(View.GONE);

    }


    public String DistGeneral_price(String product_id) {
        String query2 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date DESC, _id DESC LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            String query3 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer=0 AND price !='0.0' AND effective_date<='" + memodate + "' order by effective_date, _id DESC LIMIT 1";
            Cursor cursor3 = db.rawQuery(query3);
            if (cursor3.getCount() > 0) {
                if (cursor3.moveToFirst()) {
                    do {
                        return cursor3.getString(0);
                    } while (cursor3.moveToNext());
                }
            }
        }
        return "0.0";
    }

    public String DistCheckSlapPrice(double quantity, String product_id) {
        num.clear();
        //String query1="SELECT min_quantity From distributor_product_combinations WHERE product_id='"+product_id+"' AND combination_id=0 AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
        String query1 = "SELECT min_quantity From distributor_product_combinations WHERE product_id='" + product_id + "' AND combination_id=0 AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') order by min_quantity DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query1);
        Log.e("QUERY", query1);
        Log.e("COUNT", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    num.add(Double.parseDouble(cursor.getString(0)));
                } while (cursor.moveToNext());
            }
        }
        num.add(0, 0.0);
        num.add(quantity);
        num.add(8080.0);
        Collections.sort(num);


        int i = 0;
        for (int k = 0; k < num.size(); k++) {
            Log.e("NUM", " " + num.get(k));
            if (num.get(k) == quantity)
                i = k;
        }


        //String query="SELECT price From distributor_product_combinations WHERE product_id="+String.valueOf(product_id)+" AND combination_id=0 " +" AND "+ "min_quantity>="+String.valueOf(num.get(i-1))+" AND "+ "min_quantity <"+String.valueOf(num.get(i+1));
        //String query="SELECT price From distributor_product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";
        //String query="SELECT price From distributor_product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";

        String query = "SELECT price From distributor_product_combinations WHERE product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND min_quantity <=" + String.valueOf(num.get(i)) + "  AND effective_date=(SELECT max(effective_date) from distributor_product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') ORDER BY  min_quantity DESC LIMIT 1";

        Log.e("QUERY PRICE VALUE: ", query);
        Cursor cursor1 = db.rawQuery(query);
        Log.e("Second Cursor:", "" + cursor1.getCount());
        if (cursor1.getCount() > 0) {
            try {
                if (cursor1.moveToFirst()) {
                    do {
                        price = cursor1.getString(0);
                        Log.e("Slave price:", price);
                        return price;
                    } while (cursor1.moveToNext());
                }

            } finally {
                try {
                    cursor1.close();
                } catch (Exception ignore) {
                }
            }

        } else {
            //String query2="SELECT price From distibutor_product_prices WHERE product_id='"+product_id+ "' AND target_custommer=0 AND price!='0.0' AND effective_date=(SELECT max(effective_date) from distibutor_product_prices where product_id='"+product_id+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
            String query2 = "SELECT price From distibutor_product_prices WHERE product_id='" + product_id + "' AND target_custommer=0 AND price!='0.0' AND effective_date<='" + memodate + "' ORDER by effective_date DESC , _id DESC limit 1";

            Log.e("query2", query2);
            Cursor cursor2 = db.rawQuery(query2);
            if (cursor2.getCount() == 0) {
                return "0.0";
            }

            if (cursor2.moveToFirst()) {
                do {
                    Log.e("General price:", cursor2.getString(0));
                    return cursor2.getString(0);
                    //return "0";
                } while (cursor2.moveToNext());
            }
        }
        return "0";
    }


    public boolean DistCheckCombineQuantity(double quantity, String product_id, String combination_id) {

        Log.e("Calling this method:", "called for " + product_id);
        Log.e("quantity", "" + quantity);
        Log.e("product_id", "" + product_id);
        Log.e("combination_id", "" + combination_id);

        //Cursor cursor=db.rawQuery("SELECT *FROM distributor_product_combinations WHERE product_id= '"+product_id+"' AND combination_id='"+combination_id+"'", null);
        Cursor cursor = db.rawQuery("SELECT *FROM distributor_product_combinations WHERE combination_id='" + combination_id + "'");
        Log.e("CheckCombineQtyCount", "" + cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                double qty = Double.parseDouble(cursor.getString(3));
                Log.e("Pass Quantity:", "" + quantity);
                Log.e("Quantity:", "" + qty);
                if (quantity >= qty) {
                    Log.e("TRUE", "true");
                    return true;
                } else {
                    Log.e("FALSE", "false");
                    return false;
                }

            } while (cursor.moveToNext());
        }
        return false;
    }

    public String DistCheckPriceType(String ID) {


        Cursor cursor = db.rawQuery("SELECT * FROM distributor_product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from distributor_product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        Log.e("PRICE TYPE QUERY:", "SELECT * FROM distributor_product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from distributor_product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        if (cursor.getCount() == 0) {
            db.close();
            cursor.close();
            return "GENERAL";
        } else {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        Log.e("COMBINATION ID", cursor.getString(2));
                        if (!cursor.getString(2).equalsIgnoreCase("0")) {
                            db.close();
                            cursor.close();
                            return "COMBINE";
                        } else {
                            db.close();
                            cursor.close();
                            return "SLAVE";
                        }
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
            }
        }

        db.close();
        cursor.close();
        return "";
    }


    public boolean CheckCombineQuantity(double quantity, String product_id, String combination_id) {

        Log.e("Calling this method:", "called for " + product_id);
        Log.e("quantity", "" + quantity);
        Log.e("product_id", "" + product_id);
        Log.e("combination_id", "" + combination_id);

        //Cursor cursor=db.rawQuery("SELECT *FROM product_combinations WHERE product_id= '"+product_id+"' AND combination_id='"+combination_id+"'", null);
        Cursor cursor = db.rawQuery("SELECT *FROM product_combinations WHERE combination_id='" + combination_id + "'");
        Log.e("CheckCombineQtyCount", "" + cursor.getCount());
        if (cursor.moveToFirst()) {
            do {
                double qty = Double.parseDouble(cursor.getString(3));
                Log.e("Pass Quantity:", "" + quantity);
                Log.e("Quantity:", "" + qty);
                if (quantity >= qty) {
                    Log.e("TRUE", "true");
                    return true;
                } else {
                    Log.e("FALSE", "false");
                    return false;
                }

            } while (cursor.moveToNext());
        }
        return false;
    }

    public String CheckPriceType(String ID) {


        Cursor cursor = db.rawQuery("SELECT * FROM product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        Log.e("PRICE TYPE QUERY:", "SELECT * FROM product_combinations WHERE product_id= '" + ID + "' AND effective_date = (SELECT max(effective_date) from product_combinations where product_id='" + ID + "' AND effective_date<='" + memodate + "') ORDER BY combination_id desc");

        if (cursor.getCount() == 0) {
            db.close();
            cursor.close();
            return "GENERAL";
        } else {
            try {
                if (cursor.moveToFirst()) {
                    do {
                        Log.e("COMBINATION ID", cursor.getString(2));
                        if (!cursor.getString(2).equalsIgnoreCase("0")) {
                            db.close();
                            cursor.close();
                            return "COMBINE";
                        } else {
                            db.close();
                            cursor.close();
                            return "SLAVE";
                        }
                    } while (cursor.moveToNext());
                }
            } catch (Exception e) {
            }
        }

        db.close();
        cursor.close();
        return "";
    }

    public String General_price(String product_id) {
        String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date DESC LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            String query3 = "SELECT price From " + Tables.TABLE_NAME_PRODUCT_PRICE + " WHERE product_id='" + product_id + "' AND target_custommer=0 AND price !='0.0' AND effective_date<='" + memodate + "' order by effective_date LIMIT 1";
            Cursor cursor3 = db.rawQuery(query3);
            if (cursor3.getCount() > 0) {
                if (cursor3.moveToFirst()) {
                    do {
                        return cursor3.getString(0);
                    } while (cursor3.moveToNext());
                }
            }
        }
        return "0.0";
    }


    public String NGO_price(String product_id, String quantity) {
        String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer='" + TargetCustomer + "' AND institute_id='" + InstietuteID + "' AND effective_date<='" + memodate + "' order by effective_date LIMIT 1";
        Log.e("NGO_QUERY:", query2);


        Cursor cursor2 = db.rawQuery(query2);
        Log.e("count", " : " + cursor2.getCount());
        if (cursor2.getCount() > 0) {
            if (cursor2.moveToFirst()) {
                do {
                    return cursor2.getString(0);
                } while (cursor2.moveToNext());
            }
        } else {
            return CheckSlapPrice(Double.parseDouble(quantity), product_id);
*/
/*			   String query3="SELECT price From product_price WHERE id='"+product_id+"' AND target_custommer=0 AND price !='0.0' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
			   Cursor  cursor3= db.rawQuery(query3, null);
			   if(cursor3.getCount() > 0)
			   {
			   if(cursor3.moveToFirst())
			     {
			    	 do{
			    		 return cursor3.getString(0);
			    	 }while(cursor3.moveToNext());
			     }
			   }
*//*

        }
        return "0.0";
    }

    public String CheckSlapPrice(double quantity, String product_id) {
        num.clear();
        //String query1="SELECT min_quantity From product_combinations WHERE product_id='"+product_id+"' AND combination_id=0 AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' order by effective_date LIMIT 1";
        String query1 = "SELECT min_quantity From product_combinations WHERE product_id='" + product_id + "' AND combination_id=0 AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') order by min_quantity DESC LIMIT 1";
        Cursor cursor = db.rawQuery(query1);
        Log.e("QUERY", query1);
        Log.e("COUNT", "" + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    num.add(Double.parseDouble(cursor.getString(0)));
                } while (cursor.moveToNext());
            }
        }
        num.add(0, 0.0);
        num.add(quantity);
        num.add(8080.0);
        Collections.sort(num);


        int i = 0;
        for (int k = 0; k < num.size(); k++) {
            Log.e("NUM", " " + num.get(k));
            if (num.get(k) == quantity)
                i = k;
        }


        //String query="SELECT price From product_combinations WHERE product_id="+String.valueOf(product_id)+" AND combination_id=0 " +" AND "+ "min_quantity>="+String.valueOf(num.get(i-1))+" AND "+ "min_quantity <"+String.valueOf(num.get(i+1));
        //String query="SELECT price From product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";
        //String query="SELECT price From product_combinations WHERE product_id='"+String.valueOf(product_id)+"' AND combination_id=0 AND min_quantity <="+String.valueOf(num.get(i))+"  AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"' ORDER BY effective_date, min_quantity DESC LIMIT 1";

        String query = "SELECT price From product_combinations WHERE product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND min_quantity <=" + String.valueOf(num.get(i)) + "  AND effective_date=(SELECT max(effective_date) from product_combinations where product_id='" + String.valueOf(product_id) + "' AND combination_id=0 AND effective_date<='" + memodate + "') ORDER BY  min_quantity DESC LIMIT 1";

        Log.e("QUERY PRICE VALUE: ", query);
        Cursor cursor1 = db.rawQuery(query);
        Log.e("Second Cursor:", "" + cursor1.getCount());
        if (cursor1.getCount() > 0) {
            try {
                if (cursor1.moveToFirst()) {
                    do {
                        price = cursor1.getString(0);
                        Log.e("Slave price:", price);
                        savePreference("up" + product_id, "0");
                        return price;
                    } while (cursor1.moveToNext());
                }

            } finally {
                try {
                    cursor1.close();
                } catch (Exception ignore) {
                }
            }

        } else {
            //String query2="SELECT price From product_price WHERE product_id='"+product_id+ "' AND target_custommer=0 AND price!='0.0' AND effective_date=(SELECT max(effective_date) from product_price where product_id='"+product_id+"' AND effective_date<='"+((ParentActivity) context).getCurrentDate()+"')";
            String query2 = "SELECT price From product_price WHERE product_id='" + product_id + "' AND target_custommer=0 AND price!='0.0' AND effective_date<='" + memodate + "' ORDER by effective_date DESC limit 1";

            Log.e("query2", query2);
            Cursor cursor2 = db.rawQuery(query2);
            if (cursor2.getCount() == 0) {
                return "0.0";
            }

            if (cursor2.moveToFirst()) {
                do {
                    Log.e("General price:", cursor2.getString(0));
                    savePreference("up" + product_id, "0");
                    return cursor2.getString(0);
                    //return "0";
                } while (cursor2.moveToNext());
            }
        }
        return "0";
    }


    int getIndex(String value) {
        int pos = -1;
        for (int i = 0; i < Bonus.size(); i++) {
            HashMap<String, String> map = Bonus.get(i);
            Log.e("PRODUCT_ID", map.get("product_id"));
            if (map.get("product_id").equalsIgnoreCase(value))
                pos = i;
        }

        return pos;
    }

    public void savePreference(String key, String value) {

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public String getPreference(String key) {
        String value = "";
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        value = prefs.getString(key, "0");

        return value;

    }

    public String roundTwoDecimal(double d) {

        Log.e("Double:", String.valueOf(d));
        Log.e("Modulus:", String.valueOf(d % 1));

        if (d % 1 > 0.0) {

            DecimalFormat twoDForm = new DecimalFormat("#.##");
            String value = String.valueOf(twoDForm.format(d));
            String subValue = value.substring(value.indexOf('.'), value.length() - 1);
            Log.e("LENGTH:", subValue);
            if (subValue.length() > 1)
                return String.valueOf(twoDForm.format(d));
            else
                return String.valueOf(twoDForm.format(d)) + "0";


        } else {

            return String.valueOf(d) + "0";
        }
    }

    private void showLimitExeednotification(double giventQty, String product_id) {

        Cursor c = db.rawQuery("SELECT quantity-booking_quantity>" + giventQty + " as p from stock_info WHERE  product_id = '" + product_id + "'");
        Log.e("ProductSales", "SELECT quantity-booking_quantity>" + giventQty + " as p from stock_info WHERE  product_id = '" + product_id + "'");
        c.moveToFirst();
        if (c != null && c.getCount() > 0) {
            if (c.getInt(0) == 0) {
                Toast.makeText(context, "Your Stock Limit Exceeded", Toast.LENGTH_SHORT).show();
            }
        }


    }
}
*/
