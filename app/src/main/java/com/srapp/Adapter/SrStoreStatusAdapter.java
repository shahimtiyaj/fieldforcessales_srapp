package com.srapp.Adapter;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.srapp.Db_Actions.Tables;
import com.srapp.Model.OrderDetailsModel;
import com.srapp.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class SrStoreStatusAdapter extends RecyclerView.Adapter<SrStoreStatusAdapter.Holder> {

    private HashMap<String,ArrayList<String>> srStoreStatusData = new HashMap<>();
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.so_store_status_item,parent,false);
        return new Holder(view);
    }

    @SuppressLint("ResourceAsColor")
    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        holder.product_name_tv.setText(srStoreStatusData.get(Tables.PRODUCT_PRODUCT_NAME).get(position));
        holder.sl.setText(String.valueOf(position+1));
        holder.qty.setText(srStoreStatusData.get(Tables.Quantity).get(position));
//        holder.bookQty.setText(srStoreStatusData.get(Tables.BookedQuantity).get(position));
        holder.unit.setText(srStoreStatusData.get("bonusqty").get(position));

    }


    @Override
    public int getItemCount() {
        return srStoreStatusData.get(Tables.PRODUCT_PRODUCT_NAME).size();
    }

    public void bindData(HashMap<String,ArrayList<String>>srStoreStatusData){
        this.srStoreStatusData = srStoreStatusData;

    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView product_name_tv,sl,qty, unit,bookQty;
        LinearLayout mainLayout;


        public Holder(@NonNull View itemView) {
            super(itemView);
            product_name_tv = itemView.findViewById(R.id.product_name);
            sl = itemView.findViewById(R.id.sl);
            qty = itemView.findViewById(R.id.stock_qty);
           //m bookQty = itemView.findViewById(R.id.book_qty);
            unit = itemView.findViewById(R.id.unit);
            mainLayout = itemView.findViewById(R.id.main_item_layout);

        }
    }
}
