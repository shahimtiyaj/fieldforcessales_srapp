package com.srapp.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;


import com.srapp.R;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

public class AdapterForLedger extends BaseAdapter {

	// Declare Variables
	Context context;
	String SO_ID;

	ArrayList<HashMap<String, String>> itemListContent = new ArrayList<HashMap<String, String>>();
	public AdapterForLedger(Context context, ArrayList<HashMap<String, String>> arraylistContent) {
		this.context = context;
		itemListContent = arraylistContent;
	}

	@Override
	public int getCount() {
		return itemListContent.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
//		return position;
	}

	
	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
	
		
		View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.ledger_row_new, null);
		TextView Datetxt = (TextView)view2.findViewById(R.id.Datetxt);
		TextView Particulerstxt = (TextView)view2.findViewById(R.id.Particulerstxt);
		TextView Saletxt = (TextView)view2.findViewById(R.id.Saletxt);
		TextView Deposittxt = (TextView)view2.findViewById(R.id.Deposittxt);
		TextView txtType = (TextView)view2.findViewById(R.id.txtType);
	
		
			
		
		HashMap<String, String> mapContent = new HashMap<String, String>();
		mapContent = itemListContent.get(position);
		Datetxt.setText( mapContent.get("date"));
		Particulerstxt.setText( mapContent.get("particulars"));
		Saletxt.setText(mapContent.get("sales"));
		Deposittxt.setText(mapContent.get("deposits"));
		txtType.setText(mapContent.get("type"));
		
//		double payment_amount=roundTwoDecimals(Double.parseDouble(mapContent.get("payment_amount")));
		
		
	
		
		
		TextView txtSaleTotal=(TextView)((Activity) context).findViewById(R.id.txtSaleTotal);
		txtSaleTotal.setText("0.0");
		Double sum=0.0;
		for(int i=0;i<itemListContent.size();i++)
		{
			sum=sum+Double.parseDouble(itemListContent.get(i).get("deposits"));
		}
		
//		sum=(Double) roundTwoDecimals(sum);
		txtSaleTotal.setText(String.format("%.2f", sum));
		
		
	

		return view2;
	}

		

   


	
	 double roundTwoDecimals(double d)
	 {
	     DecimalFormat twoDForm = new DecimalFormat("#.######");
	     return Double.valueOf(twoDForm.format(d));
	 }	


	

}
