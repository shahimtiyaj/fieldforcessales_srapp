package com.srapp.Adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.srapp.Db_Actions.Data_Source;
import com.srapp.EdittextListener;
import com.srapp.R;
import com.srapp.SerialNumberEntry;
import com.srapp.TagEditText;
import com.srapp.TempData;

import java.util.ArrayList;
import java.util.HashMap;

public class AdapterForSelial extends BaseAdapter  {

    // Declare Variables
    Activity context;
    Data_Source db;
    ArrayList<HashMap<String, String>> BonusItemList = new ArrayList<HashMap<String, String>>();
    public AdapterForSelial(Activity context, ArrayList<HashMap<String, String>> arraylistContent) {
        this.context = context;
        BonusItemList = arraylistContent;
        db = new Data_Source(context);
    }

    @Override
    public int getCount() {
        return BonusItemList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
//		return position;
    }


    @Override
    public long getItemId(int position) {
        return position;
    }



    public View getView(final int listposition, View convertView, ViewGroup parent) {


        View view2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.sirial_input_row, null);
        TextView txtName = (TextView)view2.findViewById(R.id.product_name);
        final AutoCompleteTextView sirial = view2.findViewById(R.id.sirial);
        final TagEditText tagEditText =  view2.findViewById(R.id.tags);
        tagEditText.setText(BonusItemList.get(listposition).get("sirial_number"));
        tagEditText.setLeftTextListener(new EdittextListener() {
            @Override
            public void setleftText(String txt) {
                BonusItemList.get(listposition).put("sirial_number",txt);
                UpdateDbForSeliarPB(txt,BonusItemList.get(listposition).get("product_id"), TempData.OutletID);

            }

            @Override
            public void getDeletedText(String txt) {
                Log.e("deleted_sirial",txt);
                SerialNumberEntry.serial.get(listposition).add(txt);
                UpdateDbForSeliarPS(txt,"0");
            }
        });

        Log.e("","itemListContent"+BonusItemList.toString());

        HashMap<String, String> mapContent = new HashMap<String, String>();
        mapContent = BonusItemList.get(listposition);
        Log.e("","bonus_name"+mapContent.get("bonus_name"));
        txtName.setText(mapContent.get("product_name"));
        ArrayAdapter dataAdapter = new ArrayAdapter<String>(context, R.layout.outlet_dw, SerialNumberEntry.serial.get(listposition));
        dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        sirial.setAdapter(dataAdapter);
        sirial.setThreshold(1);
        sirial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sirial.showDropDown();

            }
        });

        final ArrayList<String> finalStrings = SerialNumberEntry.serial.get(listposition);;
        final HashMap<String, String> finalMapContent = mapContent;
        sirial.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                String sirialtext = finalStrings.get(position)+",";
                BonusItemList.get(listposition).put("sirial_number",tagEditText.getText()+ sirialtext);
                tagEditText.setText(tagEditText.getText()+ sirialtext);
                UpdateDbForSeliarPB(tagEditText.getText().toString().trim(),finalMapContent.get("product_id"), TempData.OutletID);
                UpdateDbForSeliarPS(finalStrings.get(position),"1");
                sirial.setText("");
                finalStrings.remove(position);
                ArrayAdapter dataAdapter = new ArrayAdapter<String>(context, R.layout.outlet_dw, SerialNumberEntry.serial.get(listposition));
                dataAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                sirial.setAdapter(dataAdapter);
                sirial.setThreshold(1);
                sirial.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sirial.showDropDown();

                    }
                });
            }
        });


        return view2;
    }

    private void UpdateDbForSeliarPB(String serial, String product_id, String outletID) {
        Log.e("UpdateQuery","update product_boolean set serial_number='"+serial+"' where product_id = '"+product_id+"' and outlet_id='"+outletID+"'");

        db.excQuery("update product_boolean set serial_number='"+serial+"' where product_id = '"+product_id+"' and outlet_id='"+outletID+"'");


    }private void UpdateDbForSeliarPS(String serial, String is_used) {

      //  db.excQuery("update product_serials set is_used='"+is_used+"' where serial_no = '"+serial+"'");


    }





}
